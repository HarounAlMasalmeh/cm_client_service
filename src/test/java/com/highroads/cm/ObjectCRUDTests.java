package com.highroads.cm;

import static com.highroads.cm.DomainObjectFactory.createAllAspectTestProduct;
import static com.highroads.cm.DomainObjectFactory.createAspectTestProduct;
import static com.highroads.cm.DomainObjectFactory.createBrokenTestProduct;
import static com.highroads.cm.DomainObjectFactory.createExtendedModelProduct;
import static com.highroads.cm.DomainObjectFactory.createProduct;
import static com.highroads.cm.DomainObjectFactory.createTemplate;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Product;
import com.highroads.cm.DomainObjectFactory.Template;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.InstanceTestClassListener;
import com.highroads.commons.TokenHelper;
import com.highroads.commons.api.exceptions.HrObjectNotFoundException;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.jayway.restassured.path.json.JsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class ObjectCRUDTests implements InstanceTestClassListener {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

    private String productsUri;
    private String tenantsUri;
    private String templatesUri;
    private String servicesUri;

    private String organizationId;
    private String organizationObjectId;

    private String tenantFolderName;
    private String tenantObjectId;

    @Autowired
    private WebApplicationContext wac;

    @PostConstruct
    public void init() {
        logger.debug("init() called");
    }

    @Override
    public void beforeClassSetUp() {
        tenantsUri = "/organizations";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        organizationId = "crudtests_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd_HH-mm-ss"));

        String testOrganization = "{\"orgId\":\"" + organizationId + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\""
                + organizationId + "\", \"orgName\":\"" + organizationId + "\", \"orgAutoUpdate\": true }";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
            given().
                log().all().
                headers("Authorization", TokenHelper.getTokenBase64String("p2a.com", "admin")).
                contentType("application/json").
                body(testOrganization).
            when().
                post("/organizations").
            andReturn().as(HashMap.class);
        // @formatter:on

        organizationObjectId = resultMap.get("id").toString();

        logger.info("== Organization successfully created - name: {}, objectId: {}", organizationId, organizationObjectId);

    }

    @Override
    public void afterClassTearDown() {
        // @formatter:off
        given().
            log().all().
            headers("Authorization", TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + organizationObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== Organization successfully deleted - name: {}, objectId: {}", organizationId, organizationObjectId);
    }

    @Before
    public void setUp() {
        productsUri = "/products";
        tenantsUri = "/organizations";
        templatesUri = "/templates";
        servicesUri = "/services";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
                contentType("application/json").
                body(testTenant).
            when().
                post(tenantsUri).
            andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetAllProducts() {
        logger.info("== 1 query count of existing products");

        int originalProductCount = getProductCount();
        logger.info("== count of existing products is " + originalProductCount);
        logger.info("== 2 creating a new product");

        String[] prodClasses = new String[] { "Medical" };
        String productType = "HMO";
        String effectiveDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";

        Product testProd = createProduct(prodClasses, productType, effectiveDate, endDate, pStatus, dueDate);

        logger.info(testProd.toString());

        // @formatter:off
        String objId =
                given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                queryParam("tenantFolder", tenantFolderName).
                body(testProd).
                when().
                post(productsUri).
                andReturn().body().asString();
        // @formatter:on

        logger.info("== product created with id " + objId);

        int productCountAfterCreation = getProductCount();

        // Assert.assertEquals("product count should increase by 1",
        // originalProductCount + 1, productCountAfterCreation);

        logger.info("== 3 retrieve the object and verify the data");

        HashMap<String, Object> product = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .when().get(productsUri + "/" + objId + "?properties=productClasses,productType,effectiveDate,endDate,productStatus,productDueDate").andReturn()
                .body().as(HashMap.class);

        assertTrue(((String) product.get("productType")).compareToIgnoreCase(productType) == 0);
        assertTrue(((String) product.get("productStatus")).compareToIgnoreCase(pStatus) == 0);

        OffsetDateTime expectedEffectiveDateTime = OffsetDateTime.parse(effectiveDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        OffsetDateTime expectedEndDateTime = OffsetDateTime.parse(endDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        OffsetDateTime expectedProductDueDateTime = OffsetDateTime.parse(dueDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        assertTrue(expectedEffectiveDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("effectiveDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
        assertTrue(expectedEndDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("endDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
        assertTrue(expectedProductDueDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("productDueDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));

        logger.info("== 4 update the object");

        // change two fields
        prodClasses = new String[] { "Dental" };
        productType = "PPO";

        testProd = createProduct(prodClasses, productType, effectiveDate, endDate, pStatus, dueDate);

        // send the update request
        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").body(testProd).when().patch(productsUri + "/" + objId);

        logger.info("== updated product having id " + objId);

        // retrieve the updated product and verify
        product = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(productsUri + "/" + objId + "?properties=productClass,productType,effectiveDate,endDate,productStatus,productDueDate").andReturn().body()
                .as(HashMap.class);

        assertTrue(((String) product.get("productType")).compareToIgnoreCase(productType) == 0);
        assertTrue(((String) product.get("productStatus")).compareToIgnoreCase(pStatus) == 0);

        expectedEffectiveDateTime = OffsetDateTime.parse(effectiveDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        expectedEndDateTime = OffsetDateTime.parse(endDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        expectedProductDueDateTime = OffsetDateTime.parse(dueDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        assertTrue(expectedEffectiveDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("effectiveDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
        assertTrue(expectedEndDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("endDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
        assertTrue(expectedProductDueDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("productDueDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));

        logger.info("== 5 delete the object");

        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .delete(productsUri + "/" + objId).then().statusCode(HttpServletResponse.SC_OK);

        // logger.info("== 6 verify product count goes back down to original
        // count");
        // int productCountAfterDeletion = getProductCount();
        // Assert.assertEquals("product count should be the same as original
        // count ", originalProductCount, productCountAfterDeletion);
    }

    @Test
    public void testMultiValuePropArray() {
        logger.info("== 1 creating a new product with a multi-value (array) property");

        String[] emptyList = new String[0];
        String[] oneList = { "HMO Local" };
        String[] twoList = { "HMO Local", "HMO NE" };
        doMultiValuePropTest(null);
        doMultiValuePropTest(emptyList);
        doMultiValuePropTest(oneList);
        doMultiValuePropTest(twoList);
    }

    @Test
    public void testSingleValuePropArray() {
        String[] twoList = { "HMO Local", "HMO NE" };
        doSingleValuePropTest(twoList); // make sure we didn't break any
                                        // single-valued properties
    }

    @Test
    public void testCreateProductWithUnknownProperty() {
        try {
            boolean realProduct = false;
            doProductPropTest(realProduct);
        } catch (HrObjectNotFoundException e) {
            logger.info("Product creation attempt with non-existent property (aspect or otherwise) failed as expected");
            // e.printStackTrace();
        }
    }

    // test for aspect-defined properties being returned when querying the
    // collection
    @Test
    public void testAspectPropertyRetrieval() {
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);

        // create product
        String productId = doCreateProduct(testProd, tenantFolderName);
        logger.debug("Waiting for the products to be indexed in Alfresco. Sleeping...");
        try {
            Thread.sleep(90000);
        } catch (InterruptedException e) {
        }

        @SuppressWarnings("unchecked")
        ArrayList<?> resultList = given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .when().get(productsUri).andReturn().as(ArrayList.class);
        // then().
        // body("[0]", hasItems("productClasses","marketSegments")); //
        // aspect-defined props
        // // Ashish's running code uses this form: body("[0].name",
        // equalTo(objectType));

        assertTrue(resultList.size() > 0);
        HashMap<String, Object> prod1 = (HashMap<String, Object>) resultList.get(0);
        assertTrue(prod1.containsKey("productClasses"));
    }

    // Test model extensions - ensure base model cannot use org-specific
    // properties
    @Test
    public void testCreateModelNoExtendedProduct() {
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        boolean udfproduct_isShowOnDocument = true;

        Product testProd = createExtendedModelProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate, udfproduct_isShowOnDocument);
        // this should fail:
        try {
            String objId = doCreateProduct(testProd, "highroads.com"); // highroads.com
                                                                       // model
                                                                       // does
                                                                       // not
                                                                       // have
                                                                       // any
                                                                       // model
                                                                       // extensions
            JsonPath jpath = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("highroads.com")).when()
                    .get(productsUri + "/" + objId + "?properties=udfproduct_isShowOnDocument").andReturn().body().jsonPath();
            @SuppressWarnings("unused")
            boolean retrievedNewProp = jpath.getBoolean("udfproduct_isShowOnDocument");
            fail("extended model property was allowed on base model");
        } catch (Exception e) {
            logger.info("extended property usage not allowed on base model as expected");
        }

    }

    // Test model extensions - create a product with org-specific properties
    // that overlaps another org's props
    @Test
    public void testCreateOverlapModelExtendedProduct() {
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        boolean udfproduct_isSmokingCessationStepTherapyRequired1 = true;
        boolean udfproduct_isSmokingCessationStepTherapyRequired2 = false;

        Product testProd1 = createExtendedModelProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate,
                udfproduct_isSmokingCessationStepTherapyRequired1);
        Product testProd2 = createExtendedModelProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate,
                udfproduct_isSmokingCessationStepTherapyRequired2);
        String objId1 = doCreateProduct(testProd1, "upmc.com");
        String objId2 = doCreateProduct(testProd2, "fieldinsurance.com");
        JsonPath jpath1 = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("upmc.com")).when()
                .get(productsUri + "/" + objId1 + "?properties=udfproduct_isSmokingCessationStepTherapyRequired").andReturn().body().jsonPath();
        boolean retrievedNewProp = jpath1.getBoolean("udfproduct_isSmokingCessationStepTherapyRequired");
        assertEquals(udfproduct_isSmokingCessationStepTherapyRequired1, retrievedNewProp);

        JsonPath jpath2 = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("fieldinsurance.com")).when()
                .get(productsUri + "/" + objId2 + "?properties=udfproduct_isSmokingCessationStepTherapyRequired").andReturn().body().jsonPath();
        retrievedNewProp = jpath2.getBoolean("udfproduct_isSmokingCessationStepTherapyRequired");
        assertEquals(udfproduct_isSmokingCessationStepTherapyRequired2, retrievedNewProp);
    }

    // manual test for creating a number of Products - for performance testing
    // @Test
    public void testCreate500ProductsWithAspectProperties() {
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        String[] pFamilies = { "PPO NE", "HMO NE" };
        String[] mktSegments = { "Medium (51-100)", "Large (101-150)" };
        String[] memberFundings = { "HSA", "HRA" };

        for (int i = 0; i < 500; i++) {
            // create product with all 'potential/reasonable' aspects
            String prodName = "testProd_" + i;
            Product testProd = createAllAspectTestProduct(prodName, prodClasses, prodType, startDate, endDate, pStatus, dueDate, pFamilies, mktSegments,
                    memberFundings);

            @SuppressWarnings("unused")
            String objId = doCreateProduct(testProd);
        }
    }

    /**
     * Test that product should be created in the given tenant.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testCreateProductInGivenTenant() {
        // create a second test tenant
        String tenantFolderName2 = "tenant.test." + System.currentTimeMillis();
        String testTenant2 = "{\"orgId\":\"" + tenantFolderName2 + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName2
                + "\", \"orgName\":\"" + tenantFolderName2 + "\"}";

        HashMap<String, Object> resultMap = given().log().all()
                .headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).contentType("application/json")
                .body(testTenant2).when().post(tenantsUri).andReturn().as(HashMap.class);
        String tenantObjectId2 = resultMap.get("id").toString();
        logger.debug("testCreateTenantInGivenTenant " + tenantFolderName2 + " created");

        // get current number of products both test tenants
        int numOfProductInT1 = getProductCount(tenantFolderName);
        int numOfProductInT2 = getProductCount(tenantFolderName2);
        Assert.assertEquals(numOfProductInT1, 0);
        Assert.assertEquals(numOfProductInT2, 0);

        // prepare one product
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);

        /**
         * Test create product
         */
        // create one product in tenant 1
        String productIdInT1 = doCreateProduct(testProd, tenantFolderName);
        logger.debug("Waiting for the products to be indexed in Alfresco. Sleeping...");
        try {
            Thread.sleep(90000);
        } catch (InterruptedException e) {
        }
        Assert.assertEquals(getProductCount(tenantFolderName), 1);
        Assert.assertEquals(getProductCount(tenantFolderName2), 0);

        // create one product in tenant 2
        String productIdInT2 = doCreateProduct(testProd, tenantFolderName2);
        logger.debug("Waiting for the products to be indexed in Alfresco. Sleeping...");
        try {
            Thread.sleep(90000);
        } catch (InterruptedException e) {
        }
        Assert.assertEquals(getProductCount(tenantFolderName), 1);
        Assert.assertEquals(getProductCount(tenantFolderName2), 1);

        /**
         * Test retrieve and update on of the product
         */
        Map<String, ?> product2 = retrieveProduct(productIdInT2, tenantFolderName2);
        Assert.assertEquals("HMO", product2.get("productType"));
        // update product in tenant 2
        testProd.setProductTypes(new String[]{"PPO"});
        updateProduct(productIdInT2, testProd, tenantFolderName2);
        // verify that product in tenant 2 changed
        product2 = retrieveProduct(productIdInT2, tenantFolderName2);
        Assert.assertEquals("PPO", product2.get("productType"));
        // but product in tenant 1 stay the same
        Map<String, ?> product1 = retrieveProduct(productIdInT1, tenantFolderName);
        Assert.assertEquals(product1.get("productType"), "HMO");

        // /**
        // * Test delete give product by objId
        // */
        // // delete product with objId
        // // deleteProduct(productIdInT2, tenantFolderName); // TODO:
        // tenantFolderName has no effect by now in the deletion, same as
        // retrieve. Only getObject is refactored
        // deleteProduct(productIdInT2, tenantFolderName2);
        // // verify nothing changed
        // Assert.assertEquals(getProductCount(tenantFolderName), 1);
        // Assert.assertEquals(getProductCount(tenantFolderName2), 0);
        //
        // // delete the second tenant
        // given().
        // headers(GenericController.AUTHORIZATION_HEADER_NAME,
        // TokenHelper.getTokenBase64String("p2a.com", "admin")).
        // when().delete(tenantsUri + "/" + tenantObjectId2).
        // then().statusCode(HttpServletResponse.SC_OK);
        //
        // logger.error("testCreateTenantInGivenTenant " + tenantFolderName2 +
        // " deleted");
    }

    /**
     * Test product access across tenants.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testObjectOwnershipAccess() {
        // test retrieval of Product in tenant1 by user in tenant2

        // create a second test tenant
        String tenantFolderName2 = "tenant.test." + System.currentTimeMillis();
        String testTenant2 = "{\"orgId\":\"" + tenantFolderName2 + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName2
                + "\", \"orgName\":\"" + tenantFolderName2 + "\"}";

        // @formatter:off
        HashMap<String, Object> resultMap =
                given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName2)).
                contentType("application/json").
                body(testTenant2).
                when().
                post(tenantsUri).
                andReturn().as(HashMap.class);
        // @formatter:on

        String tenantObjectId2 = resultMap.get("id").toString();
        logger.debug("testObjectOwnershipAccess " + tenantFolderName2 + " created");

        // hold on to product folder for addition object-access testing
        Map<String, Object> folders = (Map<String, Object>) resultMap.get("folders");
        String productFolderId2 = folders.get("product").toString();
        logger.debug("test tenant product folder Id: " + productFolderId2);

        // create one product in tenant 2
        // prepare one product
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String productIdInT2 = doCreateProduct(testProd, tenantFolderName2);

        // check to see we do not allow access to objects across tenants
        testObjectOwnership(productIdInT2, tenantFolderName, HttpServletResponse.SC_NOT_FOUND);

        // check to see if we can access a folder across tenants
        // this test is not valid with the AMP-based get-object impl until we
        // have ACLs in place
        // testObjectOwnership(productFolderId2, tenantFolderName,
        // HttpServletResponse.SC_NOT_FOUND);

        // check to see if we can access a folder within tenant
        testObjectOwnership(productFolderId2, tenantFolderName2, HttpServletResponse.SC_OK);

        // check to see if p2a.com users can access a folder within tenant
        testObjectOwnership(productFolderId2, "p2a.com", new String[] { "HRSuperAdmin" }, HttpServletResponse.SC_OK);

        // delete the second tenant
        // @formatter:off
        given().
        headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com")).
        when().
        delete(tenantsUri + "/" + tenantObjectId2).
        then().statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.error("testObjectOwnershipAccess " + tenantFolderName2 + " deleted");
    }

    // test that test-tenant can access p2a.com objects
    @Test
    public void testGlobalObjectAccess() {
        // given().
        // headers(GenericController.AUTHORIZATION_HEADER_NAME,
        // TokenHelper.getTokenBase64String(tenantFolderName)).
        // when().
        // then().
        // statusCode(HttpServletResponse.SC_OK);
        int serviceCount = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(servicesUri).then().statusCode(HttpServletResponse.SC_OK).contentType("application/json").extract().jsonPath().getInt("size()");

        logger.debug("serviceCount: " + serviceCount);

        assertTrue("expected service-count > 0", serviceCount > 0);
    }

    // ensure a PATCH with no 'name' property does not change an object's
    // existing name
    @Test
    public void testPatchProductWithoutName() {
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String effectiveDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        String name = "Product_ABC";

        Product testProd = createProduct(name, prodClasses, prodType, effectiveDate, endDate, pStatus, dueDate, null, null, null);

        String objId = given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").queryParam("tenantFolder", tenantFolderName).body(testProd).when().post(productsUri).andReturn().body()
                .asString();

        logger.info("== product created with id " + objId);

        given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when().get(productsUri + "/" + objId)
                .then().body("name", equalTo(name)).and().body("productStatus", equalTo(pStatus));
        logger.info("== new product status verified");

        String newProdStatus = "New";
        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").body("{ \"productStatus\": \"" + newProdStatus + "\" }").when().patch(productsUri + "/" + objId);
        logger.info("== product status updated");

        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(productsUri + "/" + objId).then().body("name", equalTo(name)).and().body("productStatus", equalTo(newProdStatus));
        logger.info("== product status validated");
    }

    // test the ingress and egress of dates over the API
    @Test
    public void testPatchTemplateWithNewDates() throws IOException {
        // create system-named template, then patch name, productType
        // - the latter is defined in an aspect, different from the
        // effectiveDates aspect
        Template template = createTemplate("2015-01-01T03:30:04.965+00:00", "2015-12-31T00:00:00+00:00", "SBC", "Product");

        String templateId = given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").queryParam("tenantFolder", tenantFolderName).body(template).when().post(templatesUri).andReturn().body()
                .asString();

        logger.info("template created with id " + templateId);

        String name = "Ham sheet";
        String productType = "PPO";
        String effectiveDate = "2016-01-01T00:00:00+00:00";
        String endDate = "2016-12-31T00:00:00+00:00"; // this date format is
        // returned by API, ensure
        // we can ingest it
        String templatePatchJson = String.format(
                "{\"name\": \"%s\", " + "\"productType\": \"%s\", " + "\"effectiveDate\": \"%s\", " + "\"endDate\": \"%s\" " + "}", name, productType,
                effectiveDate, endDate);

        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").body(templatePatchJson).when().patch(productsUri + "/" + templateId);
        logger.info("== template name updated, template productType added (an aspect-defined property)");

        // retrieve the the source template and verify its relation with target
        // section
        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> product = given().
        headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
        get(templatesUri + "/" + templateId).
        andReturn().
        body().as(HashMap.class);
        // @formatter:on

        assertTrue(((String) product.get("productType")).compareToIgnoreCase(productType) == 0);
        assertTrue(((String) product.get("name")).compareToIgnoreCase(name) == 0);
        OffsetDateTime expectedEffectiveDateTime = OffsetDateTime.parse(effectiveDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        OffsetDateTime expectedEndDateTime = OffsetDateTime.parse(endDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        assertTrue(expectedEffectiveDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("effectiveDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
        assertTrue(expectedEndDateTime
                .isEqual(OffsetDateTime.parse((String) product.get("endDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
    }

    @Ignore
    @Test
    public void testObjectDateTimeFormat() {
        // object and collection retrieval return date/time values in a form
        // such as:
        // "creationDate": "2015-08-21T03:30:04.965+0000"
        // need to support sending this format in as well
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        boolean hidden = true;
        String pStatus = "Draft";

        // <don't mess with these values>
        String effectiveDate = "2015-01-01"; // date only
        String endDate = "2015-12-31T03:30:04.965+00:00"; // date/time
        String productDueDate = "2016-01-01T03:30:04.965+01:00";// date/time
                                                                // with non-zero
                                                                // tz offset
        String expectedProductDueDate = "2016-01-01T02:30:04"; // TZ stripped
                                                               // and
                                                               // incorporated
                                                               // into time
                                                               // value

        // another product with different time formats
        String effectiveDate2 = "2015-01-01"; // date only
        String endDate2 = "2015-12-31T03:30:04"; // date/time no tz offset
        String productDueDate2 = "2016-01-01T03:30:04+01:00"; // date/time with
                                                              // non-zero tz
                                                              // offset
                                                              // </end don't mess with these values>

        // create product with date-only and date-time values
        Product testProd = createAspectTestProduct(prodClasses, prodType, effectiveDate, endDate, pStatus, productDueDate, hidden);
        Product testProd2 = createAspectTestProduct(prodClasses, prodType, effectiveDate2, endDate2, pStatus, productDueDate2, hidden);

        String objId = doCreateProduct(testProd);
        String objId2 = doCreateProduct(testProd2);

        // now retrieve the object and check the dates
        Map<String, ?> productInfo = retrieveProduct(objId, tenantFolderName);
        String getEffectiveDate = productInfo.get("effectiveDate").toString();
        String getEndDate = productInfo.get("endDate").toString();
        String getDueDate = productInfo.get("productDueDate").toString();

        Map<String, ?> productInfo2 = retrieveProduct(objId2, tenantFolderName);
        String getEffectiveDate2 = productInfo2.get("effectiveDate").toString();
        String getEndDate2 = productInfo2.get("endDate").toString();
        String getDueDate2 = productInfo2.get("productDueDate").toString();

        logger.debug("=======================");
        logger.debug("effectiveDate out/back : " + effectiveDate + "/" + getEffectiveDate);
        logger.debug("endDate out/back       : " + endDate + "/" + getEndDate);
        logger.debug("dueDate out/back       : " + productDueDate + "/" + getDueDate);
        logger.debug("=======================");
        logger.debug("effectiveDate2 out/back: " + effectiveDate2 + "/" + getEffectiveDate2);
        logger.debug("endDate2 out/back      : " + endDate2 + "/" + getEndDate2);
        logger.debug("dueDate2 out/back      : " + productDueDate2 + "/" + getDueDate2);
        logger.debug("=======================");

        // testing only - TODO remove these comments after commit
        // with SearchService, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
        // simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        // effectiveDate out/back: 2015-01-01/2015-01-01T05:00:00.000Z << TZ
        // offset was added to date-only
        // endDate out/back :
        // 2015-12-31T03:30:04.965+00:00/2015-12-31T03:30:04.965Z << TZ value
        // unchanged, but offset "+00:00" was changed to "Z"
        // dueDate out/back :
        // 2016-01-01T03:30:04.965+01:00/2016-01-01T02:30:04.965Z << TZ offset
        // value was incorporated into the time, and offset returned as "Z"

        // with SearchService, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"),
        // simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        // effectiveDate out/back: 2015-01-01/2015-01-01T05:00:00
        // endDate out/back : 2015-12-31T03:30:04.965+00:00/2015-12-31T03:30:04
        // dueDate out/back : 2016-01-01T03:30:04.965+01:00/2016-01-01T02:30:04

        // with SearchService, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"),
        // NO SETTING of
        // simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        // effectiveDate out/back: 2015-01-01/2015-01-01T00:00:00
        // endDate out/back : 2015-12-31T03:30:04.965+00:00/2015-12-30T22:30:04
        // dueDate out/back : 2016-01-01T03:30:04.965+01:00/2015-12-31T21:30:04
        // end TODO

        // Where we ended up (keep these comments!):
        // Search Service (all object-fetches, single and collections) will
        // format dates as such:
        // simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); //
        // no TZ
        // simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        // This config essentially homogenizes all date/times to ISO_DATE_TIME
        // e.g. '2011-12-03T10:15:30'
        //
        // Content Mgmt Service attempts to parse dates of the following forms:
        // full date/time format string: yyyy-MM-dd'T'HH:mm:ss.SSSXXX
        // (ISO_OFFSET_DATE_TIME - the default for OffsetDateTime.parse())
        //
        // date-time only: ISO_DATE_TIME
        // '2011-12-03T10:15:30', '2011-12-03T10:15:30+01:00' or
        // '2011-12-03T10:15:30+01:00[Europe/Paris]'
        //
        // date-only: ISO_LOCAL_DATE
        // yyyy-MM-dd (ISO_LOCAL_DATE)
        // Dates coming in to ContentService on object-create or update are
        // shown below as "input";
        // Those same dates returned by the object-fetch endpoints are shown
        // below as "output"
        // =======================
        // (1) effectiveDate input/output : 2015-01-01/2015-01-01T05:00:00
        // (2) endDate input/output :
        // 2015-12-31T03:30:04.965+00:00/2015-12-31T03:30:04
        // (3) dueDate input/output :
        // 2016-01-01T03:30:04.965+01:00/2016-01-01T02:30:04
        // =======================
        // (4) effectiveDate2 input/output: 2015-01-01/2015-01-01T05:00:00
        // (5) endDate2 input/output : 2015-12-31T03:30:04/2015-12-31T03:30:04
        // (6) dueDate2 input/output :
        // 2016-01-01T03:30:04+01:00/2016-01-01T02:30:04
        // =======================

        // assert (1) (and (4))
        Assert.assertTrue("date-only field mangled upon return", getEffectiveDate.startsWith(effectiveDate));
        // is this a problem?
        Assert.assertTrue("date-only field should be augmented with UTC TZ offset", getEffectiveDate.endsWith("T05:00:00"));
        // assert (2)
        Assert.assertTrue("full date-time field should be stripped of zero TZ offset", getEndDate.equals(endDate2));
        // assert (3)
        Assert.assertTrue("full date-time field should be stripped of non-zero TZ offset and updated with that offset",
                getDueDate.equals(expectedProductDueDate));
        // assert (5)
        Assert.assertTrue("ISO_DATE_TIME format input with no TZ offset should produce identical output", getEndDate2.equals(endDate2));
        // assert (6)
        Assert.assertTrue("ISO_DATE_TIME format input should be stripped of non-zero TZ offset and updated with that offset",
                getDueDate2.equals(expectedProductDueDate));
    }

    // for debug
    // good blog at
    // http://marxsoftware.blogspot.com/2014/09/datetime-formattingparsing-java-8-style.html
    // even better running code (our own ;-) in
    // ContentServiceImpl.convertStringToDate()
    @Test
    public void testDateConversions() {
        // String dateText = "2015-12-31T03:30:04.965-0000"; // as originally
        // returned from collections/object-fetch endpoints
        String dateText = "2015-12-31T03:30:04.965+01:00"; // as returned from
                                                           // collections/object-fetch
                                                           // endpoints

        try {
            OffsetDateTime dateTime = OffsetDateTime.parse(dateText);
            String dateString = dateTime.toString();
            logger.debug("original date/time string: " + dateText);
            logger.debug("corresponding dateString: " + dateString);
        } catch (DateTimeParseException pe) {
            logger.error("date parse error: " + dateText, pe);
            fail("date parse error: " + dateText);
        }
    }

    /////////////
    // private //
    /////////////

    // test retrieval of known object that exists in 'tenant2' by a user in
    // 'tenant1'
    private void testObjectOwnership(String objectIdInT2, String tenantT1Id, String[] roles, int expectedResult) {
        given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantT1Id, "admin", roles)).when()
                .get(productsUri + "/" + objectIdInT2).then().statusCode(expectedResult);
    }

    private void testObjectOwnership(String objectIdInT2, String tenantT1Id, int expectedResult) {
        given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantT1Id)).when().get(productsUri + "/" + objectIdInT2)
                .then().statusCode(expectedResult); // was:
                                                                                                                                                                                                           // HttpServletResponse.SC_NOT_FOUND,
                                                                                                                                                                                                           // now: generalized
    }

    private Map retrieveProduct(String objId, String tenantFolderName) {
        return given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(productsUri + "/" + objId + "?properties=productClass,productType,effectiveDate,endDate,productStatus,productDueDate").andReturn().body()
                .as(Map.class);
    }

    private void updateProduct(String objId, Product testProd, String tenantFolderName) {
        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").body(testProd).when().patch(productsUri + "/" + objId).then().statusCode(HttpServletResponse.SC_OK);
    }

    private void deleteProduct(String objId, String tenantFolderName) {
        // @formatter:off
        given().
        log().all().
        headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
        delete(productsUri + "/" + objId).
        then().statusCode(HttpServletResponse.SC_UNAUTHORIZED);
        // @formatter:on
    }

    private String doProductPropTest(boolean goodProduct) {
        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        Product testProd = (goodProduct) ? createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate)
                : createBrokenTestProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate, "noneSuchPropValue");
        String objId = doCreateProduct(testProd);
        return objId;
    }

    private String doCreateProduct(Product testProd) {
        return doCreateProduct(testProd, tenantFolderName);
    }

    private String doCreateProduct(Product testProd, String tenantFolderName) {

        String objId = given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").body(testProd).when().post(productsUri).andReturn().body().asString();

        return objId;
    }

    private void doMultiValuePropTest(String[] stringProps) {

        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        String[] pFamily = stringProps;

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate, pFamily);

        logger.info(testProd.toString());

        String objId = given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").queryParam("tenantFolder", tenantFolderName).body(testProd).when().post(productsUri).andReturn().body()
                .asString();

        logger.info("== 2 product created with id " + objId);
        logger.info("== 3 retrieve the object and verify the data");

        JsonPath jpath = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(productsUri + "/" + objId + "?properties=productFamilies").andReturn().body().jsonPath();

        // this getList() will throw exception if productFamily is NOT a list
        List<String> productFamilyList = (List) jpath.getList("productFamilies");
        if (stringProps != null && stringProps.length != 0) {
            assertNotNull(productFamilyList);
            for (String s : productFamilyList) {
                logger.info("== 4 retrieved list property: '" + s + "'");
            }
            assertEquals("set/get propertyList sizes don't match", stringProps.length, productFamilyList.size());
        }
    }

    // first fix to multi-value prop broke the returned results for 'get all' -
    // ALL properties were returned as arrays!
    // this test will detect that
    private void doSingleValuePropTest(String[] stringProps) {

        String[] prodClasses = new String[] { "Medical" };
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00+00:00";
        String endDate = "2015-12-31T00:00:00+00:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00+00:00";
        String[] pFamily = stringProps;

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate, pFamily);

        logger.info(testProd.toString());

        String objId = given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .contentType("application/json").body(testProd).when().post(productsUri).andReturn().body().asString();

        logger.info("== 2 product created with id " + objId);
        logger.info("== 3 retrieve the object and verify the data");

        JsonPath jpathSingleProduct = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(productsUri + "/" + objId + "?properties=productType").andReturn().body().jsonPath();

        String productClassSingle = jpathSingleProduct.getString("productType");
        assertEquals(productClassSingle, prodType);

        logger.info("== 4 retrieve all product objects (collection/search) and verify the data");
        logger.debug("Waiting for the products to be indexed in Alfresco. Sleeping...");
        try {
            Thread.sleep(90000);
        } catch (InterruptedException e) {
        }

        JsonPath jpath = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(productsUri + "/").andReturn().body().jsonPath();

        logger.debug("product collection retrieved, result: \n" + jpath.prettyPrint());
        List<String> productClassList = jpath.getList("productClass"); // broken
                                                                       // until
                                                                       // GET
                                                                       // /<collections>
                                                                       // is
                                                                       // fixed
        List<ArrayList<Object>> productFamilyList = jpath.getList("productFamilies"); // broken
                                                                                      // until
                                                                                      // GET
                                                                                      // /<collections>
                                                                                      // is
                                                                                      // fixed

        assert (productClassList.size() != 0);
        @SuppressWarnings("unused")
        String productClass = productClassList.get(0); // better be scalar or
                                                       // exception (test fails)

        assert (productFamilyList.size() != 0);
        // look for non-null productFamily values, check that they are indeed an
        // array
        for (ArrayList<Object> pFam : productFamilyList) {
            if (pFam != null) {
                assert (pFam.size() != 0);
            }
        }
    }

    private int getProductCount() {
        return getProductCount(tenantFolderName);
    }

    private int getProductCount(String tenantFolderName) {
        return given().header(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when().get(productsUri).then()
                .statusCode(HttpServletResponse.SC_OK).contentType("application/json").extract().jsonPath().getInt("size()");
    }
}
