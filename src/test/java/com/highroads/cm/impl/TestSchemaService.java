package com.highroads.cm.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.highroads.cm.Application;
import com.highroads.commons.config.MarklogicConnectionProperties;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.MLSchemaServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class TestSchemaService {

    @Autowired
    private MarklogicConnectionProperties marklogicConnectionProperties;

    @Autowired
    private MLSchemaServiceImpl schemaService;

    private UserToken userToken;

    @Before
    public void setUp() {
        //        new MarklogicDAO(marklogicConnectionProperties);

        schemaService.init();
        userToken = new UserToken();
        userToken.setOrganization("p2a.com");
        userToken.setUsername("admin@p2a.com");
    }

    @Test
    public void shouldGetAllSchemaTypes() {

        final List<Object> objectSchemaTypes = schemaService.getObjectSchemaTypes(userToken);
        assertEquals(46, objectSchemaTypes.size());
    }

    @Test
    public void shouldGetObjectSchema() {

        final List<String> requiredProperties = Arrays
                .asList(new String[] { "effectiveDate", "planType", "grandfatheredPlan", "planDueDate" });

        final ObjectNode node = schemaService.getCMType(userToken, "plan");
        final JsonNode required = node.findValue("required");
        for (final JsonNode r : required) {
            assertTrue(requiredProperties.contains(r.textValue()));
        }
    }

    @Test
    public void shouldGetCMType() {

        final ObjectNode plan = schemaService.getModelObjectDef(userToken, "plan");
        assertEquals("plan", plan.get("name").textValue());
        final JsonNode associations = plan.get("associations");
        final JsonNode linkedProducts = associations.get("linkedProducts");

        assertEquals("linkedProducts", linkedProducts.get("name").textValue());
    }

    @Test
    public void shouldGetSchema() {

        final ObjectNode schema = schemaService.getSchema(userToken);
        assertNotNull(schema.findValue("plan"));

    }
}
