package com.highroads.cm.impl;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.Callable ;

public class TestConcurrentCreateAssociationCallable implements Callable<Boolean> {

    private static final String TEST_PLAN_JSON = "src/test/content/TestPlan.json" ;

    private String                          sourceObjectType    ;
    private String                          sourceObjectId      ;
    private String                          targetObjectId      ;
    private String                          associationName     ;
    private String                          bearer              ;

    // ----------------------------------------------------------------------------------------------------

    @SuppressWarnings("unchecked")

    public TestConcurrentCreateAssociationCallable( String sourceObjectType, String sourceObjectId, String targetObjectId, String associationName, String bearer ) {
        this.sourceObjectType   = sourceObjectType  ;
        this.sourceObjectId     = sourceObjectId    ;
        this.targetObjectId     = targetObjectId    ;
        this.associationName    = associationName   ;
        this.bearer             = bearer            ;
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Boolean call() throws Exception {

        String command = "C:\\shahin\\curl_748_0\\curl -vX POST http://localhost:8080/cm/" + sourceObjectType + "/" + sourceObjectId + "/" + associationName + "/" + targetObjectId
                + " --header \"Content-Type: application/json\"  "
                + " -H \"Authorization: Bearer " + bearer  + "\"" ;

        StringBuilder output = new StringBuilder() ;


        Process p;
        try {
            p = Runtime.getRuntime().exec( command ) ;
            System.out.println( "create association:" + sourceObjectType + "/" + sourceObjectId + "/" + associationName
                    + "/" + targetObjectId + "  -> by thread: " + Thread.currentThread().getName() ) ;
            p.waitFor();
            BufferedReader reader =new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine())!= null) {
                System.out.append( Thread.currentThread().getName() + ": association created: " + line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        return true ;
    }
}
