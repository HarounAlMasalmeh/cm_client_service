package com.highroads.cm.impl;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.Callable ;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.parser.JSONParser;

public class TestConcurrentPatchCallable implements Callable<Boolean> {

    public static final Pattern OBJECT_TYPE_PLURAL_PATTERN = Pattern.compile("(.+)s$");

    private static final String TEST_PLAN_JSON = "src/test/content/TestPlan.json" ;

    private String                  objectType                      ;
    private String                  objectId                        ;
    private String                  updateJson                      ;
    private Map<String, Object>     objectProperties = null         ;
    private String                  bearer                          ;

    // ----------------------------------------------------------------------------------------------------

    @SuppressWarnings("unchecked")
    public TestConcurrentPatchCallable( String objectId, int index, String bearer, String objectType ) throws Exception {

        super() ;
        this.objectId = objectId ;
        this.bearer   = bearer   ;

        this.objectType = objectType ;

        Path path ;
        if ( objectType.equalsIgnoreCase( "plans" ) ) {
            path = Paths.get( "src/test/content/update" + singularizeObjectType( objectType ) + ((index % 10) + 1) + ".json" ) ;
        } else {
            path = Paths.get( "src/test/content/update" + singularizeObjectType( objectType ) + ((index % 2) + 1) + ".json" ) ;
        }

        updateJson = "\"" + path.toFile().getAbsolutePath() + "\" " ;
        String josnContent = new String( Files.readAllBytes( path ) ) ;
        JSONParser jsonParser = new JSONParser() ;
        Object object = jsonParser.parse( josnContent ) ;
        objectProperties = (Map<String, Object>) object ;
    }


    // ----------------------------------------------------------------------------------------------------

    private String singularizeObjectType(String ref) {
        Matcher m = OBJECT_TYPE_PLURAL_PATTERN.matcher(ref);
        if (m.find()) {
            return m.group(1);
        } else {
            return ref;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Boolean call() throws Exception {

        String command = "C:\\shahin\\curl_748_0\\curl -vX PATCH http://localhost:8080/cm/" + objectType + "/" + objectId
                + " -d @" + updateJson + " "
                + " --header \"Content-Type: application/json\"  "
                + " -H \"Authorization: Bearer " + bearer  + "\"" ;
        StringBuilder output = new StringBuilder() ;


        Process p;
        try {
            p = Runtime.getRuntime().exec( command ) ;
            System.out.println( String.format( "%s : %60.60s -> by thread %s", objectId, objectProperties, Thread.currentThread().getName() ) ) ;
            p.waitFor();
            BufferedReader reader =new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine())!= null) {
                System.out.append( Thread.currentThread().getName() + ": " + objectId + " patched." + line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        return true ;
    }
}
