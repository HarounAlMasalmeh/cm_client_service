package com.highroads.cm.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;

/**
 *
 * @author Shahin
 *
 *         tests create, read, update
 */
public class TestObjectIdUtil {

    public static final Logger logger = LoggerFactory.getLogger(TestObjectIdUtil.class);

    public static ContentService contentService;
    protected static UserToken userToken;
    private List<CleanupObject> objectCleanupList;
    private String organizationId;

    private Map<String, Object> metadata;
    private MultipartFile file;
    private Map<String, Object> docSourceData;

    // -----------------------------------------------------------------------

    public TestObjectIdUtil(ContentService contentService, UserToken userToken, String organizationId) {
        super();
        TestObjectIdUtil.contentService = contentService;
        TestObjectIdUtil.userToken = userToken;
        this.organizationId = organizationId;

        // test 'binary' document with associations to documentSources
        // these keys need to be schema-compliant
        metadata = new HashMap<>();

        // metadata.put("name", "testbinarydoc1.docx"); let system generate unique name to avoid collisions
        metadata.put("docName", "testbinarydoc1.docx");
        metadata.put("documentStatus", "Draft");
        metadata.put("watermark", "testing the watermark");

        file = new MockMultipartFile("junit.txt", "junit.txt", "text/plain", "This is a test file".getBytes());

        objectCleanupList = new ArrayList<>();

        // documentSource instance data - minimal required fields
        docSourceData = new HashMap<>();
        docSourceData.put("isBaseSource", true);
        docSourceData.put("isRequiredSource", true);
        docSourceData.put("sourceType", "Offering");
        docSourceData.put("effectiveDate", "2017-01-07T17:44:15.149-05:00");

    }

    // -----------------------------------------------------------------------

    public void validateObjectId(Map<String, Object> docMetadata) {
        String docObjectIdProp = docMetadata.get("objectId").toString();
        assertNotNull(docObjectIdProp);
        assertTrue("objectId is 0!", !docObjectIdProp.equals("0"));
    }

    // -----------------------------------------------------------------------

    public Map<String, Object> testShadowMetadata(Integer associationExpansionLevel) {

        String docObjectId = testDocMetadataUpdatesInsertShadow(1);
        Map<String, Object> docMetadata = contentService.getObjectById(userToken, docObjectId, P2ACollection.DOCUMENTS,
                associationExpansionLevel, null);
        validateObjectId(docMetadata);

        return docMetadata;
    }

    // -----------------------------------------------------------------------
    /*
     * This scenario is known to produce a document with a shadowId of 0 (ref to
     * shadow doc corrupted);
     * this can also cause the object's objectId to be set to 0.
     * create document
     * create documentSource1
     * associate document to documentSource1
     * associate documentSource1 to dataSource (say an offering)
     * create documentSource2
     * associate document to documentSource2 -> fails because document shadowId
     * = 0
     *
     */

    // -----------------------------------------------------------------------

    public void testDoc2docSourcesAssociationScenario() {

        String offeringId = "";

        // create document
        String docObjectId = createDoc();

        // create documentSource 1
        String docSourceName = "documentSources_" + System.nanoTime(); // timing can cause name collisions
        String docSourceId1 = createDocSource(docSourceName);

        // associate document to documentSource
        List<String> doc2dataSources = createAssoc(P2ACollection.DOCUMENTS, docObjectId, "documentSources", Arrays.asList(docSourceId1));

        // associate documentSource to offering
        List<String> dataSource2Offering = createAssoc(P2ACollection.DOCUMENTSOURCES, docSourceId1, "dataSource", Arrays.asList(offeringId));

        // create documentSource 2 - use separate map, as contentService.createContent has modified docSourceData (FIXME!)
        docSourceName = "documentSources_" + System.nanoTime(); // timing can cause name collisions
        String docSourceId2 = createDocSource(docSourceName);

        // this should result in docObjectId having a shadowId of 0:
        // associate document to documentSource
        doc2dataSources = createAssoc(P2ACollection.DOCUMENTS, docObjectId, "documentSources", Arrays.asList(docSourceId2));

        Map<String, Object> docMetadata = contentService.getObjectById(userToken, docObjectId, P2ACollection.DOCUMENTS, 1, null);
        String docObjectIdProp = docMetadata.get("objectId").toString();
        String docShadowIdProp = docMetadata.get("shadowId").toString();
        assertNotNull(docObjectIdProp);
        assertNotNull(docShadowIdProp);
        assertTrue("objectId is 0!", !docObjectIdProp.equals("0"));
        assertTrue("shadowId is 0!", !docShadowIdProp.equals("0"));

        addToCleanUp(P2ACollection.DOCUMENTS, docShadowIdProp);
    }

    // -----------------------------------------------------------------------

    public void addToCleanUp(P2ACollection objectType, String objectId) {

        CleanupObject cleanupObject = new CleanupObject(objectType, objectId);
        logger.info("added to cleanup list: " + cleanupObject);
        objectCleanupList.add(cleanupObject);
    }

    // -----------------------------------------------------------------------

    /*
     * Test scenario where we simply update the metadata for a doc.
     *
     */
    public String testDocMetadataUpdatesInsertShadow(int loopCount) {
        // create document
        String docObjectId = createDoc();

        // use null instead of 0 for associationExpansionLevel to prevent association traversal across these non-existent
        // test shadow-docs
        Map<String, Object> docMetadata = contentService.getObjectById(userToken, docObjectId, P2ACollection.DOCUMENTS, null, null);

        String docObjectIdProp = docMetadata.get("objectId").toString();
        assertNotNull(docObjectIdProp);
        assertTrue("objectId is 0!", !docObjectIdProp.equals("0"));

        // this used to result in docObjectId having an objectId or shadowId of 0:
        Map<String, Object> newProps = new HashMap<>();
        for (int i = 0; i < loopCount; i++) {
            // let's just use the doc-id appended with a number for shadowId
            String docShadowIdProp = String.format("%s-%d", docObjectId, i);
            newProps.put("shadowId", docShadowIdProp);
            contentService.updateContent(userToken, P2ACollection.DOCUMENTS, docObjectId, newProps);
            Map<String, Object> newMetadata = checkMetadataIntegrity(docObjectId);
            compareMetadata(docMetadata, newMetadata);
        }

        return docObjectId;
    }

    // -----------------------------------------------------------------------

    /*
     * check doc metadata integrity
     */
    public Map<String, Object> checkMetadataIntegrity(String docId) {
        // get doc metadata
        Map<String, Object> docMetadata = contentService.getObjectById(userToken, docId, P2ACollection.DOCUMENTS, null, null);

        String docObjectIdProp = docMetadata.get("objectId").toString();
        String docShadowIdProp = docMetadata.get("shadowId").toString();
        assertNotNull(docObjectIdProp);
        assertNotNull(docShadowIdProp);
        assertTrue("objectId is 0!", !docObjectIdProp.equals("0"));
        assertTrue("shadowId is 0!", !docShadowIdProp.equals("0"));
        return docMetadata;
    }

    // -----------------------------------------------------------------------

    /*
     * compare new metadata to initial metadata
     */
    public void compareMetadata(Map<String, Object> baseMetadata, Map<String, Object> newMetadata) {
        // really only expect shadowId to be different; new ones have a "-xxx" appended to them
        String baseObjectId = baseMetadata.get("objectId").toString();
        String newObjectId = baseMetadata.get("objectId").toString();

        //        String baseShadowId = baseMetadata.get("shadowId").toString(); // base doesn't have a shadowId
        String newShadowId = newMetadata.get("shadowId").toString();

        assertTrue("objectIds are different", newObjectId.equals(baseObjectId));
        //        assertTrue("shadowIds are unexpectedly different", newShadowId.startsWith(baseObjectId));

        Set<String> baseKeys = baseMetadata.keySet();
        for (String key : baseKeys) {
            String baseValue = baseMetadata.get(key).toString();
            Object newValueObj = newMetadata.get(key);
            String newValue = newValueObj != null ? newValueObj.toString() : "";
            if (!newValue.equals(baseValue)) {
                logger.warn("newValue does not equal baseValue: {}, {}", newValue, baseValue);
            }
        }
    }

    // -----------------------------------------------------------------------

    public String objectUri(String collectionType, String objectId) {
        return String.format("/%s/%s/%s", organizationId, collectionType, objectId);
    }

    // -----------------------------------------------------------------------

    public String createDoc() {
        // make copy of metadata map before creating doc
        Map<String, Object> docProps = new HashMap<>(metadata);
        String docId = contentService.uploadContent(userToken, P2ACollection.DOCUMENTS, docProps, file);
        assertNotNull(docId);

        logger.debug("created docId: " + docId);
        addToCleanUp(P2ACollection.DOCUMENTS, docId);

        return docId;
    }

    // -----------------------------------------------------------------------

    public String createDocSource(String docSourceName) {
        // contentService.createContent() writes all over the input hashmap!
        // so make a copy for each call

        Map<String, Object> docSource = new HashMap<>(docSourceData);
        docSource.put("name", docSourceName);
        String docSourceId = contentService.createContent(userToken, P2ACollection.DOCUMENTSOURCES, docSource);
        assertNotNull(docSourceId);

        logger.debug("created docSourceId: " + docSourceId);
        addToCleanUp(P2ACollection.DOCUMENTSOURCES, docSourceId);

        return docSourceId;
    }

    // -----------------------------------------------------------------------

    public List<String> createAssoc(P2ACollection srcCollectionType, String srcObjectId, String associationName, List<String> targetObjectIds) {
        List<String> associations = contentService.createAssociation(userToken, srcCollectionType, srcObjectId, associationName,
                targetObjectIds);
        assertNotNull(associations);
        logger.debug(String.format("created association from source (%s): %s, target(s): %s", srcCollectionType, srcObjectId, targetObjectIds));

        return associations;
    }

    // -----------------------------------------------------------------------

    public void deleteObjects() {

        logger.debug("cleanup list (size: " + objectCleanupList.size() + "): \n" + objectCleanupList);

        for (CleanupObject cleanupObject : objectCleanupList) {
            try {
                contentService.deleteObject(userToken, cleanupObject.objectType, cleanupObject.objectId);
                logger.info("deleted object: " + cleanupObject);
            } catch (Exception e) {
                // ignore
            }
        }
    }

    // -----------------------------------------------------------------------

    class CleanupObject {

        P2ACollection objectType;
        String objectId;

        public CleanupObject(P2ACollection objectType, String objectId) {
            this.objectType = objectType;
            this.objectId = objectId;
        }

        @Override
        public String toString() {
            return "[" + objectType + "," + objectId + "]";
        }

    }
}