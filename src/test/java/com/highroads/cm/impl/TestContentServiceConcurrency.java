package com.highroads.cm.impl;

import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.highroads.commons.api.exceptions.HrObjectNotFoundException;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;

/**
 *
 * @author Shahin
 *
 *         tests create, read, update
 */
public class TestContentServiceConcurrency {

    private static final Logger logger = LoggerFactory.getLogger(TestContentServiceConcurrency.class);

    private ContentService contentService;
    private UserToken userToken;
    private P2ACollection objectType;

    // ----------------------------------------------------------------------------------------------------

    public TestContentServiceConcurrency(ContentService contentService, UserToken userToken, P2ACollection objectType) {
        super();
        this.contentService = contentService;
        this.userToken = userToken;
        this.objectType = objectType;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * tests following methods in {@link ContentService}:
     * {@link ContentService#deleteObject}
     *
     * @param objectId
     * @throws Exception
     */
    void testDelete(String objectId) throws Exception {

        Map<String, Object> read = contentService.getObjectById(userToken, objectId, objectType, null, null, null);
        contentService.deleteObject(userToken, objectType, objectId);
        try {
            read = contentService.getObjectById(userToken, objectId, objectType, null, null, null);
        } catch (Exception e) {
            if (e instanceof HrObjectNotFoundException) {
                return;
            }
        }
        assertTrue("should not be able to read an object after deleting", false);
    }

    // ----------------------------------------------------------------------------------------------------

    public String createPlan() throws Exception {

        String objectId = testCreateObject(P2ACollection.PLANS, "src/test/content/TestPlan.json");
        Map<String, Object> read = contentService.getObjectById(userToken, objectId, P2ACollection.PLANS, null, null, null);
        logger.info("\ncreated plan " + objectId + ":\n{\n   endDate: " + read.get("endDate") + "\n   planStatus: " + read.get("planStatus") + "\n}");
        return objectId;
    }

    // ----------------------------------------------------------------------------------------------------

    public String createService() throws Exception {

        String objectId = testCreateObject(P2ACollection.SERVICES, "src/test/content/TestService.json");
        Map<String, Object> read = contentService.getObjectById(userToken, objectId, P2ACollection.SERVICES, null, null, null);
        logger.info("\ncreated service " + objectId + ":\n{\n   effectiveDate: " + read.get("effectiveDate") + "\n   serviceName: " + read.get("serviceName")
                + "\n}");
        return objectId;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * tests following methods in {@link ContentService}:
     * {@link ContentService#createContent}
     * {@link ContentService#getObjectById(UserToken, String, String, Integer, String)}
     *
     * @throws Exception
     */
    String testCreateObject(P2ACollection objectType, String jsonFileName) throws Exception {

        String josnContent = new String(Files.readAllBytes(Paths.get(jsonFileName)));
        JSONParser jsonParser = new JSONParser();
        Object object = jsonParser.parse(josnContent);

        @SuppressWarnings("unchecked")
        Map<String, Object> objectProperties = (Map<String, Object>) object;
        String objectId = contentService.createContent(userToken, objectType, objectProperties);
        return objectId;
    }
}
