package com.highroads.cm.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.highroads.commons.batch.BatchProperties;
import com.highroads.commons.cm.CMHelper;
import com.highroads.commons.config.MarklogicConnectionProperties;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.queue.QueueService;
import com.highroads.commons.search.MarklogicSearchManager;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;
import com.highroads.p2alibs.contentservice.MarklogicContentServiceImpl;
import com.highroads.p2alibs.contentservice.ModelSchemaService;
import com.highroads.p2alibs.patchutil.PatchUtil;
import com.highroads.p2alibs.validator.JsonValidator;

@RunWith(MockitoJUnitRunner.class)
@Ignore("need to move config into props file - pending")
public class TestBinaryMetadataMgmt {

    private static final Logger logger = LoggerFactory.getLogger(TestBinaryMetadataMgmt.class);

    // collect all our created objects for later deletion
    private static List<String> objectCleanupList;

    private static final String TEST_ORG = "sutter.com";
    //    private static final String TEST_ORG = "objectId0.com";
    private static final String adminUsername = "admin@local-" + TEST_ORG;
    private static final String TEST_HOST = "localhost";
    //    private static final String TEST_HOST = "192.168.2.99"; // use buildint, port 8100
    private static final int TEST_PORT = 8200;
    //    private static final int TEST_PORT = 8100; // buildint

    private static int sleepTime = 0; // 1; // seconds

    private static Map<String, Object> metadata;

    // document sources to be associated with binary test doc
    private static Map<String, Object> docSourceData;

    // offering associated with documentSource
    private static Map<String, Object> offeringData;

    private static ContentService contentService;
    private static MarklogicConnectionProperties marklogicConnectionProperties;
    private TestObjectIdUtil testObjectIdUtil;

    private static UserToken userToken;
    //    private static String docObjectId;
    //    private static String docSourceId1;
    //    private static String docSourceId2;
    private static String offeringId;

    @Mock
    private BatchProperties batchProperties;

    @Mock
    MarklogicDAO marklogicDAO;

    @Mock
    private PatchUtil patchUtil;

    @Mock
    private QueueService queueService;

    @Mock
    private JsonValidator jsonValidator;

    @Mock
    private ModelSchemaService modelSchemaService;

    @Mock
    private MarklogicSearchManager marklogicSearchManager;

    @Mock
    private ObjectNode objectNode;

    @Mock
    private CMHelper cmHelper;

    @BeforeClass
    public static void setUp_() throws FileNotFoundException, IOException {

        objectCleanupList = new ArrayList<>();

        userToken = new UserToken();
        userToken.setOrganization(TEST_ORG);
        userToken.setUsername(adminUsername);

        marklogicConnectionProperties = new MarklogicConnectionProperties();
        marklogicConnectionProperties.setHost(TEST_HOST);
        marklogicConnectionProperties.setPort(TEST_PORT);
        marklogicConnectionProperties.setUser(adminUsername);
        marklogicConnectionProperties.setLockManagerLockRetry(10); // need non-zero or lock fails
        marklogicConnectionProperties.setLockManagerLockWait(250);
        marklogicConnectionProperties.setLockManagerCollectionName("locks");
        marklogicConnectionProperties.setConnectionPoolEnabled(true);
        marklogicConnectionProperties.setMaxConnections(25);
        marklogicConnectionProperties.setMaxConnectionsPerOrganization(5);

        marklogicConnectionProperties.setVersioned(true);
        marklogicConnectionProperties.setDocumentVersioned(true);
        marklogicConnectionProperties.setTemplateVersioned(true);
        marklogicConnectionProperties.setPlanVersioned(true);
        marklogicConnectionProperties.setProductVersioned(true);
        marklogicConnectionProperties.setOfferingVersioned(true);

        new MarklogicDAO(marklogicConnectionProperties);

        // test 'binary' document with associations to documentSources
        // these keys need to be schema-compliant
        metadata = new HashMap<>();

        // metadata.put("name", "testbinarydoc1.docx"); let system generate unique name to avoid collisions
        metadata.put("docName", "testbinarydoc1.docx");
        metadata.put("documentStatus", "Draft");
        metadata.put("watermark", "testing the watermark");

        // documentSource instance data - minimal required fields
        docSourceData = new HashMap<>();
        docSourceData.put("isBaseSource", true);
        docSourceData.put("isRequiredSource", true);
        docSourceData.put("sourceType", "Offering");
        docSourceData.put("effectiveDate", "2017-01-07T17:44:15.149-05:00");
    }

    @Before
    public void setUp() {
        when(modelSchemaService.getSchema(any(UserToken.class))).thenReturn(objectNode);
        when(marklogicSearchManager.isBinaryObjectType("documents")).thenReturn(true);
        when(marklogicSearchManager.isBinaryObjectType("documentSources")).thenReturn(false);
        when(marklogicSearchManager.isBinaryObjectType("offerings")).thenReturn(false);

        contentService = new MarklogicContentServiceImpl(jsonValidator, modelSchemaService, marklogicSearchManager, marklogicDAO, patchUtil,
                cmHelper);
        testObjectIdUtil = new TestObjectIdUtil(contentService, userToken, TEST_ORG);

        // offering instance data - associated with first documentSource
        offeringData = new HashMap<>();
        offeringData.put("benefitYear", "2017");
        offeringId = contentService.createContent(userToken, P2ACollection.OFFERINGS, offeringData);
        assertNotNull(offeringId);

        objectCleanupList.add(objectUri("offerings", offeringId));
    }

    private String objectUri(String collectionType, String objectId) {
        return String.format("/%s/%s/%s", TEST_ORG, collectionType, objectId);
    }

    @Test
    public void test1DocToDocSourcesAssociations() throws IllegalStateException, IOException {
        logger.debug("testing single associations scenario...");
        testObjectIdUtil.testDoc2docSourcesAssociationScenario();
        logger.debug("... done");
    }

    @Test
    public void test10DocToDocSourcesAssociations() throws IllegalStateException, IOException, InterruptedException {
        logger.debug("testing 10x associations scenario...");
        for (int i = 0; i < 10; i++) {
            testObjectIdUtil.testDoc2docSourcesAssociationScenario();
            if (sleepTime > 0) {
                logger.debug("sleeping...");
                Thread.sleep(sleepTime * 1000);
            }
        }
        logger.debug("... done");
    }

    @Test
    public void test100DocToDocSourcesAssociations() throws IllegalStateException, IOException, InterruptedException {
        logger.debug("testing 100x associations scenario...");
        for (int i = 0; i < 100; i++) {
            testObjectIdUtil.testDoc2docSourcesAssociationScenario();
            if (sleepTime > 0) {
                logger.debug("sleeping...");
                Thread.sleep(sleepTime * 1000);
            }
        }
        logger.debug("... done");
    }

    @Test
    public void test1DocMetadataUpdateInsertShadow() {
        testObjectIdUtil.testDocMetadataUpdatesInsertShadow(1);
    }

    @Test
    public void test10DocMetadataUpdateInsertShadow() {
        testObjectIdUtil.testDocMetadataUpdatesInsertShadow(10);
    }

    @Test
    public void test100DocMetadataUpdateInsertShadow() {
        testObjectIdUtil.testDocMetadataUpdatesInsertShadow(100);
    }

    @Test
    public void testGetDocNullAssocInvalidShadow() {
        testObjectIdUtil.testShadowMetadata(null);
    }

    /**
     * attempt to fold in shadowDoc in get response when there is an invalid
     * shadowId
     * (shadowDoc with given Id does not exist)
     */
    @Test(expected = com.highroads.commons.api.exceptions.HrObjectNotFoundException.class)
    public void testGetDocZeroAssocInvalidShadow() {
        testObjectIdUtil.testShadowMetadata(new Integer(0));
    }

    // -----------------------------------------------------------------------

    @After
    public void cleanup() {
        testObjectIdUtil.deleteObjects();
    }

    // -----------------------------------------------------------------------
}
