package com.highroads.cm.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.Application;
import com.highroads.cm.batch.BatchAction;
import com.highroads.cm.batch.BatchService;
import com.highroads.commons.batch.BatchProperties;
import com.highroads.commons.cm.CMHelper;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.queue.QueueService;
import com.highroads.commons.search.MarklogicSearchManager;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.MarklogicContentServiceImpl;
import com.highroads.p2alibs.contentservice.ModelSchemaService;
import com.highroads.p2alibs.validator.JsonValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class BulkBatchResubmit {

    @Autowired
    BatchService batchService;

    @Autowired
    MarklogicDAO marklogicDAO;

    @Test
    public void testBulkBatchResubmit() throws Exception {
        final BatchProperties batchProperties = null;
        final QueueService queueService = null;
        final JsonValidator jsonValidator = null;
        final ModelSchemaService modelSchemaService = null;
        final MarklogicSearchManager marklogicSearchManager = null;
        final CMHelper cmHelper = null;
        UserToken userToken = new UserToken();
        BatchAction resubmitFailedFlag = BatchAction.RESUBMIT_ALL;

        ObjectMapper objectMapper = new ObjectMapper();
        String bulkBatchStausList = "[\n  {\n    \"name\": \"z11\",\n    \"batchClass\": \"Concurrent\",\n    \"batchStatus\": \"Submitted\",\n    \"objectId\": \"489cec20-766e-4889-9ed4-8eb7f315e182\",\n    \"associations\": {\n      \"batchJobs\": [\n        {\n          \"objectId\": \"f9eb1d08-a4d2-4137-b0be-19d4c27d24f5\"\n        }\n      ]\n    },\n    \"createdBy\": \"qatest@upmc.com\",\n    \"creationDate\": \"2017-06-23T12:55:45.514+05:30\",\n    \"lastModifiedBy\": \"admin@upmc.com\",\n    \"lastModificationDate\": \"2018-01-08T15:10:13.622+05:30\",\n    \"objectType\": \"batch\",\n    \"batchSubmittedBy\": \"admin@upmc.com\",\n    \"batchSubmittedDate\": \"2018-01-08T15:10:13.622+05:30\",\n    \"batchJobs\": [\n      \"f9eb1d08-a4d2-4137-b0be-19d4c27d24f5\"\n    ]\n  },\n  {\n    \"name\": \"a2\",\n    \"batchClass\": \"Concurrent\",\n    \"batchStatus\": \"Submitted\",\n    \"objectId\": \"487504e6-456c-4884-a1c3-86da2a2822aa\",\n    \"associations\": {\n      \"batchJobs\": [\n        {\n          \"objectId\": \"c6d37954-58b9-465a-b39c-d04d3318974a\"\n        }\n      ]\n    },\n    \"createdBy\": \"qatest@upmc.com\",\n    \"creationDate\": \"2017-06-16T00:39:24.114+05:30\",\n    \"lastModifiedBy\": \"admin@upmc.com\",\n    \"lastModificationDate\": \"2018-01-08T15:10:13.778+05:30\",\n    \"objectType\": \"batch\",\n    \"batchSubmittedBy\": \"admin@upmc.com\",\n    \"batchSubmittedDate\": \"2018-01-08T15:10:13.778+05:30\",\n    \"batchJobs\": [\n      \"c6d37954-58b9-465a-b39c-d04d3318974a\"\n    ]\n  },\n  {\n    \"name\": \"afad\",\n    \"batchClass\": \"Concurrent\",\n    \"batchStatus\": \"Submitted\",\n    \"objectId\": \"cfca3199-37c7-443d-9bba-98f0cf741043\",\n    \"associations\": {\n      \"batchJobs\": [\n        {\n          \"objectId\": \"b6855971-faf8-4c7a-87c2-379f49eb6d25\"\n        }\n      ]\n    },\n    \"createdBy\": \"qatest@upmc.com\",\n    \"creationDate\": \"2017-06-15T21:34:53.651+05:30\",\n    \"lastModifiedBy\": \"admin@upmc.com\",\n    \"lastModificationDate\": \"2018-01-05T17:21:49.063+05:30\",\n    \"objectType\": \"batch\",\n    \"batchSubmittedBy\": \"admin@upmc.com\",\n    \"batchSubmittedDate\": \"2018-01-05T17:21:49.063+05:30\",\n    \"batchJobs\": [\n      \"b6855971-faf8-4c7a-87c2-379f49eb6d25\"\n    ]\n  }\n]";
        String batchIdsStr = "[\r\n      \"489cec20-766e-4889-9ed4-8eb7f315e182\",\r\n     \"cfca3199-37c7-443d-9bba-98f0cf741043\"\r\n    ]\r\n";

        List<Map<String, Object>> batchesStatusList = null;
        List<String> batchIds = null;
        batchesStatusList = objectMapper.readValue(bulkBatchStausList, List.class);
        batchIds = objectMapper.readValue(batchIdsStr, List.class);

        MarklogicContentServiceImpl mls = new MarklogicContentServiceImpl(jsonValidator, modelSchemaService, marklogicSearchManager, marklogicDAO, null,
                cmHelper);
        Map<String, String> actual = batchService.getInvalidBatches(userToken, batchesStatusList, batchIds, resubmitFailedFlag);

        //Return submit,Draft,Queued,In Progress list of bulkBatch
        assertThat(actual.size(), is(equalTo(2)));
    }

}
