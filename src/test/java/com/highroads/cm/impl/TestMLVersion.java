package com.highroads.cm.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.Application;
import com.highroads.commons.config.MarklogicConnectionProperties;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.sso.UserToken;
import com.highroads.commons.version.MLVersionImpl;
import com.highroads.commons.version.MLVersionInterface;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.FailedRequestException;
import com.marklogic.client.document.BinaryDocumentManager;
import com.marklogic.client.document.DocumentManager;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.io.BytesHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.SearchHandle;
import com.marklogic.client.query.MatchDocumentSummary;
import com.marklogic.client.query.QueryManager;
import com.marklogic.client.query.StructuredQueryBuilder;
import com.marklogic.client.query.StructuredQueryDefinition;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class TestMLVersion {

    private final String BINARY_1_FILE_LOCATION = "src/test/content/TestBinary.pdf";
    private final String BINARY_2_FILE_LOCATION = "src/test/content/TestBinary2.pdf";
    private final String BINARY_FILE_SUFFIX = BINARY_1_FILE_LOCATION.split("\\.")[1];

    @Autowired
    private MarklogicConnectionProperties connectionProperties;
    private UserToken token;
    private MLVersionImpl mlv;

    @Autowired
    MarklogicDAO marklogicDAO;

    private static String MANAGED_DOCUMENT_DIRECTORY = "/managed-document-test/";

    private static boolean debug = false;

    static {
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.ERROR);
    }

    @Before
    public void setup() {
        token = new UserToken();
        token.setUsername("admin");
        DatabaseClient databaseClient = marklogicDAO.getDatabaseClient("admin", "");
        mlv = new MLVersionImpl(databaseClient);
        deleteManagedDocuments();
    }

    @Test
    /* Trying to manage an already managed document should fail */
    public void managingAlreadyManagedDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        Map<String, Object> object = getTestObject();
        mlv.insertAndManageJSON(uri, null, object);
        try {
            mlv.insertAndManageJSON(uri, null, object);
            // Shouldn't get here - error should be thrown when trying to manage same document a second time
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("already exists and is managed"));
        }
    }

    @Test
    /* Trying to manage an already managed binary should fail */
    public void managingAlreadyManagedBinary() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        mlv.insertAndManageBinary(uri, null, getBinaryFile(BINARY_1_FILE_LOCATION), new HashMap<>());
        try {
            mlv.insertAndManageBinary(uri, null, getBinaryFile(BINARY_1_FILE_LOCATION), new HashMap<>());
            // Shouldn't get here - error should be thrown when trying to manage same document a second time
            assertTrue(false);
        } catch (FailedRequestException e) {
            // Error message should contain specific text
            assertTrue(e.getFailedRequest().getMessage().contains("already exists and is managed"));
        }
    }

    @Test
    /* Shouldn't be able to call insert and manage if document already exists */
    public void managingExistingDocumentButOverwriting() throws Exception {
        String uri = generateManagedDocumentURI();
        Map<String, Object> object = getTestObject();
        createDocument(uri, object);
        try {
            mlv.insertAndManageJSON(uri, null, object);
            // Shouldn't get here - error should be thrown if document exists and trying to over-write with new content
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("manage before updating"));
        }
    }

    @Test
    /* Shouldn't be able to call insert and manage if binary already exists */
    public void managingExistingBinaryButOverwriting() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        createBinary(uri, getBinaryFile(BINARY_1_FILE_LOCATION));
        try {
            mlv.insertAndManageBinary(uri, new String[] {}, getBinaryFile(BINARY_1_FILE_LOCATION), new HashMap<>());
            // Shouldn't get here - error should be thrown if binary exists and trying to over-write with new content
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("manage before updating"));
        }
    }

    @Test
    /* Adding and managing document which doesn't exist should succeed */
    public void addingNewDocument() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        String returnedURI = mlv.insertAndManageJSON(uri, new String[] {}, object);
        assertTrue(uri.equals(returnedURI));
    }

    @Test
    /* Adding and managing document which doesn't exist should succeed */
    public void addingNewBinary() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        String returnedURI = mlv.insertAndManageBinary(uri, new String[] {}, getBinaryFile(BINARY_1_FILE_LOCATION), new HashMap<>());
        assertTrue(uri.equals(returnedURI));
    }

    @Test
    public void managingExistingDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        Map<String, Object> object = getTestObject();
        createDocument(uri, object);
        try {
            String returnedURI = mlv.insertAndManageJSON(uri, new String[] {}, null);
            // Should get here - error should be not be thrown if document exists if null object supplied
            assertTrue(uri.equals(returnedURI));
        } catch (FailedRequestException e) {
            assertTrue(false);
        }
    }

    @Test
    public void managingExistingBinary() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        createBinary(uri, getBinaryFile(BINARY_1_FILE_LOCATION));
        try {
            String returnedURI = mlv.insertAndManageBinary(uri, new String[] {}, null, new HashMap<>());
            // Should get here - error should be not be thrown if document exists if null object supplied
            assertTrue(uri.equals(returnedURI));
        } catch (FailedRequestException e) {
            assertTrue(false);
        }
    }

    /*
     * Same as managingExistingDocument, but using manage not
     * insertAndManageJSON
     */
    @Test
    public void managingExistingDocument2() throws Exception {
        String uri = generateManagedDocumentURI();
        Map<String, Object> object = getTestObject();
        createDocument(uri, object);
        try {
            String returnedURI = mlv.manageJSON(uri);
            // Should get here - error should be not be thrown if document exists if null object supplied
            assertTrue(uri.equals(returnedURI));
        } catch (FailedRequestException e) {
            assertTrue(false);
        }
    }

    @Test
    public void managingExistingBinary2() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        createBinary(uri, getBinaryFile(BINARY_1_FILE_LOCATION));
        try {
            String returnedURI = mlv.manageBinary(uri);
            // Should get here - error should be not be thrown if document exists if null object supplied
            assertTrue(uri.equals(returnedURI));
        } catch (FailedRequestException e) {
            assertTrue(false);
        }
    }

    @Test
    public void mustSupplyURIWhenManagingDocument() throws Exception {
        Map<String, Object> object = getTestObject();
        try {
            mlv.insertAndManageJSON((String) null, new String[] {}, object);
            // Shouldn't get here - error should be thrown if uri not supplied
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("uri missing"));
        }
    }

    @Test
    public void mustSupplyURIWhenManagingBinary() throws Exception {
        try {
            mlv.insertAndManageBinary((String) null, new String[] {}, getBinaryFile(BINARY_1_FILE_LOCATION), new HashMap<>());
            // Shouldn't get here - error should be thrown if uri not supplied
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("uri missing"));
        }
    }

    @Test
    public void managingNonExistentDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        try {
            mlv.insertAndManageJSON(uri, new String[] {}, null);
            // Should not get here - error should be thrown if document doesn't exist and content not supplied
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("does not exist"));
        }

    }

    @Test
    public void managingNonExistentBinary() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        try {
            mlv.insertAndManageBinary(uri, new String[] {}, null, new HashMap<>());
            // Should not get here - error should be thrown if document doesn't exist and content not supplied
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("does not exist"));
        }

    }

    /*
     * Same as managingNonExistentDocument, but using manageJSON rather than
     * insertAndManageJSON
     */
    @Test
    public void managingNonExistentDocument2() throws Exception {
        String uri = generateManagedDocumentURI();
        try {
            mlv.manageJSON(uri);
            // Should not get here - error should be thrown if document doesn't exist and content not supplied
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("does not exist"));
        }

    }

    /*
     * Same as managingNonExistentBinary, but using manageBinary rather than
     * insertAndManageBinary
     */
    @Test
    public void managingNonExistentBinary2() throws Exception {
        String uri = generateManagedDocumentURI(BINARY_FILE_SUFFIX);
        try {
            mlv.manageBinary(uri);
            // Should not get here - error should be thrown if document doesn't exist and content not supplied
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("does not exist"));
        }

    }

    @Test
    /* Checking out a checked-in document should succeed */
    public void checkout() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        mlv.checkout(uri);
        assertTrue(true);
    }

    @Test
    /* Checking out non-existent document */
    public void checkoutNonExistentDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        try {
            mlv.checkout(uri);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("DLS-UNMANAGED"));
        }
    }

    @Test
    /* Checking out already checked out document */
    public void checkoutCheckedOutDocument() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        mlv.checkout(uri);
        try {
            mlv.checkout(uri);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("checked out already"));
        }
    }

    @Test
    /* Checking in a checked-out document should succeed */
    public void checkin() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        mlv.checkout(uri);
        mlv.checkin(uri);
        assertTrue(true);
    }

    @Test
    /* Checking in a checked-out document should succeed */
    public void checkinWhenNotCheckedOut() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        try {
            mlv.checkin(uri);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("You do not have checkout"));
        }
    }

    @Test
    /* Update with checkout should succeed */
    public void updateJSON() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, object);
        mlv.checkout(uri);
        Map<String, Object> newObject = getTestObject();
        String returnValue = mlv.updateJSON(uri, newObject);
        assertTrue(returnValue.equals(uri));
        Map<String, Object> storedObject = mlv.getJSONDocument(uri, null);
        assertTrue(newObject.toString().equals(storedObject.toString()));
    }

    @Test
    /* Update with checkout should succeed */
    public void updateBinary() throws Exception {
        String uri = mlv.insertAndManageBinary(generateManagedDocumentURI(), new String[] {}, getBinaryFile(BINARY_1_FILE_LOCATION), new HashMap<>());
        mlv.checkout(uri);
        String returnValue = mlv.updateBinary(uri, getBinaryFile(BINARY_2_FILE_LOCATION), new HashMap<>());
        assertTrue(returnValue.equals(uri));
        byte[] storedObject = mlv.getBinaryDocument(uri, null);
        assertTrue(Arrays.equals(storedObject, getBinaryFile(BINARY_2_FILE_LOCATION)));
    }

    @Test
    /* Update without checkout should fail */
    public void updateWithoutCheckout() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        try {
            mlv.updateJSON(uri, getTestObject());
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("No checkout for"));
        }
    }

    @Test
    @Ignore("Revisit")
    /* Update without own checkout should fail */
    public void updateWithoutOwnCheckout() throws Exception {
        DatabaseClient databaseClient = marklogicDAO.getDatabaseClient("admin", "");
        MLVersionInterface mlvAlternate = new MLVersionImpl(databaseClient);
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        mlv.checkout(uri);
        try {
            mlvAlternate.updateJSON(uri, getTestObject());
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("No checkout for"));
        }
    }

    @Test
    /* Update without checkout should fail */
    public void updateUnmanagedDocument() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        createDocument(uri, object);
        try {
            mlv.updateJSON(uri, getTestObject());
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("manage before updating"));
        }
    }

    @Test
    /* Update non-existent document should fail */
    public void updateNonExistentDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        try {
            mlv.updateJSON(uri, getTestObject());
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("doesn't exist - can't update"));
        }
    }

    @Test
    public void testIsManaged() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        assertTrue(mlv.isManaged(uri));
    }

    @Test
    public void testIsManagedForNonExistentDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        assertFalse(mlv.isManaged(uri));
    }

    @Test
    public void testIsManagedForUnmanagedDocument() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        createDocument(uri, object);
        assertFalse(mlv.isManaged(uri));
    }

    @Test
    /* Test get returns expected object */
    public void get() throws Exception {
        Map<String, Object> object = getTestObject();
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, object);
        Map<String, Object> storedObject = mlv.getJSONDocument(uri, null);
        assertTrue(storedObject.toString().equals(object.toString()));
    }

    @Test
    public void testDelete() throws Exception {
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, getTestObject());
        mlv.delete(uri);
        try {
            mlv.getJSONDocument(uri, null);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("not under management"));
        }
    }

    @Test
    public void cantDeleteDeletedDocument() throws Exception {
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, getTestObject());
        mlv.delete(uri);
        try {
            mlv.delete(uri);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("not under management"));

        }
    }

    @Test
    public void cantDeleteNonExistentDocument() throws Exception {
        String uri = generateManagedDocumentURI();
        try {
            mlv.delete(uri);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("not under management"));
        }
    }

    @Test
    /*
     * Check retrieval of versions is correct
     */
    @Ignore("Revisit")
    public void testVersioning() throws Exception {
        Map<String, Object> v1 = getTestObject();
        Map<String, Object> v2 = getTestObject();
        Map<String, Object> v3 = getTestObject();
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, v1);
        mlv.checkout(uri);
        mlv.updateJSON(uri, v2);
        mlv.updateJSON(uri, v3);

        assertTrue(mlv.getJSONDocument(uri, 1).toString().equals(v1.toString()));
        assertTrue(mlv.getJSONDocument(uri, 2).toString().equals(v2.toString()));
        assertTrue(mlv.getJSONDocument(uri, 3).toString().equals(v3.toString()));
    }

    @Test
    /*
     * Throws error if version does not exist
     */
    public void nonExistentVersion() throws Exception {
        Map<String, Object> v1 = getTestObject();
        Map<String, Object> v2 = getTestObject();
        Map<String, Object> v3 = getTestObject();
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, v1);
        mlv.checkout(uri);
        mlv.updateJSON(uri, v2);
        mlv.updateJSON(uri, v3);

        try {
            mlv.getJSONDocument(uri, 4);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("has no version number"));
        }
    }

    @Test
    @Ignore("Revisit")
    public void testRevert() throws Exception {
        Map<String, Object> v1 = getTestObject();
        Map<String, Object> v2 = getTestObject();
        Map<String, Object> v3 = getTestObject();
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, v1);
        mlv.checkout(uri);
        mlv.updateJSON(uri, v2);
        mlv.updateJSON(uri, v3);
        // Revert to v2
        mlv.revert(uri, 2);

        // Check current version is v2
        assertTrue(mlv.getJSONDocument(uri, null).toString().equals(v2.toString()));
    }

    @Test
    public void history() throws Exception {
        Map<String, Object> v1 = getTestObject();
        Map<String, Object> v2 = getTestObject();
        Map<String, Object> v3 = getTestObject();
        String uri = mlv.insertAndManageJSON(generateManagedDocumentURI(), new String[] {}, v1);
        mlv.checkout(uri);
        mlv.updateJSON(uri, v2);
        mlv.updateJSON(uri, v3);

        List<Map<String, Object>> history = mlv.getHistory(uri);

        if (debug) {
            Iterator i = history.iterator();
            while (i.hasNext()) {
                System.out.println(i.next().toString());
            }
        }
        assertTrue(history.size() == 3);
    }

    @Test
    /* Admin can break checkout */
    public void breakCheckoutAsAdmin() throws Exception {
        DatabaseClient databaseClient = marklogicDAO.getDatabaseClient("admin", "");
        mlv = new MLVersionImpl(databaseClient);
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        mlv.checkout(uri);
        if (debug) {
            System.out.println("Break checkout URI is " + uri);
        }
        mlv.breakCheckout(uri);
        assertTrue(true);
        mlv = new MLVersionImpl(databaseClient);
    }

    @Test
    @Ignore("Revisit")
    /* Non-admin cannot break checkout */
    public void breakCheckoutWhenNotAdmin() throws Exception {
        DatabaseClient databaseClient = marklogicDAO.getDatabaseClient("admin", "");
        MLVersionInterface mlvAlternate = new MLVersionImpl(databaseClient);
        Map<String, Object> object = getTestObject();
        String uri = generateManagedDocumentURI();
        mlv.insertAndManageJSON(uri, new String[] {}, object);
        mlv.checkout(uri);
        try {
            mlvAlternate.breakCheckout(uri);
            assertTrue(false);
        } catch (FailedRequestException e) {
            assertTrue(e.getFailedRequest().getMessage().contains("dls-admin privilege required"));
        }
    }

    @Test
    public void binary() throws Exception {
        byte[] binary = getBinaryFile(BINARY_1_FILE_LOCATION);
    }

    /* Utility method to make a test object with a given type */
    private static Map<String, Object> getTestObject() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Key", java.util.UUID.randomUUID());
        return map;
    }

    /* Utility URI generation methods */
    private static String generateManagedDocumentURI() {
        return generateManagedDocumentURI("json");
    }

    private static String generateManagedDocumentURI(String suffix) {
        return MANAGED_DOCUMENT_DIRECTORY + java.util.UUID.randomUUID() + "." + suffix;
    }

    /* Utility method to create a document */
    private void createDocument(String uri, Map<String, Object> object) throws Exception {
        ObjectMapper o = new ObjectMapper();
        DatabaseClient client = DatabaseClientFactory.newClient(connectionProperties.getHost(), connectionProperties.getPort(),
                connectionProperties.getUser(), connectionProperties.getPassword(), Authentication.DIGEST);

        JSONDocumentManager mgr = client.newJSONDocumentManager();

        JsonNode jsonNode = o.readValue(o.writeValueAsString(object), JsonNode.class);
        JacksonHandle handle = new JacksonHandle(jsonNode);

        mgr.write(uri, handle);
        client.release();
    }

    /* Utility method to create a binary document */
    private void createBinary(String uri, byte[] binary) throws Exception {
        DatabaseClient client = DatabaseClientFactory.newClient(connectionProperties.getHost(), connectionProperties.getPort(),
                connectionProperties.getUser(), connectionProperties.getPassword(), Authentication.DIGEST);

        BinaryDocumentManager mgr = client.newBinaryDocumentManager();
        BytesHandle handle = new BytesHandle(binary);

        mgr.write(uri, handle);
        client.release();
    }

    @After
    /* Delete all documents created by testing */
    public void deleteManagedDocuments() {
        DatabaseClient client = DatabaseClientFactory.newClient(connectionProperties.getHost(), connectionProperties.getPort(),
                connectionProperties.getUser(), connectionProperties.getPassword(), Authentication.DIGEST);
        QueryManager queryMgr = client.newQueryManager();
        DocumentManager mgr = client.newDocumentManager();
        StructuredQueryBuilder sb = queryMgr.newStructuredQueryBuilder();
        // put code from examples here
        StructuredQueryDefinition criteria = sb.directory(true, MANAGED_DOCUMENT_DIRECTORY);
        SearchHandle searchHandle = new SearchHandle();
        queryMgr.search(criteria, searchHandle);

        // iterate over the result documents
        MatchDocumentSummary[] docSummaries = searchHandle.getMatchResults();

        for (MatchDocumentSummary docSummary : docSummaries) {
            String uri = docSummary.getUri();
            mgr.delete(uri);
        }
        client.release();
    }

    /* Get binary content */
    public byte[] getBinaryFile(String path) throws Exception {
        Path _path = Paths.get(path);
        return Files.readAllBytes(_path);
    }

}
