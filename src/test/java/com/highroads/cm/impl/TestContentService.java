package com.highroads.cm.impl;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
@Ignore("Revisit")
public class TestContentService {
    /*
     * private static VersionContentService versionContentService;
     * private static ContentService contentService;
     * 
     * private static MarklogicConnectionProperties
     * marklogicConnectionProperties;
     * 
     * private static MLJSONDocumentImpl document;
     * 
     * @Autowired
     * private BatchService batchService;
     * 
     * @Mock
     * MarklogicDAO marklogicDAO;
     * 
     * private static String uri;
     * 
     * private static String draftBatchURI;
     * 
     * private static String failedBatchURI;
     * 
     * private static String submittedBatchURI;
     * 
     * private static String cancelledBatchURI;
     * 
     * private static UserToken userToken;
     * 
     * private static String objectId;
     * private static String binObjectId;
     * private static String jsonObjectId;
     * 
     * private static String logoId;
     * 
     * private static MultipartFile file;
     * 
     * private static MultipartFile logo;
     * 
     * private static Map<String, Object> metadata;
     * 
     * @Mock
     * private BatchProperties batchProperties;
     * 
     * @Mock
     * private PatchUtil patchUtil;
     * 
     * @Mock
     * private QueueService queueService;
     * 
     * @Mock
     * private JsonValidator jsonValidator;
     * 
     * @Mock
     * private ModelSchemaService modelSchemaService;
     * 
     * @Mock
     * private MarklogicSearchManager marklogicSearchManager;
     * 
     * @Mock
     * private ObjectNode objectNode;
     * 
     * @Mock
     * private CMHelper cmHelper;
     * 
     * // @Mock
     * // private Boolean booleanResult;
     * 
     * private static final ObjectMapper MAPPER = new ObjectMapper();
     * 
     * private static List<String> draftBatchJobIds = new ArrayList<>();
     * private static List<String> failedBatchJobIds = new ArrayList<>();
     * private static List<String> submittedBatchJobIds = new ArrayList<>();
     * private static List<String> cancelledBatchJobIds = new ArrayList<>();
     * 
     * private static final String adminUsername = "admin@local-upmc.com";
     * 
     * @BeforeClass
     * public static void setUp_() throws FileNotFoundException, IOException {
     * 
     * userToken = new UserToken();
     * userToken.setOrganization("upmc.com");
     * userToken.setUsername(adminUsername);
     * 
     * marklogicConnectionProperties = new MarklogicConnectionProperties();
     * marklogicConnectionProperties.setHost("localhost");
     * marklogicConnectionProperties.setPort(8200);
     * marklogicConnectionProperties.setUser("admin@local-upmc.com");
     * marklogicConnectionProperties.setLockManagerLockRetry(10); // need
     * non-zero or lock fails
     * marklogicConnectionProperties.setLockManagerLockWait(250);
     * marklogicConnectionProperties.setLockManagerCollectionName("locks");
     * 
     * new MarklogicDAO(marklogicConnectionProperties);
     * 
     * Map<String, Object> fragment = new HashMap<>();
     * fragment.put("key1", "value1");
     * fragment.put("key2", "value2");
     * fragment.put("objectId", "100A");
     * 
     * document = MLJSONDocumentImpl.getInstance("upmc.com", adminUsername,
     * "plans", fragment,ma);
     * uri = document.create();
     * 
     * draftBatchURI = createBatch("Draft", draftBatchJobIds);
     * failedBatchURI = createBatch("Failed", failedBatchJobIds);
     * submittedBatchURI = createBatch("Submitted", submittedBatchJobIds);
     * cancelledBatchURI = createBatch("Cancelled", cancelledBatchJobIds);
     * 
     * metadata = new HashMap<>();
     * metadata.put("KEY1", "VALUE");
     * 
     * file = new MockMultipartFile("junit.txt", "junit.txt", "text/plain",
     * "This is a test file".getBytes());
     * 
     * File sampelFile = new File("src/test/data/sample.jpg");
     * 
     * logo = new MockMultipartFile("sample.jpg", "sample.jpg", "image/jpeg",
     * IOUtils.toByteArray(new FileInputStream(sampelFile)));
     * }
     * 
     * private static String createBatch(String status, List<String>
     * batchJobIds) throws IOException, JsonProcessingException {
     * 
     * createBatchJobs(batchJobIds);
     * Map<String, Object> batch = new LinkedHashMap<>();
     * batch.put("batchClass", "Concurrent");
     * batch.put("batchStatus", status);
     * batch.put("batchSubmittedBy", "admin@p2a.com");
     * List<Map<String, Object>> associations = new ArrayList<>();
     * for (String batchJob : batchJobIds) {
     * Map<String, Object> job = new HashMap<>();
     * job.put("objectId", batchJob);
     * associations.add(job);
     * }
     * Map<String, Object> associationObject = new HashMap<>();
     * associationObject.put("batchJobs", associations);
     * batch.put("associations", associationObject);
     * 
     * return MLJSONDocumentImpl.getInstance("upmc.com", adminUsername, "batch",
     * batch).create();
     * 
     * }
     * 
     * private static void createBatchJobs(List<String> batchJobIds) throws
     * IOException, JsonProcessingException {
     * 
     * File batchJson = new File("src/test/data/batchJobs.json");
     * JsonNode batchJsonNode = MAPPER.readTree(batchJson);
     * Map<String, Object> result = MAPPER.convertValue(batchJsonNode,
     * Map.class);
     * List<Map<String, Object>> batches = (List<Map<String, Object>>)
     * result.get("batchJobs");
     * for (Map<String, Object> batch : batches) {
     * batchJobIds.add(MLJSONDocumentImpl.getInstance("upmc.com", adminUsername,
     * "batchJobs", batch).create());
     * }
     * }
     * 
     * @Before
     * public void setUp() {
     * Queue queue = new BatchProperties.Queue();
     * Name name = new BatchProperties.Queue.Name();
     * name.setTenant("upmc.com");
     * queue.setName(name);
     * when(modelSchemaService.getSchema(any(UserToken.class))).thenReturn(
     * objectNode);
     * when(marklogicSearchManager.isBinaryObjectType("documents")).thenReturn(
     * true);
     * when(marklogicSearchManager.isBinaryObjectType("plans")).thenReturn(false
     * );
     * 
     * contentService = new MarklogicContentServiceImpl(jsonValidator,
     * modelSchemaService, marklogicSearchManager, marklogicDAO, patchUtil,
     * cmHelper);
     * 
     * when(batchProperties.getQueue()).thenReturn(queue);
     * }
     * 
     * @Test
     * public void shouldReplaceDocument() {
     * 
     * Map<String, Object> obj1 = new HashMap<>();
     * obj1.put("key1", "value1");
     * obj1.put("key2", "value2");
     * 
     * // create new doc
     * MLJSONDocumentImpl doc = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "plans", obj1);
     * String docId = doc.create();
     * 
     * // read it back, change some values
     * Map<String, Object> readObj1 = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "plans", docId).read();
     * readObj1.put("key1", "value1-replaced");
     * readObj1.put("key2", "value2-replaced");
     * readObj1.put("key3", "value-3-new");
     * 
     * // replace the doc
     * MLJSONDocumentImpl.getInstance("upmc.com", adminUsername, "plans", docId,
     * readObj1).write();
     * Map<String, Object> repl = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "plans", docId).read();
     * 
     * assertEquals("value1-replaced", repl.get("key1"));
     * assertEquals("value2-replaced", repl.get("key2"));
     * assertEquals("value-3-new", repl.get("key3"));
     * MLJSONDocumentImpl.getInstance(userToken.getOrganization(),
     * userToken.getUsername(), "plans", docId).delete();
     * }
     * 
     * @Test(expected =
     * com.highroads.commons.api.exceptions.HrConflictException.class)
     * public void shouldRejectDocCreateDuplicateObjectId() throws
     * IllegalStateException, IOException {
     * Map<String, Object> fragment = new HashMap<>();
     * fragment.put("key1", "value1");
     * fragment.put("key2", "value2");
     * fragment.put("objectId", uri);
     * 
     * document = MLJSONDocumentImpl.getInstance("upmc.com", adminUsername,
     * "plans", fragment);
     * uri = document.create();
     * }
     * 
     * @Test
     * public void shouldCreateBinaryDocument() throws IllegalStateException,
     * IOException {
     * 
     * objectId = contentService.uploadContent(userToken, "documents", metadata,
     * file);
     * assertNotNull(objectId);
     * String tokenBase64String = TokenHelper.getTokenBase64String(userToken);
     * ResponseEntity<InputStreamResource> objectContent =
     * contentService.getObjectContent(tokenBase64String, objectId,
     * "documents");
     * InputStreamResource body = objectContent.getBody();
     * byte[] buffer = new byte[256];
     * body.getInputStream().read(buffer, 0, 255);
     * assertEquals("This is a test file", new String(buffer).trim());
     * }
     * 
     * @Test
     * public void testPatchSameNameBinary() throws IllegalStateException,
     * IOException {
     * 
     * Map<String, Object> localmetadata = new HashMap<>();
     * localmetadata.put("name", "binaryDocumentName");
     * // required fields for document:
     * localmetadata.put("isUnit", false);
     * localmetadata.put("effectiveDate", DATEFORMAT.format(new Date()));
     * localmetadata.put("numSources", 1);
     * 
     * binObjectId = contentService.uploadContent(userToken, "documents",
     * localmetadata, file);
     * assertNotNull(binObjectId);
     * 
     * localmetadata.put("isUnit", true);
     * localmetadata.put("numSources", 2);
     * 
     * contentService.updateContent(userToken, "documents", binObjectId,
     * localmetadata);
     * Map<String, Object> docMetadata = contentService.getObjectById(userToken,
     * binObjectId, "document", null, null);
     * 
     * assertEquals(docMetadata.get("name").toString(), "binaryDocumentName");
     * assertEquals(docMetadata.get("isUnit"), true);
     * assertEquals(docMetadata.get("numSources"), 2);
     * }
     * 
     * @Test(expected =
     * com.highroads.commons.api.exceptions.HrConflictException.class)
     * public void testCreateSameNameBinary() throws IllegalStateException,
     * IOException {
     * 
     * Map<String, Object> localmetadata = new HashMap<>();
     * localmetadata.put("name", "binaryDocumentName2");
     * // required fields for document:
     * localmetadata.put("isUnit", false);
     * localmetadata.put("effectiveDate", DATEFORMAT.format(new Date()));
     * localmetadata.put("numSources", 1);
     * 
     * binObjectId = contentService.uploadContent(userToken, "documents",
     * localmetadata, file);
     * assertNotNull(binObjectId);
     * 
     * // now try to create same doc again
     * contentService.uploadContent(userToken, "documents", localmetadata,
     * file);
     * }
     * 
     * @Test
     * public void testPatchSameNameJson() throws IllegalStateException,
     * IOException {
     * 
     * Map<String, Object> localmetadata = new HashMap<>();
     * localmetadata.put("name", "testJsonDocumentName");
     * // required fields for plan:
     * localmetadata.put("grandfatheredPlan", false);
     * localmetadata.put("planType", "Standard");
     * localmetadata.put("effectiveDate", DATEFORMAT.format(new Date()));
     * localmetadata.put("planDueDate", DATEFORMAT.format(new Date()));
     * 
     * jsonObjectId = contentService.createContent(userToken, "plans",
     * localmetadata);
     * assertNotNull(jsonObjectId);
     * 
     * localmetadata.put("grandfatheredPlan", true);
     * localmetadata.put("planType", "Custom");
     * 
     * contentService.updateContent(userToken, "plans", jsonObjectId,
     * localmetadata);
     * Map<String, Object> docMetadata = contentService.getObjectById(userToken,
     * jsonObjectId, "plan", null, null);
     * 
     * assertEquals(docMetadata.get("name").toString(), "testJsonDocumentName");
     * assertEquals(docMetadata.get("grandfatheredPlan"), true);
     * assertEquals(docMetadata.get("planType").toString(), "Custom");
     * }
     * 
     * @Test(expected =
     * com.highroads.commons.api.exceptions.HrConflictException.class)
     * public void testCreateSameNameJson() throws IllegalStateException,
     * IOException {
     * 
     * Map<String, Object> localmetadata = new HashMap<>();
     * localmetadata.put("name", "testJsonDocumentName2");
     * // required fields for plan:
     * localmetadata.put("grandfatheredPlan", false);
     * localmetadata.put("planType", "Standard");
     * localmetadata.put("effectiveDate", DATEFORMAT.format(new Date()));
     * localmetadata.put("planDueDate", DATEFORMAT.format(new Date()));
     * 
     * jsonObjectId = contentService.createContent(userToken, "plans",
     * localmetadata);
     * assertNotNull(jsonObjectId);
     * 
     * // now try to create same doc again
     * contentService.createContent(userToken, "plans", localmetadata);
     * }
     * 
     * @Test
     * public void shouldGetLogo() throws FileNotFoundException, IOException {
     * logoId = contentService.uploadContent(userToken, "logos", metadata,
     * logo);
     * assertNotNull(logoId);
     * 
     * final Map<String, Object> retrieveLogo =
     * contentService.retrieveLogo(userToken);
     * String image = retrieveLogo.get("image").toString();
     * 
     * final File sampelFile = new File("src/test/data/sample.jpg");
     * byte[] byteArray = IOUtils.toByteArray(new FileInputStream(sampelFile));
     * final String imgString = Base64.getEncoder().encodeToString(byteArray);
     * String result = "data:" + "image/jpeg" + ";base64," + imgString;
     * 
     * assertEquals(result, image);
     * }
     * 
     * @Test
     * public void shouldCreateAssociation() {
     * 
     * contentService.createAssociation(userToken, "plan", uri,
     * "MyAssociation1", Arrays.asList(new String[] { "3", "5" }));
     * final Map<String, Object> object =
     * contentService.getObjectById(userToken, uri, "plan", -1, null);
     * assertEquals(uri, object.get("objectId"));
     * final Map<String, Object> associations = (Map<String, Object>)
     * object.get("associations");
     * final List<Map<String, Object>> myAssociation1 = (List<Map<String,
     * Object>>) associations.get("MyAssociation1");
     * assertEquals("3", myAssociation1.get(0).get("objectId"));
     * assertEquals("5", myAssociation1.get(1).get("objectId"));
     * 
     * contentService.createAssociation(userToken, "plan", uri,
     * "MyAssociation1", Arrays.asList(new String[] { "2" }));
     * final Map<String, Object> object1 =
     * contentService.getObjectById(userToken, uri, "plan", -1, null);
     * assertEquals(uri, object1.get("objectId"));
     * final Map<String, Object> associations1 = (Map<String, Object>)
     * object1.get("associations");
     * final List<Map<String, Object>> myAssociation11 = (List<Map<String,
     * Object>>) associations1.get("MyAssociation1");
     * assertEquals("3", myAssociation11.get(0).get("objectId"));
     * assertEquals("5", myAssociation11.get(1).get("objectId"));
     * assertEquals("2", myAssociation11.get(2).get("objectId"));
     * assertEquals(3, myAssociation11.size());
     * }
     * 
     * @Test
     * public void shouldDeleteAssociation() {
     * 
     * contentService.createAssociation(userToken, "plan", uri, "toDelete",
     * Arrays.asList(new String[] { "10", "11", "12" }));
     * Map<String, Object> object = contentService.getObjectById(userToken, uri,
     * "plan", -1, null);
     * assertEquals(uri, object.get("objectId"));
     * Map<String, Object> associations = (Map<String, Object>)
     * object.get("associations");
     * List<Map<String, Object>> myAssociation1 = (List<Map<String, Object>>)
     * associations.get("toDelete");
     * assertEquals("10", myAssociation1.get(0).get("objectId"));
     * assertEquals("11", myAssociation1.get(1).get("objectId"));
     * assertEquals("12", myAssociation1.get(2).get("objectId"));
     * 
     * contentService.deleteAssociation(userToken, "plan", uri, "toDelete",
     * Arrays.asList(new String[] { "11", "10" }));
     * object = contentService.getObjectById(userToken, uri, "plan", -1, null);
     * assertEquals(uri, object.get("objectId"));
     * associations = (Map<String, Object>) object.get("associations");
     * myAssociation1 = (List<Map<String, Object>>)
     * associations.get("toDelete");
     * assertEquals("12", myAssociation1.get(0).get("objectId"));
     * 
     * }
     * 
     * @Test
     * public void shouldSubmitBatch() {
     * 
     * List<String> batchIds = new ArrayList<>();
     * batchIds.add(draftBatchURI);
     * Map<String, String> submitBatch = batchService.submitBatches(userToken,
     * TokenHelper.getTokenBase64String(userToken), batchIds);
     * 
     * ArgumentCaptor<String> arg1 = ArgumentCaptor.forClass(String.class);
     * ArgumentCaptor<String> arg2 = ArgumentCaptor.forClass(String.class);
     * 
     * verify(queueService, atLeast(1)).enqueue(arg1.capture(), arg2.capture());
     * verify(queueService, atLeast(1)).setValue(arg1.capture(),
     * arg2.capture());
     * 
     * assertEquals(1, submitBatch.size());
     * 
     * Entry<String, String> entry = submitBatch.entrySet().iterator().next();
     * 
     * assertEquals("Submitted", entry.getValue());
     * assertEquals(draftBatchURI, entry.getKey());
     * 
     * MLJSONDocumentImpl instance = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "batch", draftBatchURI);
     * Map<String, Object> read = instance.read();
     * String batchStatus = (String) read.get("batchStatus");
     * assertEquals("Submitted", batchStatus);
     * String batchSubmittedBy = (String) read.get("batchSubmittedBy");
     * assertEquals(adminUsername, batchSubmittedBy);
     * 
     * }
     * 
     * @Test
     * public void shouldCancelBatch() {
     * 
     * List<String> batchIds = new ArrayList<>();
     * batchIds.add(submittedBatchURI);
     * Map<String, String> submitBatch = batchService.submitBatches(userToken,
     * TokenHelper.getTokenBase64String(userToken), batchIds);
     * List<String> batchKeys = new ArrayList<>();
     * 
     * batchKeys.add(submitBatch.keySet().stream().findFirst().get());
     * Map<String, String> cancelBatch = batchService.cancelBatch(userToken,
     * batchKeys);
     * 
     * assertEquals(1, cancelBatch.size());
     * assertEquals("Cancelling",
     * cancelBatch.values().stream().findFirst().get());
     * assertEquals(submittedBatchURI,
     * cancelBatch.keySet().stream().findFirst().get());
     * 
     * MLJSONDocumentImpl instance = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "batch", submittedBatchURI);
     * Map<String, Object> read = instance.read();
     * String batchStatus = (String) read.get("batchStatus");
     * assertEquals("Cancelling", batchStatus);
     * String batchSubmittedBy = (String) read.get("batchSubmittedBy");
     * assertEquals("admin@p2a.com", batchSubmittedBy);
     * 
     * }
     * 
     * @Test
     * public void shouldResubmitAllBatch() {
     * 
     * List<String> batchIds = new ArrayList<>();
     * batchIds.add(failedBatchURI);
     * 
     * Map<String, String> resubmitBatch = batchService.resubmitBatch(userToken,
     * TokenHelper.getTokenBase64String(userToken), batchIds,
     * BatchAction.RESUBMIT_ALL);
     * 
     * assertEquals(1, resubmitBatch.size());
     * assertEquals("Submitted",
     * resubmitBatch.values().stream().findFirst().get());
     * assertEquals(failedBatchURI,
     * resubmitBatch.keySet().stream().findFirst().get());
     * 
     * MLJSONDocumentImpl instance = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "batch", failedBatchURI);
     * Map<String, Object> read = instance.read();
     * String batchStatus = (String) read.get("batchStatus");
     * assertEquals("Submitted", batchStatus);
     * String batchSubmittedBy = (String) read.get("batchSubmittedBy");
     * assertEquals(adminUsername, batchSubmittedBy);
     * }
     * 
     * @Test
     * public void shouldResubmitAllCancelledBatch() {
     * 
     * List<String> batchIds = new ArrayList<>();
     * batchIds.add(cancelledBatchURI);
     * 
     * Map<String, String> resubmitBatch = batchService.resubmitBatch(userToken,
     * TokenHelper.getTokenBase64String(userToken), batchIds,
     * BatchAction.CANCEL_FAILED);
     * 
     * assertEquals(1, resubmitBatch.size());
     * assertEquals("Submitted",
     * resubmitBatch.values().stream().findFirst().get());
     * assertEquals(cancelledBatchURI,
     * resubmitBatch.keySet().stream().findFirst().get());
     * 
     * MLJSONDocumentImpl instance = MLJSONDocumentImpl.getInstance("upmc.com",
     * adminUsername, "batch", cancelledBatchURI);
     * Map<String, Object> read = instance.read();
     * String batchStatus = (String) read.get("batchStatus");
     * assertEquals("Submitted", batchStatus);
     * String batchSubmittedBy = (String) read.get("batchSubmittedBy");
     * assertEquals(adminUsername, batchSubmittedBy);
     * }
     * 
     * @Test
     * public void shouldFindObjects() {
     * 
     * Map<String, Object> objectByIds =
     * contentService.getObjectByIds(userToken, Arrays.asList(new String[] { uri
     * }), Arrays.asList(new String[] { "key1" }));
     * Map<String, Object> object = (Map<String, Object>) objectByIds.get(uri);
     * assertEquals("value1", object.get("key1"));
     * assertEquals(uri, object.get("objectId"));
     * }
     * 
     * @Test(expected =
     * com.highroads.commons.api.exceptions.HrObjectNotFoundException.class)
     * public void shouldDeleteDocument() {
     * 
     * Map<String, Object> propreties = new HashMap<>();
     * propreties.put("KEY_TEST", "VALUE_TEST");
     * String objectId = contentService.createContent(userToken, "plans",
     * propreties);
     * propreties = contentService.getObjectById(userToken, objectId, "plans",
     * null, null);
     * assertEquals("VALUE_TEST", propreties.get("KEY_TEST"));
     * contentService.deleteObject(userToken, "plans", objectId);
     * propreties = contentService.getObjectById(userToken, objectId, "plans",
     * null, null);
     * }
     * 
     * @AfterClass
     * public void tearDown() {
     * 
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/plans/" + uri);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/documents/" + objectId);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/logos/" + logoId);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batch/" + draftBatchURI);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batch/" + failedBatchURI);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batch/" + submittedBatchURI);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batch/" + cancelledBatchURI);
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/plans/" + jsonObjectId);
     * marklogicDAO.getDatabaseClient(adminUsername).newBinaryDocumentManager().
     * delete("/upmc.com/documents/" + binObjectId);
     * 
     * for (String batchJob : draftBatchJobIds) {
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batchJobs/" + batchJob);
     * }
     * for (String batchJob : cancelledBatchJobIds) {
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batchJobs/" + batchJob);
     * }
     * for (String batchJob : submittedBatchJobIds) {
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batchJobs/" + batchJob);
     * }
     * for (String batchJob : failedBatchJobIds) {
     * marklogicDAO.getDatabaseClient(adminUsername).newJSONDocumentManager().
     * delete("/upmc.com/batchJobs/" + batchJob);
     * }
     * }
     */
}
