package com.highroads.cm.impl;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
@Ignore("Revisit")
public class TestContentServicePatchOperations {
    /*
     * private static MarklogicConnectionProperties
     * marklogicConnectionProperties;
     * 
     * private ContentService contentService;
     * 
     * private static final SimpleDateFormat dateFormatter = new
     * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
     * 
     * private static UserToken userToken;
     * 
     * private static String uri;
     * 
     * @Mock
     * private JsonValidator jsonValidator;
     * 
     * @Mock
     * MarklogicDAO marklogicDAO;
     * 
     * @Mock
     * private ModelSchemaService modelSchemaService;
     * 
     * @Mock
     * private ObjectNode objectNode;
     * 
     * @Mock
     * private PatchUtil patchUtil;
     * 
     * @Mock
     * private CMHelper cmHelper;
     * 
     * @BeforeClass
     * public static void setUp_() {
     * marklogicConnectionProperties = new MarklogicConnectionProperties();
     * marklogicConnectionProperties.setHost("localhost");
     * marklogicConnectionProperties.setPort(13001);
     * marklogicConnectionProperties.setUser("admin@upmc.com");
     * new MarklogicDAO(marklogicConnectionProperties);
     * 
     * userToken = new UserToken();
     * userToken.setOrganization("upmc.com");
     * userToken.setUsername("admin@upmc.com");
     * 
     * Map<String, Object> fragment = new HashMap<>();
     * fragment.put("key1", "value1");
     * fragment.put("key2", "value2");
     * fragment.put("objectId", "100A");
     * 
     * uri = MLJSONDocumentImpl.getInstance("upmc.com", "admin@upmc.com",
     * "plans", fragment).create();
     * }
     * 
     * @Before
     * public void setUp() {
     * when(modelSchemaService.getSchema(any(UserToken.class))).thenReturn(
     * objectNode);
     * contentService = new MarklogicContentServiceImpl(jsonValidator,
     * modelSchemaService, null, marklogicDAO, patchUtil, cmHelper);
     * }
     * 
     * @Test
     * public void shouldAddMetadata() {
     * 
     * Date date = new Date();
     * 
     * Map<String, Object> object = new LinkedHashMap<>();
     * object.put("isLocked", "true");
     * object.put("lockedBy", "junit");
     * object.put("lockedAt", dateFormatter.format(date));
     * 
     * contentService.updateContent(userToken, "plans", uri, object);
     * 
     * Map<String, Object> read =
     * MLJSONDocumentImpl.getInstance(userToken.getOrganization(),
     * userToken.getUsername(), "plans", uri).read();
     * assertEquals("true", read.get("isLocked"));
     * assertEquals("junit", read.get("lockedBy"));
     * assertEquals(dateFormatter.format(date), read.get("lockedAt"));
     * }
     * 
     * @Test
     * public void shouldRemoveMetadata() {
     * 
     * Map<String, Object> object = new LinkedHashMap<>();
     * object.put("isLocked", "false");
     * 
     * contentService.updateContent(userToken, "plans", uri, object);
     * Map<String, Object> read = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", uri).read();
     * assertNull(read.get("isLocked"));
     * assertNull(read.get("lockedBy"));
     * assertNull(read.get("lockedAt"));
     * }
     * 
     * @Test
     * public void shouldAddNode() {
     * Map<String, Object> object = new LinkedHashMap<>();
     * object.put("a", "A");
     * 
     * Map<String, Object> operation = new LinkedHashMap<>();
     * operation.put("op", "add");
     * operation.put("path", "/b");
     * operation.put("value", "B");
     * List<Map<String, Object>> operations = new ArrayList<>();
     * operations.add(operation);
     * String planId = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", object).create();
     * contentService.updateContent(userToken, "plans", planId, operations);
     * 
     * Map<String, Object> read = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", planId).read();
     * assertEquals("B", read.get("b"));
     * assertEquals("A", read.get("a"));
     * MLJSONDocumentImpl.getInstance(userToken.getOrganization(),
     * userToken.getUsername(), "plans", planId).delete();
     * }
     * 
     * @Test
     * public void shouldReplaceValue() {
     * Map<String, Object> object = new LinkedHashMap<>();
     * object.put("a", "A");
     * 
     * Map<String, Object> operation = new LinkedHashMap<>();
     * operation.put("op", "add");
     * operation.put("path", "/a");
     * operation.put("value", "B");
     * List<Map<String, Object>> operations = new ArrayList<>();
     * operations.add(operation);
     * String planId = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", object).create();
     * contentService.updateContent(userToken, "plans", planId, operations);
     * 
     * Map<String, Object> read = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", planId).read();
     * assertEquals("B", read.get("a"));
     * MLJSONDocumentImpl.getInstance(userToken.getOrganization(),
     * userToken.getUsername(), "plans", planId).delete();
     * }
     * 
     * @Test
     * public void shouldAddNodeAndReplaceValue() {
     * Map<String, Object> object = new LinkedHashMap<>();
     * object.put("a", "A");
     * 
     * Map<String, Object> operation1 = new LinkedHashMap<>();
     * operation1.put("op", "add");
     * operation1.put("path", "/a");
     * operation1.put("value", "AA");
     * 
     * Map<String, Object> operation2 = new LinkedHashMap<>();
     * operation2.put("op", "add");
     * operation2.put("path", "/b");
     * operation2.put("value", "B");
     * 
     * List<Map<String, Object>> operations = new ArrayList<>();
     * operations.add(operation1);
     * operations.add(operation2);
     * 
     * String planId = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", object).create();
     * contentService.updateContent(userToken, "plans", planId, operations);
     * 
     * Map<String, Object> read = MLJSONDocumentImpl.getInstance("upmc.com",
     * "admin@upmc.com", "plans", planId).read();
     * assertEquals("AA", read.get("a"));
     * assertEquals("B", read.get("b"));
     * MLJSONDocumentImpl.getInstance(userToken.getOrganization(),
     * userToken.getUsername(), "plans", planId).delete();
     * }
     * 
     * @AfterClass
     * public static void deleteDocuments() {
     * MLJSONDocumentImpl.getInstance(userToken.getOrganization(),
     * userToken.getUsername(), "plans", uri).delete();
     * }
     */
}
