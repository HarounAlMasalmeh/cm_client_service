package com.highroads.cm;

/**
 * Test class for bulk API endpoint.
 * @author atingley
 * @version
 * @see
 * @since
 * List of changes since initial version is released
 * Created, atingley, 17-July-2015
 *
 * Processes bulk API requests of the form:
 * {
 *   [
 *     { "_type":"typeName", "resource": <resource-definition> },
 *     { "_type":"typeName", "resource": <resource-definition> },
 *     ...
 *   ]
 * }
 */

import static com.highroads.cm.DomainObjectFactory.createProduct;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Product;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
//@Ignore("this bulk API is superseded by the Batch framework")
@Ignore("Revisit")
public class BulkApiTests {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

    private String bulkApiUri;
    private String tenantsUri;
    private String productsUri;

    private String tenantFolderName;
    private String tenantObjectId;

    @Autowired
    private WebApplicationContext wac;

    @PostConstruct
    public void init() {
        logger.debug("init() called");

        bulkApiUri = "/bulk";
        tenantsUri = "/organizations";
        productsUri = "/products";
    }

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(testTenant).
            when().
                post(tenantsUri).
            andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Test
//    @Ignore
    public void testBulkCreate() {
        logger.info("in Bulk create");

        String[] prodClasses = new String[] {"Medical"};
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00-05:00";
        String endDate = "2015-12-31T00:00:00-05:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00-05:00";
        String[] pFamily = { "HMO Local" };
        String[] mktSegments = { "Mid Group" };
        String[] memberFundings = { "HSA", "HIA" };

        String testProductNameBase = "bulkProductTest_";
        Set<String> expectedNames = new HashSet<String>();
        Set<String> actualNames = new HashSet<String>();

        Product testProd;
        // ObjectContainer container;
        Map<String, Object> objectMap;
        ArrayList<Map<String, Object>> bulkObjects = new ArrayList<Map<String, Object>>();
        // let's create 5 products
        for (int i = 0; i < 5; i++) {
            String testProductName = testProductNameBase + i;
            expectedNames.add(testProductName);
            testProd = createProduct(testProductNameBase + i, prodClasses, prodType, startDate, endDate, pStatus, dueDate, pFamily, mktSegments, memberFundings);
            objectMap = new HashMap<String, Object>();
            objectMap.put("_type", "product");
            objectMap.put("resource", testProd);
            bulkObjects.add(objectMap);
        }

        long clockStarted = System.nanoTime();

        // @formatter:off
        @SuppressWarnings("unchecked")
        Map<String,Object> result =
            given().
                log().all().
                contentType("application/json").
                body(bulkObjects).
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                post(bulkApiUri).
            andReturn().as(HashMap.class);
        // @formatter:on

        long durationInMillis = TimeUnit.MILLISECONDS.convert(System.nanoTime() - clockStarted, TimeUnit.NANOSECONDS);

        logger.info("returned from POST to " + bulkApiUri + ", duration (ms): " + durationInMillis);

        assertTrue(result.get("status").toString().equals("success"));
        @SuppressWarnings("unchecked")
        ArrayList<Map<String,String>> resultList = (ArrayList<Map<String, String>>) result.get("responses");
        assertTrue("bulk result is incorrect size", resultList.size() == 5);

        resultList.forEach(response -> {
            String status = response.get("status").toString();
            logger.debug("returned status: " + status);
            if (response.containsKey("message")) {
                String message = response.get("message");
                logger.debug("message: " + message);
            } else {
                // have objectId
                String objectId = response.get("id");
                logger.debug("objectId: " + objectId);
                Map<String, Object> product = retrieveProduct(objectId, tenantFolderName);
                String productName = product.get("name").toString();
                actualNames.add(productName);
                logger.debug("retrieved product name: " + productName);
            }
        });

        assertTrue("actual product name set does not equal expected product name set", actualNames.equals(expectedNames));
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> retrieveProduct(String objId, String tenantFolderName) {
        // @formatter:off
        return given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(productsUri + "/" + objId + "?properties=name").
        andReturn().body().as(Map.class);
        // @formatter:on

    }

}
