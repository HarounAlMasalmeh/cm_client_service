package com.highroads.cm;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.highroads.commons.InstanceTestClassListener;
import com.highroads.commons.SpringInstanceTestClassRunner;
import com.highroads.commons.TokenHelper;
import com.highroads.commons.config.MarklogicConnectionProperties;
import com.highroads.commons.connectionpool.MarklogicSessionManagerPool;
import com.highroads.commons.dao.MarklogicDAO;
import com.marklogic.client.DatabaseClient;

/**
 * This test requires CM to be running.
 * TODO: Fix this! Write a (socket) server class that this test can instantiate
 * and examine, to test the pooling mechanism, NOT authorization of users in
 * MarkLogic!
 * TODO: move this to commons
 *
 * @author atingley
 *
 */
@RunWith(SpringInstanceTestClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class })
@WebAppConfiguration
public class MarklogicConnectionPoolTests implements InstanceTestClassListener {

    private static final Logger logger = LoggerFactory.getLogger(MarklogicConnectionPoolTests.class);

    @Autowired
    MarklogicDAO marklogicDAO;

    @Autowired
    MarklogicConnectionProperties marklogicConnectionProperties;

    private AtomicInteger count = new AtomicInteger(0);
    private static final String usernameBase = "user";
    private static final String nonUsernameBase = "nonuser";

    private static final int TEST_USERS_SMALL = 10;
    private static final int TEST_USERS_LARGE = 50;
    private static String usersUri;

    private class UserThread extends Thread {

        private String orgId;
        private String username;
        private boolean releaseSession;
        private boolean connectionPoolExhausted = false;

        private UserThread(String organizationId, String username) {
            this.orgId = organizationId;
            this.username = username;
            this.releaseSession = true; // previous default behavior
        }

        private UserThread(String organizationId, String username, boolean releaseSession) {
            this(organizationId, username);
            this.releaseSession = releaseSession;
        }

        @Override
        public void run() {
            DatabaseClient client = null;
            try {
                logger.debug("calling getSession({}, {})", orgId, username);
                client = marklogicDAO.getDatabaseClient(username, orgId);
                count.getAndIncrement();
                Thread.sleep(500);
            } catch (NoSuchElementException nse) {
                logger.warn("pool exhausted: ", nse.getMessage());
                this.connectionPoolExhausted = true;
                throw nse;
            } catch (Exception e) {
                e.printStackTrace(System.err);
            } finally {
                if (client != null && releaseSession) {
                    logger.debug("calling releaseConnection({}, {}, {})", orgId, username, client);
                    marklogicDAO.releaseConnection(username);
                } else {
                    logger.debug("session is null or releaseSession is false; no need to releaseConnection({}, {}, {})", orgId, username, "<null>");
                }
            }
        }
    }

    @PostConstruct
    public void init() {
        logger.debug("init: maxOrgPoolConnections: {}", marklogicConnectionProperties.getMaxConnectionsPerOrganization());
    }

    @Before
    public void setUp() {
        logger.debug("initializing new connection pool");
        marklogicDAO.resetSessionManagerPool();
    }

    @Override
    public void beforeClassSetUp() {
        //        logger.debug("beforeClassSetup: syncing test users in org highroads.com, usersUri: " + usersUri);
        //
        //        // ensure org users exist or access is denied
        //        syncTestUsers("highroads.com", TEST_USERS_LARGE);
    }

    @Override
    public void afterClassTearDown() {
        // no-op
    }

    // test reuse of single connection
    @Test
    public void testBasicPoolingWithSingleUserSingleBorrow() {
        logger.debug("\ntestBasicPoolingWithSingleUserSingleBorrow\n");

        try {
            int limit = 10; // can be any number, only one connection is expected
            String organizationId = "highroads.com";
            String userId = "admin@highroads.com";
            count.set(0);

            ExecutorService es = new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(limit));

            boolean releaseSession = false; // don't return connection to pool - should see only one borrowed connection for this userId
            for (int i = 0; i < limit; i++) {
                UserThread ut = new UserThread(organizationId, userId, releaseSession);
                es.submit(ut);
            }

            es.shutdown();

            try {
                es.awaitTermination(3, TimeUnit.MINUTES);

            } catch (InterruptedException ignored) {
            }

            MarklogicSessionManagerPool pool = marklogicDAO.getSessionManagerPool();
            logger.debug("Pool Stats:\n Created:[" + pool.getCreatedCount() + "], Borrowed:[" + pool.getBorrowedCount() + "]");
            Assert.assertEquals(limit, count.get());
            Assert.assertEquals(1, pool.getBorrowedCount());
        } catch (Exception ex) {
            fail("Exception:" + ex);
        }
    }

    // test parallel connection requests, single user - should allocate only one connection
    @Test
    public void testBasicPoolingWithSingleUserMultipleBorrowsParallel() {
        logger.debug("\ntestBasicPoolingWithSingleUserMultipleBorrowsParallel\n");

        try {
            int limit = marklogicConnectionProperties.getMaxConnectionsPerOrganization();
            String organizationId = "sutter.com";
            String userId = "admin@sutter.com";
            count.set(0);

            ExecutorService es = new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(limit));

            boolean releaseSession = true; // return connection to pool - should see 'limit' borrowed connections for this userId
            for (int i = 0; i < limit; i++) {
                UserThread ut = new UserThread(organizationId, userId, releaseSession);
                es.submit(ut);
            }

            es.shutdown();

            try {
                es.awaitTermination(3, TimeUnit.MINUTES);

            } catch (InterruptedException ignored) {
            }

            MarklogicSessionManagerPool pool = marklogicDAO.getSessionManagerPool();
            logger.debug("Pool Stats:\n Created:[" + pool.getCreatedCount() + "], Borrowed:[" + pool.getBorrowedCount() + "]");
            Assert.assertEquals(limit, count.get());
            Assert.assertEquals(1, pool.getBorrowedCount());
        } catch (Exception ex) {
            fail("Exception:" + ex);
        }
    }

    // test use of new connection per request - serial
    @Test
    public void testBasicPoolingWithSingleUserMultipleBorrowsSerial() {
        logger.debug("\ntestBasicPoolingWithSingleUserMultipleBorrowsSerial\n");

        try {
            int limit = marklogicConnectionProperties.getMaxConnectionsPerOrganization();
            String organizationId = "upmc.com";
            String userId = "admin@upmc.com";
            count.set(0);

            boolean releaseSession = true; // return connection to pool - should see 'limit' borrowed connections for this userId
            for (int i = 0; i < limit; i++) {

                DatabaseClient client = null;
                try {
                    logger.debug("calling getSession({}, {})", organizationId, userId);
                    client = marklogicDAO.getDatabaseClient(userId, organizationId);
                    count.getAndIncrement();
                    Thread.sleep(500);
                } catch (NoSuchElementException nse) {
                    logger.warn("pool exhausted: ", nse.getMessage());
                    // keep going // throw nse;
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                } finally {
                    if (client != null && releaseSession) {
                        logger.debug("calling releaseConnection({}, {}, {})", organizationId, userId, client);
                        marklogicDAO.releaseConnection(userId);
                    } else {
                        logger.debug("session is null or releaseSession is false; no need to releaseConnection({}, {}, {})", organizationId, userId, "<null>");
                    }
                }
            }

            MarklogicSessionManagerPool pool = marklogicDAO.getSessionManagerPool();
            logger.debug("Pool Stats:\n Created:["
                    + pool.getCreatedCount() + "], Borrowed:["
                    + pool.getBorrowedCount() + "], Returned:["
                    + pool.getReturnedCount() + "]");
            Assert.assertEquals(limit, count.get());
            Assert.assertEquals(limit, pool.getBorrowedCount());
            Assert.assertEquals(limit, pool.getReturnedCount());

        } catch (Exception ex) {
            fail("Exception:" + ex);
        }
    }

    // test overlimit
    @SuppressWarnings("unchecked")
    @Test
    public void testBasicPoolingOverLimit() {
        logger.debug("\ntestBasicPoolingOverLimit\n");

        try {
            int limit = marklogicConnectionProperties.getMaxConnectionsPerOrganization() + 1; // one over limit;
            String organizationId = "fieldinsurance.com";
            String username = "admin";
            count.set(0);

            ExecutorService es = new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(limit));
            ArrayList<Future<UserThread>> tasks = new ArrayList<>();
            for (int i = 0; i < limit; i++) {
                UserThread ut = new UserThread(organizationId, username + i + "@" + organizationId);
                tasks.add((Future<UserThread>) es.submit(ut));
            }

            es.shutdown();

            try {
                es.awaitTermination(3, TimeUnit.MINUTES);
            } catch (InterruptedException ignored) {
            }

            MarklogicSessionManagerPool pool = marklogicDAO.getSessionManagerPool();
            logger.debug("Pool Stats:\n Created:[" + pool.getCreatedCount() + "], Borrowed:[" + pool.getBorrowedCount() + "]");
            int failed = 0;
            for (int i = 0; i < limit; i++) {
                try {
                    Future<UserThread> f = tasks.get(i);
                    UserThread t = f.get();
                } catch (ExecutionException ex) {
                    // this cause for this exception is thrown by the borrowObject() method in the connection pool
                    // (kinda cool how it's captured then re-thrown upon f.get() ;-)
                    // this connection-borrow attempt failed
                    if (ex.getCause() instanceof NoSuchElementException) {
                        failed++;
                    } else {
                        fail("unexpected exception: " + ex);
                    }

                }
            }
            Assert.assertEquals(1, failed);
            Assert.assertEquals(limit - 1, count.get());
            Assert.assertEquals(limit - 1, pool.getBorrowedCount());

        } catch (Exception ex) {
            fail("unexpected exception: " + ex);
        }
    }

    // test boundaries for multiple tenants
    @SuppressWarnings("unchecked")
    @Test
    public void testBasicPoolingMultiTenants() {
        logger.debug("\ntestBasicPoolingMultiTenants\n");
        int limit = marklogicConnectionProperties.getMaxConnectionsPerOrganization();
        String[] organizationIds = { "highroads.com", "upmc.com", "fieldinsurance.com", "p2ademo.com", "sutter.com" };

        // test props define 5 max connections per tenant, 25 total connections max
        // occupy pool to max, then try to borrow one more connection, should fail
        count.set(0);
        boolean releaseConnection = false;
        for (String orgId : organizationIds) {
            runPoolTests(limit, orgId, "admin", releaseConnection);
        }
        MarklogicSessionManagerPool pool = marklogicDAO.getSessionManagerPool();
        Assert.assertEquals(organizationIds.length * limit, count.get());
        Assert.assertEquals(organizationIds.length * limit, pool.getBorrowedCount());

        // now try to get one more
        logger.debug("calling getDatabaseClient({}, {})", "highroads.com", "testuser@highroads.com");
        try {
            DatabaseClient client = marklogicDAO.getDatabaseClient("testuser@highroads.com", "highroads.com");
            count.getAndIncrement();
        } catch (NoSuchElementException nse) {
            // expected
        }
        Assert.assertEquals(organizationIds.length * limit, count.get());
        Assert.assertEquals(organizationIds.length * limit, pool.getBorrowedCount());
    }

    @Ignore
    @Test
    public void testSmallUserGroupConcurrentSessionsWithDifferentUser() {
        int limit = TEST_USERS_SMALL;
        String organizationId = "highroads.com";
        count.set(0);

        MarklogicSessionManagerPool pool = runPoolTests(limit, organizationId, usernameBase); // known users
        Assert.assertEquals(limit, count.get());
        Assert.assertEquals(count.get(), pool.getBorrowedCount());
    }

    @Ignore
    @Test
    public void testLargeUserGroupConcurrentSessionsWithDifferentUser() {
        int limit = TEST_USERS_LARGE;
        String organizationId = "highroads.com";
        count.set(0);

        MarklogicSessionManagerPool pool = runPoolTests(limit, organizationId, usernameBase); // known users
        Assert.assertEquals(limit, pool.getBorrowedCount());
        Assert.assertEquals(limit, count.get());
        Assert.assertTrue(marklogicConnectionProperties.getMaxConnectionsPerOrganization() >= pool.getCreatedCount());
    }

    /**
     * Test that exceeds max org pool size with unknown users, then attempts
     * connection with known user.
     * Org's pool should NOT be exhausted, known user should be let through.
     */
    @Ignore
    @Test
    public void testMaxOrgPoolSizeConcurrentSessionsWithUnknownUsers() {
        int limit = marklogicConnectionProperties.getMaxConnectionsPerOrganization() + 1;
        String organizationId = "highroads.com";
        count.set(0);

        MarklogicSessionManagerPool pool = runPoolTests(limit, organizationId, nonUsernameBase); // unknown users
        Assert.assertEquals(0, count.get());
        Assert.assertEquals(limit, pool.getBorrowedCount());
        Assert.assertEquals(limit, pool.getReturnedCount());

        // now see if we can get connections with real users
        count.set(0);
        pool = runPoolTests(limit, organizationId, usernameBase); // known users
        Assert.assertEquals(limit, count.get());
        Assert.assertEquals(2 * limit, pool.getBorrowedCount());
        Assert.assertEquals(2 * limit, pool.getReturnedCount());
    }

    private MarklogicSessionManagerPool runPoolTests(int limit, String organizationId, String testUsernameBase) {
        return runPoolTests(limit, organizationId, testUsernameBase, true);
    }

    private MarklogicSessionManagerPool runPoolTests(int limit, String organizationId, String testUsernameBase, boolean releaseConnection) {
        MarklogicSessionManagerPool pool = null;
        try {
            ExecutorService es = new ThreadPoolExecutor(limit, limit, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(limit));

            for (int i = 0; i < limit; i++) {
                UserThread ut = new UserThread(organizationId, testUsernameBase + i + "@" + organizationId, releaseConnection);
                es.submit(ut);
            }

            es.shutdown();

            try {
                es.awaitTermination(1, TimeUnit.MINUTES);

            } catch (InterruptedException ignored) {
            }

            pool = marklogicDAO.getSessionManagerPool();
            logger.debug("Pool Stats:\n Created:[" + pool.getCreatedCount()
                    + "], Borrowed:[" + pool.getBorrowedCount()
                    + "], Returned:[" + pool.getReturnedCount() + "]");
        } catch (Exception ex) {
            fail("Exception:" + ex);
        }
        return pool;
    }

    private static void syncTestUsers(String organizationId, int limit) {
        for (int i = 0; i < limit; i++) {
            syncUser(organizationId, usernameBase + i);
        }
    }

    private static void syncUser(String organizationId, String username) {
        // establish user in org within backend db
        HttpHeaders authHeaders = new HttpHeaders();
        String orgRootGroupName = orgShortGroupName(organizationId); // e.g. "highroads_com"
        authHeaders.add("Authorization",
                TokenHelper.getTokenBase64String(organizationId, username, new String[] { orgRootGroupName }, new String[] { "OrganizationAdmin" }));
        RestTemplate restTemplate = new RestTemplate();

        @SuppressWarnings({ "rawtypes", "unchecked" })
        HttpEntity syncRequest = new HttpEntity(authHeaders);
        ResponseEntity<Object> response = restTemplate.exchange(usersUri, HttpMethod.PUT, syncRequest, Object.class);
    }

    /**
     * Replace '.' with '_' in group names.
     *
     * @param groupName
     * @return
     */
    private static String orgShortGroupName(String orgId) {
        assert (orgId != null && orgId != "") : "groupName cannot be null";
        return orgId.replaceAll("\\.", "_"); // '.' disallowed in group names, repl with '_'
    }
}
