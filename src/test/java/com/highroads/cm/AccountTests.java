package com.highroads.cm;

import static com.highroads.cm.DomainObjectFactory.createAccount;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.Assert.assertTrue;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Account;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class AccountTests {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private MockMvc mockMvc;
    private String accountsUri;
    private String tenantFolderName;
    private String tenantObjectId;
    private String tenantsUri;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        accountsUri = "/accounts";
        tenantsUri = "/organizations";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;
        tenantFolderName = "tenant.test." + System.currentTimeMillis();
        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json").
            body(testTenant).
        when().
            post(tenantsUri).
        andReturn().as(HashMap.class);

        tenantObjectId = resultMap.get("objectId").toString();
        logger.info("Created tenant " + tenantFolderName + ", id: " + tenantObjectId + "for account test...");
    }

    @After
    public void tearDown() {
        // delete the test tenant
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);

        logger.info("Deleted test tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testAccountCrudOperations() {
        String accountName = "GE";
        String accountStatus = "Prospect";
        String marketSegment = "Large Group";
        String supportPhoneNumber = "888-123-4567";
        String supportWebsiteAddress = "http://support.ge.com";
        String token = TokenHelper.getTokenBase64String(tenantFolderName);

        logger.info("1. Testing creating account...");
        Account testAccount = createAccount(accountName, accountStatus, marketSegment, supportPhoneNumber, supportWebsiteAddress);
        String accountId =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, token).
                contentType("application/json").
                queryParam("tenantFolder", tenantFolderName).
                body(testAccount).
            when().
                post(accountsUri).
            andReturn().body().asString();
        logger.info("Account created with id {}", accountId);

        logger.info("2. Testing retriving account...");
        HashMap<String, Object> account =
            given().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, token).
            when().
                get(accountsUri + "/" + accountId).
            andReturn().body().as(HashMap.class);

        assertTrue(((String) account.get("accountName")).compareToIgnoreCase(accountName) == 0);
        assertTrue(((String) account.get("accountStatus")).compareToIgnoreCase(accountStatus) == 0);
        assertTrue(((String) account.get("marketSegment")).compareToIgnoreCase(marketSegment) == 0);
        assertTrue(((String) account.get("supportPhoneNumber")).compareToIgnoreCase(supportPhoneNumber) == 0);
        assertTrue(((String) account.get("supportWebsiteAddress")).compareToIgnoreCase(supportWebsiteAddress) == 0);

        logger.info("3. Testing updating account...");
        testAccount.setAccountStatus("Active");
        testAccount.setRenewalStartDate("2017-01-01T00:00:00+00:00");
        testAccount.setRenewalEndDate("2017-12-31T00:00:00+00:00");
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, token).
            contentType("application/json").
            body(testAccount).
        when().
            patch(accountsUri + "/" + accountId);

        logger.info("Retrieve and compare updated account with id {}", accountId);
        account =
            given().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, token).
            when().
                get(accountsUri + "/" + accountId).
            andReturn().body().as(HashMap.class);

        assertTrue(((String) account.get("accountName")).compareToIgnoreCase(accountName) == 0);
        assertTrue(((String) account.get("accountStatus")).compareToIgnoreCase(testAccount.getAccountStatus()) == 0);

        OffsetDateTime expectedRenewalStartDate = OffsetDateTime.parse(testAccount.getRenewalStartDate(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        assertTrue(expectedRenewalStartDate.isEqual(OffsetDateTime.parse((String) account.get("renewalStartDate"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"))));
        OffsetDateTime expectedRenewalEndDate = OffsetDateTime.parse(testAccount.getRenewalEndDate(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        assertTrue(expectedRenewalEndDate.isEqual(OffsetDateTime.parse((String) account.get("renewalEndDate"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"))));

        logger.info("4. Testing deleting account...");
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, token).
        when().
            delete(accountsUri + "/" + accountId).
        then().
            statusCode(HttpServletResponse.SC_NO_CONTENT);
    }
}
