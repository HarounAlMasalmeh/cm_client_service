package com.highroads.cm;

import static com.highroads.cm.DomainObjectFactory.createProduct;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Product;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.InstanceTestClassListener;
import com.highroads.commons.SpringInstanceTestClassRunner;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringInstanceTestClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class SetUnsetPropertyTests implements InstanceTestClassListener {
	private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

	private static String productsUri;
	private static String tenantsUri;

    private static String tenantFolderName;
    private static String tenantObjectId;

    private static String[] prodClasses = new String[] {"Medical"};
    private static String prodType = "HMO";
    private static String startDate = "2015-01-01T00:00:00-05:00";
    private static String endDate = "2015-12-31T00:00:00-05:00";
    private static String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
    private static String dueDate = "2016-01-01T00:00:00-05:00";

    @Autowired
    private WebApplicationContext wac;

    @Value("${content.repo.private.properties}")
    private String hiddenProperties;

    @PostConstruct
    public void init() {
        logger.debug("init() called");
    }

    // Test set and unset of single- and multi-valued date and non-date properties (text, int, boolean, date)
    // (we don't have any multi-date or multi-boolean properties at present)
    //
    // Also try to unset required effectiveDate prop
    //
    // Properties (object) - type:
    // Native properties:
    //      distributionChannels (commonContentBase) - multi-text
    //          <value>Public Exchange</value>
    //          <value>Private Exchange</value>
    //      providerTierLevel (commonContentBase) - single-text
    //      rangeValues (membercostshare) - multi-int
    //      numProviderTiers (commonContentBase) - single int
    //      isSmartTemplate (template) - single boolean (mandatory)
    //      pcpRequired (commonContentBase) - single boolean (optional)
    //      productDueDate (product) - single-date
    // Aspect-defined properties:
    //      productFamilies (product) - multi-text
    //      effectiveDate (commonContentBase) - single-date (mandatory)
    //      endDate (commonContentBase) - single-date (optional)


    // productFamilies is a multi-valued text property defined by an aspect
    // - set it, then unset it using empty array
    @SuppressWarnings("unchecked")
    @Test
    public void testSetUnsetEmptyArrayMultiTextAspectProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // verify productFamilies key is present, but with no value, since we have not set it
        // (it appears Aspect-defined properties always appear on object-retrieval for those aspects defined as mandatory,
        // whether a value has been set/unset or not)
        Map<String, Object> resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("productFamilies"));

        // add multi-value text property
        String patchJson = "{\"productFamilies\": [\"HMO Local\", \"HMO NE\"] }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify productFamilies value
        resultMap = getObject(productsUri, objId);

        ArrayList<String> prodFamilies = (ArrayList<String>) resultMap.get("productFamilies");
        String pf1 = prodFamilies.get(0);
        String pf2 = prodFamilies.get(1);
        assertTrue( (pf1.equals("HMO Local") && pf2.equals("HMO NE")) || (pf1.equals("HMO NE") && pf2.equals("HMO Local")) );

        // now clear the productFamilies value using empty array
        patchJson = "{\"productFamilies\": [] }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("productFamilies"));
    }

    // productFamilies is a multi-valued text property defined by an aspect
    // - set it, then unset it using null
    @SuppressWarnings("unchecked")
    @Test
    public void testSetUnsetNullMultiTextAspectProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // add multi-value text property
        String patchJson = "{\"productFamilies\": [\"HMO Local\", \"HMO NE\"] }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify productFamilies value
        Map<String, Object> resultMap = getObject(productsUri, objId);

        ArrayList<String> prodFamilies = (ArrayList<String>) resultMap.get("productFamilies");
        String pf1 = prodFamilies.get(0);
        String pf2 = prodFamilies.get(1);
        assertTrue( (pf1.equals("HMO Local") && pf2.equals("HMO NE")) || (pf1.equals("HMO NE") && pf2.equals("HMO Local")) );

        // now clear the productFamilies value using null
        patchJson = "{\"productFamilies\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("productFamilies"));
    }

    // distributionChannels is a multi-valued text property defined as a native property
    // - set it, then unset it using empty array
    @SuppressWarnings("unchecked")
    @Test
    public void testSetUnsetEmptyArrayMultiTextNativeProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // verify distributionChannels key is NOT present
        // (it appears natively-defined properties will NOT appear on object-retrieval when no value has been set for them;
        // however, after ANY value has been set, then after 'unset', the property WILL appear with null value).
        Map<String, Object> resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("distributionChannels"));

        // add multi-value text property (does this allow the same value twice?)
        String patchJson = "{\"distributionChannels\": [\"Public Exchange\", \"Public Exchange\"] }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify productFamilies value
        resultMap = getObject(productsUri, objId);

        ArrayList<String> multiVal = (ArrayList<String>) resultMap.get("distributionChannels");
        String pf1 = multiVal.get(0);
        String pf2 = multiVal.get(1);
        assertTrue( (pf1.equals("Public Exchange") && pf2.equals("Public Exchange")) ); // || (pf1.equals("HMO NE") && pf2.equals("HMO Local")) );

        // now clear the productFamilies value using empty array
        patchJson = "{\"distributionChannels\": [] }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("distributionChannels"));
    }

    // distributionChannels is a multi-valued text property defined as a native property
    // - set it, then unset it using null
    @SuppressWarnings("unchecked")
    @Test
    public void testSetUnsetNullMultiTextNativeProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // add multi-value text property (does this allow the same value twice? - YES)
        String patchJson = "{\"distributionChannels\": [\"Public Exchange\", \"Public Exchange\"] }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify productFamilies value
        Map<String, Object> resultMap = getObject(productsUri, objId);

        ArrayList<String> multiVal = (ArrayList<String>) resultMap.get("distributionChannels");
        String pf1 = multiVal.get(0);
        String pf2 = multiVal.get(1);
        assertTrue( (pf1.equals("Public Exchange") && pf2.equals("Public Exchange")) ); // || (pf1.equals("HMO NE") && pf2.equals("HMO Local")) );

        // now clear the productFamilies value using null
        patchJson = "{\"distributionChannels\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("distributionChannels"));
    }

    // phone is a single-valued text property defined by an aspect (hr:userInfo) - set it, then unset it using null
    @Test
    public void testSetUnsetNullSingleTextAspectProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // verify property key is NOT present (hr:userInfo is not a mandatory aspect)
        Map<String, Object> resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("phone"));

        // add single-value text property
        String patchJson = "{\"phone\": \"555-1212\" }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify property value
        resultMap = getObject(productsUri, objId);

        String phone = resultMap.get("phone").toString();
        assertTrue(phone.equals("555-1212"));

        // now clear the property value using null
        patchJson = "{\"phone\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("phone"));
    }

    // productStatus is a single-valued text property defined as a native property - set it, then unset it using null
    @Test
    public void testSetUnsetNullSingleTextNativeProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // retrieve the the product and verify property value
        Map<String, Object> resultMap = getObject(productsUri, objId);

        String productStatus = resultMap.get("productStatus").toString();
        assertTrue(productStatus.equals(pStatus));

        // now clear the property value using null
        String patchJson = "{\"productStatus\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("productStatus"));
    }

    // endDate is a single-valued date property defined by an aspect
    // - set it, then unset it using null
    @Test
    public void testSetUnsetNullSingleDateAspectProp() {

        // note this creates a product with an endDate
        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // verify property key is NOT present (hr:userInfo is not a mandatory aspect)
        Map<String, Object> resultMap = getObject(productsUri, objId);
        assertTrue(resultMap.containsKey("endDate"));

        OffsetDateTime expectedEndDateTime = OffsetDateTime.parse(endDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        assertTrue(expectedEndDateTime.isEqual(OffsetDateTime.parse((String) resultMap.get("endDate"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));

        // now clear the property value using null
        String patchJson = "{\"endDate\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("endDate"));
    }

    // planDueDate is a single-valued date property defined as a native property; also defined as mandatory;
    // - set it, then unset it using null
    @Test
    public void testSetUnsetNullSingleDateNativeProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // retrieve the the product and verify property value
        Map<String, Object> resultMap = getObject(productsUri, objId);

        String productStatus = resultMap.get("productStatus").toString();
        assertTrue(productStatus.equals(pStatus));

        // now clear the property value using null
        String patchJson = "{\"productStatus\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("productStatus"));
    }

    // isPublished is a single-valued date property defined by an aspect (hr:publishable); default val is false
    // - set it, then unset it using null (do we get default 'false' back?)
    @Test
    public void testSetUnsetNullSingleBooleanAspectProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // verify property key is NOT present (hr:publishable is not a mandatory aspect)
        Map<String, Object> resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("isPublished"));

        // add single-value boolean property
        String patchJson = "{\"isPublished\": true }"; // set to non-default value
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify property value
        resultMap = getObject(productsUri, objId);
        boolean isPublished = (boolean) resultMap.get("isPublished");
        assertTrue(isPublished);

        // now clear the property value using null
        patchJson = "{\"isPublished\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("isPublished"));
    }

    // numProviderTiers is a single-valued integer property defined as a native property
    // - set it, then unset it using null
    @Test
    public void testSetUnsetNullSingleIntegerNativeProp() {

        Product testProd = createProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate);
        String objId = doCreateProduct(testProd);

        // retrieve the the product and verify property value
        Map<String, Object> resultMap = getObject(productsUri, objId);

        Object obj = resultMap.get("numProviderTiers");
        assertTrue(obj == null);

        // add single-value int property
        String patchJson = "{\"numProviderTiers\": 7 }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the the product and verify property value
        resultMap = getObject(productsUri, objId);
        int numProviderTiers = (int) resultMap.get("numProviderTiers");
        assertTrue(numProviderTiers == 7);

        // now clear the property value using null
        patchJson = "{\"numProviderTiers\": null }";
        patchObject(productsUri, objId, patchJson);

        // retrieve the product and verify property key is NOT present
        resultMap = getObject(productsUri, objId);
        assertFalse(resultMap.containsKey("numProviderTiers"));
    }


    /////////////
    // private //  // see ProductTests for a collection of helper functions that we can share as we expand these tests
    /////////////

    private void patchObject(String baseUri, String objId, String patchJson) {
        // @formatter:off
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            contentType("application/json").
            body(patchJson).
        when().
            patch(baseUri + "/" + objId);
    }

    private Map<String,Object> getObject(String baseUri, String objId) {

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
                given().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                    get(baseUri + "/" + objId).
                andReturn().as(HashMap.class);
        // @formatter:on

        return resultMap;
    }

    private String doCreateProduct(Product testProd) {
        return doCreateProduct(testProd, tenantFolderName);
    }

    private String doCreateProduct(Product testProd, String tenantFolderName) {
        // @formatter:off
        String objId =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(testProd).
            when().
                post(productsUri).
            andReturn().body().asString();
        // @formatter:on

        return objId;
    }

    @Override
    public void beforeClassSetUp() {
        productsUri = "/products";
        tenantsUri = "/organizations";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json").
            body(testTenant).
        when().
            post(tenantsUri).
        andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Override
    public void afterClassTearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

}
