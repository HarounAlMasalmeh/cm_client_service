package com.highroads.cm;

import static com.highroads.cm.DomainObjectFactory.createAspectTestProduct;
import static com.highroads.cm.DomainObjectFactory.createExtendedModelProduct;
import static com.highroads.cm.DomainObjectFactory.createTemplate;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Product;
import com.highroads.cm.DomainObjectFactory.Template;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.InstanceTestClassListener;
import com.highroads.commons.SpringInstanceTestClassRunner;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.jayway.restassured.path.json.JsonPath;

@RunWith(SpringInstanceTestClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class AspectPropertyTests implements InstanceTestClassListener {
	private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

	private static String productsUri;
	private static String tenantsUri;
    private static String templatesUri;

    private static String tenantFolderName;
    private static String tenantObjectId;

    @Autowired
    private WebApplicationContext wac;

    @PostConstruct
    public void init() {
        logger.debug("init() called");
    }

    // Object-creation tests will test the so-called "optional aspects" functionality, those types for which
    // "mandatory aspects" are defined but for which all properties are optional;
    // CM must attach the aspect(s) or the object will not be created (Alfresco will throw a constraint violation exception).

    @Test
    public void testCreateProductWithAspectProperty() {
        String[] prodClasses = new String[] {"Medical"};
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00-05:00";
        String endDate = "2015-12-31T00:00:00-05:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00-05:00";
        boolean hidden = true;
        Product testProd = createAspectTestProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate, hidden);
        @SuppressWarnings("unused")
        String objId = doCreateProduct(testProd);
    }

    @Test
    public void testPatchTemplateWithNoAspectProperty() throws IOException {
        // create system-named template, then patch name only
        // effectiveDate, endDate are provided because these are defined in a mandatory aspect
        Template template = createTemplate("2015-01-01T00:00:00-05:00", "2015-12-31T00:00:00-05:00", "SBC", "Product");

        String templateId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(template).
                when().
                    post(templatesUri).
                andReturn().body().asString();

        logger.info("template created with id " + templateId);

        String name = "Ham sheet" + System.currentTimeMillis();
        String templatePatchJson = String.format("{\"name\": \"%s\"}", name);

        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            contentType("application/json").
            body(templatePatchJson).
        when().
            patch(templatesUri + "/" + templateId);
        logger.info("== template name updated (native property)");

        //retrieve the the source template and verify its name has changed
        @SuppressWarnings("unchecked")
        HashMap<String,Object> resultMap =
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(templatesUri + "/" + templateId).
        andReturn().as(HashMap.class);

        String resultName = resultMap.get("name").toString();
        assertTrue(name.equals(resultName));
    }

    @Test
    public void testPatchTemplateWithNewAspectProperty() throws IOException {
        // create system-named template, then patch name, productType
        // - the latter is defined in an aspect, different from the effectiveDates aspect
        Template template = createTemplate("2015-01-01T00:00:00-05:00", "2015-12-31T00:00:00-05:00", "SBC", "Product");

        String templateId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(template).
                when().
                    post(templatesUri).
                andReturn().body().asString();

        logger.info("template created with id " + templateId);

        String name = "TestTemplateName" + System.currentTimeMillis();
        String productType = "PPO";     // aspect-defined property
        String documentType = "CSOB";   // change SBC doctype to CSOB
        String templatePatchJson = String.format("{\"name\": \"%s\", \"productType\": \"%s\", \"documentType\": \"%s\" }",
                name, productType, documentType);

        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            contentType("application/json").
            body(templatePatchJson).
        when().
            patch(templatesUri + "/" + templateId);
        logger.info("== template name updated, template productType added (an aspect-defined property), template documentType updated");


        //retrieve the the source template and verify its name, productType, and documentType have changed
        @SuppressWarnings("unchecked")
        HashMap<String,Object> resultMap =
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(templatesUri + "/" + templateId).
        andReturn().as(HashMap.class);
//        then().
//            body("productType", equalTo(productType)).and().
//            body("name", equalTo(name));

        String resultName = resultMap.get("name").toString();
        String resultProductType = resultMap.get("productType").toString();
        String resultDocType = resultMap.get("documentType").toString();
        assertTrue(name.equals(resultName));
        assertTrue(productType.equals(resultProductType));
        assertTrue(documentType.equals(resultDocType));
    }

    // Test model extensions - create a product with org-specific properties
    @Test
    public void testCreateModelExtendedProduct() {
        String[] prodClasses = new String[] {"Medical"};
        String prodType = "HMO";
        String startDate = "2015-01-01T00:00:00-05:00";
        String endDate = "2015-12-31T00:00:00-05:00";
        String pStatus = "Draft"; // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00-05:00";
        boolean udfproduct_isSmokingCessationStepTherapyRequired = true;

        Product testProd = createExtendedModelProduct(prodClasses, prodType, startDate, endDate, pStatus, dueDate,
                udfproduct_isSmokingCessationStepTherapyRequired);
        String objId = doCreateProduct(testProd, "upmc.com");
        JsonPath jpath =
                given().
                      headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("upmc.com")).
                when().
get(productsUri + "/" + objId + "?properties=udfproduct_isSmokingCessationStepTherapyRequired").
                andReturn().
                body().jsonPath();
        boolean retrievedNewProp = jpath.getBoolean("udfproduct_isSmokingCessationStepTherapyRequired");
        assertEquals(udfproduct_isSmokingCessationStepTherapyRequired, retrievedNewProp);
    }

    /////////////
    // private //  // see ProductTests for a collection of helper functions that we can share as we expand these tests
    /////////////



    private String doCreateProduct(Product testProd) {
        return doCreateProduct(testProd, tenantFolderName);
    }

    private String doCreateProduct(Product testProd, String tenantFolderName) {

        String objId =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(testProd).
            when().
                post(productsUri).
            andReturn().body().asString();

        return objId;
    }

    @Override
    public void beforeClassSetUp() {
        productsUri = "/products";
        tenantsUri = "/organizations";
        templatesUri = "/templates";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json").
            body(testTenant).
        when().
            post(tenantsUri).
        andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Override
    public void afterClassTearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

}
