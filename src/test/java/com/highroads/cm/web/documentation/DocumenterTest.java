package com.highroads.cm.web.documentation;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.Application;

import io.github.robwin.markup.builder.MarkupLanguage;
import io.github.robwin.swagger2markup.Swagger2MarkupConverter;
import springfox.documentation.staticdocs.SwaggerResultHandler;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class, Documenter.class })
public class DocumenterTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Rule
    public final RestDocumentation restDocumentation = new RestDocumentation("target/asciidoc");

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    // This is a special "Test" case because it is used to generate the static documentation
    public void convertRemoteSwaggerToAsciiDoc() throws Exception {

        //@formatter:off
        String outputDir = "target/swagger";
        MvcResult mvcResult = this.mockMvc
                .perform(get("/v2/api-docs")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(SwaggerResultHandler
                        .outputDirectory(outputDir)
                        .build())
                .andExpect(status()
                        .isOk())
                .andReturn();

        String swaggerJsonAsString = mvcResult
                .getResponse()
                .getContentAsString();


        Swagger2MarkupConverter
        .fromString(swaggerJsonAsString)
        .withMarkupLanguage(MarkupLanguage.ASCIIDOC)
        .build()
        .intoFolder("src/docs/asciidoc/generated");

        String[] files = new File("src/docs/asciidoc/generated")
                .list();

        assertThat(files.length, is(3));
        List<String> filesList =  Arrays.asList(files);
        filesList.sort(new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        assertThat(filesList, is(Arrays.asList("definitions.adoc", "overview.adoc", "paths.adoc")));
        //@formatter:on
    }
}
