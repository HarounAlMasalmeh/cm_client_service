package com.highroads.cm.web.controller.validator;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.highroads.commons.api.exceptions.HrException;
import com.highroads.p2alibs.validator.JsonValidator;
import com.highroads.p2alibs.validator.JsonValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JsonValidatorImpl.class)
public class JsonValidatorTest {

    @Autowired
    JsonValidator validator;
    ObjectMapper mapper = new ObjectMapper();

    @Test
    @SuppressWarnings("unchecked")
    /**
     * Confirms that the JsonValidator supports deep check validation to return
     * all errors from single validate call
     *
     * @throws Exception
     */
    public void multipleValidationErrorTest() throws Exception {
        InputStream is = getClass().getResourceAsStream("/validator/planWithErrors.json");
        Map<String, Object> jsonAsMap = mapper.readValue(is, Map.class);
        InputStream schemaIs = getClass().getResourceAsStream("/bootstrap/1.44/upmc.com/model-schema-dictionary.json.txt");
        Map<String, Object> schemaMap = mapper.readValue(schemaIs, Map.class);
        ObjectNode schema = mapper.valueToTree(schemaMap);
        try {
            validator.validate(jsonAsMap, schema, "plan");
        } catch (HrException hrex) {
            System.out.println(hrex.getHrExceptionDetails().getData().size());
            assertTrue(hrex.getHrExceptionDetails().getData().size() == 6);
        }
    }
}
