package com.highroads.cm;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.impl.TenantService;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.highroads.commons.sso.UserToken;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Creates unnecessary tenants. Also, TestTenantService.java should suffice.")
public class TenantControllerTests {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String TEST_TENANT_ID_ROOT = "tenant.com";
    private static final String TENANT_OBJECT_TYPE = "organization";

    private MockMvc mockMvc;

    private String tenantsUri;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private TenantService tenantService;

    @Before
    public void setUp() {
        tenantsUri = "/organizations";
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;
    }

    /**
     * Create, update, and delete a test tenant
     */
    //@Test
    public void testTenant() {
        logger.debug("== in testTenant");

        String tenantOrgId = generateTenantOrgId();
        Map<String, Object> tenantInfo = createTestTenant(tenantOrgId);
        tenantOrgId = tenantInfo.get("orgId").toString();
        String objId = tenantInfo.get("id").toString();
        logger.debug("== tenant created with id " + objId);

        given().log().all().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com")).when()
                .get(tenantsUri + "/" + objId).then().body("name", equalTo(tenantOrgId)).and().body("objectType", equalTo(TENANT_OBJECT_TYPE)).and()
                .body("orgStatus", equalTo("Active")).and().body("licensedModules", equalTo(null));
    }

    /**
     * sync-users tests
     */
    @Test
    public void testSyncUserInvalidRoles() {

        UserToken ut = new UserToken();
        ut.setOrganization("test.com");
        ut.setUsername("user@test.com");

        String[] roles = { "badrole1", "badrole2" };
        Set<String> expectedRoles = new HashSet<String>(Arrays.asList(roles));
        ut.setRoles(Arrays.asList(roles));

        tenantService.syncUser(ut);
        assertTrue(Collections.emptyList().equals(ut.getRoles()));
    }

    /**
     * Create test tenant
     *
     * @param tenantOrgId
     * @return map with keys "id"
     */
    private Map<String, Object> createTestTenant(String tenantOrgId) {

        String testTenant = "{\"orgId\":\"" + tenantOrgId + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantOrgId + "\", \"orgName\":\""
                + tenantOrgId + "\"}";

        logger.debug(testTenant);

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json")
            .body(testTenant).
        when().post(tenantsUri).
        andReturn().as(HashMap.class);

        String objId = resultMap.get("id").toString();
        logger.debug("== tenant created with id " + objId);

        return resultMap;
    }

    private String generateTenantOrgId() {
        String suffix = "" + System.currentTimeMillis();
        String tenantOrgId = TEST_TENANT_ID_ROOT + "." + suffix;
        return tenantOrgId;
    }

}