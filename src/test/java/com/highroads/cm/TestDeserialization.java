package com.highroads.cm;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.commons.model.P2ACollection;

public class TestDeserialization {

    @Test
    public void test1() throws IOException {
        String c = "{ \"collection\": \"plans\"}";

        ObjectMapper mapper = new ObjectMapper();

        Map<String, P2ACollection> rslt = mapper.readValue(c, new TypeReference<Map<String, P2ACollection>>() {
        });

        P2ACollection collection = rslt.get("collection");

        assertEquals("plan", collection.getObjectType());

    }

}
