package com.highroads.cm.utils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;


public class DocNameUtilTest {

    private static Map<String, Object> plan = new HashMap<String, Object>();
    private static Map<String, Object> offering = new HashMap<String, Object>();
    private static Map<String, Object> template = new HashMap<String, Object>();
    private DocNameUtil namingUtil = new DocNameUtil();

    @BeforeClass
    public static void prepareTests(){
        plan.put("productClasses", Arrays.asList("Pharmacy"));
        plan.put("udfplan_benefitID", "1D07");
        offering.put("benefitYear", "2017");
    }

    @Test
    public void testStaticTest(){
        assertEquals("staticTextTest", namingUtil.evaluateToken("staticTextTest", offering, plan, template));
    }

    @Test
    public void testPropertyLookup(){
        assertEquals("2017", namingUtil.evaluateToken("offering.benefitYear", offering, plan, template));
    }

    @Test
    public void testConditionalEvaluation(){
        assertEquals("RX", namingUtil.evaluateToken("plan.productClasses=Pharmacy?RX:", offering, plan, template));
        plan.put("productClasses", Arrays.asList("Medical"));
        assertEquals("", namingUtil.evaluateToken("plan.productClasses=Pharmacy?RX:", offering, plan, template));
    }
}
