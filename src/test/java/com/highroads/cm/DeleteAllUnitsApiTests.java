package com.highroads.cm;

/**
 * Test class for delete all unit API endpoint.
 * @author paksentiev
 * @version
 * @see
 * @since
 */

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.jayway.restassured.module.mockmvc.response.MockMvcResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("obsolete")
public class DeleteAllUnitsApiTests
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    //    @Autowired
    //    private SessionManager cmisSessionManager;


    private MockMvc mockMvc;

    private String tenantsUri;
    private String tenantFolderName;
    private String tenantObjectId;

    @Autowired
    private WebApplicationContext wac;

    @PostConstruct
    public void init() {
        logger.debug("init() called");
        tenantsUri = "/organizations";
    }

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(testTenant).
            when().
                post(tenantsUri).
            andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeleteAllUnit() {
        logger.info("in unit delete");
        MockMvcResponse resp =
                given().
                        log().all().
                        headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                        contentType("application/json")
                        .body("{ \"name\":\"My Unit 1\"}").
                        when().post("/units");
        resp =
                given().
                        log().all().
                        headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                        contentType("application/json")
                        .body("{ \"name\":\"My Unit 2\"}").
                        when().post("/units");

        ArrayList<Object> units = null;
        try {
            for (int i = 0; i < 6; i++) { // TODO: make this a shared utility across all unit-tests
                logger.debug("fetching units from Alfresco...");
                units = given().
                        log().all().
                        headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                        when().
                        get("/units").
                        andReturn().
                        body().as(ArrayList.class);
                if ((units == null) || (units.size() != 2)) {
                    logger.debug("Waiting for the units to be indexed in Alfresco. Sleeping...");
                    Thread.sleep(15000);
                } else if ((units.size() == 2) || (i >= 6)) {
                    break;
                }
            }
            assert(units != null);
            assert(units.size() == 2);
        } catch (InterruptedException e) {}

        given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                delete("/units").
                then().
                statusCode(HttpServletResponse.SC_OK);
        logger.info("== deleted all unit objects for " + tenantFolderName);

        units = given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                get("/units").
                andReturn().
                body().as(ArrayList.class);
        assert(units.size() == 0);
    }
}
