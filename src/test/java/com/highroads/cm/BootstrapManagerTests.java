package com.highroads.cm;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class BootstrapManagerTests {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    private String servicesUri;
    private String organizationsUri;

    private String organizationFolderName;
    private String organizationObjectId;

    @Before
    public void setUp() {
        servicesUri = "/services";
        organizationsUri = "/organizations";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        organizationFolderName = "tenant.test." + System.currentTimeMillis();

        String testOrganization = "{\"orgId\":\"" + organizationFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + organizationFolderName
                + "\", \"orgName\":\"" + organizationFolderName + "\"}";

        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers("Authorization", TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json").
            body(testOrganization).
        when().
            post(organizationsUri).
        andReturn().as(HashMap.class);

        organizationObjectId = resultMap.get("id").toString();

        logger.info("== 0 Created organization " + organizationFolderName + ", id: " + organizationObjectId);
    }

    @After
    public void tearDown() {
        // delete the test organization
        given().
            log().all().
            headers("Authorization", TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(organizationsUri + "/" + organizationObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);

        logger.info("== 0 Deleted organization " + organizationFolderName + ", id: " + organizationObjectId);
    }

    @Test
    public void testValidateServicesOnOrganizationCreation() {
        logger.info("== 1 Retrieving services for organization");

        // @formatter:off
        @SuppressWarnings("unchecked")
        ArrayList<Object> services = given().
                                log().all().
                                headers("Authorization", TokenHelper.getTokenBase64String(organizationFolderName)).
                            when().
                                get(servicesUri).
                            andReturn().
                                body().as(ArrayList.class);
        // @formatter:on

        assert(services.size() > 0);
    }

}

