package com.highroads.cm;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class ModelSchemaTests {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String typesUri;
    private String tenantFolderName;
    private String tenantObjectId;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        typesUri = "/types";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json").
            body(testTenant).
        when().
            post("/organizations").
        andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete("/organizations/" + tenantObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Test
    public void testModelSchemaPlan() {
        // @formatter:off
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(typesUri + "/plan").
        then().
            statusCode(200).
            body("[0].name", equalTo("plan")).and().
            body("[0].displayName", equalTo("HighRoads Plan"));
        // @formatter:on

        logger.debug("== plan successfully retrieved");
    }

    @Test
    public void testModelSchemaProduct() {
        // @formatter:off
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(typesUri + "/product").
        then().
            body("[0].name", equalTo("product")).and().
            body("[0].displayName", equalTo("HighRoads Product"));
        // @formatter:on

        logger.debug("== product successfully retrieved");
    }

    @Test
    public void testModelSchemaDocument() {
        // @formatter:off
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(typesUri + "/document").
        then().
            statusCode(200).
            body("[0].name", equalTo("document")).and().
            body("[0].displayName", equalTo("Generated Document"));
        // @formatter:on

        logger.debug("== document successfully retrieved");
    }

    @Test
    public void testModelSchemaDeletionRule() {
        // @formatter:off
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(typesUri + "/deletionRule").
        then().
            statusCode(200).
            body("[0].name", equalTo("deletionRule")).and().
            body("[0].displayName", equalTo("Highroads Deletion Rule Type"));
        // @formatter:on

        logger.debug("== deletionRule successfully retrieved");
    }

    @Test
    public void testModelSchemaTypes() {
        // @formatter:off
        String[] objectTypes =
           given().
               log().all().
               headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
           when().
               get(typesUri).
           then().
               statusCode(200).
           extract().as(String[].class);
        // @formatter:on

        // Verify that you got some object types
        assertTrue("No object types were retrieved", objectTypes.length > 0);

        for (String objectType : objectTypes) {
            logger.debug("== " + objectType);

            // @formatter:off
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                get(typesUri + "/" + objectType).
            then().
                statusCode(200).
                body("[0].name", equalTo(objectType));
            // @formatter:on
        }

        logger.debug("== All schema types successfully retrieved");
    }
}
