package com.highroads.cm.security;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.highroads.p2alibs.security.MLRole;
import com.highroads.p2alibs.security.MLUser;

public class TestMLUser extends MLSecurityTest {

    private MLUser mlUser;

    private MLRole mlRole;

    private final List<MLRole> mlRoles = new ArrayList<>();

    @Override
    @Before
    public void setUp() {
        super.setUp();
        this.mlRole = new MLRole("local", "upmc-role", null, null, null);
        super.managementClient.post(MLRole.RESOURCE, this.mlRole.writeRequest());

        this.mlRoles.add(new MLRole("local", "upmc-role"));
        this.mlUser = new MLUser("local", "localuser@upmc.com", this.mlRoles);
        super.managementClient.post(MLUser.RESOURCE, this.mlUser.writeRequest());
    }

    @Test
    public void shouldFindUser() {
        final String response = super.managementClient.get(MLUser.RESOURCE + this.mlUser.getName(), this.mlUser.readRequestParams());
        assertTrue(response.contains("localuser@local-upmc.com"));
    }

    @Test(expected = HttpClientErrorException.class)
    public void shouldNotFindUser() {
        super.managementClient.get(MLUser.RESOURCE + "TEST", this.mlUser.readRequestParams());
    }

    @After
    public void tearDown() {

        super.managementClient.delete(MLUser.RESOURCE + mlUser.getName(), mlUser.readRequestParams());
        super.managementClient.delete(MLRole.RESOURCE + mlRole.getName(), mlRole.readRequestParams());
    }

}
