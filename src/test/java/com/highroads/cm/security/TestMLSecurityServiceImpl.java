package com.highroads.cm.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.highroads.p2alibs.security.MLCollection;
import com.highroads.p2alibs.security.MLExecutePrivilege;
import com.highroads.p2alibs.security.MLPrivilege;
import com.highroads.p2alibs.security.MLRole;
import com.highroads.p2alibs.security.MLSecurityServiceImpl;
import com.highroads.p2alibs.security.MLURIPrivilege;
import com.highroads.p2alibs.security.MLUser;

public class TestMLSecurityServiceImpl extends MLSecurityTest {

    private MLSecurityServiceImpl securityService;

    private MLRole mlRole;

    private MLURIPrivilege mlURIPrivilege;

    private MLUser mlUser;

    private MLRole mlRoleWithOutUser;

    private final List<MLRole> mlRoles = new ArrayList<MLRole>();
    private final List<MLCollection> collections = new ArrayList<MLCollection>();
    private final List<MLPrivilege> privilegs = new ArrayList<MLPrivilege>();

    @Override
    @Before
    public void setUp() {
        super.setUp();
        this.securityService = new MLSecurityServiceImpl();
        this.securityService.setManagementClient(super.managementClient);

        this.collections.add(new MLCollection("test-upmc"));
        this.privilegs.add(new MLExecutePrivilege("rest-reader"));
        this.privilegs.add(new MLExecutePrivilege("rest-writer"));
        this.mlRole = new MLRole("local", "test-upmc-role", this.mlRoles, this.privilegs, this.collections);
        this.securityService.create(this.mlRole);

        mlRoles.add(new MLRole("local", "test-upmc-role"));
        mlURIPrivilege = new MLURIPrivilege("test-upmc", "test-upmc");
        this.securityService.create(mlURIPrivilege);

        this.mlUser = new MLUser("local", "admin@upmc.com", this.mlRoles);
        this.securityService.create(mlUser);

        mlRoleWithOutUser = new MLRole("local", "test-ml-role-without-user");
        this.securityService.create(mlRoleWithOutUser);

    }

    @After
    public void tearDown() {
        this.securityService.delete(mlURIPrivilege);
        this.securityService.delete(mlUser);
        this.securityService.delete(mlRole);
        this.securityService.delete(mlRoleWithOutUser);
    }

    @Test
    public void shouldFindMLURIPrivilegeByName() throws IOException {
        final MLPrivilege privilege = this.securityService.find(new MLURIPrivilege("test-upmc", "test-upmc"));
        assertEquals("test-upmc", privilege.getName());
    }

    @Test
    public void shouldNotFindMLURIPrivilegeByName() throws IOException {
        assertNull(this.securityService.find(new MLURIPrivilege("local", "junit-test-uri-123")));
    }

    @Test
    public void shouldFindMLRole() throws IOException {
        final MLRole mlRole = this.securityService.find(new MLRole("local", "test-upmc-role"));
        assertEquals("local-test-upmc-role", mlRole.getName());
    }

    @Test
    public void shouldFindMLUser() throws IOException {
        final MLUser mlUser = this.securityService.find(new MLUser("local", "admin@upmc.com"));
        assertEquals("admin@local-upmc.com", mlUser.getName());
    }

    @Test
    public void shouldFindUsersForARole() {
        final List<MLUser> users = securityService.findUsersByRole(new MLRole("local", "test-upmc-role"));
        assertEquals("admin@local-upmc.com", users.get(0).getName());
    }

    @Test
    public void shouldNotFindUsersForARole() {
        final List<MLUser> users = securityService.findUsersByRole(new MLRole("local", "test-1ml-role-without-user"));
        assertEquals(0, users.size());
    }

}
