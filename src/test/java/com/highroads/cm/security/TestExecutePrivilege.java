package com.highroads.cm.security;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.highroads.p2alibs.security.MLExecutePrivilege;

public class TestExecutePrivilege extends MLSecurityTest {

    private MLExecutePrivilege executePrivilege;

    @Override
    @Before
    public void setUp() {
        super.setUp();
        this.executePrivilege = new MLExecutePrivilege("test-execution-privilege");
        this.managementClient.post(MLExecutePrivilege.RESOURCE, this.executePrivilege.writeRequest());
    }

    @Test
    public void shouldFindPrivilege() {

        final String restReader = super.managementClient.get(MLExecutePrivilege.RESOURCE + "test-execution-privilege",
                this.executePrivilege.readRequestParams());
        assertNotNull(restReader);
        assertTrue(restReader.contains("test-execution-privilege"));
    }

    @After
    public void tearDown() {
        super.managementClient.delete(MLExecutePrivilege.RESOURCE + "test-execution-privilege", executePrivilege.readRequestParams());
    }

}
