package com.highroads.cm.security;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.highroads.p2alibs.security.MLURIPrivilege;

public class TestMLURIPrivilege extends MLSecurityTest {

    private MLURIPrivilege mluriPrivilege;

    @Override
    @Before
    public void setUp() {
        super.setUp();
        this.mluriPrivilege = new MLURIPrivilege("junit-test-uri", "junit-test-uri");
    }

    @After
    public void tearDown() {
        super.managementClient.delete(MLURIPrivilege.RESOURCE + "junit-test-uri", mluriPrivilege.readRequestParams());
    }

    @Test
    public void shouldCreateURIPermission() {

        super.managementClient.post(MLURIPrivilege.RESOURCE, this.mluriPrivilege.writeRequest());
        final Map<String, String> params = new HashMap<>();
        params.put("kind", "uri");
        params.put("format", "json");
        final String response = this.managementClient.get(MLURIPrivilege.RESOURCE + "junit-test-uri", params);
        assertNotNull(response);
        assertTrue(response.contains("junit-test-uri"));
    }

}
