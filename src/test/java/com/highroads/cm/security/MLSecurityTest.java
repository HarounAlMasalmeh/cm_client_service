package com.highroads.cm.security;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.highroads.cm.Application;
import com.highroads.commons.config.MarklogicConnectionProperties;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.p2alibs.security.MLRESTManagementClient;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public abstract class MLSecurityTest {

    protected MLRESTManagementClient managementClient;
    @Autowired
    private MarklogicConnectionProperties marklogicConnectionProperties;

    @Before
    public void setUp() {
        new MarklogicDAO(marklogicConnectionProperties);
        this.managementClient = new MLRESTManagementClient(marklogicConnectionProperties);
    }

}
