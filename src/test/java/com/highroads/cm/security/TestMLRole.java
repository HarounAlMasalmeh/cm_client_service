package com.highroads.cm.security;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.highroads.p2alibs.security.MLCapability;
import com.highroads.p2alibs.security.MLCollection;
import com.highroads.p2alibs.security.MLExecutePrivilege;
import com.highroads.p2alibs.security.MLPermission;
import com.highroads.p2alibs.security.MLPrivilege;
import com.highroads.p2alibs.security.MLRole;

public class TestMLRole extends MLSecurityTest {

    private MLRole mlRole;

    private final List<MLRole> mlRoles = new ArrayList<>();
    private final List<MLPrivilege> privilegs = new ArrayList<>();
    private final List<MLCollection> collections = new ArrayList<>();
    private final List<MLPermission> permissions = new ArrayList<>();

    @Override
    @Before
    public void setUp() {
        super.setUp();
        this.privilegs.add(new MLExecutePrivilege("rest-reader"));
        this.privilegs.add(new MLExecutePrivilege("rest-writer"));
        this.collections.add(new MLCollection("upmc"));
        this.mlRole = new MLRole("local", "upmc-role", this.mlRoles, this.privilegs, this.collections);
        super.managementClient.post(MLRole.RESOURCE, this.mlRole.writeRequest());
    }

    @Test
    public void shouldCreateMLRole() {
        final String response = super.managementClient.get(MLRole.RESOURCE + this.mlRole.getName(), this.mlRole.readRequestParams());
        assertTrue(response.contains("\"name\":\"local-upmc-role\""));
    }

    @Test
    public void shouldUpdateMLRole() {

        this.permissions.add(new MLPermission(new MLRole("local", "upmc-role"), MLCapability.READ));
        this.permissions.add(new MLPermission(new MLRole("local", "upmc-role"), MLCapability.UPDATE));
        this.mlRole = new MLRole("local", "upmc-role", this.permissions);
        super.managementClient.put(MLRole.RESOURCE + this.mlRole.getName() + "/properties", this.mlRole.updateRequest());

        final String response = super.managementClient.get(MLRole.RESOURCE + this.mlRole.getName(), this.mlRole.readRequestParams());
        assertTrue(response
                .contains(
                        "\"relation\":[{\"roleref\":\"local-upmc-role\", \"capability\":\"update\"}, {\"roleref\":\"local-upmc-role\", \"capability\":\"read\"}]"));
    }

    @After
    public void tearDown() {

        super.managementClient.delete(MLRole.RESOURCE + mlRole.getName(), mlRole.readRequestParams());
    }

}
