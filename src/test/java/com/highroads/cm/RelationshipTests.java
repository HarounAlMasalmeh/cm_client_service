package com.highroads.cm;

import static com.highroads.cm.DomainObjectFactory.createProduct;
import static com.highroads.cm.DomainObjectFactory.createProductService;
import static com.highroads.cm.DomainObjectFactory.createSection;
import static com.highroads.cm.DomainObjectFactory.createService;
import static com.highroads.cm.DomainObjectFactory.createTemplate;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Product;
import com.highroads.cm.DomainObjectFactory.ProductService;
import com.highroads.cm.DomainObjectFactory.Section;
import com.highroads.cm.DomainObjectFactory.Service;
import com.highroads.cm.DomainObjectFactory.Template;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.jayway.restassured.path.json.JsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class RelationshipTests {
	private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

    private String tenantObjectId;
    private String tenantsUri;
	private String productsUri;
    private String templatesUri;
    private String sectionsUri;
    private String productServicesUri;
    private String servicesUri;

    @Autowired
    private WebApplicationContext wac;

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    private String tenantFolderName;

    @Before
    public void setUp() {
        tenantsUri = "/organizations";
        productsUri = "/products";
        templatesUri = "/templates";
        sectionsUri = "/sections";
        productServicesUri = "/productservices";
        servicesUri = "/services";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
            contentType("application/json").
            body(testTenant).
        when().
            post(tenantsUri).
        andReturn().as(HashMap.class);

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);

        logger.info("== 0 created tenant " + tenantFolderName);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        given().
        	headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Test
    public void testSingleLevelRelationship() throws IOException {
        String relationType = "sections";

        Template template = createTemplate("2015-01-01T00:00:00-05:00", "2015-12-31T00:00:00-05:00", "SBC", "Product");
        Section section = createSection("testSectionName");

        String templateId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(template).
                when().
                    post(templatesUri).
                andReturn().body().asString();

        logger.info("template created with id " + templateId);

        String sectionId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(section).
                when().
                    post(sectionsUri).
                andReturn().body().asString();

        logger.info("section created with id " + sectionId);

        logger.info("create " + relationType + " relation from " + templateId + " to " + sectionId);

        String relationUrl = templatesUri + "/" + templateId + "/" + relationType + "/" + sectionId;
        String relationId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                    post(relationUrl).
                andReturn().body().asString();

        logger.info("created relation with id " + relationId);

        //retrieve the the source template and verify its relation with target section
        String sectionObjId = sectionId;
        given().
        	headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(templatesUri + "/" + templateId).
        then().
            body("sections.size()", equalTo(1)).and().
            body("sections[0]", containsString(sectionObjId));
    }

    @Test
    public void testSingleLevelRelationshipWithDelete() throws IOException {
        String relationType = "sections";

        Template template = createTemplate("2015-01-01T00:00:00-05:00", "2015-12-31T00:00:00-05:00", "SBC", "Product");
        Section section = createSection("testSectionName");

        String templateId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(template).
                when().
                    post(templatesUri).
                andReturn().body().asString();

        logger.info("template created with id " + templateId);

        String sectionId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(section).
                when().
                    post(sectionsUri).
                andReturn().body().asString();

        logger.info("section created with id " + sectionId);

        logger.info("create " + relationType + " relation from " + templateId + " to " + sectionId);

        String relationUrl = templatesUri + "/" + templateId + "/" + relationType + "/" + sectionId;
        String relationId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                    post(relationUrl).
                andReturn().body().asString();

        logger.info("created relation with id " + relationId);

        //retrieve the the source template and verify its relation with target section
        String sectionObjId = sectionId;
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(templatesUri + "/" + templateId).
        then().
            body("sections.size()", equalTo(1)).and().
            body("sections[0]", containsString(sectionObjId));

        // Delete the association that was just created
        relationUrl = templatesUri + "/" + templateId + "/" + relationType + "/" + sectionId;
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            delete(relationUrl).
        then().
            statusCode(HttpServletResponse.SC_OK);

        logger.info("deleted relation with id " + relationId);

        // Retrieve the the source template and verify its relation does not exist
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
            given().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                get(templatesUri + "/" + templateId).
            andReturn().as(HashMap.class);

        assertFalse(resultMap.containsKey("sections"));
    }

    @Test
    public void testMultiLevelRelationshipResponse() throws IOException {

        Product topProduct = createProduct(new String[] { "Medical" }, "HMO", "2015-01-01T00:00:00-05:00", "2015-12-31T00:00:00-05:00", "New",
                "2016-01-01T00:00:00-05:00");
        String[] productClasses = { "Medical", "Dental" };
        Service service = createService("serviceName", "Inpatient", "Physician Office", "Preventive", "Under Review", productClasses);

        String topProductId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(topProduct).
                when().
                    post(productsUri).
                andReturn().body().asString();

        String serviceId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(service).
                when().
                    post(servicesUri).
                andReturn().body().asString();

        ProductService productService = createProductService("ProductName", topProductId, "ProductServiceName", serviceId, true, true, true,
                "A service exception entry");
        String productServiceId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                    contentType("application/json").
                    queryParam("tenantFolder", tenantFolderName).
                    body(productService).
                when().
                    post(productServicesUri).
                andReturn().body().asString();

        // Product -> ProductServices
        String relationType = "productservices";
        logger.info("create " + relationType + " relation from " + topProductId + " to " + productServiceId);

        String relationUrl = productsUri + "/" + topProductId + "/" + relationType + "/" + productServiceId;
        String relationId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                    post(relationUrl).
                andReturn().body().asString();

        logger.info("created relation with id " + relationId);

        // ProductService -> Service
        relationType = "prodservice";
        logger.info("create " + relationType + " relation from " + productServiceId + " to " + serviceId);

        relationUrl = productServicesUri + "/" + productServiceId + "/" + relationType + "/" + serviceId;
        relationId =
                given().
                    log().all().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                    post(relationUrl).
                andReturn().body().asString();

        logger.info("created relation with id " + relationId);

        // for debug/logging
        JsonPath jpath = null;
        String objectDump = null;

        //========1 retrieve top product without relation expansion
        String productServiceObjId = productServiceId;
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(productsUri + "/" + topProductId).
        then().
            body("productservices.size()", equalTo(1)).and().
            body("productservices[0]", containsString(productServiceObjId));

        // for debug
        logger.debug("associationExpansionLevel=0/unspecified, returned object dump:");
        jpath =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                get(productsUri + "/" + topProductId).
            andReturn().
            body().jsonPath();
        objectDump = jpath.prettyPrint();


        //========2 retrieve top product with one level expansion
        String serviceObjId = serviceId;
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(productsUri + "/" + topProductId + "?associationExpansionLevel=1").
        then().
            body("productservices.size()", equalTo(1)).and().
            body("productservices[0].objectId", containsString(productServiceObjId)).and().
            body("productservices[0].prodservice[0]", containsString(serviceObjId)); // associations return the collection name, not the association name

        // for debug
        logger.debug("associationExpansionLevel=1, returned object dump:");
        jpath =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                get(productsUri + "/" + topProductId + "?associationExpansionLevel=1").
            andReturn().
            body().jsonPath();
        objectDump = jpath.prettyPrint();

        //========3 retrieve top product with two level expansion
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(productsUri + "/" + topProductId + "?associationExpansionLevel=2").
        then().
            body("productservices.size()", equalTo(1)).and().
            body("productservices[0].objectId", containsString(productServiceObjId)).and().
            body("productservices[0].prodservice[0].objectId", containsString(serviceObjId));

        // for debug
        logger.debug("associationExpansionLevel=2, returned object dump:");
        jpath =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                get(productsUri + "/" + topProductId + "?associationExpansionLevel=2").
            andReturn().
            body().jsonPath();
        objectDump = jpath.prettyPrint();

        //========4
        //retrieve top product with maximum level expansion
        //because we have only two level association, it should be the same result as validation above
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
        when().
            get(productsUri + "/" + topProductId + "?associationExpansionLevel=-1").
        then().
            body("productservices.size()", equalTo(1)).and().
            body("productservices[0].objectId", containsString(productServiceObjId)).and().
            body("productservices[0].prodservice[0].objectId", containsString(serviceObjId));

        // for debug
        logger.debug("associationExpansionLevel=-1, returned object dump:");
        jpath =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().
                get(productsUri + "/" + topProductId + "?associationExpansionLevel=-1").
            andReturn().
            body().jsonPath();
        objectDump = jpath.prettyPrint();
    }
}

