package com.highroads.cm;




//simplified version of the original domain classes for easier testing only
//so we don't cook up json strings in the code

//encapsulated inside the factory so there won't be conflict with client classes

public class DomainObjectFactory {

	private DomainObjectFactory() {}

	static Product createProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate) {
		return new Product(pclasses, ptype, peffective, pend, pStatus, pDueDate);
	}

	static Product createProduct(String[] pclasses, String[] ptypeList, String peffective, String pend, String pStatus, String pDueDate) {
        return new Product(pclasses, ptypeList, peffective, pend, pStatus, pDueDate);
    }

    static Product createProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, String[] pFamily) {
        return new Product(pclasses, ptype, peffective, pend, pStatus, pDueDate, pFamily);
	}

    static Product createProduct(String pname, String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, String[] pFamily,
            String[] mktSegments, String[] memberFundings) {
        return new Product(pname, pclasses, ptype, peffective, pend, pStatus, pDueDate, pFamily, mktSegments, memberFundings);
    }

    static Product createExtendedModelProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, boolean extendedPropValue) {
        return new ExtendedModelProduct(pclasses, ptype, peffective, pend, pStatus, pDueDate, extendedPropValue);
    }

    static Product createBrokenTestProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, String anyValue) {
        return new BrokenTestProduct(pclasses, ptype, peffective, pend, pStatus, pDueDate, anyValue);
    }

    static Product createAspectTestProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, boolean hidden) {
        return new AspectTestProduct(pclasses, ptype, peffective, pend, pStatus, pDueDate, hidden);
    }

    static Product createAllAspectTestProduct(String pName, String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate,
            String[] pFamilies, String[] mktSegments, String[] memberFundings) {
        return new Product(pName, pclasses, ptype, peffective, pend, pStatus, pDueDate, pFamilies, mktSegments, memberFundings);
    }

    static ProductService createProductService(String productName, String productId, String productServiceName, String serviceId, boolean isCovered,
            boolean preCertificationRequired, boolean includeGlobalCostShare,
            String serviceException) {
        return new ProductService(productName, productId, productServiceName, serviceId, isCovered, preCertificationRequired, includeGlobalCostShare,
                serviceException);
    }

    static Service createService(String serviceName, String category, String subCategory, String level, String serviceStatus, String[] productClasses) {
        return new Service(serviceName, category, subCategory, level, serviceStatus, productClasses);
    }

    static Template createTemplate(String effectiveDate, String endDate, String documentType, String businessEntity) {
        return new Template(effectiveDate, endDate, documentType, businessEntity);
    }

    static Section createSection(String name) {
        return new Section(name);
    }

    static Account createAccount(String accountName, String accountStatus, String accountMarketSegment, String accountSupportPhoneNumber, String accountSupportWebsiteAddress) {
        return new Account(accountName, accountStatus, accountMarketSegment, accountSupportPhoneNumber, accountSupportWebsiteAddress);
    }

	static class Product {

	    private String pName;
        private String[] productClasses; // required
		private String[] productTypes;

		private String effectiveDate;
		private String endDate;

		private String productStatus;
		private String productDueDate;

		private String[] productFamilies;
		private String[] marketSegments; // required
	    private String[] memberFundings; // required

	    private String[] providerTierDisplayNames = new String[] {"Test"};

        public Product() {
		}

        public Product(String[] pclasses, String[] ptypeList, String peffective, String pend, String pStatus, String pDueDate) {
            this.pName = "productName" + System.nanoTime(); // ensure unique name
            if (pclasses != null) {
                this.productClasses = new String[pclasses.length];
                System.arraycopy(pclasses, 0, this.productClasses, 0, pclasses.length);
            } else {
                this.productClasses = new String[0];
            }
            this.productTypes = ptypeList;
            this.effectiveDate = peffective;
            this.endDate = pend;
            this.productStatus = pStatus;
            this.productDueDate = pDueDate;
            this.productFamilies = new String[0];
            this.marketSegments = new String[0];
            this.memberFundings = new String[0];
        }

		public Product(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate) {
		    this.pName = "productName" + System.nanoTime(); // ensure unique name
            if (pclasses != null) {
                this.productClasses = new String[pclasses.length];
                System.arraycopy(pclasses, 0, this.productClasses, 0, pclasses.length);
            } else {
                this.productClasses = new String[0];
            }
			this.productTypes = new String[]{ptype};
			this.effectiveDate = peffective;
			this.endDate = pend;
			this.productStatus = pStatus;
			this.productDueDate = pDueDate;
			this.productFamilies = new String[0];
			this.marketSegments = new String[0];
            this.memberFundings = new String[0];
		}

        public Product(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, String[] pFamily) {
            this(pclasses, ptype, peffective, pend, pStatus, pDueDate);
            if (pFamily != null) {
                this.productFamilies = new String[pFamily.length];
                System.arraycopy(pFamily, 0, this.productFamilies, 0, pFamily.length);
            } else {
                this.productFamilies = new String[0];
            }
        }

        // productFamilies and memberFundings and even productType are optional aspects; only productClass and marketSegments are required
        // this constructor takes all of the required and optional props
        public Product(String pName, String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate,
                String[] pFamilies, String[] mktSegments, String[] memberFundings) {

            this(pclasses, ptype, peffective, pend, pStatus, pDueDate, pFamilies);
            this.pName = pName;

            if (mktSegments != null) {
                this.marketSegments = new String[mktSegments.length];
                System.arraycopy(mktSegments, 0, this.marketSegments, 0, mktSegments.length);
            } else {
                this.marketSegments = new String[0];
            }

            if (memberFundings != null) {
                this.memberFundings = new String[memberFundings.length];
                System.arraycopy(memberFundings, 0, this.memberFundings, 0, memberFundings.length);
            } else {
                this.memberFundings = new String[0];
            }
        }

        public String getName() {
            return pName;
        }
		public String[] getProductClasses() {
			return productClasses;
		}
//		public void setProductClass(String productClass) {
//			this.productClass = productClass;
//		}
		public String[] getProductTypes() {
			return productTypes;
		}
		public void setProductTypes(String[] productTypes) {
			this.productTypes = productTypes;
		}
		public String getEffectiveDate() {
			return effectiveDate;
		}
		public void setEffectiveDate(String effectiveDate) {
			this.effectiveDate = effectiveDate;
		}
		public String getEndDate() {
			return endDate;
		}
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
        public String getProductStatus() {
            return productStatus;
        }
        public void setProductStatus(String productStatus) {
            this.productStatus = productStatus;
        }
        public String getProductDueDate() {
            return productDueDate;
        }
        public void setProductDueDate(String productDueDate) {
            this.productDueDate = productDueDate;
        }
        public String[] getProductFamilies() {
            return productFamilies;
        }
        public String[] getMarketSegments() {
            return marketSegments;
        }
        public String[] getMemberFundings() {
            return memberFundings;
        }

		public String[] getProviderTierDisplayNames() {
            return providerTierDisplayNames;
        }

        public void setProviderTierDisplayNames(String[] providerTierDisplayNames) {
            this.providerTierDisplayNames = providerTierDisplayNames;
        }

        @Override
		public String toString() {
			return "Product [productClass=" + productClasses + ", productTypes="
					+ productTypes + ", effectiveDate=" + effectiveDate
					+ ", endDate=" + endDate
					+ ", productStatus=" + productStatus + ", productDueDate=" + productDueDate
	                + ", productFamilies=" + productFamilies
	                + ", marketSegments=" + marketSegments
                    + ", memberFundings=" + memberFundings
                    + ", providerTierDisplayNames=" + providerTierDisplayNames
					+ "]";
		}
	}

	// a class that has "user-defined field" ("UDF"), an extended property
    static class ExtendedModelProduct extends Product {
        private boolean udfproduct_isSmokingCessationStepTherapyRequired;

        public ExtendedModelProduct() {
        }

        public ExtendedModelProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, boolean extendedPropValue) {
            super(pclasses, ptype, peffective, pend, pStatus, pDueDate);
            this.udfproduct_isSmokingCessationStepTherapyRequired = extendedPropValue;
        }

        public boolean getUdfproduct_isSmokingCessationStepTherapyRequired() {
            return udfproduct_isSmokingCessationStepTherapyRequired;
        }
    }
	// a class that has an unknown/undefined property - not natively defined nor in any aspect
	static class BrokenTestProduct extends Product {
        private String noWayThisPropExists7734UpsideDown;

        public BrokenTestProduct() {
        }

        public BrokenTestProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, String unknownPropValue) {
            super(pclasses, ptype, peffective, pend, pStatus, pDueDate);
            this.noWayThisPropExists7734UpsideDown = unknownPropValue;
        }

        public String getNoWayThisPropExists7734UpsideDown() {
            return noWayThisPropExists7734UpsideDown;
        }

        public void setNoWayThisPropExists7734UpsideDown(String noWayThisPropExists7734UpsideDown) {
            this.noWayThisPropExists7734UpsideDown = noWayThisPropExists7734UpsideDown;
        }

	}

    // a class that has a property defined in an Aspect - actually many of the props here are already defined in aspects, don't need a 'base' aspect
    static class AspectTestProduct extends Product {
//        private boolean hidden;  // defined in sys:hidden aspect  - this must be namespace-qualified

        public AspectTestProduct() {
        }

        public AspectTestProduct(String[] pclasses, String ptype, String peffective, String pend, String pStatus, String pDueDate, boolean hidden) {
            super(pclasses, ptype, peffective, pend, pStatus, pDueDate);
//            this.hidden = hidden;
        }

//        public boolean getHidden() {
//            return hidden;
//        }
//
//        public void setHidden(boolean hidden) {
//            this.hidden = hidden;
//        }
    }

    static class ProductService {

        // these are required:
        private String productName;
        private String productServiceName;
        private String productId;
        private String serviceId;
        private boolean isCovered;
        private boolean preCertificationRequired;
        private boolean includeGlobalCostShare;
        private String serviceException; // not required

        public ProductService() {
        }

        public ProductService(String productName, String productId, String productServiceName, String serviceId, boolean isCovered,
                boolean preCertificationRequired, boolean includeGlobalCostShare,
                String serviceException) {
            super();
            this.productName = productName;
            this.productId = productId;
            this.productServiceName = productServiceName;
            this.serviceId = serviceId;
            this.isCovered = isCovered;
            this.preCertificationRequired = preCertificationRequired;
            this.includeGlobalCostShare = includeGlobalCostShare;
            this.serviceException = serviceException;
        }

        public String getProductId() {
            return productId;
        }

        public String getServiceId() {
            return serviceId;
        }

        public boolean getIsCovered() {
            return isCovered;
        }

        public boolean getPreCertificationRequired() {
            return preCertificationRequired;
        }

        public boolean getIncludeGlobalCostShare() {
            return includeGlobalCostShare;
        }

        public String getServiceException() {
            return serviceException;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductServiceName() {
            return productServiceName;
        }

        public void setProductServiceName(String productServiceName) {
            this.productServiceName = productServiceName;
        }

    }

    static class Service {

        private String serviceName;
        private String category;
        private String subCategory;
        private String level;
        private String serviceStatus;       // New, Draft, Under Review, Approved, Denied
        private String[] productClasses;    // required; Medical, Dental, Vision, Pharmacy Combination
        private String effectiveDate;       // required as of latest model changes
        private String[] productTypes;      // required as of latest model changes


        public Service() {
        }

        public Service(String serviceName, String category, String subCategory, String level, String serviceStatus, String[] productClasses) {
            super();
            this.serviceName = serviceName;
            this.category = category;
            this.subCategory = subCategory;
            this.level = level;
            this.serviceStatus = serviceStatus;
            this.productClasses = productClasses;
            this.effectiveDate = "2015-12-31T00:00:00+00:00"; // just hard-code
                                                              // for now
            this.productTypes = new String[] { "HMO", "PPO" }; // just hard-code for now
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getCategory() {
            return category;
        }

        public String getSubCategory() {
            return subCategory;
        }

        public String getLevel() {
            return level;
        }

        public String getServiceStatus() {
            return serviceStatus;
        }

        public String[] getProductClasses() {
            return productClasses;
        }

        public String getEffectiveDate() {
            return effectiveDate;
        }

        public String[] getProductTypes() {
            return productTypes;
        }
    }

    static class Template {
        private String effectiveDate;   // required
        private String endDate;
        private String documentType;    // SBC, SOB, ...
        private String businessEntity;  // Product, Plan, Rider

        public Template(String effectiveDate, String endDate, String documentType, String businessEntity) {
            super();
            this.effectiveDate = effectiveDate;
            this.endDate = endDate;
            this.documentType = documentType;
            this.businessEntity = businessEntity;
        }

        public String getEffectiveDate() {
            return effectiveDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public String getDocumentType() {
            return documentType;
        }

        public String getBusinessEntity() {
            return businessEntity;
        }
    }

    static class Section {

        private String name;

        public Section() {
        }

        public Section(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    static class Account {
        private String accountName;                 // required
        private String accountNumber="";
        private String accountStatus;               // required
        private String marketSegment;               // required
        private String[] primaryContactEmails= new String[]{"abc@test.highroads.com"};
        private String supportPhoneNumber;          // required
        private String supportWebsiteAddress;       // required
        private String renewalStartDate="2016-07-07T00:00:00.000-05:00";
        private String renewalEndDate="2016-07-07T00:00:00.000-05:00";

        public Account(String accountName, String accountStatus, String marketSegment, String supportPhoneNumber,
                String supportWebsiteAddress) {
            this.accountName = accountName;
            this.accountStatus = accountStatus;
            this.marketSegment = marketSegment;
            this.supportPhoneNumber = supportPhoneNumber;
            this.supportWebsiteAddress = supportWebsiteAddress;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public String getAccountStatus() {
            return accountStatus;
        }

        public void setAccountStatus(String accountStatus) {
            this.accountStatus = accountStatus;
        }

        public String getMarketSegment() {
            return marketSegment;
        }

        public void setMarketSegment(String marketSegment) {
            this.marketSegment = marketSegment;
        }

        public String[] getPrimaryContactEmails() {
            return primaryContactEmails;
        }

        public void setPrimaryContactEmails(String[] primaryContactEmails) {
            this.primaryContactEmails = primaryContactEmails;
        }

        public String getSupportPhoneNumber() {
            return supportPhoneNumber;
        }

        public void setSupportPhoneNumber(String supportPhoneNumber) {
            this.supportPhoneNumber = supportPhoneNumber;
        }

        public String getSupportWebsiteAddress() {
            return supportWebsiteAddress;
        }

        public void setSupportWebsiteAddress(String supportWebsiteAddress) {
            this.supportWebsiteAddress = supportWebsiteAddress;
        }

        public String getRenewalStartDate() {
            return renewalStartDate;
        }

        public void setRenewalStartDate(String renewalStartDate) {
            this.renewalStartDate = renewalStartDate;
        }

        public String getRenewalEndDate() {
            return renewalEndDate;
        }

        public void setRenewalEndDate(String renewalEndDate) {
            this.renewalEndDate = renewalEndDate;
        }
    }
}
