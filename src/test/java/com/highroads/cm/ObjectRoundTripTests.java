package com.highroads.cm;

/**
 * Test class for pulling objects out of the system and pushing them back in.
 * @author atingley
 * @version
 * @see
 * @since
 */

import static com.highroads.cm.DomainObjectFactory.createProduct;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.cm.DomainObjectFactory.Product;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.jayway.restassured.module.mockmvc.response.ValidatableMockMvcResponse;
import com.jayway.restassured.path.json.JsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ObjectRoundTripTests
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

    private String tenantsUri;
    private String productsUri;
    private String tenantFolderName;
    private String tenantObjectId;

    @Autowired
    private WebApplicationContext wac;

    @PostConstruct
    public void init() {
        logger.debug("init() called");
        tenantsUri = "/organizations";
        productsUri = "/products";
    }

    // create single tenant for all tests
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
                contentType("application/json").
                body(testTenant).
            when().
                post(tenantsUri).
            andReturn().as(HashMap.class);
        // @formatter:on

        tenantObjectId = resultMap.get("objectId").toString();

        logger.info("== created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        // @formatter:off
        given().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(tenantsUri + "/" + tenantObjectId).
        then().statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Test
    public void testPullThenPatchThenCreateObject() {
        logger.info("in testPullThenPatchThenCreateObject");

        String prodClasses[] = new String[] {"Medical"};
        String[] prodTypeList = new String[]{"HMO"};
        String startDate = "2015-01-01T00:00:00-05:00";
        String endDate = "2015-12-31T00:00:00-05:00";
        String pStatus = "Draft";       // New, Draft, Under Review, Approved, Denied
        String dueDate = "2016-01-01T00:00:00-05:00";
        Product testProd = createProduct(prodClasses, prodTypeList, startDate, endDate, pStatus, dueDate);

        // instantiate a test Product object
        String objId = doCreateProduct(testProd);

        logger.info("== product created with id " + objId);

        // now retrieve it - as JSON ...
        JsonPath jpath =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            when().get(productsUri + "/" + objId).
                andReturn().body().jsonPath();

        String productJson = jpath.prettyPrint();
        logger.debug("retrieved test Product JSON: " + productJson);

        // ... and turn around and push it back as a PATCH to do an update,
        // in this case is the identity transform (we're not changing anything)
        // this isn't really all that reasonable but can happen
        ValidatableMockMvcResponse resp =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(productJson).
            when().
                patch(productsUri + "/" + objId).
            then().
                statusCode(HttpServletResponse.SC_OK);

        // now create a new object with the retrieved object as its source, just change the name
        Map<String,Object> objectMap = jpath.get();
        String origName = objectMap.get("name").toString();
        logger.debug("original object name: " + origName);
        String newName = origName + "_" + System.currentTimeMillis();
        logger.debug("setting new object name to original + timestamp: " + newName);
        objectMap.put("name", newName);
        objectMap.remove("objectId"); // if not removed, will encounter CONFLICT error

        // ... and turn around and push it back as a POST - creates a new object
        resp =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(objectMap).
            when().
                post(productsUri).
            then().
                statusCode(HttpServletResponse.SC_CREATED);
    }

    private String doCreateProduct(Product product) {
        // instantiate a test Product object
        String objId =
            given().
                log().all().
                headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                contentType("application/json").
                body(product).
            when().
                post(productsUri).
            andReturn().body().asString();

        return objId;
    }
}
