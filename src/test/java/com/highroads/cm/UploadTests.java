package com.highroads.cm;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.web.controller.DocumentMetadata;
import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class UploadTests {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    private String tenantFolderName;
    private String tenantObjectId;
    private String tenantsUri;

    @Before
    public void setUp() {
        tenantsUri = "/organizations";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        tenantFolderName = "tenant.test." + System.currentTimeMillis();

        String testTenant = "{\"orgId\":\"" + tenantFolderName + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + tenantFolderName + "\", \"orgName\":\""
                + tenantFolderName + "\"}";

        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap = given().log().all()
                .headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).contentType("application/json")
                .body(testTenant).when().post(tenantsUri).andReturn().as(HashMap.class);

        tenantObjectId = resultMap.get("id").toString();

        logger.info("== 0 created tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @After
    public void tearDown() {
        // delete the test tenant
        given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String("p2a.com", "admin")).when()
                .delete(tenantsUri + "/" + tenantObjectId).then().statusCode(HttpServletResponse.SC_OK);

        logger.info("== 0 deleted tenant " + tenantFolderName + ", id: " + tenantObjectId);
    }

    @Test
    public void testDocumentUpAndDownloads() throws IOException {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("documentFormat", "Word"); // Word, PDF
        props.put("documentStatus", "Final"); // Draft, Final, Outdated, Expired
        props.put("endDate", "2015-01-01T00:00:00-05:00"); // aspect-defined
                                                           // property
        doTestUpAndDownloads("document", props);
    }

    @Test
    public void testDocumentUpAndDownloadsNewMetadata() throws IOException {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("documentFormat", "Word"); // Word, PDF
        props.put("documentStatus", "Final"); // Draft, Final, Outdated, Expired
        props.put("endDate", "2015-01-01T00:00:00-05:00"); // aspect-defined
                                                           // property
        doTestUpAndDownloads("document", props, false); // new metadata format
    }

    @Test
    public void testTemplateUpAndDownloads() throws IOException {
        // arbitrarily test with the no-namespace version
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("lastUsed", "2014-12-31T00:00:00-05:00");
        props.put("templateStatus", "Published"); // Draft, Published, Expired
        props.put("effectiveDate", "2016-12-31T00:00:00-05:00");
        doTestUpAndDownloadsWithoutNamespace("template", props);
    }

    @Test
    public void testTemplateUpAndDownloadsWithDescription() throws IOException {
        String description = "description property set at time of template creation";

        // arbitrarily test with the no-namespace version
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("lastUsed", "2014-12-31T00:00:00-05:00");
        props.put("templateStatus", "Published"); // Draft, Published, Expired
        props.put("description", description);
        props.put("effectiveDate", "2016-12-31T00:00:00-05:00");
        String objId = doTestUpAndDownloadsWithoutNamespace("template", props);

        // now retrieve metadata and test for description
        Map<String, Object> result = getObject("/templates", objId);
        String resultDescription = result.get("description").toString();
        assertTrue(description.equals(resultDescription));

        // update the description
        description = "updated description after template creation";
        String patchJson = "{ \"description\" : \"" + description + "\" }";
        patchObject("/templates", objId, patchJson);

        // now retrieve the updated metadata and test for new description
        result = getObject("/templates", objId);
        resultDescription = result.get("description").toString();
        assertTrue(description.equals(resultDescription));
    }

    @Test
    public void testTemplateUpAndDownloadsDates() throws IOException {
        // arbitrarily test with the no-namespace version
        Map<String, Object> props = new HashMap<String, Object>();

        String inputLastUsed = "2014-12-31T00:00:00-05:00";
        props.put("lastUsed", inputLastUsed); // ISO formatted date
        props.put("templateStatus", "Published"); // Draft, Published, Expired
        props.put("effectiveDate", "2016-12-31T00:00:00-05:00");
        String objId = doTestUpAndDownloadsWithoutNamespace("template", props);
        HashMap<String, Object> doc = getContentMetaData("/templates", objId);

        OffsetDateTime expectedLastUsedDateTime = OffsetDateTime.parse(inputLastUsed, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        assertTrue(expectedLastUsedDateTime.isEqual(OffsetDateTime.parse((String) doc.get("lastUsed"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
    }

    @Test
    public void testTemplateUpAndDownloadsAspectProps() throws IOException {
        // arbitrarily test with the no-namespace version
        Map<String, Object> props = new HashMap<String, Object>();

        String inputLastUsed = "2014-12-31T00:00:00-05:00";
        props.put("lastUsed", inputLastUsed); // ISO formatted date
        String inputEffDate = "2015-01-01T03:30:04";
        String extendedEffDate = inputEffDate + ".965+00:00";
        props.put("effectiveDate", extendedEffDate);
        String inputEndDate = "2015-12-31T00:00:00-05:00";
        props.put("endDate", inputEndDate);
        props.put("templateStatus", "Published"); // Draft, Published, Expired
        String objId = doTestUpAndDownloadsWithoutNamespace("template", props);
        HashMap<String, Object> doc = getContentMetaData("/templates", objId);

        OffsetDateTime expectedEffectiveDateTime = OffsetDateTime.parse(extendedEffDate, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
        OffsetDateTime expectedEndDateTime = OffsetDateTime.parse(inputEndDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        assertTrue(expectedEffectiveDateTime.isEqual(OffsetDateTime.parse((String) doc.get("effectiveDate"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
        assertTrue(expectedEndDateTime.isEqual(OffsetDateTime.parse((String) doc.get("endDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))));
    }

    @Test
    public void testSectionUpAndDownloads() throws IOException {
        doTestUpAndDownloads("section", null);
    }

    @Test
    public void testSectionUpAndDownloadsNewMetadata() throws IOException {
        doTestUpAndDownloads("section", null, false); // new metadata format
    }

    @Test
    public void testUpAndDownloadsAuthQueryParam() throws IOException {
        doTestUpAndDownloadsQueryParam("document", null);
    }

    @Test
    public void testFileUpAndDownloads() throws IOException {
        // arbitrarily test with the no-namespace version
        doTestUpAndDownloadsWithoutNamespace("file", null);
    }

    /*
     * Test with full Document metadata as retrieved with GET
     * /documents/<docId>:
     *
     * { "author": "Mohamed Maamoun", "businessEntity": "Plan", "content": {
     * "contentUrl":
     * "store://2015/10/30/12/38/121cec2b-0c1b-4d5a-851e-91908c7940c7.bin",
     * "mimetype":
     * "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
     * , "size": 8403, "encoding": "UTF-8", "locale": "en_US", "id": 988,
     * "infoUrl":
     * "contentUrl=store://2015/10/30/12/38/121cec2b-0c1b-4d5a-851e-91908c7940c7.bin|mimetype=application/vnd.openxmlformats-officedocument.wordprocessingml.document|size=8403|encoding=UTF-8|locale=en_US_"
     * }, "createdBy": "asmith@highroads.com", "creationDate":
     * "2015-10-30T16:38:16", "documentFormat": "Word", "documentStatus":
     * "Draft", "documentType": "SBC", "effectiveDate": "2015-10-30T04:00:00",
     * "endDate": "2015-11-03T05:00:00", "fundingArrangements": [
     * "Fully Insured", "ASO" ], "isUnit": false, "lastModificationDate":
     * "2015-10-30T16:38:16", "lastModifiedBy": "asmith@highroads.com",
     * "lastThumbnailModification": [ "doclib:1446223096740" ],
     * "marketSegments": [ "Individual Under 65", "Individual Over 65",
     * "Small Group", "Mid Group", "Large Group", "National Account", "Student"
     * ], "name": "NA-test_plan-SBC_635818054950219114", "numProviderTiers": 8,
     * "numSources": 1, "objectId": "d7de77d2-9dbf-4cb5-a835-75a46d0fafb5",
     * "objectType": "document", "productClasses": [ "Medical", "Dental",
     * "Vision", "Pharmacy" ], "productTypes": [ "HMO" ], "versionLabel": "1.0"
     * }
     */
    @Test
    public void testFullDocumentMetadataUpload() {

        String jsonStringMetadata = "{"
                + "\"author\": \"Mohamed Maamoun\","
                + "\"businessEntity\": \"Plan\","
                + "\"content\": {"
                + "\"contentUrl\": \"store://2015/10/30/12/38/121cec2b-0c1b-4d5a-851e-91908c7940c7.bin\","
                + "\"mimetype\": \"application/vnd.openxmlformats-officedocument.wordprocessingml.document\","
                + "\"size\": 8403,"
                + "\"encoding\": \"UTF-8\","
                + "\"locale\": \"en_US\","
                + "\"id\": 988,"
                + "\"infoUrl\": \"contentUrl=store://2015/10/30/12/38/121cec2b-0c1b-4d5a-851e-91908c7940c7.bin|mimetype=application/vnd.openxmlformats-officedocument.wordprocessingml.document|size=8403|encoding=UTF-8|locale=en_US_\""
                + "}," + "\"createdBy\": \"asmith@highroads.com\"," + "\"creationDate\": \"2015-10-30T16:38:16-05:00\"," + "\"documentFormat\": \"Word\","
                + "\"documentStatus\": \"Final\"," + "\"documentType\": \"SBC\"," + "\"effectiveDate\": \"2015-10-30T00:00:00-05:00\","
                + "\"endDate\": \"2015-11-03T00:00:00-05:00\"," + "\"fundingArrangements\": [" + "\"Fully Insured\"," + "\"ASO\"" + "]," + "\"isUnit\": false,"
                + "\"lastModificationDate\": \"2015-10-30T16:38:16-05:00\"," + "\"lastModifiedBy\": \"asmith@highroads.com\","
                + "\"lastThumbnailModification\": [" + "\"doclib:1446223096740\"" + "]," + "\"marketSegments\": [" + "\"Individual Under 65\","
                + "\"Individual Over 65\"," + "\"Small Group\"," + "\"Mid Group\"," + "\"Large Group\"," + "\"National Account\"," + "\"Student\"" + "],"
                + "\"name\": \"NA-test_plan-SBC_635818054950219114\"," + "\"numProviderTiers\": 8," + "\"numSources\": 1,"
                + "\"objectId\": \"d7de77d2-9dbf-4cb5-a835-75a46d0fafb5\"," + "\"objectType\": \"document\"," + "\"productClasses\": [" + "\"Medical\","
                + "\"Dental\"," + "\"Vision\"," + "\"Pharmacy\"" + "]," + "\"productTypes\": [" + "\"HMO\"" + "]," + "\"versionLabel\": \"1.0\"" + "}";

        try {
            HashMap<String, Object> metadata = new ObjectMapper().readValue(jsonStringMetadata, HashMap.class);
            doTestUpAndDownloads("document", metadata, false);
        } catch (IOException e) {
            e.printStackTrace();
            fail("failed to parse metadata string into JSON");
        }
    }

    private void patchObject(String baseUri, String objId, String patchJson) {
        // @formatter:off
        given().
            log().all().
            headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
            contentType("application/json").
            body(patchJson).
        when().
            patch(baseUri + "/" + objId);
    }

    private Map<String,Object> getObject(String baseUri, String objId) {

        // @formatter:off
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap =
                given().
                    headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).
                when().
                    get(baseUri + "/" + objId).
                andReturn().as(HashMap.class);
        // @formatter:on

        return resultMap;
    }

    // wrapper around old way of sending metadata - default is old way for
    // backward-compatibility
    private void doTestUpAndDownloads(String objectType, Map<String, Object> metadataProps) throws IOException {
        doTestUpAndDownloads(objectType, metadataProps, true);
    }

    @SuppressWarnings("unchecked")
    private void doTestUpAndDownloads(String objectType, Map<String, Object> metadataProps, boolean backCompatMode) throws IOException {
        // upload testing

        logger.info("== 1 post " + objectType);

        // create a temp file for testing
        long timestamp = System.currentTimeMillis();
        final String originalFileName = "test-" + timestamp + ".txt";
        final byte[] originalContent = ("multipart test" + timestamp).getBytes();

        File file = tmpFolder.newFile(originalFileName);

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(originalContent);
        fos.close();

        // construct the meta data
        // empty properties is not allowed by Alfresco
        Map<String, Object> props = new HashMap<>();
        if (metadataProps != null) {
            props.putAll(metadataProps);
        }
        props.put("name", file.getName()); // set our own test name, overwriting
                                           // if necessary

        Object objectMetadata = null;
        if (backCompatMode) {
            DocumentMetadata metaData = new DocumentMetadata();
            metaData.setProperties(props);
            objectMetadata = metaData;
        } else {
            objectMetadata = props;
        }

        String objectTypeUri = "/" + objectType + "s";

        // post the request to upload the file
        @SuppressWarnings("unchecked")
        String objId = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).multiPart("file", file)
                .multiPart("metadata", objectMetadata, "application/json").when().post(objectTypeUri).andReturn().body().asString();

        assertTrue(!objId.startsWith("{")); // error string starts with "{"
        logger.info("== document created with id " + objId);

        // download the file and verify its content
        byte[] contentDownloaded = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(objectTypeUri + "/" + objId + "/content").andReturn().body().asByteArray();

        Assert.assertArrayEquals(originalContent, contentDownloaded);

        // download the metadata and verify
        // TODO verify application metadata after they are added to the content
        // model
        Map<String, Object> resultMap = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(objectTypeUri + "/" + objId).andReturn().as(HashMap.class);

        Assert.assertTrue(objId.startsWith(resultMap.get("objectId").toString()));
        Assert.assertTrue(file.getName().equals(resultMap.get("name").toString()));

        doTestMetadata(objectTypeUri, objId);
    }

    private HashMap<String, Object> getContentMetaData(String objectTypeUri, String objId) {
        @SuppressWarnings("unchecked")
        HashMap<String, Object> resultMap = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName))
                .when().get(objectTypeUri + "/" + objId).andReturn().as(HashMap.class);
        return resultMap;
    }

    // test for downloading content with userToken passed as query parameter
    // instead of Authorization header;
    // for require.js functionality
    // TODO: Add test that includes both Authorization header AND query param;
    // Authorization header takes precedence
    // TODO: Add test that includes neither Authorization header NOR query
    // param; should be rejected (BAD_REQUEST)
    private void doTestUpAndDownloadsQueryParam(String objectType, Map<String, Object> metadataProps) throws IOException {
        // upload testing

        logger.info("== 1 post " + objectType);

        // create a temp file for testing
        long timestamp = System.currentTimeMillis();
        final String originalFileName = "test-" + timestamp + ".txt";
        final byte[] originalContent = ("multipart test" + timestamp).getBytes();

        File file = tmpFolder.newFile(originalFileName);

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(originalContent);
        fos.close();

        // construct the meta data
        DocumentMetadata metaData = new DocumentMetadata();

        // empty properties is not allowed by Alfresco
        Map<String, Object> props = new HashMap<>();
        props.put("name", file.getName());
        if (metadataProps != null) {
            props.putAll(metadataProps);
        }
        metaData.setProperties(props);

        String objectTypeUri = "/" + objectType + "s";

        // post the request to upload the file
        String objId = given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).multiPart("file", file)
                .multiPart("metadata", metaData, "application/json").when().post(objectTypeUri).andReturn().body().asString();

        logger.info("== document created with id " + objId);

        // download the file and verify its content; usertoken is passed as
        // query param instead of Authorization header
        byte[] contentDownloaded = given().queryParam("usertoken", TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(objectTypeUri + "/" + objId + "/content").andReturn().body().asByteArray();

        Assert.assertArrayEquals(originalContent, contentDownloaded);
    }

    //
    private String doTestUpAndDownloadsWithoutNamespace(String objectType, Map<String, Object> metadataProps) throws IOException {
        // upload testing

        logger.info("== 1 post " + objectType);

        // create a temp file for testing
        long timestamp = System.currentTimeMillis();
        final String originalFileName = "test-" + timestamp + ".txt";
        final byte[] originalContent = ("multipart test" + timestamp).getBytes();

        File file = tmpFolder.newFile(originalFileName);

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(originalContent);
        fos.close();

        // construct the meta data
        DocumentMetadata metaData = new DocumentMetadata();

        // empty properties is not allowed by Alfresco
        Map<String, Object> props = new HashMap<>();
        props.put("name", file.getName());
        if (metadataProps != null) {
            props.putAll(metadataProps);
        }
        metaData.setProperties(props);

        String objectTypeUri = "/" + objectType + "s";

        // post the request to upload the file
        String objId = given()
                .headers(GenericController.AUTHORIZATION_HEADER_NAME,
                        TokenHelper.getTokenBase64String(tenantFolderName, "admin", new String[] { "HRSuperAdmin" })).multiPart("file", file)
                .multiPart("metadata", metaData, "application/json").

                when().post(objectTypeUri).andReturn().body().asString();

        logger.info("== document created with id " + objId);

        // download the file and verify its content
        byte[] contentDownloaded = given()
                .headers(GenericController.AUTHORIZATION_HEADER_NAME,
                        TokenHelper.getTokenBase64String(tenantFolderName, "admin", new String[] { "HRSuperAdmin" })).when()
                .get(objectTypeUri + "/" + objId + "/content").andReturn().body().asByteArray();

        Assert.assertArrayEquals(originalContent, contentDownloaded);

        // download the metadata and verify
        // TODO verify application metadata after they are added to the content
        // model
        String strippedObjId = objId;
        given().headers(GenericController.AUTHORIZATION_HEADER_NAME,
                TokenHelper.getTokenBase64String(tenantFolderName, "admin", new String[] { "HRSuperAdmin" })).when().get(objectTypeUri + "/" + objId).then()
                .body("objectId", containsString(strippedObjId)).and().body("name", equalTo(file.getName()));

        doTestMetadata(objectTypeUri, objId);
        return objId;
    }

    private void doTestMetadata(String objectTypeUri, String objId) {
        if ("/templates".equals(objectTypeUri)) {
            doTestTemplateMetadata(objectTypeUri, objId);
        } else if ("/documents".equals(objectTypeUri)) {
            doTestDocumentMetadata(objectTypeUri, objId);
        } else {
            logger.warn("no metadata tests implemented for objectTypeUri: " + objectTypeUri);
        }
    }

    private void doTestTemplateMetadata(String objectTypeUri, String objId) {
        given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(objectTypeUri + "/" + objId).then().body("templateStatus", equalTo("Published")).and().body("lastUsed", startsWith("2014-12-31"));
    }

    private void doTestDocumentMetadata(String objectTypeUri, String objId) {
        given().headers(GenericController.AUTHORIZATION_HEADER_NAME, TokenHelper.getTokenBase64String(tenantFolderName)).when()
                .get(objectTypeUri + "/" + objId).then().body("documentFormat", equalTo("Word")).and().body("documentStatus", equalTo("Final"));
    }
}
