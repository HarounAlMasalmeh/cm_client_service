package com.highroads.cm.version;

import static org.junit.Assert.fail;

import java.lang.reflect.Method ;

import com.highroads.commons.sso.UserToken ;

public class GenericReflectiveTester {

    private Class<?>[] paramTypes           = { UserToken.class, String.class, String.class } ;
    private Class<?>[] getHistoryParamTypes = { UserToken.class, String.class, String.class, Integer.class, Integer.class } ;
    private Class<?>[] revertParamTypes     = { UserToken.class, String.class, String.class, String.class, String.class } ;

    private UserToken   userToken   ;
    private String      objectType  ;
    private String      objectId    ;

    // ----------------------------------------------------------------------------------------------------

    public GenericReflectiveTester( UserToken userToken, String objectType, String objectId ) {
        super() ;
        this.userToken = userToken ;
        this.objectType = objectType ;
        this.objectId = objectId ;
    }

    public void testInvliadArgumentsFor( Object object, String methodName ) throws Exception {

        Method method = null ;
        Class<? extends Object> clazz = Class.forName( object.getClass().getName() ) ;
        method = clazz.getMethod( methodName, paramTypes ) ;

        testNullToken        ( method, getBadParams( null,          objectType,     objectId ) ) ;
        testNullObjectType   ( method, getBadParams( userToken,     null,           objectId ) ) ;
        testEmptyObjectType  ( method, getBadParams( userToken,     "",             objectId ) ) ;
        testUnknownObjectType( method, getBadParams( userToken,     "banana",       objectId ) ) ;

        testNullObjectId     ( method, getBadParams( userToken,     objectType,     null     ) ) ;
        testEmptyObjectId    ( method, getBadParams( userToken,     objectType,     ""       ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testInvliadOffsetLimitArgumentsFor( Object object, String methodName ) throws Exception {

        Method method = null ;
        Class<? extends Object> clazz = Class.forName( object.getClass().getName() ) ;
        method = clazz.getMethod( methodName, getHistoryParamTypes ) ;

        testNullToken            ( method, getBadParams( null,          objectType,     objectId,   1,      1    ) ) ;
        testNullObjectType       ( method, getBadParams( userToken,     null,           objectId,   1,      1    ) ) ;
        testEmptyObjectType      ( method, getBadParams( userToken,     "",             objectId,   1,      1    ) ) ;
        testUnknownObjectType    ( method, getBadParams( userToken,     "banana",       objectId,   1,      1    ) ) ;
        testNullObjectId         ( method, getBadParams( userToken,     objectType,     null,       1,      1    ) ) ;
        testEmptyObjectId        ( method, getBadParams( userToken,     objectType,     "",         1,      1    ) ) ;
        testNegativeOffset       ( method, getBadParams( userToken,     objectType,     objectId,  -1,      1    ) ) ;
        testNegativeLimit        ( method, getBadParams( userToken,     objectType,     objectId,   1,     -1    ) ) ;
        testNoOffsetNegativeLimit( method, getBadParams( userToken,     objectType,     objectId,   null,  -1    ) ) ;
        testTooBigOffset         ( method, getBadParams( userToken,     objectType,     objectId,   1000,   1    ) ) ;
        testTooBigLimit          ( method, getBadParams( userToken,     objectType,     objectId,   1,      1000 ) ) ;
        testNoOffsetTooBigLimit  ( method, getBadParams( userToken,     objectType,     objectId,   null,   1000 ) ) ;
        testGoodOffsetNoLimit    ( method, getBadParams( userToken,     objectType,     objectId,   1,      null ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testInvliadVersionIdArgumentFor( Object object, String methodName ) throws Exception {

        Method method = null ;
        Class<? extends Object> clazz = Class.forName( object.getClass().getName() ) ;
        method = clazz.getMethod( methodName, revertParamTypes ) ;


        testNullToken        ( method, getBadParams( null,          objectType,     objectId,   "1"    ) ) ;

        testNullObjectType   ( method, getBadParams( userToken,     null,           objectId,   "1"    ) ) ;
        testEmptyObjectType  ( method, getBadParams( userToken,     "",             objectId,   "1"    ) ) ;
        testUnknownObjectType( method, getBadParams( userToken,     "banana",       objectId,   "1"    ) ) ;

        testNullObjectId     ( method, getBadParams( userToken,     objectType,     null,       "1"    ) ) ;
        testEmptyObjectId    ( method, getBadParams( userToken,     objectType,     "",         "1"    ) ) ;

        testNegativeVersionId( method, getBadParams( userToken,     objectType,     objectId,   "-2"   ) ) ;
        testNegativeVersionId( method, getBadParams( userToken,     objectType,     objectId,   "0"    ) ) ;
        testTooBigVersionId  ( method, getBadParams( userToken,     objectType,     objectId,   "1000" ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    private Object[] getBadParams( UserToken userToken, String objectType, String objectId ) {

        Object[] params = new Object[ 3 ] ;

        params[ 0 ] = userToken ;
        params[ 1 ] = objectType ;
        params[ 2 ] = objectId ;

        return params ;
    }

    // ----------------------------------------------------------------------------------------------------

    private Object[] getBadParams( UserToken userToken, String objectType, String objectId, Integer offset, Integer limit ) {

        Object[] params = new Object[ 5 ] ;

        params[ 0 ] = userToken ;
        params[ 1 ] = objectType ;
        params[ 2 ] = objectId ;
        params[ 3 ] = offset ;
        params[ 4 ] = limit ;

        return params ;
    }

    // ----------------------------------------------------------------------------------------------------

    private Object[] getBadParams( UserToken userToken, String objectType, String objectId, String versionId ) {

        Object[] params = new Object[ 4 ] ;

        params[ 0 ] = userToken ;
        params[ 1 ] = objectType ;
        params[ 2 ] = objectId ;
        params[ 3 ] = versionId ;

        return params ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNullToken( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with null userToken. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNullObjectType( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with null objectType. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    private void testEmptyObjectType( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with empty objectType. params: %s", method.getName(), stringParams( params ) ) ) ;
    }


    // ----------------------------------------------------------------------------------------------------

    public void testNullObjectId( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with null objectId. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testEmptyObjectId( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with empty objectId. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testUnknownObjectType( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with unknown objectType. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNullObjectProperties( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with null object properties. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNegativeVersionId( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with negative versionId. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testTooBigVersionId( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with big versionId. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNegativeOffset( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with negative offset. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNegativeLimit( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with negative limit. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNoOffsetNegativeLimit( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with no offset and negative limit. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testTooBigOffset( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with too big offset. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testTooBigLimit( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with too big limit. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testNoOffsetTooBigLimit( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with no offset and too big limit. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    public void testGoodOffsetNoLimit( Method method, Object[] params ) {

        try {
            method.invoke( null, params ) ;
        } catch( Exception e ) {
            return ;
        }
        fail( String.format( "%s: should have failed with good offset and missing limit parameter. params: %s", method.getName(), stringParams( params ) ) ) ;
    }

    // ----------------------------------------------------------------------------------------------------

    private String stringParams( Object[] params ) {

        StringBuilder sb = new StringBuilder() ;
        sb.append( "[" ) ;
        for ( int i = 0; i < params.length; i ++ ) {
            sb.append( params[i] ) ;
            if ( i < (params.length - 1) ) {
                sb.append( ", " ) ;
            }
        }
        sb.append( "]" ) ;

        return sb.toString() ;
    }

}
