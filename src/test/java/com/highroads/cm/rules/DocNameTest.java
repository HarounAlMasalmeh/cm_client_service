package com.highroads.cm.rules;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.StatelessKieSession;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.highroads.cm.Application;

@RunWith(MockitoJUnitRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class DocNameTest {

    @InjectMocks
    private RulesEngineServiceImpl rulesService;

    private static Map<String, Object> docNameParams;

    @Before
    public void setupParams() {
        Map<String, Object> rxRider = new HashMap<>();
        rxRider.put("udfplan_benefitID", "1F12");
        Map<String, Object> dentalVision = new HashMap<>();
        dentalVision.put("name", "EB02");
        Map<String, Object> offering = new HashMap<>();
        offering.put("SCID", "5C1D");
        offering.put("udfOffering_erisaPlan", true);
        offering.put("effectiveDate", "2017-11-01T00:00:00.000-05:00");
        offering.put("endDate", "2018-10-01T00:00:00.000-05:00");

        docNameParams = new HashMap<>();
        docNameParams.put("docType", "SBC");
        docNameParams.put("rxPlan", rxRider);
        docNameParams.put("dentalVisionPlan", dentalVision);
        docNameParams.put("offering", offering);
        docNameParams.put("srcDateFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        docNameParams.put("targetDateFormat", "MMyy");
    }

    @Test
    public void sbcIndividual() {
        docNameParams.put("medPlan", getIndivMedPlan());
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("XAE81_PPO_RX1F12_EB02_2017_5C1D"));
    }

    @Test
    public void sbcIndividualWithoutRider() {
        docNameParams.remove("rxPlan");
        docNameParams.remove("dentalVisionPlan");
        docNameParams.put("medPlan", getIndivMedPlan());
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("XAE81_PPO_2017_5C1D"));
    }

    @Test
    public void sbcSmallAca() {
        docNameParams.put("medPlan", getSmallAcaMedPlan());
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("XAE81_PPO_RX1F12_EB02_1117_1018_5C1D"));
    }

    @Test
    public void sbcSmallMuw() {
        docNameParams.put("medPlan", getSmallMuwMedPlan());
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("XAE81_PPO_RX1F12_EB02_1117_1018"));
    }

    @Test
    public void sbcLargeCustomPostSale() {
        docNameParams.put("medPlan", getLargePostSaleMedPlan());
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("XAE81_PPO_RX1F12_EB02_1117_1018"));
    }

    @Test
    public void sbcLargeCustomPreSale() {
        docNameParams.put("medPlan", getLargePreSaleMedPlan());
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("GROUPHIGHROADS_tempCode1_XAE81_PPO_RX1F12_EB02_1117_1018"));
    }

    @Test
    public void sbcLargeCustomPreSaleNonErisa() {
        docNameParams.put("medPlan", getLargePreSaleMedPlan());
        ((Map) docNameParams.get("offering")).put("udfOffering_erisaPlan", false);
        StatelessKieSession ksession = rulesService.getRulesSession("upmc.com", "documentName");
        ksession.execute(docNameParams);
        assertThat(docNameParams.get("docName"), is("GROUPHIGHROADS_tempCode1_XAE81_PPO_RX1F12_EB02_1117_1018_NE"));
    }

    private Map<String, Object> getMedPlan() {
        Map<String, Object> medPlan = new HashMap<>();
        medPlan.put("udfplan_planCode", "XAE81");
        medPlan.put("productTypes", Arrays.asList("PPO"));
        medPlan.put("planYear", Arrays.asList("2017"));
        return medPlan;
    }

    private Map<String, Object> getIndivMedPlan() {
        Map<String, Object> indivMedPlan = getMedPlan();
        indivMedPlan.put("marketSegments", Arrays.asList("Individual Under 65"));
        return indivMedPlan;
    }

    private Map<String, Object> getSmallAcaMedPlan() {
        Map<String, Object> smallAcaMedPlan = getMedPlan();
        smallAcaMedPlan.put("marketSegments", Arrays.asList("Small Group"));
        smallAcaMedPlan.put("grandfatheredPlan", false);
        return smallAcaMedPlan;
    }

    private Map<String, Object> getSmallMuwMedPlan() {
        Map<String, Object> smallMuwMedPlan = getMedPlan();
        smallMuwMedPlan.put("marketSegments", Arrays.asList("Small Group"));
        smallMuwMedPlan.put("grandfatheredPlan", true);
        return smallMuwMedPlan;
    }

    private Map<String, Object> getLargePostSaleMedPlan() {
        Map<String, Object> largePostSaleMedPlan = getMedPlan();
        largePostSaleMedPlan.put("marketSegments", Arrays.asList("Large Group"));
        largePostSaleMedPlan.put("name", "plan name includes plan code XAE81");
        return largePostSaleMedPlan;
    }

    private Map<String, Object> getLargePreSaleMedPlan() {
        Map<String, Object> largePreSaleMedPlan = getMedPlan();
        largePreSaleMedPlan.put("marketSegments", Arrays.asList("Large Group"));
        largePreSaleMedPlan.put("name", "plan name includes year 2019");
        largePreSaleMedPlan.put("udfplan_groupName", "GROUPHIGHROADS");
        return largePreSaleMedPlan;
    }

}
