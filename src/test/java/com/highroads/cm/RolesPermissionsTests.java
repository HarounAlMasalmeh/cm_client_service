package com.highroads.cm;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.highroads.commons.InstanceTestClassListener;
import com.highroads.commons.ObjectFactory;
import com.highroads.commons.SpringInstanceTestClassRunner;
import com.highroads.commons.TokenHelper;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringInstanceTestClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Ignore("Revisit")
public class RolesPermissionsTests implements InstanceTestClassListener {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String organizationsUri;

    private static String organizationId;
    private static String organizationObjectId;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @SuppressWarnings("unchecked")
    @Override
    public void beforeClassSetUp() {
        organizationsUri = "/organizations";

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        RestAssuredMockMvc.mockMvc = mockMvc;

        organizationId = "rolespermissionstests_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd_HH-mm-ss"));

        String testOrganization = "{\"orgId\":\"" + organizationId + "\", \"orgStatus\":\"Active\", \"orgNamespace\":\"" + organizationId
                + "\", \"orgName\":\"" + organizationId + "\"}";

        // @formatter:off
        HashMap<String, Object> resultMap = given().
                                                log().all().
                                                headers("Authorization", TokenHelper.getTokenBase64String("p2a.com", "admin")).
                                                contentType("application/json").
                                                body(testOrganization).
                                            when().
                                                post("/organizations").
                                            andReturn().
                                                as(HashMap.class);
        // @formatter:on

        organizationObjectId = resultMap.get("id").toString();

        logger.info("== Organization successfully created - name: {}, objectId: {}", organizationId, organizationObjectId);

    }

    @Override
    public void afterClassTearDown() {
        // @formatter:off
        given().
            log().all().
            headers("Authorization", TokenHelper.getTokenBase64String("p2a.com", "admin")).
        when().
            delete(organizationsUri + "/" + organizationObjectId).
        then().
            statusCode(HttpServletResponse.SC_OK);
        // @formatter:on

        logger.info("== Organization successfully deleted - name: {}, objectId: {}", organizationId, organizationObjectId);
    }

    @SuppressWarnings("serial")
    @Test
    public void validateReadAllPermission() {
        // Create Standard plan
        HashMap<String, Object> standardPlan = new HashMap<String, Object>();
        standardPlan.put("name", "StandardPlan1");
        standardPlan.put("planType", "Standard");

        // @formatter:off
        String standardPlanObjectId = given().
                                            log().all().
                                            headers("Authorization", TokenHelper.getTokenBase64String(organizationId, "admin", new String[]{"Claims"})).
                                            contentType("application/json").
                                            body(ObjectFactory.createPlan(standardPlan)).
                                        when().
                                            post("/plans").
                                        andReturn().
                                            asString();
        // @formatter:on
        logger.info("== Plan successfully created - objectId: {}", standardPlanObjectId);

        // Create Standard plan
        HashMap<String, Object> customPlan = new HashMap<String, Object>();
        customPlan.put("name", "CustomPlan1");
        customPlan.put("planType", "Custom");

        // @formatter:off
        String customPlanObjectId = given().
                                        log().all().
                                        headers("Authorization", TokenHelper.getTokenBase64String(organizationId, "admin", new String[]{"Claims"})).
                                        contentType("application/json").
                                        body(ObjectFactory.createPlan(customPlan)).
                                    when().
                                        post("/plans").
                                    andReturn().
                                        asString();
        // @formatter:on
        logger.info("== Plan successfully created - objectId: {}", customPlanObjectId);
    }
}
