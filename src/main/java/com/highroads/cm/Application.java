package com.highroads.cm;

import com.highroads.p2alibs.contentservice.DynamicEnumSupport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.highroads.cm.utils.DocNameUtil;
import com.highroads.commons.log.PerformanceLogger;


@SpringBootApplication
@ComponentScan(basePackages = "com.highroads")
public class Application extends SpringBootServletInitializer {

    private static Class<Application> applicationClass = Application.class;

    public static void main(String[] args) {
        SpringApplication.run(com.highroads.cm.Application.class, args);
    }

    @Bean
    public PerformanceLogger performanceLogger() {
        return new PerformanceLogger();
    }

    // <apt> this added to run on external web container
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    // when I run cm service as a Spring bootapp, the following code will add /cm context to the context path.
    // Automation tests rely on /cm context to be present.
    @Bean
    public EmbeddedServletContainerCustomizer embeddedServletContainerCustomizer(@Value("/cm") final String contextPath) {
        return embeddedContainerConfig -> embeddedContainerConfig.setContextPath(contextPath);
    }

    @Bean
    public DocNameUtil docNameUtil() {
        return new DocNameUtil();
    }

  @Bean
  public DynamicEnumSupport dynamicEnumSupport() {
    return new DynamicEnumSupport();
  }
}
