package com.highroads.cm.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.model.P2AEnvironment;

@Component
public class P2AEnvironmentResolver {

    private List<P2AEnvironment> environnentKeys;

    P2AEnvironmentResolver() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        InputStream stream = getClass().getResourceAsStream("/feature-flag-tokens.json");
        environnentKeys = mapper.readValue(stream, new TypeReference<List<P2AEnvironment>>() {
        });
    }

    public P2AEnvironment getEnvironmentKey(String environmentName) {
        return environnentKeys
                .stream().parallel()
                .filter(env -> env.getName().equals(environmentName)).findAny().get();
    }

}
