package com.highroads.cm.impl;


/**
 * Used for both requests and responses to create or place an Alfresco Group in a Tenant's Authority Zone,
 * or to place a user into an existing group.
 * @author atingley
 *
 */
public class Group extends RootGroup {

    private String childName = "";

    public Group() {
    }

    public Group(String orgId, String memberName, String displayName) {
        super(orgId, memberName, displayName);  // member name is name of group (or user) to add to parent group
    }

    /**
     * Alias for returning child group name (backend uses 'memberName')
     * @return
     */
    public String getMemberName() {
        return groupName;
    }

    /**
     * additional more straightforward way to get new field childName
     * @return
     */
    public String getChildName() {
        return childName;
    }
}
