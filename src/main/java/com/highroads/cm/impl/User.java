package com.highroads.cm.impl;


/**
 * Used for both requests and responses to create an Alfresco User in a Tenant's Authority Zone.
 * Required inputs on request: orgId, userName, firstName, lastName, email.
 * Optional: password
 * @author atingley
 *
 */
public class User extends SyncUser {

    protected String firstName = "";
    protected String lastName = "";
    protected String email = "";
    protected String password = "";

    public User() {
    }

    public User(String orgId, String userName, String firstName, String lastName, String email, String password) {
        this.orgId = orgId;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public User(String orgId, String userName, String firstName, String lastName, String email, String password,
            String[] groups, String[] roles, String[] actors) {
        this(orgId, userName, firstName, lastName, email, password);
        setGroups(groups);
        setRoles(roles);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "[User { firstName: " + firstName + ", lastName: " + lastName
                + ", email: " + email + ", password: " + password
                + ", <base>:" + super.toString() + "}]";
    }
}
