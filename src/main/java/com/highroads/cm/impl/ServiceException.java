package com.highroads.cm.impl;

// TODO: move this to commons
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    // TODO: Use Spring constants, move these outta here
    public static final int OK = 200;
    public static final int CREATED = 201;
    public static final int BAD_REQUEST = 400;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int CONFLICT = 409;

    private int status;

    public ServiceException(int status, String message) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
