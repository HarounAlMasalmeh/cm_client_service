package com.highroads.cm.impl;

/**
 * Used for both requests and responses to create Alfresco Authority Zones.
 * Only zoneName is passed on the request, id and url is included in the response.
 * @author atingley
 *
 */

// TODO: I don't think I need this, Zonename is same as orgId...
public class Zone extends ReqRespBase {

    public Zone() {
    }

    public Zone(String zoneName) {
        this.orgId = zoneName;
    }
}
