package com.highroads.cm.impl;


/**
 * Used for both requests and responses to create an Alfresco User in a Tenant's Authority Zone.
 * This class is specifically used to synchronize users with their current roles, group-memberships, and actor assignments.
 * Required inputs on request: orgId, userName; Optional: groups, roles, actors
 * @author atingley
 *
 */
public class SyncUser extends MinimalUser {

    private String[] groups = null;
    private String[] roles = null;
    private int syncStatus;


    public SyncUser() {
    }

    public SyncUser(String orgId, String userName, String[] groups, String[] roles) {
        super(orgId, userName);
        setGroups(groups);
        setRoles(roles);
    }

    public void setGroups(String[] groups) {
        this.groups = (groups != null) ? groups : new String[0];
    }

    public void setRoles(String[] roles) {
        this.roles = (roles  != null) ? roles  : new String[0];
    }

    public String[] getGroups() {
        return groups;
    }

    public String[] getRoles() {
        return roles;
    }

    public int getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }
}
