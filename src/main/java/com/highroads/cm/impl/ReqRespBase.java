package com.highroads.cm.impl;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Used for both requests and responses to create HighRoads-customized Alfresco entities.
 * @author atingley
 *
 */

public class ReqRespBase {

    protected String orgId = ""; // unique organization ID == organization's domain name
    protected String id = "";    // object ID
    protected String url = "";   // object URL
    protected String result = "";// request result
    protected int status;        // HTTP status

    public ReqRespBase() {
    }

    public ReqRespBase(String orgId) {
        assert (orgId != null);
        this.orgId = orgId;
    }

    public String getOrgId() {
        return orgId;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getResult() {
        return result;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
