package com.highroads.cm.impl;

public class DuplicateUserException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 4L;

    public DuplicateUserException(int status, String message) {
        super(status, message);
    }

}
