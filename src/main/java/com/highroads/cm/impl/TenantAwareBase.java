package com.highroads.cm.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.search.MarklogicSearchManager;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.query.QueryDefinition;
import com.marklogic.client.query.QueryManager;

public class TenantAwareBase {

    private final Logger log = org.slf4j.LoggerFactory.getLogger(TenantAwareBase.class);
    @Autowired
    MarklogicSearchManager marklogicSearchManager;
    @Autowired
    MarklogicDAO marklogicDao;
    @Value("${search.service.maxItems}")
    private Integer searchServiceMaxItems;

    public List<JsonNode> getTenants() {
    	List<JsonNode> organizations = new ArrayList<>();
        final QueryManager queryManager = marklogicDao.getDatabaseClient().newQueryManager();
    	final String searchQuery = "TYPE:\"organization\"";
    	QueryDefinition query = null;
    	try {
    		query = marklogicSearchManager.getSearchQueryDefinition("p2a.com", "admin", queryManager, searchQuery);
    	}catch(ParseException pe) {
    		ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    String.format("Error parsing search query / Invalid search query parameter - q: \"%s\"", searchQuery));
    	}
    	Long startIndex = 1L;
    	JsonNode jsonResults;
    	do {
    		JacksonHandle results = queryManager.search(query, new JacksonHandle(), startIndex);
    		jsonResults = results.get();

    		ArrayNode resultsArray = (ArrayNode) jsonResults.findPath("results");
    		for(final JsonNode doc: resultsArray) {
    			organizations.add(doc.get("content"));
    		}
    		startIndex += jsonResults.get("page-length").asInt();
    	} while (startIndex <= jsonResults.get("total").asInt());
    	return organizations;
    }

    protected List<JsonNode> getObjectsForTenant(String userName, String tenantName, String objectType){
        String searchQuery = String.format("TYPE:\"%s\"", objectType);
        Integer startIndex = null;
        final QueryManager queryManager = marklogicDao.getDatabaseClient(userName, tenantName).newQueryManager();
        ArrayList<JsonNode> docsArray = new ArrayList<JsonNode>();
        JsonNode jsonResults;
        QueryDefinition query = null;
        if (startIndex == null) {
            startIndex = 1;
        }
        try {
            query = marklogicSearchManager.getSearchQueryDefinition(tenantName, userName, queryManager, searchQuery);
        } catch (final ParseException e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    String.format("Error parsing search query / Invalid search query parameter - q: \"%s\"", searchQuery));
        }
        do {
            final JacksonHandle results = queryManager.search(query, new JacksonHandle(), Long.valueOf(startIndex));
            jsonResults = results.get();
            final ArrayNode resultsArray = (ArrayNode) jsonResults.findPath("results");
            log.debug(String.format("Search total found: %d", jsonResults.get("total").asLong()));
            log.debug(String.format("Search total time: %s", jsonResults.get("metrics").get("total-time").asText()));
            for (final JsonNode resultDoc : resultsArray) {
                docsArray.add(resultDoc.get("content"));
            }
            if (docsArray.size() == searchServiceMaxItems) {
                log.warn("Exceeded the Max size of Search result");
                break;
            }
            startIndex += jsonResults.get("page-length").asInt();
        } while (startIndex < jsonResults.get("total").asInt());

        return docsArray;
	}

    public MarklogicSearchManager getMarklogicSearchManager() {
        return marklogicSearchManager;
    }

    public void setMarklogicSearchManager(MarklogicSearchManager marklogicSearchManager) {
        this.marklogicSearchManager = marklogicSearchManager;
    }

    public MarklogicDAO getMarklogicDao() {
        return marklogicDao;
    }

    public void setMarklogicDao(MarklogicDAO marklogicDao) {
        this.marklogicDao = marklogicDao;
    }

    public Integer getSearchServiceMaxItems() {
        return searchServiceMaxItems;
    }

    public void setSearchServiceMaxItems(Integer searchServiceMaxItems) {
        this.searchServiceMaxItems = searchServiceMaxItems;
    }
}
