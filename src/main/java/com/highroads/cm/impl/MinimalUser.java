package com.highroads.cm.impl;


/**
 * Used for both requests and responses to create an Alfresco User in a Tenant's Authority Zone.
 * Bare minimum required to create a user.  Currently declared abstract as we don't plan to use this class standalone.
 * Required inputs on request: orgId, userName
 * @author atingley
 *
 */
public abstract class MinimalUser extends ReqRespBase {

    protected String userName = "";

    public MinimalUser() {
    }

    public MinimalUser(String orgId, String userName) {
        super(orgId);
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
