package com.highroads.cm.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.HasLogger;
import com.highroads.cm.MLRestExtensionService;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.sso.UserToken;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.extensions.ResourceManager;
import com.marklogic.client.extensions.ResourceServices;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.util.RequestParameters;

@Component
public class MLRestExtensionServiceImpl implements HasLogger, MLRestExtensionService {

    private final MarklogicDAO marklogicDao;
    private final ObjectMapper objectMapper = new ObjectMapper();

    // tenant-keyed services associated to tenant-specific db connections
    private Map<String, MLServiceWrapper> tenantServices = new HashMap<>();

    private class MLServiceWrapper extends ResourceManager {

        private String organizationId;
        private String extension;

        MLServiceWrapper(String organizationId, String extension) {
            this.organizationId = organizationId;
            this.extension = extension;
        }

        @Override
        public ResourceServices getServices() {
            return super.getServices();
        }
    }

    @Autowired
    public MLRestExtensionServiceImpl(MarklogicDAO marklogicDao) {
        this.marklogicDao = marklogicDao;
    }

    @Override
    public JsonNode executeExtension(UserToken userToken, String extension, String method, Map<String, String> queryMap, Map<String, Object> payload) {

        String username = userToken.getUsername();
        String organizationId = userToken.getOrganization();
        queryMap.put("tenant", organizationId);
        if (payload != null) {
            for (Entry<String, Object> entry : payload.entrySet()) {
                if (entry.getValue() != null) {
                    queryMap.put(entry.getKey(), entry.getValue().toString());
                }
            }
        }

        JsonNode output = null;
        switch (method) {
            case "put":
                output = put(username, organizationId, extension, queryMap, payload);
                break;
            case "get":
                output = get(username, organizationId, extension, queryMap);
                break;
            case "post":
                output = post(username, organizationId, extension, queryMap, payload);
                break;
        }
        return output;
    }

    private JsonNode put(String username, String organizationId, String extension, Map<String, String> parameters, Map<String, Object> payload) {
        return putOrPost(username, organizationId, "put", extension, parameters, payload);

    }

    private JsonNode get(String username, String organizationId, String extension, Map<String, String> parameters) {
        registerExtension(username, organizationId, extension);
        RequestParameters params = buildParameters(extension, parameters);
        ResourceServices services = getTenantServices(organizationId, extension);
        JacksonHandle output = new JacksonHandle();
        services.get(params, output);
        return output.get();

    }

    private JsonNode post(String username, String organizationId, String extension, Map<String, String> parameters, Map<String, Object> payload) {
        return putOrPost(username, organizationId, "post", extension, parameters, payload);
    }

    private JsonNode putOrPost(String username, String organizationId, String method, String extension, Map<String, String> parameters,
            Map<String, Object> payload) {
        registerExtension(username, organizationId, extension);
        RequestParameters params = buildParameters(extension, parameters);
        ResourceServices services = getTenantServices(organizationId, extension);

        if (payload == null) {
            payload = new HashMap<>();
        }
        JacksonHandle input = new JacksonHandle(objectMapper.valueToTree(payload));
        JacksonHandle output = new JacksonHandle();
        if (method.equals("put")) {
            services.put(params, input, output);
        } else {
            services.post(params, input, output);
        }
        return output.get();
    }

    private RequestParameters buildParameters(String extension, Map<String, String> parameters) {
        RequestParameters params = new RequestParameters();
        params.add("service", extension);
        parameters.forEach(params::add);
        return params;
    }

    private void registerExtension(String username, String organizationId, String extension) {
        String tenantServicesKey = getTenantExtensionKey(organizationId, extension);
        if (!tenantServices.containsKey(tenantServicesKey)) {
            MLServiceWrapper services = new MLServiceWrapper(organizationId, extension);
            //DatabaseClient client = marklogicDao.getDatabaseClient(username, organizationId);
            // for now we'll use the admin@domain.com user and grant that tenant admin user ML-admin rights
            DatabaseClient client = marklogicDao.getDatabaseClient(getTenantUsername("admin", organizationId), organizationId);
            client.init(extension, services);
            tenantServices.put(tenantServicesKey, services);
        }
    }

    private ResourceServices getTenantServices(String organizationId, String extension) {
        MLServiceWrapper serviceWrapper = tenantServices.get(getTenantExtensionKey(organizationId, extension));
        if (serviceWrapper != null) {
            return serviceWrapper.getServices();
        } else {
            return null;
        }
    }

    private String getTenantExtensionKey(String organizationId, String extension) {
        return organizationId + "-" + extension;
    }

    private String getTenantUsername(String userpart, String organizationId) {
        return userpart + "@" + organizationId;
    }
}
