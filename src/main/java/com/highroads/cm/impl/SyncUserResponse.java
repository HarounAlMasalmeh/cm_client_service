package com.highroads.cm.impl;

import java.util.HashMap;
import java.util.Map;

/**
 * Used for responses to sync an Alfresco User in a Tenant's Authority Zone.
 * This class is used to marshall the group/role/actor changes that were effected as a result of the user sync.
 * @author atingley
 *
 */
public class SyncUserResponse extends SyncUser {

    private Map<String, Object> updates = new HashMap<String,Object>();
    private Map<String, Object> roleInfo = new HashMap<String,Object>();

    public SyncUserResponse() {
    }

    // for use in TenantService
    public SyncUserResponse(String orgId, String userName, String[] groups, String[] roles) {
        super(orgId, userName, groups, roles);
    }

    public Map<String, Object> getUpdates() {
        return updates;
    }

    public void setUpdates(Map<String, Object> updates) {
        this.updates = updates;
    }

    public Map<String, Object> getRoleInfo() {
        return roleInfo;
    }

    public void setRoleInfo(Map<String, Object> roleInfo) {
        this.roleInfo = roleInfo;
    }

    public String[] getGroupAdds() {
        Map<String,Object> gMap = (Map<String, Object>) updates.get("groups");
        String[] groupAdds = (gMap != null) ? (String[]) gMap.get("adds") : new String[0];
        return groupAdds;
    }

    public String[] getGroupRemoves() {
        Map<String,Object> gMap = (Map<String, Object>) updates.get("groups");
        String[] groupRemoves = (gMap != null) ? (String[]) gMap.get("removes") : new String[0];
        return groupRemoves;
    }

    public String[] getRoleAdds() {
        Map<String,Object> gMap = (Map<String, Object>) updates.get("roles");
        String[] roleAdds = (gMap != null) ? (String[]) gMap.get("adds") : new String[0];
        return roleAdds;
    }

    public String[] getRoleRemoves() {
        Map<String,Object> gMap = (Map<String, Object>) updates.get("roles");
        String[] roleRemoves = (gMap != null) ? (String[]) gMap.get("removes") : new String[0];
        return roleRemoves;
    }

    // TODO: remove these
    public String[] getActorAdds() {
        Map<String,Object> gMap = (Map<String, Object>) updates.get("actors");
        String[] actorAdds = (gMap != null) ? (String[]) gMap.get("adds") : new String[0];
        return actorAdds;
    }

    public String[] getActorRemoves() {
        Map<String,Object> gMap = (Map<String, Object>) updates.get("actors");
        String[] actorRemoves = (gMap != null) ? (String[]) gMap.get("removes") : new String[0];
        return actorRemoves;
    }

//    @Override
//    public String toString() {
//        return "[SyncUser { "
//                + "groupAdds: " + StringUtils.join(getGroupAdds()) + ", groupRemoves: " + StringUtils.join(getGroupRemoves())
//                + ", roleAdds: " + StringUtils.join(getRoleAdds()) + ", roleRemoves: " + StringUtils.join(getRoleRemoves())
//                + ", actorAdds: " + StringUtils.join(getActorAdds()) + ", actorRemoves: " + StringUtils.join(getActorRemoves())
//                + ", <base>:" + super.toString() + "}]";
//    }
}
