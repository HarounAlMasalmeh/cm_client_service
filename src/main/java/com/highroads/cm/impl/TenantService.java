package com.highroads.cm.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import com.highroads.commons.sso.UserToken;

/**
 * HighRoads Customized Tenant Service
 *
 * This service creates and maintains HighRoads Tenants, their Users, Groups and
 * Roles.
 *
 * @author atingley
 */
public interface TenantService {

    /**
     * Create Tenant
     * Creates and bootstraps a tenant (HighRoads customer).
     * Creates root-level folder, sub-folders, Alfresco authority zone, admin
     * group and admin user.
     *
     * @param orgId
     *            - organization ID
     * @param orgRootFolder
     *            - parent folder for this org
     * @param payloadProperties
     *            - other properties for the org
     * @return Map with return params for created Tenant; null if error
     *
     */
    public Map<String, Object> createTenant(String orgId, String orgRootFolder,
            Map<String, Object> payloadProperties);

    /**
     * Get user info - return most data in ApiToken, along with Role definitions
     * (permissions white-list).
     *
     * @param apiToken
     *            - decrypted API token (base64-encoded)
     * @return Map with user's orgId, username, roles, groups, actors; and role
     *         definitions
     */
    public Map<String, Object> getUser(UserToken apiToken); // note: ApiToken is moving to commons

    /**
     * Synchronize user's roles, group-memberships, and actor assignments.
     * The authentication token for a user is the authoritative source for a
     * user's roles, groups,
     * and actor assignments.
     * This method is used to keep those assignments in sync with that
     * authoritative source.
     *
     * @param orgId
     *            - organization ID
     * @param userName
     *            - user's username; we are using the user's email address for
     *            username
     * @param groups
     *            - user's user-group-membership list
     * @param roles
     *            - users' role-membership list
     * @param properties
     *            - optional user properties such as first, last name (NO)
     * @return Map with return params for created User; null if error
     */
    public Map<String, Object> syncUser(final UserToken userToken);

    /**
     * Update the object with the given objectType in the organization
     * specified in the userToken with the provided objectProperties
     *
     * @param userToken
     *            The userToken of the caller
     * @param objectType
     *            The object/entity type to create (prob not needed)
     * @param objectId
     *            The object/entity ID to update
     * @param objectProperties
     *            The properties to be set for the entity
     */
    void updateTenant(UserToken userToken, String objectId, Map<String, Object> objectProperties);

    /**
     * PATCH an tenant - This method supports patch as per
     * https://tools.ietf.org/html/rfc6902
     *
     * @param userToken
     * @param objectType
     * @param objectId
     * @param objectProperties
     */
    void updateTenant(UserToken userToken, String objectId, List<Map<String, Object>> operations);

    /**
     * Delete tenant (yes, this really deletes a tenant at the moment!)
     * In reality should just mark the tenant inactive (TODO: mark as deleted?)
     *
     * @param objectId
     *            - tenant object ID
     * @return Map with return params for deleted tenant (id, status); null if
     *         error
     */
    public Map<String, Object> deleteTenant(String objectId);

    /**
     * Temporary URI to bootstrap the organization to a version
     *
     * @param organization
     * @param currentVersion
     */
    @Deprecated
    /* @deprecated Moved to gdm */
    public void bootstrapOrganization(String organizationId, String currentVersion) throws URISyntaxException, IOException;

}
