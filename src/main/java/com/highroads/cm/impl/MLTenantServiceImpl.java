package com.highroads.cm.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.authorization.AuthorizationManager;
import com.highroads.commons.dao.Document;
import com.highroads.commons.dao.DocumentCoordinates;
import com.highroads.commons.dao.MLJSONDocumentImpl;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.search.MarklogicSearchManager;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.JSONDocumentFactory;
import com.highroads.p2alibs.contentservice.ModelSchemaService;
import com.highroads.p2alibs.security.MLCapability;
import com.highroads.p2alibs.security.MLCollection;
import com.highroads.p2alibs.security.MLExecutePrivilege;
import com.highroads.p2alibs.security.MLPermission;
import com.highroads.p2alibs.security.MLPrivilege;
import com.highroads.p2alibs.security.MLRole;
import com.highroads.p2alibs.security.MLSecurityService;
import com.highroads.p2alibs.security.MLURIPrivilege;
import com.highroads.p2alibs.security.MLUser;
import com.marklogic.client.FailedRequestException;
import com.marklogic.client.ForbiddenUserException;
import com.marklogic.client.ResourceNotFoundException;

// * HIGRDS-14981 : Support for tenant-specific database - force file change,
// git is confused...

@Component
@ConditionalOnProperty(prefix = "marklogic.connection.", name = "required", matchIfMissing = false)
public class MLTenantServiceImpl implements TenantService {

    @Autowired
    private P2AEnvironmentResolver p2aEnvironmentResolver;

    private static final String BOOTSTRAP_KEY = "bootstrapKey";

    private static final String STATUS_JSON_FILE = "status.json";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final MLSecurityService mlSecurityService;
    private final MarklogicSearchManager marklogicSearchManager;
    private final AuthorizationManager authorizationManager;
    private final ModelSchemaService modelSchemaService;

    private static final String NAME = "name";
    private static final String IS_ROLE_ACTIVE = "isRoleActive";
    private static final String SYSTEM_TENANT = "p2a.com";
    private static final String SYSTEM_TENANT_ADMIN_USER = "admin@p2a.com";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private JSONDocumentFactory jsonDocumentFactory;

    private String environment = "local";
    private boolean initialized = false;

    private MarklogicDAO marklogicDAO;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @Autowired
    public MLTenantServiceImpl(final MLSecurityService mlSecurityService, final AuthorizationManager authorizationManager,
            final MarklogicSearchManager marklogicSearchManager,
            final MarklogicDAO marklogicDAO, final ModelSchemaService modelSchemaService) {

        // passing MarklogicDAO to constructor forces it to have already been instantiated, a nice trick...
        // (note it is not used otherwise except in static context)
        // see also http://stackoverflow.com/questions/30454643/how-to-make-autowire-object-initialized-in-applicationlistener
        // inlined here:
        //        @Component
        //        public class AppContextListener implements ApplicationListener<ContextRefreshedEvent> {
        //            @Override
        //            public void onApplicationEvent(ContextRefreshedEvent event) {
        //                ApplicantContext context = event.getApplicationContext();
        //                MyElasticsearchRepository repository = context.getBean(MyElasticSearchRepository.class);
        //                //do stuff
        //            }
        //        }

        this.mlSecurityService = mlSecurityService;
        this.authorizationManager = authorizationManager;

        this.marklogicSearchManager = marklogicSearchManager;
        this.modelSchemaService = modelSchemaService;
        this.marklogicDAO = marklogicDAO;
        this.environment = marklogicDAO.getSecurityEnvironment();
    }

    @EventListener
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        // Check if at least the p2a organization exists
        // If not, log a warning and return as GDM will need to be initiated first

        String dbAdminUser = marklogicDAO.getConnectionConfig().getAdminUser();

        DocumentCoordinates organizationCoords = new DocumentCoordinates(SYSTEM_TENANT, P2ACollection.ORGANIZATIONS, null);
        Document orgCount = MLJSONDocumentImpl.getInstance(dbAdminUser,
                new HashMap<String, Object>(), marklogicDAO, organizationCoords);
        if (!orgCount.isExists("orgId", "*")) {
            logger.warn("CM Service: system not initialized, no tenants found, GDM Service and initial bootstrap must execute");
        } else {
            logger.info("CM Service: initialized successfully");
        }

    }

    @Override
    public Map<String, Object> createTenant(final String orgId, final String orgRootFolder, final Map<String, Object> payloadProperties) {

        final List<MLPrivilege> mlPrivileges = new ArrayList<>();

        //create uri privilege
        final MLPrivilege mlPrivilege = new MLURIPrivilege(orgId + "-uri-privilege", orgId);
        if (this.mlSecurityService.find(new MLURIPrivilege(orgId + "-uri-privilege")) == null) {
            this.mlSecurityService.create(mlPrivilege);
        }
        mlPrivileges.add(mlPrivilege);

        //create a role with rest-reader, rest-writer execution privilege
        mlPrivileges.add(new MLExecutePrivilege("rest-writer"));

        // all documents created by this role will be marked with following collection
        // tenant admin 'pseudo-user' also has marklogic 'admin' role for running some ML extensions
        final MLRole mlRole = new MLRole(environment, orgId + "-role",
                Arrays.asList(new MLRole[] { new MLRole(environment, "p2a.com-read-role"), new MLRole(StringUtils.EMPTY, "admin") }),
                mlPrivileges,
                Arrays.asList(new MLCollection[] { new MLCollection(orgId) }));
        if (this.mlSecurityService.find(new MLRole(environment, orgId + "-role")) == null) {
            this.mlSecurityService.create(mlRole);
        }

        //add default permissions for documents created by this role
        final List<MLPermission> permissions = new ArrayList<>();
        permissions.add(new MLPermission(mlRole, MLCapability.READ));
        permissions.add(new MLPermission(mlRole, MLCapability.UPDATE));
        this.mlSecurityService.update(new MLRole(StringUtils.EMPTY, mlRole.getName(), permissions));

        //create tenant admin user
        final MLUser mlUser = new MLUser(environment, "admin@" + orgId, Arrays.asList(new MLRole[] { mlRole }));
        if (this.mlSecurityService.find(new MLUser(environment, "admin@" + orgId)) == null) {
            this.mlSecurityService.create(mlUser);
        }

        /* Organization should always be created as p2a admin */
        final MLUser mlAdminP2A = this.mlSecurityService.find(new MLUser(environment, "admin@" + "p2a.com"));
        // Document doc = JSONDocumentFactory.create(orgId, mlUser.getName(),
        // "organizations", payloadProperties);
        Document doc = jsonDocumentFactory.create(orgId, mlAdminP2A.getName(), P2ACollection.ORGANIZATIONS, payloadProperties);

        if (doc.isExists("orgId", (String) payloadProperties.get("orgId"))) { // 'orgId' given in object map conflicts with existing object
            ExceptionHelper.throwException(HttpStatus.CONFLICT, HrErrorCode.CONFLICT,
                    String.format("An object with this orgId already exists: %s", payloadProperties.get("orgId")));
        }
        if (doc.isExists("orgNamespace", (String) payloadProperties.get("orgNamespace"))) { // 'orgNamepsace' given in object map conflicts with existing object
            ExceptionHelper.throwException(HttpStatus.CONFLICT, HrErrorCode.CONFLICT,
                    String.format("An object with this orgNamespace already exists: %s", payloadProperties.get("orgNamespace")));
        }

        String objectId = doc.create();
        final Map<String, Object> result = new HashMap<>();
        result.put("objectId", objectId);

        /*
         * Bootstrapping is now supported by GDM. New tenant will get
         * bootstrapped when gdm comes up.
         */
        /*
         * TODO : In future, send a notification to GDM for new tenant so that
         * it will bootstrap it.
         */
        /*
        //@formatter:off
        try {
            ArrayList<Version> bootstrapVersions = bootstrapManager.buildBootstrapVersionList();
            if (0 == bootstrapVersions.size()) {
                logger.error("bootstrap folder cannot be found or is empty.");
            } else
                bootstrapManager.bootstrapOrganization(orgId, null, objectId, bootstrapVersions);
        } catch (URISyntaxException e) {
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR,
                    HrErrorCode.INTERNAL_SERVER_ERROR,
                    String.format("Error bootstrapping organization - organizationId: \"%s\" - error: \"%s\"", orgId,
                            e.getMessage()));
        } catch (IOException e) {
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR,
                    HrErrorCode.INTERNAL_SERVER_ERROR,
                    String.format("Error bootstrapping organization - organizationId: \"%s\" - error: \"%s\"", orgId,
                            e.getMessage()));
        }*/
        //@formatter:on
        return result;
    }

    @Override
    public Map<String, Object> getUser(final UserToken userToken) {

        final Map<String, Object> result = new HashMap<>();
        final List<Map<String, Object>> userRoles = getValidUserRoles(userToken);
        result.put("user", userToken);
        result.put("roleDefinitions", userRoles);
        result.put("environment", p2aEnvironmentResolver.getEnvironmentKey(environment));
        return result;
    }

    private List<Map<String, Object>> getValidUserRoles(UserToken userToken) {

        List<String> tokenRoles = userToken.getRoles();
        final List<Map<String, Object>> userRoles = new ArrayList<>();

        // at some point we should decide if we want to merge token roles with configured roles,
        // or replace token with configured;
        // at this point we only look for configured user roles if the token contains no roles whatsoever
        if (null == tokenRoles || tokenRoles.size() == 0) {
            // no roles assigned to this user - fall back to user config in tenant, if any
            insertMissingUserRoles(userToken);
            tokenRoles = userToken.getRoles();
            // If still no roles, just return
            if (null == tokenRoles || tokenRoles.size() == 0) {
                return userRoles;
            }
        }

        final List<Map<String, Object>> validRoles = authorizationManager.getAllRoles(userToken.getOrganization());
        for (final String strRoleName : tokenRoles) {
            for (final Map<String, Object> role : validRoles) {
                if (role.get(NAME) != null && role.get(NAME).equals(strRoleName) && role.get(IS_ROLE_ACTIVE).equals(true)) {
                    logger.debug("User has role:" + strRoleName);
                    userRoles.add(role);
                }
            }
        }
        return userRoles;
    }

    @SuppressWarnings("unchecked")
    private void insertMissingUserRoles(UserToken userToken) {
        try {
            // Insert the role information if it is not present
            if (userToken.getRoles() == null || userToken.getRoles().size() == 0) {
                logger.info("No role assignments in userToken for user, checking for local config, user: " + userToken.getUsername());

                // TODO: Need a way to tell read to look exclusively in tenant, NOT in system tenant (p2a.com) if not found in user tenant

                DocumentCoordinates userCoordinates = new DocumentCoordinates(userToken.getOrganization(), P2ACollection.USERS, userToken.getUsername());

                Map<String, Object> userData = MLJSONDocumentImpl
                        .getInstance("admin", marklogicDAO, userCoordinates).read();
                if (userData != null) {
                    logger.debug("... found");

                    if (userData.containsKey("userName")) {
                        logger.debug("... setting username: " + userData.get("userName").toString());

                        userToken.setUsername((String) userData.get("userName"));
                    }

                    if (userData.containsKey("roles")) {
                        logger.debug("... setting roles: " + userData.get("roles").toString());

                        userToken.setRoles((List<String>) userData.get("roles"));
                    }

                    // take user's first and last name from the local user base, if configured;
                    // note this will overwrite that given in the SSO token
                    // (this code is here for the case where first and last name are NOT given in the token)
                    if (userData.containsKey("firstName")) {
                        logger.debug("... setting firstName: " + userData.get("firstName").toString());

                        userToken.setFirstName(userData.get("firstName").toString());
                    }

                    if (userData.containsKey("lastName")) {
                        logger.debug("... setting lastName: " + userData.get("lastName").toString());

                        userToken.setLastName(userData.get("lastName").toString());
                    }
                } else {
                    logger.warn("no local user configuration, username {}", userToken.getUsername());
                }
            }
        } catch (Exception e) {
            logger.info("No user role information for user: " + userToken.getUsername());
        }
    }

    @Override
    public Map<String, Object> syncUser(final UserToken userToken) {

        final List<Map<String, Object>> validUserRoles = getValidUserRoles(userToken);
        if (validUserRoles.size() > 0) {
            final MLUser mlUser = mlSecurityService.find(new MLUser(environment, userToken.getUsername()));
            if (null == mlUser) {
                logger.debug("syncUser: creating username " + userToken.getUsername() + ", environment: " + environment);
                mlSecurityService.create(new MLUser(environment, userToken.getUsername(),
                        Arrays.asList(new MLRole[] { new MLRole(environment, userToken.getOrganization() + "-role") })));
            } else {
                logger.debug("user already exists: " + userToken.getUsername());
            }
        } else {
            userToken.setRoles(Collections.emptyList());
        }

        final Map<String, Object> result = new HashMap<>();
        result.put("user", userToken);

        return result;
    }

    @Override
    public Map<String, Object> deleteTenant(final String objectId) {

        // <apt> can't delete the uri-privilege, these are shared across all environments hosted on the same server
        //        mlSecurityService.delete(new MLURIPrivilege(environment, objectId + "-uri-privilege"));
        final List<MLUser> mlUsers = mlSecurityService.findUsersByRole(new MLRole(environment, objectId + "-role"));
        mlSecurityService.delete(mlUsers);
        mlSecurityService.delete(new MLRole(environment, "objectId"));

        final Map<String, Object> result = new HashMap<>();
        result.put("deleted", objectId);

        return result;
    }

    @Deprecated
    /* @deprecated Bootstrapping is now supported by GDM */
    // not sure what to do with this?
    @Override
    public void bootstrapOrganization(final String organizationId, final String currentVersion)
            throws URISyntaxException, IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTenant(UserToken userToken, String objectId, List<Map<String, Object>> operations) {
        try {
            P2ACollection objectType = P2ACollection.ORGANIZATIONS;

            final JsonNode object = jsonDocumentFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId)
                    .load();
            final JsonPatch patch = MAPPER.convertValue(operations, JsonPatch.class);
            final JsonNode patchedJsonNode = patch.apply(object);
            @SuppressWarnings("unchecked")
            final Map<String, Object> patchedObject = MAPPER.convertValue(patchedJsonNode, Map.class);
            modelSchemaService.validateDocument(userToken, objectType, patchedObject);
            jsonDocumentFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId, patchedObject).write();
        } catch (final ResourceNotFoundException e) {
            logger.warn(e.getMessage(), e);
            ExceptionHelper.throwException(HttpStatus.NOT_FOUND, HrErrorCode.NOT_FOUND,
                    String.format("The requested object not found - type: organization, objectId: %s", objectId));
        } catch (final ForbiddenUserException e) {
            logger.warn(e.getMessage(), e);
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED,
                    String.format("You don't have permission to request - type: organization, objectId: %s", objectId));
        } catch (final FailedRequestException | JsonPatchException | IOException | ProcessingException e) {
            logger.error(e.getMessage(), e);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR,
                    String.format("Error while updating object type: organization, objectId: %s", objectId));
        }
    }

    @Override
    public void updateTenant(UserToken userToken, String objectId, Map<String, Object> objectProperties) {
        try {
            P2ACollection collectionId = P2ACollection.ORGANIZATIONS;

            final Map<String, Object> document = jsonDocumentFactory
                    .create(userToken.getOrganization(), userToken.getUsername(), collectionId, objectId).read();
            document.putAll(objectProperties);
            //validate document after patch, do we have to version the schema also? what if we patch a document that was created as per an old schema?
            modelSchemaService.validateDocument(userToken, collectionId, document);
            jsonDocumentFactory.create(userToken.getOrganization(), userToken.getUsername(), collectionId, objectId, document).write();

        } catch (final ResourceNotFoundException e) {
            logger.warn(e.getMessage(), e);
            ExceptionHelper.throwException(HttpStatus.NOT_FOUND, HrErrorCode.NOT_FOUND,
                    String.format("The requested object not found - type: organization, objectId: %s", objectId));
        } catch (final ForbiddenUserException e) {
            logger.warn(e.getMessage(), e);
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED,
                    String.format("You don't have permission to request - type: organization, objectId: %s", objectId));
        } catch (final FailedRequestException | IOException | ProcessingException e) {
            logger.error(e.getMessage(), e);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR,
                    String.format("Error while updating object type: organization, objectId: %s", objectId));
        }
    }

}
