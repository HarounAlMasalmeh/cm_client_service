package com.highroads.cm.impl;

/**
 * Used for both requests and responses to create an Alfresco Root Group in a Tenant's Authority Zone.
 * @author atingley
 *
 */
public class RootGroup extends ReqRespBase {

    protected String groupName = "";    // names root group, or child group
    protected String displayName = "";

    public RootGroup() {
    }

    public RootGroup(String orgId, String groupName, String displayName) {
        super(orgId);
        this.groupName = groupName;
        this.displayName = (displayName != null) ? displayName : "";
    }

    public String getGroupName() {
        return groupName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
