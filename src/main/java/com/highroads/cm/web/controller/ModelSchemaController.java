package com.highroads.cm.web.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ModelSchemaService;

@RestController
public class ModelSchemaController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ModelSchemaService modelSchemaService;

    /**
     * Method to retrieve the Alfresco Model schema in Json format
     *
   * @param objectType       The objectType of the model object. The
     *            namespace in the objectType is not required and should be
     *            omitted. e.g. "/types/plan", "types/product"
   * @param includeInherited Specifies if inherited properties are to be included in the
     *            response. <tt>true</tt> to include the inherited properties,
     *            <tt>false</tt> otherwise.
   * @param includeAll       Specifies if cmis properties are to be included in the
     *            response. <tt>true</tt> to include the cmis properties,
     *            <tt>false</tt> otherwise they are filtered based on the
     *            property file <tt>ModelMapping.properties</tt>.
     * @return The Json representation of the schema model object
     * @throws Exception
     */
    @RequestMapping(value = "/types/{objectType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ObjectNode> getObjectSchema(@PathVariable("objectType") String objectTypeId,
            @RequestParam(value = "includeInherited", required = false, defaultValue = "true") boolean includeInherited,
            @RequestParam(value = "includeAll", required = false, defaultValue = "false") boolean includeAll,
            @RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {
        logger.trace("getObjectSchema - objectTypeId: {}, includeInherited: {}, includeAll: {}", objectTypeId, includeInherited, includeAll);

        UserToken userToken = UserToken.deserialize(authorization);
        List<ObjectNode> nodeList = modelSchemaService.getObjectSchema(userToken, objectTypeId, includeInherited, includeAll);
        return nodeList;
    }

    /**
     * Method to retrieve all the p2a model object types
     *
     * @return The Json representation of all the schema model types supported
     * @throws Exception
     */
    @RequestMapping(value = "/types", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Object> getObjectSchemaTypes(@RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {
        logger.trace("getObjectSchemaTypes");

        UserToken userToken = UserToken.deserialize(authorization);
        List<Object> nodeList = modelSchemaService.getObjectSchemaTypes(userToken);
        return nodeList;
    }

    /**
     * Method to retrieve the run-time (in-memory) representation of the
     * tenant's "model.json"; will include tenant-managed extensions, if any
     *
     * @return The Json representation of the tenant data model "model.json",
     *         overlaid with any tenant model extensions
     * @throws Exception
     */
    @RequestMapping(value = "/model", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getModel(@RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization)
            throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        Map<String, Object> modelMap = modelSchemaService.getModel(userToken);
        return new ResponseEntity<>(modelMap, HttpStatus.OK);
    }

    /**
     * Method to retrieve the run-time (in-memory) representation of the
     * tenant's "cmtypes.json"; will include tenant-managed extensions, if any
     *
     * @return The Json representation of the tenant data model "cmtypes.json",
     *         overlaid with any tenant model extensions
     * @throws Exception
     */
    @RequestMapping(value = "/cmtypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getCmtypes(@RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization)
            throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        Map<String, Object> cmtypes = modelSchemaService.getCMTypes(userToken);
        return new ResponseEntity<>(cmtypes, HttpStatus.OK);
    }

    /**
     * Method to retrieve the run-time (in-memory) representation of the
     * tenant's "model-schema-dictionary.json"; will include tenant-managed
     * extensions, if any
     *
     * @return The Json representation of the tenant data model
     *         "model-schema-dictionary.json",
     *         overlaid with any tenant model extensions
     * @throws Exception
     */
    @RequestMapping(value = "/schema", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getSchema(@RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        ObjectNode schema = modelSchemaService.getSchema(userToken);
        return new ResponseEntity<>(schema, HttpStatus.OK);
    }

    /**
     * Method to retrieve the run-time (in-memory) representation of the
     * tenant's model constraints, i.e. enum lists.
     * @return The Json representation of the tenant in-memory data model constraints.
     * @throws Exception
     */
    @RequestMapping(value = "/constraints", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getModelConstraints(@RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        ObjectNode schema = modelSchemaService.getModelConstraints(userToken);
        return new ResponseEntity<>(schema, HttpStatus.OK);
    }

    @RequestMapping(value = "/model/reset", method = RequestMethod.POST)
    public ResponseEntity<Void> updateModels(@RequestHeader(value = GenericController.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {
      UserToken userToken = UserToken.deserialize(authorization);
      modelSchemaService.updateModels(userToken);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
