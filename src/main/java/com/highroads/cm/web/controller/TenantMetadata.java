package com.highroads.cm.web.controller;


public class TenantMetadata {
    private String id;

	private String orgDomain;
	private String orgId;
	private String isActive;
	private String isBanned;

	public String getOrgDomain() {
		return orgDomain;
	}
	public void setOrgDomain(String orgDomain) {
		this.orgDomain = orgDomain;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getIsBanned() {
		return isBanned;
	}
	public void setIsBanned(String isBanned) {
		this.isBanned = isBanned;
	}

	@Override
	public String toString() {
		return "TenantMetadata [orgId=" + orgId + ", orgDomain=" + orgDomain + "]";
	}


}
