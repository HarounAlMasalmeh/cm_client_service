/**
 *
 */
package com.highroads.cm.web.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.highroads.commons.api.exceptions.HrErrorHandlerAdvice;


@ControllerAdvice
public class CMErrorHandlerAdvice extends HrErrorHandlerAdvice {

}
