package com.highroads.cm.web.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.impl.TenantService;
import com.highroads.commons.HrSystem;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.api.exceptions.HrException;
import com.highroads.commons.authorization.AuthorizationManager;
import com.highroads.commons.config.ContentRepositoryProperties;
import com.highroads.commons.model.AdminOverride;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;
import com.highroads.commons.model.AdminOverride ;


// TODO: Javadocs!!
// TODO: Rewrite!!!

@RestController
public class TenantController implements HrSystem {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private TenantService tenantService;
    private AuthorizationManager authorizationManager;
    private ContentRepositoryProperties contentRepositoryProperties;
    private ContentService contentService;

    @Autowired
    public TenantController(TenantService tenantService, AuthorizationManager authorizationManager,
            ContentRepositoryProperties contentRepositoryProperties, ContentService contentService) {
        this.tenantService = tenantService;
        this.authorizationManager = authorizationManager;
        this.contentRepositoryProperties = contentRepositoryProperties;
        this.contentService = contentService;
    }

    @RequestMapping(value = "/organizations", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Map<String, Object> createTenant(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestBody Map<String, Object> payloadProperties) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        if (authorizationManager.isAuthorized(userToken, "POST", P2ACollection.ORGANIZATIONS.getObjectType(), null) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        // TODO: check for orgId, orgName being unique
        Object org = payloadProperties.get("orgId"); // this really is carried on the payload, as invoking user is member of 'system' p2a.com org
        if (null == org) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, "missing required parameter orgId");
        }
        Object namespace = payloadProperties.get("orgNamespace");
        if (null == namespace) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, "missing required parameter orgNamespace");
        }

        // create tenant
        String orgId = (String) org;
        return tenantService.createTenant(orgId, "/organizations", payloadProperties);
    }

    @RequestMapping(value = "/organizations/{objectId}", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> getTenant(@PathVariable String objectId,
            @RequestParam(value = "associationExpansionLevel", required = false) Integer associationExpansionLevel,
            @RequestParam(value = "properties", required = false) String[] properties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        // only allow p2a.com access to orgs; furthermore, provide admin access to orgs
        if (!SYSTEM_ORGANIZATION.equals(userToken.getOrganization())) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        // since we are allowing only p2a admin to invoke this endpoint, and p2a admin needs access to any object,
        // set 'admin override' in request to content service
        Map<String, Object> object = contentService.getObjectById(userToken, objectId, P2ACollection.ORGANIZATIONS,
                associationExpansionLevel, properties, AdminOverride.TRUE, null); // isAdminOverride == true
        return object;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/organizations/{objectId}", method = RequestMethod.PATCH, consumes = "application/json")
    public ResponseEntity<Void> patchTenant(@PathVariable String objectId, @RequestBody Object payloadProperties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        logger.debug("patchTenant - userToken.organizationId: {}, objectId: {}, objectProperties: {}", userToken.getOrganization(),
                objectId, payloadProperties);

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, P2ACollection.ORGANIZATIONS, 0, null);

        if (authorizationManager.isAuthorized(userToken, "PATCH", P2ACollection.ORGANIZATIONS.getObjectType(), object) == false
                || ((String) object.get("orgId")).compareToIgnoreCase(userToken.getOrganization()) != 0) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        //legacy patch
        if (payloadProperties instanceof Map) {
            tenantService.updateTenant(userToken, objectId, (Map<String, Object>) payloadProperties);
        }
        //patch using specification
        else if (payloadProperties instanceof List) {
            tenantService.updateTenant(userToken, objectId, (List<Map<String, Object>>) payloadProperties);
        } else {
            logger.error("patchTenant - invalid input. userToken.organizationId: {}, payloadType {} ",
                    userToken.getOrganization(), payloadProperties.getClass());
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST);
        }

        logger.debug("Successfully patched organization with objectId: {}", objectId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // serialize user-sync result map to string
    public String serialize(Map<String, Object> resultMap) {
        String json;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = mapper.writeValueAsString(resultMap);
        } catch (JsonProcessingException e) {
            json = "{\"status\":\"500\", \"error\":\"Cannot serialize user-sync result\"}";
        }
        return json;
    }

    @RequestMapping(value = "/organizations/{objectId}", method = RequestMethod.DELETE)
    public Map<String, Object> deleteTenant(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @PathVariable String objectId) throws Exception {

        logger.debug("deleting tenant with object id " + objectId);

        UserToken userToken = UserToken.deserialize(authorization);

        logger.debug("deleteTenant() - userToken.organizationId: {}, objectType: {}", userToken.getOrganization(), P2ACollection.ORGANIZATIONS);

        if (authorizationManager.isAuthorized(userToken, "DELETE", P2ACollection.ORGANIZATIONS.getObjectType(), null) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        return tenantService.deleteTenant(objectId);
    }

    // TODO: Don't return all info in UserToken, only that which is "public" and non-org-specific
    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> getUser(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) {

        Map<String, Object> result = null;
        UserToken userToken;
        try {
            userToken = UserToken.deserialize(authorization);
            result = tenantService.getUser(userToken);
        } catch (Exception e) { // TODO: specialize from Exception
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    String.format("problem with apiToken: %s", e.getMessage()));
        }

        return result;
    }

    /**
     * Sync a user based on authoritative data passed in on an authorization
     * header.
     * Format of the header is as follows:
     *
     * {
     * "issuer": "issuing-organization-identifier",
     * "username": "user@domain.com",
     * "organization": "HighRoads",
     * "roles": [
     * "Sales Manager"
     * ],
     * "groups": [
     * "Sales"
     * ],
     * "actors": [
     * "Actor1",
     * "Actor2"
     * ],
     * "notbefore": "yyyy-MM-ddTHH:mm:ssZ",
     * "notOnOrAfter": "yyyy-MM-ddTHH:mm:ssZ",
     * "renewUntil": "yyyy-MM-ddTHH:mm:ssZ",
     * "timetolive": "yyyy-MM-ddTHH:mm:ssZ", // really a clock-time, not an
     * interval
     * "namespace": "org-namespace"
     * }
     *
     * From Spring docs: @RequestMapping with Headers: We can specify the
     * headers that should be present to invoke the handler method.
     * http://www.journaldev.com/3358/spring-mvc-requestmapping-annotation-
     * example-with-controller-methods-headers-params-requestparam-pathvariable
     *
     * @return
     */
    @RequestMapping(value = "/users", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<Map<String, Object>> syncUser(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) {

        // TODO only GW should be calling this method

        UserToken userToken = null;
        Map<String, Object> result = null;
        try {
            userToken = UserToken.deserialize(authorization);

            result = tenantService.syncUser(userToken);
            List<String> roles = userToken.getRoles();
            if (null == roles || roles.size() == 0) {
                logger.error("apiToken does not specify roles, and no role config found, username: " +
                        userToken.getUsername());
                ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED,
                        String.format("No valid role assignments for user: %s", userToken.getUsername()));
            }
        } catch (HrException e) {
            throw e;
        } catch (Exception e) {
            logger.error("syncUser: unexpected exception:", e);
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    String.format("problem with apiToken: %s", e.getMessage()));
        }

        // there is a better way to do this, maybe next time (would want to use better technique all the way from back-end)
        // http://springinpractice.com/2013/10/07/handling-json-error-object-responses-with-springs-resttemplate
        //        if (!status.series().equals(HttpStatus.Series.SUCCESSFUL)) {
        //            String serialResult = serialize(result);
        //            HrErrorCode error = (status.series().equals(HttpStatus.Series.SERVER_ERROR)) ? HrErrorCode.INTERNAL_SERVER_ERROR : HrErrorCode.UNAUTHORIZED;
        //            ExceptionHelper.throwException(status, error, serialResult);
        //        }
        if (userToken != null) {
            logger.info("successfully synchronized user, username: {}, organization {}", userToken.getUsername(), userToken.getOrganization());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{objectId}", method = RequestMethod.DELETE)
    public Map<String, Object> deleteUser(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @PathVariable String objectId) {

        // TODO: incorporate authorization header
        logger.debug("deleting user with object id " + objectId);
        return new HashMap<>();
    }

    @RequestMapping(value = "/bootstrap/{currentVersion}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public boolean bootstrapOrganization(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @PathVariable String currentVersion) throws Exception {
        UserToken userToken;
        try {
            userToken = UserToken.deserialize(authorization);

            tenantService.bootstrapOrganization(userToken.getOrganization(), currentVersion);
        } catch (URISyntaxException | IOException e) {
            throw e;
        } catch (Exception e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    String.format("Invalid Api Token: %s", e.getMessage()));
        }

        return true;
    }
}
