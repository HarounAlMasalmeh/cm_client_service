package com.highroads.cm.web.controller;

import java.util.Map;

public class DocumentMetadata {

	private String location;
	private String owner;
	private Map<String, Object> properties;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Map<String, Object> getProperties() {
		return properties;
	}
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	@Override
	public String toString() {
		return "DocumentMetadata [location=" + location + ", owner=" + owner
				+ ", properties=" + properties + "]";
	}
	
	
}
