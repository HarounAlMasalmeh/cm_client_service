package com.highroads.cm.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.highroads.cm.MLRestExtensionService;
import com.highroads.cm.rules.RuleService;
import com.highroads.commons.HrSystem;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.api.exceptions.HrException;
import com.highroads.commons.authorization.AuthorizationManager;
import com.highroads.commons.model.ObjectRequest;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.commons.web.controller.BaseController;
import com.highroads.p2alibs.constants.P2ALibConstants;
import com.highroads.p2alibs.contentservice.ContentService;
import com.highroads.p2alibs.patchutil.PatchUtil;
import com.highroads.scripting.ScriptService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import com.highroads.eventsapi.Event;
import com.highroads.eventsapi.EventService;

@RestController
public class GenericController extends BaseController implements HrSystem { // HrSystem -> SystemConstants?

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private AuthorizationManager authorizationManager;
    private ContentService contentService;

    @Autowired
    EventService ea;

    @Autowired
    private MLRestExtensionService restExtensionService;

    @Autowired
    public GenericController(AuthorizationManager authorizationManager, ContentService contentService) {
        this.authorizationManager = authorizationManager;
        this.contentService = contentService;
    }

    @Autowired
    private RuleService ruleService;

    @Value("${content.service.batch.max}")
    private int maxBatchApiCount;

    @Value("${content.service.send_event.enabled}")
    private Boolean sendEvent = true;

    @Autowired
    private PatchUtil patchUtil;

    @Autowired
    private ScriptService scriptService;

    @RequestMapping(value = "/rolesMap", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Map<String,Object>> getRolesMap(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization)
            throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        TreeMap<String, List<String>> rolesMap = authorizationManager.getRolesMap(userToken.getOrganization());
        Map<String,Object> response = new HashMap<>();
        response.put(userToken.getOrganization(), rolesMap);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Get object collection.
     *
     * @param objectType
     * @param associationExpansionLevel
     * @param properties
     * @param authorization
     * @return
     * @throws Exception
     * @deprecated
     */
    @Deprecated
    @RequestMapping(value = "/{objectType}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<?>> getObjects(@PathVariable P2ACollection objectType,
            @RequestParam(value = "associationExpansionLevel", required = false) Integer associationExpansionLevel,
            @RequestParam(value = "properties", required = false) String properties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        if (authorizationManager.isAuthorized(userToken, "GET", objectType.getObjectType(), null) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        return new ResponseEntity<>(contentService.getObjectCollection(userToken, objectType, associationExpansionLevel, properties),
                HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * updated for versioning. versionId added for versioning.
     * <p>
     * Returns a particular version of a managed document or the only version of
     * a non managed document.
     * In case of a managed document, if no versionId is provided the latest
     * version of the document is returned.
     * Otherwise the specified version of the document id returned.
     *
     * @param objectType
     *            type of the document. i.e plans
     * @param objectId
     *            the id of the document
     * @param associationExpansionLevel
     *            level of depth for associations
     * @param properties
     * @param versionId
     *            the version of the document to retrieve
     * @param authorization
     *            user authorization token for the caller of this API
     * @return if the object is not versioned, then the only version of the
     *         object is returned.
     *         if the object is versioned and if no versionId is provided, then
     *         the latest version of the object is returned.
     *         if the object is versioned and if versionId is provided, then the
     *         specific version of the object is returned.
     * @throws Exception
     *             HrBadRequestException if authorization is null or empty
     *             if objectType is null or empty
     *             if objectId is null
     *             HrInternalServerErrorException if object does not exist
     *             if object is versioned but no version of the object exists in
     *             the underlying database
     *             HrUnauthorizedException if user is not authorized to perform
     *             this action for given objectType and the result object
     */

    @RequestMapping(value = "/{collection}/{objectId:.+}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Map<String, Object>> getObjectById(
            @PathVariable String objectId,
            @PathVariable P2ACollection collection,
            @RequestParam(value = "associationExpansionLevel", required = false) Integer associationExpansionLevel,
            @RequestParam(value = "lvl", required = false) Integer lvl,
            @RequestParam(value = "properties", required = false) String[] properties,
            @RequestParam(value = "versionId", required = false) Integer versionId,
            @RequestParam(value = "expand", required = false) String expand[],
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        if (associationExpansionLevel == null && lvl != null) {
            associationExpansionLevel = lvl;
        }

        // TODO: use that POJO in the getObjectById- we'll do that shortly, it's a bit tricky
        ObjectRequest objectRequest = ObjectRequest.builder()
                .objectId(objectId)
                .collection(collection)
                .associationExpansionLevel(associationExpansionLevel)
                .properties(properties)
                .versionId(versionId)
                .expand(expand)
                .userToken(userToken)
                .build();

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, collection, associationExpansionLevel, properties, versionId,
                expand);

        if (authorizationManager.isAuthorized(userToken, "GET", collection.getObjectType(), object) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        return new ResponseEntity<>(object, HttpStatus.OK);
    }

    /**
     * updated for versioning. versionId added for versioning.
     * <p>
     * Download the content of an object.
     * Accepts auth header on either the request header or a query param; the
     * latter to support JavaScript clients (rules engine).
     * Prefer auth header if both header and query param are present.
     *
     * @param objectType
     * @param objectId
     * @param usertoken
     *            optional query parameter, alternative to
     *            <code>authorization</code> header
     * @param authorization
     *            base64-encoded authorization header
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/{objectType}/{objectId}/content", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> downloadContent(
            @PathVariable P2ACollection objectType,
            @PathVariable("objectId") String objectId,
            @RequestParam(value = "usertoken", required = false) String usertoken,
            @RequestParam(value = "versionId", required = false) Integer versionId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME, required = false) String authorization) throws Exception {

        logger.info("retrieving content for " + objectType + " with id " + objectId);

        if (null == authorization && null == usertoken) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, "no authorization header or request parameter");
        }

        String auth = authorization != null ? authorization : usertoken;
        return contentService.getObjectContent(auth, objectId, objectType, versionId);
    }

    @RequestMapping(value = "/{objectType}/{objectId}/arrayContent", method = RequestMethod.GET)
    public ResponseEntity<ByteArrayResource> getArrayContent(
            @PathVariable P2ACollection objectType,
            @PathVariable("objectId") String objectId,
            @RequestParam(value = "usertoken", required = false) String usertoken,
            @RequestParam(value = "versionId", required = false) Integer versionId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME, required = false) String authorization) throws Exception {

        logger.info("retrieving content for " + objectType + " with id " + objectId);

        if (null == authorization && null == usertoken) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, "no authorization header or request parameter");
        }

        String auth = authorization != null ? authorization : usertoken;
        return contentService.getObjectArrayContent(auth, objectId, objectType, versionId);
    }

    @ResponseBody
    @RequestMapping(value = "/schemas/schema-ext.json", method = RequestMethod.GET)
    public Map<String, Object> getSchemaExtValues(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        if (authorizationManager.isAuthorized(userToken, "GET", P2ACollection.SCHEMAS.getObjectType(), null) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        return contentService.getSchemaExtValues(userToken);
    }

    /**
     * validate object
     *
     * @param collectionId
     *            - object collection (a plural object-type)
     * @param objectProperties
     *            - map representing the object to create
     * @param authorization
     *            - Authorization header (userToken)
     * @return 204 No Content when object is valid per its schema; 400 Bad
     *         Request when not
     * @throws Exception
     */
    @RequestMapping(value = "/{collectionId}/validate", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Void> validateObject(@PathVariable P2ACollection collectionId, @RequestBody Map<String, Object> objectProperties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        // do we need to protect the validation endpoint? probably not
        //        if (authorizationManager.isAuthorized(userToken, "POST", objectType, objectProperties) == false) {
        //            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        //        }

        contentService.validateContent(userToken, collectionId, objectProperties);
        logger.debug("Successfully validated instance of {}", collectionId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Create object
     *
     * @param collectionId
     *            - object collection (a plural object-type)
     * @param objectProperties
     *            - map representing the object to create
     * @param authorization
     *            - Authorization header (userToken)
     * @return objectId
     * @throws Exception
     */
    @RequestMapping(value = "/{collectionId}", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> createObject(@PathVariable P2ACollection collectionId, @RequestBody Map<String, Object> objectProperties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        if (!authorizationManager.isAuthorized(userToken, "POST", collectionId.getObjectType(), objectProperties)) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        if (objectProperties.get(P2ALibConstants.OBJECT_NAME) == null) {
            String name = collectionId + "_" + System.nanoTime();
            objectProperties.put(P2ALibConstants.OBJECT_NAME, name);
        }
        try {
            objectProperties = scriptService.applyTenantFunction(userToken, "onCreate", collectionId,
                    objectProperties, null);
        } catch (NoSuchMethodException e) {
            // If the method is not implemented in the hook code, we proceed normally
            logger.debug(
                    "onCreate method not implemented in the ppm hook code for tenant {}", userToken.getOrganization());
        } catch (ScriptException | IOException e) {
            logger.error("Error: {}", e);
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
        String objectId = contentService.createContent(userToken, collectionId, objectProperties);
        logger.debug("Successfully created \"{}\" with objectId: {}", collectionId, objectId);

        generateCmEvent(userToken, collectionId, objectId, "object.create");

        return new ResponseEntity<>(objectId, HttpStatus.CREATED);
    }

    /**
     * Create associations
     *
     * @param sourceObjectType
     *            - Singular objectType
     * @param sourceObjectId
     *            - Id of the object for which we should create association
     * @param authorization
     *            - Authorization header (userToken)
     * @return objectId
     * @throws Exception
     */
    @RequestMapping(value = "/{sourceObjectType}/{sourceObjectId}/associations", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Map<String, Object>> createAssociations(@PathVariable P2ACollection sourceObjectType,
            @PathVariable String sourceObjectId,
            @RequestBody Map<String, Object> targetAssociations,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("createAssociations - sourceObjectType: {}, sourceObjectId: {}, relationType: {}", sourceObjectType, sourceObjectId);

        UserToken userToken = UserToken.deserialize(authorization);

        Map<String, Object> sourceObject = contentService.getObjectById(userToken, sourceObjectId, sourceObjectType, 0, null, true);
        if (authorizationManager.isAuthorized(userToken, "PATCH", sourceObjectType.getObjectType(), sourceObject) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        Map<String, Object> targetAssociationsMap = contentService.createAssociations(userToken, sourceObjectType,
                sourceObjectId, targetAssociations);
        return new ResponseEntity<>(targetAssociationsMap, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{objectType}/{objectId}", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<String> replaceObject(@PathVariable P2ACollection objectType, @PathVariable String objectId,
            @RequestBody Map<String, Object> objectProperties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);

        Map<String, Object> originalObject = contentService.getObjectById(userToken, objectId, objectType,
                null, null);
        Map<String, Object> newObjectProps = patchUtil.setSystemProperties(objectProperties, originalObject, userToken);

        // auth for PUT is same as PATCH, both make updates to an existing object
        if (authorizationManager.isAuthorized(userToken, "PATCH", objectType.getObjectType(), newObjectProps) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        if (patchUtil.isNameTaken(objectId, objectType, newObjectProps, userToken)) {
            ExceptionHelper.throwException(HttpStatus.CONFLICT, HrErrorCode.CONFLICT,
                    String.format("An object with this name already exists: %s", newObjectProps.get("name").toString()));
        }

        //Tenant specific hook
        try {
            newObjectProps = scriptService.applyTenantFunction(userToken, "onPut", objectType, newObjectProps,
                    originalObject);
        } catch (NoSuchMethodException e) {
            // If the method is not implemented in the hook code, we proceed normally
            logger.debug("onPut method not implemented in the ppm hook code for tenant " + userToken.getOrganization());
        } catch (ScriptException | IOException e) {
            logger.error("Error: " + e);
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
        long start = System.currentTimeMillis();
        contentService.writeContent(userToken, newObjectProps, objectType, objectId);
        long stop = System.currentTimeMillis();
        logger.debug("PUT Duration: {} ", stop - start);

        logger.debug("Successfully replaced \"{}\" with objectId: {}", objectType, objectId);
        return new ResponseEntity<>(objectId, HttpStatus.OK);
    }

    @RequestMapping(value = "/schemas/schema-ext.json", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<Void> updateSchemaExtValues(@RequestBody Map<String, Object> payloadProperties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        if (authorizationManager.isAuthorized(userToken, "PATCH", P2ACollection.SCHEMAS.getObjectType(), null) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        contentService.updateEnumValues(userToken, payloadProperties);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Delete all objects of the type. Currently limited to "unit" only.
     *
     * @param objectType
     * @param authorization
     * @throws Exception
     */
    @RequestMapping(value = "/{objectType}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteAllObjectsOfType(@PathVariable P2ACollection objectType,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        logger.debug("deleteAllObjectOfTypes - userToken.organizationId: {}, objectType: {}", userToken.getOrganization(), objectType);

        if (!authorizationManager.isAuthorized(userToken, "DELETE", objectType.getObjectType(), null)) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        contentService.deleteAllObjectsOfType(userToken, objectType);
        logger.debug("Successfully deleted all objects of type: {}", objectType);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Update object
     *
     * @param objectType
     * @param objectId
     * @param payloadProperties
     * @param authorization
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{objectType}/{objectId}", method = RequestMethod.PATCH, consumes = "application/json")
    public ResponseEntity<Void> patchObject(@PathVariable P2ACollection objectType, @PathVariable String objectId, @RequestBody Object payloadProperties,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        logger.debug("patchObject - userToken.organizationId: {}, objectType: {}, objectId: {}, objectProperties: {}", userToken.getOrganization(), objectType,
                objectId, payloadProperties);

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, objectType,
                null, null, true); // true -> latest-only version, not full history

        if (authorizationManager.isAuthorized(userToken, "PATCH", objectType.getObjectType(), object) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        //legacy patch
        if (payloadProperties instanceof Map) {
            Map<String, Object> filteredPayload = patchUtil.filterOutSystemProperties((Map<String, Object>) payloadProperties);
            if (objectType == P2ACollection.JOBS) {
                contentService.prepareJobPayload(userToken, filteredPayload, object);
            }
            //Tenant specific hook
            try {
                filteredPayload = scriptService.applyTenantFunction(userToken, "onPatch", objectType,
                        filteredPayload, object);
            } catch (NoSuchMethodException e) {
                // If the method is not implemented in the hook code, we proceed normally
                logger.debug("onPatch method not implemented in the ppm hook code for tenant "
                        + userToken.getOrganization());
            } catch (ScriptException | IOException e) {
                logger.error("Error: " + e);
                ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
            }
            contentService.updateContent(userToken, objectType, objectId, filteredPayload);
        }
        //patch using specification
        else if (payloadProperties instanceof List) {
            List<Map<String, Object>> filteredPayload = patchUtil.filterOutSystemProperties((List<Map<String, Object>>) payloadProperties);
            contentService.updateContent(userToken, objectType, objectId, filteredPayload);
        } else {
            logger.error("patchObject - invalid input. userToken.organizationId: {}, objectType: {}, payloadType {}",
                    userToken.getOrganization(), objectType, payloadProperties.getClass());
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST);
        }

        logger.debug("Successfully patched \"{}\" with objectId: {}", objectType, objectId);
        generateCmEvent(userToken, objectType, objectId, "object.update");
        // TODO: this should return 204/no-content; or maybe better yet a standard HR API response?
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // -----------------------------------------------------------------------

    @ApiOperation(value = "Lock an Object", notes = "Reserves exclusive access to the specified object by the specified user", response = String.class)
    @RequestMapping(value = "/{objectType}/{objectId}/lock", method = RequestMethod.POST)
    public Map<String, Object> lockObject(
            @ApiParam(value = "HttpHeaders parameter containing user authorization token.") @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @PathVariable P2ACollection objectType,
            @ApiParam(value = "Object ID to be locked.") @PathVariable String objectId) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        final Map<String, Object> lockMap = new HashMap<>();
        lockMap.put(P2ALibConstants.IS_LOCKED, true);
        lockMap.put(P2ALibConstants.LOCKED_BY, userToken.getUsername());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        lockMap.put(P2ALibConstants.LOCKED_AT, simpleDateFormat.format(new Date()));


        Map<String, Object> object = contentService.getObjectById(userToken, objectId, objectType, 0, null, true);
        if (!authorizationManager.isAuthorized(userToken, "POST", "lock", object)) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        contentService.setLock(userToken, objectType, objectId, lockMap, object); // sending the doc (metadata) doesn't help.
        logger.debug("Successfully locked \"{}\" with objectId: {}", objectType, objectId);

        return lockMap;
    }

    @ApiOperation(value = "UnLock an Object", notes = "Relinquishes exclusive access to the specified object", response = String.class)
    @RequestMapping(value = "/{objectType}/{objectId}/unlock", method = RequestMethod.POST)
    public ResponseEntity<Void> unlockObject(
            @ApiParam(value = "HttpHeaders parameter containing user authorization token.") @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @PathVariable P2ACollection objectType,
            @ApiParam(value = "Object ID to be unlocked.") @PathVariable String objectId) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        final Map<String, Object> lockMap = new HashMap<>();
        lockMap.put(P2ALibConstants.IS_LOCKED, false);
        lockMap.put(P2ALibConstants.LOCKED_BY, userToken.getUsername());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        lockMap.put(P2ALibConstants.LOCKED_AT, simpleDateFormat.format(new Date()));

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, objectType, 0, null, true);
        if (!authorizationManager.isAuthorized(userToken, "POST", "unlock", object)) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        if (null != object.get(P2ALibConstants.IS_LOCKED)) {
            contentService.clearLock(userToken, objectType, objectId, lockMap);
            logger.debug("Successfully unlocked \"{}\" with objectId: {}", objectType, objectId);
        } else {
            logger.debug("object not locked, nothing to do: \"{}\" with objectId: {}", objectType, objectId);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Delete object
     *
     * @param objectType
     * @param objectId
     * @param authorization
     * @throws Exception
     */
    // objectType is not used but necessary for request mapping
    @RequestMapping(value = "/{objectType}/{objectId:.+}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteObjectById(@PathVariable P2ACollection objectType, @PathVariable String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        logger.debug("deleteObjectById - userToken.organizationId: {}, objectType: {}, objectId: {}", userToken.getOrganization(), objectType,
                objectId);

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, objectType, 0, null);

        if (!authorizationManager.isAuthorized(userToken, "DELETE", objectType.getObjectType(), object)) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        contentService.deleteObject(userToken, objectType, objectId);
        logger.debug("Successfully deleted \"{}\" with objectId: {}", objectType, objectId);
        generateCmEvent(userToken, objectType, objectId, "object.delete");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Associate source object with one or more target objects across a named
     * association
     *
     * @param sourceObjectType
     * @param sourceObjectId
     * @param relationType
     * @param targetObjectIds
     * @param authorization
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/{sourceObjectType}/{sourceObjectId}/{relationType}/{targetObjectIds}", method = RequestMethod.POST)
    public ResponseEntity<List<String>> createAssociation(@PathVariable P2ACollection sourceObjectType, @PathVariable String sourceObjectId,
            @PathVariable String relationType,
            @PathVariable List<String> targetObjectIds,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestBody(required = false) Map<String, Object> additionalTargetIdMap)
            throws Exception {

        logger.debug("createAssociation - sourceObjectType: {}, sourceObjectId: {}, relationType: {}, targetObjectIds: {}", sourceObjectType, sourceObjectId,
                relationType, targetObjectIds);
        logger.debug("Creating one way relationship " + relationType + " from " + sourceObjectId + " to " + targetObjectIds);

        UserToken userToken = UserToken.deserialize(authorization);

        Map<String, Object> sourceObject = contentService.getObjectById(userToken, sourceObjectId, sourceObjectType, 0, null, true);
        if (authorizationManager.isAuthorized(userToken, "PATCH", sourceObjectType.getObjectType(), sourceObject) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        List<String> createdRelationIds = contentService.createAssociation(userToken, sourceObjectType, sourceObjectId,
                relationType, targetObjectIds,
                additionalTargetIdMap);
        return new ResponseEntity<>(createdRelationIds, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{sourceObjectType}/{sourceObjectId}/{relationType}", method = RequestMethod.POST)
    public ResponseEntity<List<String>> createAssociationWithNoUriId(@PathVariable P2ACollection sourceObjectType,
            @PathVariable String sourceObjectId, @PathVariable String relationType,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestBody Map<String, Object> additionalTargetIdMap) throws Exception {

        logger.debug(
                "createAssociation - sourceObjectType: {}, sourceObjectId: {}, relationType: {}", sourceObjectType,
                sourceObjectId, relationType);
        UserToken userToken = UserToken.deserialize(authorization);

        List<String> targetObjectIds = (List<String>) additionalTargetIdMap.get("targetIds");

        // We need to have the "targetIds" key in the map with values to proceed
        if (targetObjectIds == null || targetObjectIds.isEmpty()) {
            String errorMessage = String.format("%s",
                    "Need at least one targetId in the request body with targetIds key");
            logger.warn(errorMessage);
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, errorMessage);

        }

        Map<String, Object> sourceObject = contentService.getObjectById(userToken, sourceObjectId, sourceObjectType,
                0, null);
        if (authorizationManager.isAuthorized(userToken, "PATCH", sourceObjectType.getObjectType(), sourceObject) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        List<String> createdRelationIds = contentService.createAssociation(userToken, sourceObjectType, sourceObjectId,
                relationType, new ArrayList<String>(), additionalTargetIdMap);
        return new ResponseEntity<>(createdRelationIds, HttpStatus.CREATED);
    }

    /**
     * Delete named association between source object and target object(s)
     *
     * @param sourceObjectType
     * @param sourceObjectId
     * @param relationType
     * @param targetObjectIds
     * @param authorization
     * @throws Exception
     */
    @RequestMapping(value = "/{sourceObjectType}/{sourceObjectId}/{relationType}/{targetObjectIds}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteAssociation(@PathVariable P2ACollection sourceObjectType, @PathVariable String sourceObjectId,
            @PathVariable String relationType,
            @PathVariable List<String> targetObjectIds,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestBody(required = false) Map<String, Object> additionalTargetIdMap) throws Exception {

        logger.debug("deleteAssociation - sourceObjectType: {}, sourceObjectId: {}, relationType: {}, targetObjectIds: {}", sourceObjectType, sourceObjectId,
                relationType, targetObjectIds);
        UserToken userToken = UserToken.deserialize(authorization);
        Map<String, Object> sourceObject = contentService.getObjectById(userToken, sourceObjectId, sourceObjectType, 0, null);
        if (authorizationManager.isAuthorized(userToken, "PATCH", sourceObjectType.getObjectType(), sourceObject) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        contentService.deleteAssociation(userToken, sourceObjectType, sourceObjectId,
                relationType, targetObjectIds, additionalTargetIdMap);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{sourceObjectType}/{sourceObjectId}/{relationType}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteAssociationWithoutUriId(@PathVariable P2ACollection sourceObjectType,
            @PathVariable String sourceObjectId, @PathVariable String relationType,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestBody Map<String, Object> additionalTargetIdMap)

            throws Exception {

        List<String> targetObjectIds = (List<String>) additionalTargetIdMap.get("targetIds");

        // We need to have the "targetIds" key in the map with values to proceed
        if (targetObjectIds == null || targetObjectIds.isEmpty()) {
            String errorMessage = String.format("%s",
                    "Need at least one targetId in the request body with targetIds key");
            logger.warn(errorMessage);
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, errorMessage);
        }

        logger.debug("deleteAssociation - sourceObjectType: {}, sourceObjectId: {}, relationType: {}", sourceObjectType,
                sourceObjectId, relationType);
        UserToken userToken = UserToken.deserialize(authorization);
        Map<String, Object> sourceObject = contentService.getObjectById(userToken, sourceObjectId, sourceObjectType, 0, null);
        if (authorizationManager.isAuthorized(userToken, "PATCH", sourceObjectType.getObjectType(), sourceObject) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        contentService.deleteAssociation(userToken, sourceObjectType, sourceObjectId, relationType,
                new ArrayList<String>(), additionalTargetIdMap);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{sourceObjectType}/{sourceObjectId}/associations", method = RequestMethod.DELETE, consumes = "application/json")
    public ResponseEntity<Void> deleteAssociations(@PathVariable P2ACollection sourceObjectType,
            @PathVariable String sourceObjectId,
            @RequestBody Map<String, Object> targetAssociations,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("deleteAssociations - sourceObjectType: {}, sourceObjectId: {}, relationType: {}", sourceObjectType, sourceObjectId);

        UserToken userToken = UserToken.deserialize(authorization);

        Map<String, Object> sourceObject = contentService.getObjectById(userToken, sourceObjectId, sourceObjectType, 0, null, true);
        if (authorizationManager.isAuthorized(userToken, "PATCH", sourceObjectType.getObjectType(), sourceObject) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        contentService.deleteAssociations(userToken, sourceObjectType, sourceObjectId, targetAssociations);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Upload a file with associated metadata.
     *
     * @param objectType
     * @param metadata
     * @param file
     * @param authorization
     * @return created objectId
     * @throws Exception
     */
    @RequestMapping(value = "/{objectType}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<String> handleUpload(@PathVariable P2ACollection objectType, @RequestPart("metadata") Map<String, Object> metadata,
            @RequestPart("file") MultipartFile file,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        String objectId = handleUploadPrivate(objectType, metadata, authorization, file);
        return new ResponseEntity<>(objectId, HttpStatus.CREATED);
    }

    // -----------------------------------------------------------------------

    private String handleUploadPrivate(P2ACollection objectType, Map<String, Object> metadata, String authorization,
            MultipartFile file) throws Exception {

        logger.info("received multipart file " + file.getOriginalFilename() + " with metadata " + metadata);

        UserToken userToken = UserToken.deserialize(authorization);

        // old way was a map indexed by single key "properties"; we retain
        // backward-compatibility with that;
        // new way is just an "anonymous" map (as returned by the 'GET' API
        // call)
        @SuppressWarnings("unchecked")
        Map<String, Object> payloadProperties = metadata != null && metadata.containsKey("properties")
                ? (Map<String, Object>) metadata.get("properties")
                : metadata;

        if (authorizationManager.isAuthorized(userToken, "POST", objectType.getObjectType(), payloadProperties) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        String objectId = contentService.uploadContent(userToken, objectType, payloadProperties, file);
        logger.debug("Successfully uploaded and created \"{}\" with objectId: {}", objectType, objectId);
        return objectId;
    }







    /**
     * Download tenant logo as Base64 encoded string in json format.
     * No roles/permissions test is required since logo is returned to every
     * tenant user;
     * but of course the authorization header must be present to identify the
     * org
     *
     * @param authorization
     * @return JSON map containing base64-encoded value of logo file
     * @throws Exception
     */
    @RequestMapping(value = "/logos/{tenantname}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Map<String, Object>> downloadLogo(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        return new ResponseEntity<>(contentService.retrieveLogo(userToken), HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Added for versioning.
     * Updates a versioned binary document.
     * A new version of the binary document is created.
     *
     * @param objectType
     *            type of the document. i.e documents
     * @param objectId
     *            the id of the document
     * @param metadata
     *            properties/metadata for the update
     * @param file
     *            binary file for the update
     * @param authorization
     *            user authorization token for the caller of this API
     * @throws Exception
     *             HrBadRequestException if document fails to update
     *             if authorization is null or empty
     *             if objectType is null or empty
     *             if objectId is null
     *             HrInternalServerErrorException if object does not exist
     *             if object is versioned but no version of the object exists in
     *             the underlying database
     *             HrUnauthorizedException if user is not authorized to perform
     *             this action for given objectType and the result object
     */

    @RequestMapping(value = "/{objectType}/{objectId}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<Void> updateBinaryContent(
            @PathVariable P2ACollection objectType,
            @PathVariable String objectId,
            @RequestPart("metadata") Map<String, Object> metadata,
            @RequestPart("file") MultipartFile file,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);

        // old way was a map indexed by single key "properties"; we retain
        // backward-compatibility with that;
        // new way is just an "anonymous" map (as returned by the 'GET' API
        // call)
        @SuppressWarnings("unchecked")
        Map<String, Object> payloadProperties = metadata != null && metadata.containsKey("properties")
                ? (Map<String, Object>) metadata.get("properties")
                : metadata;

        if (authorizationManager.isAuthorized(userToken, "POST", objectType.getObjectType(), payloadProperties) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        Map<String, Object> filteredPayload = patchUtil.filterOutSystemProperties(payloadProperties);
        contentService.updateBinaryContent(userToken, objectType, objectId, filteredPayload, file);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // -----------------------------------------------------------------------

    @RequestMapping(value = "/branch/{objectType}/{objectId}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<Void> branchBinaryContent(
            @PathVariable P2ACollection objectType,
            @PathVariable String objectId,
            @RequestPart("metadata") Map<String, Object> metadata,
            @RequestPart("file") MultipartFile file,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        long then = System.currentTimeMillis();
        UserToken userToken = UserToken.deserialize(authorization);

        // old way was a map indexed by single key "properties"; we retain
        // backward-compatibility with that;
        // new way is just an "anonymous" map (as returned by the 'GET' API
        // call)
        @SuppressWarnings("unchecked")
        Map<String, Object> payloadProperties = metadata != null && metadata.containsKey("properties")
                ? (Map<String, Object>) metadata.get("properties")
                : metadata;

        if (authorizationManager.isAuthorized(userToken, "POST", objectType.getObjectType(), payloadProperties) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        contentService.branchBinaryContent(userToken, objectType, objectId, payloadProperties, file);
        logger.info("updateObject: " + (System.currentTimeMillis() - then));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * uploads an binary inbound datafeed file (Excel sheet for example),
     * inserts the file in database and starts the inbound
     * process by sending the objectId of the newly uploaded file to DF service.
     *
     * @param transformType
     *            type of the inbound transformation (offerings, plans, etc.)
     * @param metadata
     *            metadata associated with the binary upload
     * @param file
     *            binary file for the update
     * @param authorization
     *            user authorization token for the caller of this API
     * @throws Exception
     *             HrBadRequestException if document fails to update
     *             if authorization is null or empty
     *             if objectType is null or empty
     *             if objectId is null
     *             HrInternalServerErrorException if object does not exist
     *             if object is versioned but no version of the object exists in
     *             the underlying database
     *             HrUnauthorizedException if user is not authorized to perform
     *             this action for given objectType and the result object
     */

    @RequestMapping(value = "/datafeeds/inbound/{transformType}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<String> uploadDatafeedInbound(
            @PathVariable String transformType,
            @RequestPart("metadata") Map<String, Object> metadata,
            @RequestPart("file") MultipartFile file,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        uploadToResourceObject(transformType, file, authorization);

        String inputObjectId = handleUploadPrivate(P2ACollection.DATAFEEDINPUTS, metadata, authorization, file);

        String datafeedObjectId = contentService.startInboundProcess(userToken, P2ACollection.DATAFEEDS, inputObjectId, transformType, metadata);

        HttpStatus httpStatus = HttpStatus.CREATED;
        if (datafeedObjectId == null) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(datafeedObjectId, httpStatus);
    }

    private void uploadToResourceObject(String transformType, MultipartFile file, String authorization) throws Exception {

        if (transformType.equalsIgnoreCase("rateTables")) {
            checkFileFormat(file.getOriginalFilename());
            int endIndex = file.getOriginalFilename().lastIndexOf('.');
            Map<String, Object> rateTableMetaData = new HashMap<>();
            rateTableMetaData.put("name", file.getOriginalFilename().substring(0, endIndex));
            String fileName = file.getOriginalFilename().substring(0, endIndex).replaceAll("[^a-zA-Z0-9]", "");
            rateTableMetaData.put("description", "rateTable full excel data");
            rateTableMetaData.put("objectType", "resource");
            rateTableMetaData.put("objectId", fileName);
            rateTableMetaData.put("resourceType", "rateTable");
            String fileYear = file.getOriginalFilename().substring(0, 4);
            rateTableMetaData.put("resourceYear", fileYear);
            handleUploadPrivate(P2ACollection.RESOURCES, rateTableMetaData, getHRSuperAdmin(), file);
        }
    }

    private void checkFileFormat(String originalFilename) {
        String regex = "^(\\d+)_(\\S+)";
        String fileName = originalFilename.split("\\.")[0];
        String fileExtension = originalFilename.split("\\.")[1];
        boolean result = Pattern.matches(regex, fileName);

        if (!result || result && !fileExtension.equals("xlsx")) {
            throw new HrException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, "Invalid file name. The expected format is YYYY_*.xlsx");
        }
    }

    /**
     * get-and-increment trackingId - essentially a unique identifier generator
     * - READ ONLY
     * POST is used as this is invoking a "controller" - executing a function
     *
     * @param authorization
     * @return next uniqueId for tenant
     * @throws Exception
     */
    @RequestMapping(value = "/uniqueId", method = RequestMethod.POST)
    public ResponseEntity<String> getAndIncrementUniqueId(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        String uniqueId = contentService.getAndIncrementUniqueId(userToken);
        HttpStatus httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(uniqueId, httpStatus);
    }

    @RequestMapping(value = "/counters/{counterKey}/increment", method = RequestMethod.POST)
    public ResponseEntity<String> getAndIncrement(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization, @PathVariable String counterKey) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        String uniqueId = contentService.getAndIncrement(userToken, counterKey);
        HttpStatus httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(uniqueId, httpStatus);
    }

    /**
     * Updates the notification object after it's been acknowledged by UI.
     *
     * @param authorization
     * @param objectId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/notifications/{objectId}/ack", method = RequestMethod.POST)
    public ResponseEntity<String> ackNotifications(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @PathVariable("objectId") String objectId) throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        contentService.ackNotification(userToken, objectId);
        return new ResponseEntity<>(objectId, HttpStatus.OK);
    }

    /**
     * Returns the document name given the parameters and rules defined
     *
     * @param authorization
     * @param docType
     * @param offeringId
     * @param templateId
     * @param effectiveDate
     * @param documentOutputType
     * @param endDate
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/documentName", method = RequestMethod.GET)
    public ResponseEntity<String> getDocName(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestParam(value = "docType") String docType,
            @RequestParam(value = "language", required = false) String language,
            @RequestParam(value = "planIds") String planIds,
            @RequestParam(value = "offeringId", required = false) String offeringId,
            @RequestParam(value = "templateId") String templateId,
            @RequestParam(value = "groupId", required = false) String groupId,
            @RequestParam(value = "effectiveDate", required = false) String effectiveDate,
            @RequestParam(value = "documentOutputType", required = false) String documentOutputType,
            @RequestParam(value = "endDate", required = false) String endDate) throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        List<String> planIdList = Arrays.asList(planIds.split(","));
        String docName = ruleService.generateDocName(userToken, docType, language, planIdList,
                offeringId, templateId, effectiveDate, endDate, groupId, documentOutputType);
        return new ResponseEntity<>(docName, HttpStatus.OK);
    }

    /**
     * Reloads rules specified by user's tenant and list of rule groups provided
     * in payload. Reloads all rule groups if payload is not provided.
     *
     * @param authorization
     * @param payload
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reloadRules", method = RequestMethod.POST)
    public ResponseEntity<String> reloadRules(@RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestBody(required = false) Map<String, Object> payload) throws Exception {
        List<String> ruleGroups = null;
        if (payload != null) {
            ruleGroups = (List<String>) payload.get("ruleGroups");
        }
        UserToken userToken = UserToken.deserialize(authorization);
        ruleService.reloadRules(userToken, ruleGroups);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/internal/mlrest/{extension}/{method}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<JsonNode> invokeExtension(
            @PathVariable("extension") String extension,
            @PathVariable("method") String method,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization,
            @RequestParam(required = false) Map<String, String> queryMap,
            @RequestBody(required = false) Map<String, Object> payload) throws Exception {

        if (queryMap == null) {
            queryMap = new HashMap<>();
        }

        UserToken userToken = UserToken.deserialize(authorization);
        JsonNode output = restExtensionService.executeExtension(userToken, extension, method, queryMap, payload);

        return new ResponseEntity<>(output, HttpStatus.CREATED);

    }

    // Generate a event to the event infrastructure.
    private void generateCmEvent(UserToken userToken, P2ACollection objectType, String objectId, String action) {

        if (sendEvent) {
            try {
                String ORIGIN_SERVICE = "cm-service";
                String tenantId = userToken.getOrganization();
                String user = userToken.getUsername();
                String tag = ""; // Empty Tag for now
                String event_details = "{}"; // Empty Details for now
                Event e = new Event(ORIGIN_SERVICE, objectType.getObjectType(), objectId, tenantId, user, tag, action, event_details);
                String ret  = ea.sendEvent(e);
                logger.debug("Sent Event {} response {} ", e.toString(), ret);
            } catch (Exception e){ // Its not a great idea to do a catch all here - should refine/remove when functionality it working
                logger.error(e.getMessage());
            }

        }
    }
    /////////////////////////////////    THESE ARE FOR DEBUG ONLY   //////////////////////////////////

    @RequestMapping(value = "/internal/pool", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Object> getConnectionPoolInfo(
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        if (userToken.getOrganization().equals(SYSTEM_ORGANIZATION) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        Object poolInfo = contentService.getConnectionPoolInfo();
        return new ResponseEntity<>(poolInfo, HttpStatus.OK);
    }

    @RequestMapping(value = "/internal/pool/{username:.+}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteConnection(
            @PathVariable String username,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        if (userToken.getOrganization().equals(SYSTEM_ORGANIZATION) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }
        contentService.deleteUserConnection(username);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private String getHRSuperAdmin() throws JsonProcessingException {
        UserToken superUserToken = new UserToken();
        superUserToken.setOrganization(HrSystem.SYSTEM_ORGANIZATION);
        superUserToken.setUsername(HrSystem.SYSTEM_ADMIN_USER);
        superUserToken.setRoles(Arrays.asList(HrSystem.HR_SUPER_ADMIN));
        String serializedToken = superUserToken.serialize();
        return Base64.getEncoder().encodeToString(serializedToken.getBytes());
    }
}
