package com.highroads.cm.web.controller;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.highroads.cm.batch.BatchAction;
import com.highroads.cm.batch.BatchService;
import com.highroads.commons.HrSystem;
import com.highroads.commons.sso.UserToken;

@RestController
@ConditionalOnProperty(prefix = "batch.queue.", name = "redis")
public class BatchController {


    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private BatchService batchService;

    /**
     * Submit a batch for processing
     *
     * @param batchIds
     * @param authorization
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/batches/submit", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<List<Entry<String, String>>> submitBatch(@RequestBody List<String> batchIds,
            @RequestHeader(value = HrSystem.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        String normalizedAuthHeader = UserToken.getNormalizedToken(authorization);
        logger.debug("batch submit - userToken.organizationId: {}, batchIds: {}", userToken.getOrganization(), batchIds);
        Map<String, String> result = batchService.submitBatches(userToken, normalizedAuthHeader, batchIds);
        logger.debug("Successfully submitted batch with objectId: {}", batchIds);
        return new ResponseEntity<>(convertMap(result), HttpStatus.OK);
    }

    /**
     * Cancel a batch in status Submitted, In Progress etc.
     *
     * @param batchIds
     * @param authorization
     * @return
     * @throws Exception
     */
    @Deprecated
    @RequestMapping(value = "/batches/cancel", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<List<Entry<String, String>>> _cancelBatch(@RequestBody List<String> batchIds,
            @RequestHeader(value = HrSystem.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        logger.debug("batch cancel - userToken.organizationId: {}, batchIds: {}", userToken.getOrganization(), batchIds);
        Map<String, String> result = batchService.cancelBatch(userToken, batchIds);
        logger.debug("Successfully submitted for cancellation \"{}\" with objectId: {}", batchIds);
        return new ResponseEntity<>(convertMap(result), HttpStatus.OK);
    }

    // TODO: Replace this once UI can handle map instead of list
    private List<Entry<String, String>> convertMap(Map<String, String> map) {
        return map.entrySet().stream().collect(Collectors.toList());
    }

    // -----------------------------------------------------------------------

    @RequestMapping(value = "/v2/batches/cancel", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Map<String, String>> cancelBatch(@RequestBody List<String> batchIds,
            @RequestHeader(value = HrSystem.AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        logger.info("batch cancel - userToken.organizationId: {}, batchIds: {}", userToken.getOrganization(), batchIds);
        Map<String, String> result = batchService.cancelBatch(userToken, batchIds);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Resubmit a batch in status Cancelled, Failed etc.
     *
     * @param batchIds
     * @param authorization
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/batches/resubmit", method = RequestMethod.POST, consumes = "application/json")
    public List<Entry<String, String>> resubmitBatch(@RequestBody List<String> batchIds,
            @RequestHeader(value = HrSystem.AUTHORIZATION_HEADER_NAME) String authorization, @RequestParam(value = "flag") BatchAction flag,
            @RequestParam(value = "async", required = false, defaultValue = "false") boolean isAsync)
            throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        String normalizedAuthHeader = UserToken.getNormalizedToken(authorization);
        logger.debug("batch resubmit - userToken.organizationId: {}, batchIds: {}", userToken.getOrganization(), batchIds);
        Map<String, String> listResult = null;
        if (isAsync) {
            listResult = batchService.bulkResubmitBatch(userToken, batchIds, flag);
        } else {
            listResult = batchService.resubmitBatch(userToken, normalizedAuthHeader, batchIds, flag);

        }
        logger.debug("Successfully resubmitted \"{}\" with objectId: {}", isAsync, batchIds);
        return convertMap(listResult);
    }

    @RequestMapping(value = "/batches/archive", method = RequestMethod.POST, consumes = "application/json")
    public List<Entry<String, String>> archiveBatch(@RequestBody List<String> batchIds, @RequestParam(value = "flag") BatchAction flag,
            @RequestHeader(value = HrSystem.AUTHORIZATION_HEADER_NAME) String authorization)
            throws Exception {
        UserToken userToken = UserToken.deserialize(authorization);
        logger.debug("batch archieved - userToken.organizationId: {}, batchIds: {}", userToken.getOrganization(), batchIds);
        Map<String, String> listResult = batchService.bulkArchiveBatch(userToken, batchIds, flag);
        logger.debug("Successfully resubmitted \"{}\" with objectId: {}", flag, batchIds);
        return convertMap(listResult);
    }

}
