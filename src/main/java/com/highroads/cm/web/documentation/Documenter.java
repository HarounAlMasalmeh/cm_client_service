package com.highroads.cm.web.documentation;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.highroads.cm.web.controller.GenericController;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author dimyjeannot (Armedia)
 *
 *         Swagger Documentation implementation.
 *
 */
@Configuration
@ComponentScan(basePackageClasses = { GenericController.class })
@EnableSwagger2
public class Documenter {

    /**
     * Configures Swagger API documentation
     *
     * @return
     */
    @Bean
    public Docket workflowApi() {
        //@formatter:off
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.highroads"))
                .paths(PathSelectors.any())
                .build();
        //@formatter:on
    }

    private ApiInfo apiInfo() {
        //@formatter:off
        return new ApiInfoBuilder()
                .title("HighRoads P2A Content Manager APIs")
                .description("This is the HighRoads P2A Content Manager API documentation.")
                .termsOfServiceUrl("http://www.highroads.com/terms")
                .contact("HighRoads")
                .license("Proprietary")
                .licenseUrl("https://www.highroads.com/license")
                .version("2.0")
                .build();
        //@formatter:on
    }
}
