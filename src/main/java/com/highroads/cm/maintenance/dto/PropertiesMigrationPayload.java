package com.highroads.cm.maintenance.dto;

import lombok.Data;

import java.util.List;

@Data
public class PropertiesMigrationPayload {
    private List<PropertyRenameInformation> propertiesForRenaming;
    private List<String> stripProperties;

    @Data
    public static class PropertyRenameInformation {
        private String path;
        private String currentKey;
        private String newKey;
    }
}
