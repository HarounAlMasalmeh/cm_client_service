package com.highroads.cm.maintenance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.maintenance.dto.PropertiesMigrationPayload;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.authorization.AuthorizationManager;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MaintenanceService {

    private AuthorizationManager authorizationManager;
    private ContentService contentService;
    private ObjectMapper objectMapper;

    @Autowired
    public MaintenanceService(AuthorizationManager authorizationManager, ContentService contentService, ObjectMapper objectMapper) {
        this.authorizationManager = authorizationManager;
        this.contentService = contentService;
        this.objectMapper = objectMapper;
    }

    /**
     * Removes properties from the given object
     *
     * @param userToken
     * @param objectId
     * @param objectType
     * @param propertiesForRemoval
     * @return
     * @throws JsonProcessingException
     */
    public Map<String, Object> removeProperties(UserToken userToken, String objectId, P2ACollection objectType,
                                                List<String> propertiesForRemoval) throws JsonProcessingException {

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, objectType,
                2, null, true); // true -> latest-only version, not full history
        if (authorizationManager.isAuthorized(userToken, "DELETE", objectType.getObjectType(), object) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        DocumentContext doc = JsonPath.parse(objectMapper.writeValueAsString(object));
        propertiesForRemoval.stream()
                .forEach(doc::delete);
        Map<String, Object> updatedObject = objectMapper.convertValue(doc.json(), Map.class);
        contentService.updateContent(userToken, objectType, objectId, updatedObject);

        return updatedObject;
    }

    /**
     * Renames properties from object, effectively meaning old ones are dropped, new ones created.
     *
     * E.G. Payload
     * {
     *     "propertiesForRenaming": [
     *         {
     *             "path": "$..[?(@.udfplanService_OONPreviousRxCostShareCode)]",
     *             "currentKey": "udfplanService_OONPreviousRxCostShareCode",
     *             "newKey": "udfplanService_OONPreventiveRxCostShareCode"
     *         },
     *         {
     *             "path": "$..[?(@.udfplanService_OONPreviousRxCostShareTreatmentID)]",
     *             "currentKey": "udfplanService_OONPreviousRxCostShareTreatmentID",
     *             "newKey": "udfplanService_OONPreventiveRxCostShareTreatmentID"
     *         }
     *     ],
     *     "stripProperties": [
     *         "$.linkedPlanServices[*].linkedProduct",
     *         "$.linkedPlanServices[*].linkedService",
     *         "$.linkedProducts",
     *         "$.workflowRequests"
     *     ]
     * }
     *
     * @param userToken
     * @param objectId
     * @param objectType
     * @param payload
     * @return
     * @throws JsonProcessingException
     */
    public Map<String, Object> migrateProperties(UserToken userToken, String objectId, P2ACollection objectType,
                                                PropertiesMigrationPayload payload) throws JsonProcessingException {

        Map<String, Object> object = contentService.getObjectById(userToken, objectId, objectType,
                2, null, true); // true -> latest-only version, not full history

        if (authorizationManager.isAuthorized(userToken, "PATCH", objectType.getObjectType(), object) == false) {
            ExceptionHelper.throwException(HttpStatus.UNAUTHORIZED, HrErrorCode.UNAUTHORIZED);
        }

        DocumentContext doc = JsonPath.parse(objectMapper.writeValueAsString(object));
        payload.getStripProperties()
                .stream()
                .forEach(doc::delete);

        payload.getPropertiesForRenaming()
                .stream()
                .forEach(property -> doc.renameKey(property.getPath(), property.getCurrentKey(), property.getNewKey()));
        Map<String, Object> updatedObject = objectMapper.convertValue(doc.json(), Map.class);
        contentService.updateContent(userToken, objectType, objectId, updatedObject);

        return updatedObject;
    }
}
