package com.highroads.cm.maintenance;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.HasLogger;
import com.highroads.cm.MLRestExtensionService;
import com.highroads.cm.impl.TenantAwareBase;
import com.highroads.commons.HrSystem;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;

@Component
@EnableScheduling
@ConditionalOnProperty(prefix = "expired.lock.check.", name = "enabled", matchIfMissing = true)
public class RemoveExpiredLocks implements HasLogger {

    private static final String ORG_ID = "orgId";
    private static final String GET_LOCKED_OBJ_EXT_NAME = "get-locked-objects";
    private static final String OBJECT_TYPE = "objectType";
    private static final String LOCKED_BY = "lockedBy";
    private final ObjectMapper mapper = new ObjectMapper();

    private final List<P2ACollection> objectTypesToUnlock;

    @Autowired
    TenantAwareBase tenantAware;

    @Autowired
    private MLRestExtensionService restExtensionService;

    @Autowired
    private ContentService contentService;

    @Scheduled(cron = "${expired.lock.check.schedule.cron}")
    public void run() {
        getLogger().debug("Process running to unlock expired locks for {}", objectTypesToUnlock);
        List<String> organizations = tenantAware.getTenants().stream().map(n -> n.get(ORG_ID).asText()).collect(Collectors.toList());
        organizations.forEach(this::unlockEntitiesForOrganization);

    }

    @Autowired
    RemoveExpiredLocks(@Value("#{'${expired.lock.check.objects}'.split(',')}") String[] objectTypes) {
        // There must be a better way e.g. a custom Spring converter to convert the list of strings to a list...
        objectTypesToUnlock = Arrays.stream(objectTypes).map(P2ACollection::forValue).collect(Collectors.toList());
    }

    private void unlockEntitiesForOrganization(String orgId) {
        UserToken adminToken = getTenantToken(orgId, "admin@" + orgId);
        for (P2ACollection objectType : objectTypesToUnlock) {
            JsonNode objectUris = restExtensionService.executeExtension(adminToken, GET_LOCKED_OBJ_EXT_NAME, "get", new HashMap<>(), getPayload(objectType));
            List<String> uris = mapper.convertValue(objectUris, new TypeReference<List<String>>() {
            });
            List<String> objectIds = uris.stream().map(this::objectIdFromBaseUri).collect(Collectors.toList());
            for (String objectId : objectIds) {
                try {
                    unlockObject(adminToken, objectType, objectId);
                } catch (Exception e) {
                    getLogger().error("Failed to unlock objectType: {}, id: {} in tenant: {}", objectType, objectId, orgId, e);
                }
            }
        }
    }

    private void unlockObject(UserToken token, P2ACollection objectType, String objectId) {
        Map<String, Object> obj = contentService.getObjectById(token, objectId, objectType, 0, null);
        String lockedBy = (String) obj.get(LOCKED_BY);
        if (lockedBy == null) {
            return;
        }
        UserToken lockedByToken = token;
        lockedByToken.setUsername(lockedBy);
        contentService.updateLock(lockedByToken, objectType, objectId, false);
    }

    private UserToken getTenantToken(String tenant, String username) {
        UserToken tenantToken = new UserToken();
        tenantToken.setOrganization(tenant);
        tenantToken.setUsername(username);
        tenantToken.setRoles(Arrays.asList(HrSystem.HR_SUPER_ADMIN));
        return tenantToken;
    }

    private Map<String, Object> getPayload(P2ACollection objType) {
        return Collections.singletonMap(OBJECT_TYPE, objType.toValue());
    }

    private String objectIdFromBaseUri(String mlUri) {
        return mlUri.substring(mlUri.lastIndexOf('/') + 1);
    }

}
