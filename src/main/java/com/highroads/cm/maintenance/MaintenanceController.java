package com.highroads.cm.maintenance;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.maintenance.dto.PropertiesMigrationPayload;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.authorization.AuthorizationManager;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static com.highroads.commons.HrSystem.AUTHORIZATION_HEADER_NAME;

@RestController
@RequestMapping("/maintenance")
@Slf4j
public class MaintenanceController {

    private MaintenanceService maintenanceService;

    @Autowired
    public MaintenanceController(MaintenanceService maintenanceService) {
        this.maintenanceService = maintenanceService;
    }

    /**
     * Removes properties from object
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{objectType}/{objectId}/properties", method = RequestMethod.DELETE, consumes = "application/json")
    public ResponseEntity clearProperties(@PathVariable("objectType") P2ACollection objectType,
                                          @PathVariable("objectId") String objectId,
                                          @RequestBody List<String> propertiesForRemoval,
                                          @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        log.info("About to remove properties {} from objectType: {}, objectId: {}", propertiesForRemoval, objectType, objectId);
        UserToken userToken = UserToken.deserialize(authorization);
        Map<String, Object> updatedObject = maintenanceService.removeProperties(userToken, objectId, objectType, propertiesForRemoval);

        log.info("Successfully removed properties {} from objectId: {}", propertiesForRemoval, objectId);
        return ResponseEntity.ok(updatedObject);
    }

    /**
     * Renames properties from object, effectively meaning old ones are dropped, new ones created.
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{objectType}/{objectId}/properties/migrate", method = RequestMethod.PATCH, consumes = "application/json")
    public ResponseEntity migrateProperties(@PathVariable("objectType") P2ACollection objectType,
                                                     @PathVariable("objectId") String objectId,
                                                     @RequestBody PropertiesMigrationPayload payload,
                                                     @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        UserToken userToken = UserToken.deserialize(authorization);
        Map<String, Object> updatedObject = maintenanceService.migrateProperties(userToken, objectId, objectType, payload);

        log.info("Successfully renamed properties {} for objectId: {}", payload.getPropertiesForRenaming(), objectId);
        return ResponseEntity.ok(updatedObject);
    }
}
