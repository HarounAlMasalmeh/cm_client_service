package com.highroads.cm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//TODO Relocate this to commons
public interface HasLogger {
    default Logger getLogger() {
        return LoggerFactory.getLogger(getClass());
    }
}
