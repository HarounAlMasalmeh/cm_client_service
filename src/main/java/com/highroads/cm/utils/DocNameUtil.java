package com.highroads.cm.utils;

import java.util.List;
import java.util.Map;
/**
 * This class is used to evaluate docNameConvetions in globalSettings
 * @author chong
 *
 */
public class DocNameUtil {

    final static String CONDITION_INDICATOR = "?";
    final static String PROPERTY_INDICATOR = ".";
    final static String TRUEFALSE_DIVIDER = ":";
    final static String OPERATOR_EQUALS = "=";

    public String evaluateToken(String expression, Map<String, Object> offering, Map<String, Object> plan, Map<String, Object> template){
        String returnString = expression;
        if (expression.contains(CONDITION_INDICATOR)){
            String trueExpression = null;
            String falseExpression = null;
            String trueFalseBlock = expression.substring(expression.indexOf(CONDITION_INDICATOR)+1);
            if (trueFalseBlock.split(TRUEFALSE_DIVIDER).length==1){
                if(trueFalseBlock.startsWith(TRUEFALSE_DIVIDER)){
                    trueExpression = "";
                    falseExpression = trueFalseBlock.replace(TRUEFALSE_DIVIDER, "");
                }else{
                    trueExpression = trueFalseBlock.replace(TRUEFALSE_DIVIDER, "");
                    falseExpression = "";
                }
            }else {
                trueExpression = trueFalseBlock.substring(0, expression.indexOf(TRUEFALSE_DIVIDER));
                falseExpression = trueFalseBlock.substring(expression.indexOf(TRUEFALSE_DIVIDER) + 1);
            }

            if(evaluateBoolean(expression.substring(0, expression.indexOf(CONDITION_INDICATOR)), offering, plan, template)){
                returnString = evaluateValue(trueExpression, offering, plan, template);
            }else {
                returnString = evaluateValue(falseExpression, offering, plan, template);
            }
        }else if(expression.contains(PROPERTY_INDICATOR)){
            returnString = evaluateValue(expression, offering, plan, template);
        }
        // else, just returns static string as is if no evaluation is required
        return returnString;
    }

    /**
     * Currently supports looking up property values in objects to check if they match a certain value
     * @param substring
     * @param offering
     * @param plan
     * @param template
     * @return
     */
    private boolean evaluateBoolean(String expression, Map<String, Object> offering, Map<String, Object> plan, Map<String, Object> template) {
        String leftExpr = expression.substring(0, expression.indexOf(OPERATOR_EQUALS));
        String rightExpr = expression.substring(expression.lastIndexOf(OPERATOR_EQUALS)+1);

        Object leftObj = evaluateToken(leftExpr, offering, plan, template);
        Object rightObj = evaluateToken(rightExpr, offering, plan, template);

        if((leftObj instanceof List)^(rightObj instanceof List)){
            if(leftObj instanceof List)
                return ((List)leftObj).contains(rightObj);
            else
                return ((List)rightObj).contains(leftObj);
        }else{
            return leftObj.equals(rightObj);
        }
    }

    private String evaluateValue(String expression, Map<String, Object> offering, Map<String, Object> plan, Map<String, Object> template) {
        String returnString = "";
        if(!expression.contains(PROPERTY_INDICATOR))
            return expression;
        Object value = null;
        switch(expression.substring(0, expression.indexOf(PROPERTY_INDICATOR))){
            case "offering":
                value = offering.getOrDefault(expression.substring(expression.lastIndexOf(PROPERTY_INDICATOR)+1), "");
                break;
            case "plan":
                value = plan.getOrDefault(expression.substring(expression.lastIndexOf(PROPERTY_INDICATOR)+1), "");
                break;
            case "template":
                value = template.getOrDefault(expression.substring(expression.lastIndexOf(PROPERTY_INDICATOR)+1), "");
                break;
        }
        returnString = value instanceof List? (String)((List)value).get(0): (String) value;
        return returnString;
    }

}
