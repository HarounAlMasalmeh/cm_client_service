package com.highroads.cm;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(locations="classpath:ModelFilter.properties")
public class ModelMapping {
    private List<String> excludeProperties;
    private List<String> excludeIngressProperties; // known props to ignore on incoming requests

    public List<String> getExcludeProperties() {
        return excludeProperties;
    }

    public void setExcludeProperties(List<String> excludeProperties) {
        this.excludeProperties = excludeProperties;
    }

    public List<String> getExcludeIngressProperties() {
        return excludeIngressProperties;
    }

    public void setExcludeIngressProperties(List<String> excludeIngressProperties) {
        this.excludeIngressProperties = excludeIngressProperties;
    }

    public String convertToCmisId(String objectId) {
        if (objectId.contains(":") == false) {
            return "D:hr:" + objectId;
        }

        return objectId;
    }

    public boolean filterRelationship(String relationId) {
        if (relationId.contains(":hr:")) {
            return true;
        }

        return false;
    }

    public String getSimpleName(String id) {
        if (id.contains(":")) {
            String[] tokens = id.split(":");
            return tokens[tokens.length - 1];
        }

        return id;
    }

    public String getNamespace(String id) {
        if (id.contains(":")) {
            String[] tokens = id.split(":");
            return tokens[tokens.length - 2];
        }

        return id;
    }

    public boolean filterProperty(String property) {
        if (excludeProperties.contains(getSimpleName(property))) {
            return false;
        }

        return true;
    }
}
