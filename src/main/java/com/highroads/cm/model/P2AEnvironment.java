package com.highroads.cm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class P2AEnvironment {

    @JsonProperty
    private String name;

    @JsonProperty
    private String key;

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

}
