/**
 *
 */
package com.highroads.cm.version.controller;

import java.util.List;
import java.util.Map;

import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;

/**
 * @author Shahin
 *
 *         Interface for versioning APIs called from
 *         {@link GenericVersionController}.
 *         All the methods in this interface are implemented in
 *         {@link MLVersionContentServiceImpl} class.
 */

public interface VersionContentService {

    public Map<String, Object> isVersioned(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public void breakCheckout(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public void checkin(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public void checkin(final UserToken userToken, final P2ACollection objectType, final String objectId, final String retain) throws Exception;

    public void checkout(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public List<Map<String, Object>> getHistory(final UserToken userToken, final P2ACollection objectType, final String objectId, final Integer offset,
            final Integer limit, boolean includeNames) throws Exception;

    public List<Map<String, Object>> getHistory(final UserToken userToken, final P2ACollection objectType, final String objectId, final Integer offset,
            final Integer limit, String format, boolean includeNames) throws Exception;

    public String revert(final UserToken userToken, final P2ACollection objectType, final String objectId, final Integer versionId, String annotation)
            throws Exception;

    public String manageJSON(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public String manageBinary(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public Map<String, Object> getStatus(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception;

    public Map<String, Object> convertToVersioned(final UserToken userToken, final P2ACollection objectType) throws Exception;

    public List<?> getObjectCollection(UserToken userToken, P2ACollection singularObjectType, Integer associationExpansionLevel, String properties,
            Integer startIndex, String searchQuery);
}
