
package com.highroads.cm.version.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrBadRequestException;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.api.exceptions.HrException;
import com.highroads.commons.api.exceptions.HrInternalServerErrorException;
import com.highroads.commons.config.MarklogicConnectionProperties;
import com.highroads.commons.dao.DocumentVersionManager;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.dao.VersionHistory;
import com.highroads.commons.dao.VersionUtil;
import com.highroads.commons.model.ModelManager;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.search.MarklogicSearchManager;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;
import com.highroads.p2alibs.contentservice.DocumentFactoryBase;
import com.highroads.p2alibs.contentservice.DocumentVersionManagerFactory;
import com.highroads.p2alibs.contentservice.ModelSchemaService;
import com.marklogic.client.FailedRequestException;
import com.marklogic.client.document.ServerTransform;
import com.marklogic.client.impl.FailedRequest;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.query.QueryDefinition;
import com.marklogic.client.query.QueryManager;

/**
 * @author Shahin
 *
 *         MarkLogic Implementation for {@link VersionContentService}.
 *         Contains implementation of non-CRUD methods related to version
 *         management of objects (example: checkin, checkout, getHistory, etc.).
 *
 *         The following comments apply to all the methods implemented in this
 *         class:
 *
 * @param userToken
 * @param objectType
 *            example: plans
 * @param objectId
 *            example: e6e912a3-1cf9-4e7c-9a9b-afb781e59f8f
 *
 *
 * @throws HrBadRequestException
 *             if userToken is null
 *             if objectType is null or empty
 *             if objectId is null or empty
 *             HrUnauthorizedException if user is not authorized to perform this
 *             action for given objectType
 *             HrInternalServerErrorException if document does not
 *             exist(document represented by objectType and objectId)
 *
 */

@Component
public class MLVersionContentServiceImpl implements VersionContentService {

    @Autowired
    private MarklogicConnectionProperties mlProperties;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${search.service.maxItems}")
    private Integer searchServiceMaxItems;

    private ValidationUtil validator = new ValidationUtil();
    private VersionUtil versionUtil = new VersionUtil();
    private ContentService contentService;
    private ModelSchemaService modelSchemaService;

    private final MarklogicSearchManager marklogicSearchManager;
    private final MarklogicDAO marklogicDAO;

    @Autowired
    private ModelManager modelManager;


    @Autowired
    DocumentVersionManagerFactory documentVersionManagerFactory;

    @Autowired
    DocumentFactoryBase documentFactoryBase;

    @Autowired
    public MLVersionContentServiceImpl(final ModelSchemaService modelSchemaService, MarklogicSearchManager marklogicSearchManager, MarklogicDAO marklogicDAO,
            ContentService contentService) {
        this.modelSchemaService = modelSchemaService;
        this.marklogicSearchManager = marklogicSearchManager;
        this.contentService = contentService;
        this.marklogicDAO = marklogicDAO;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     *
     * Verifies if an object represented by objectType and objectId is managed
     * (versioned) by MarkLogic.
     *
     * @return true if the object is versioned (managed by MarkLogic)
     *
     *         Please refer to the header comments for more info about the
     *         parameters and exceptions thrown
     *         by this method.
     */

    @Override
    public Map<String, Object> isVersioned(final UserToken userToken, final P2ACollection objectType, final String objectId) throws HrException {

        validator.validate(userToken, objectType, objectId);

        try {
            DocumentVersionManager document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);
            return document.isVersioned();
        } catch (FailedRequestException e) {
            throwHighRoadException("isManaged", String.format("/%s/%s", objectType, objectId), e);
        }
        return Collections.emptyMap();
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Called by admin user to break the checkout on a document checked out by
     * another user.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @Override
    public void breakCheckout(final UserToken userToken, final P2ACollection objectType, final String objectId) throws HrException {

        validator.validate(userToken, objectType, objectId);

        try {
            DocumentVersionManager document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);
            document.breakCheckout();
        } catch (FailedRequestException e) {
            throwHighRoadException("breakCheckout", String.format("/%s/%s", objectType, objectId), e);
        }
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     *
     * creates and throws a HrException to the caller.
     * called by {@link #breakCheckout}, {@link #checkin}, {@link #checkout},
     * {@link #isManaged},
     * {@link #getAllVersionsAscendingOrder}, {@link #manageBinary},
     * {@link #manageJSON} and {@link #revert}
     *
     * @param title
     *            example: breakCheckout
     * @param uri
     *            example: /plans/e6e912a3-1cf9-4e7c-9a9b-afb781e59f8f *
     * @param e
     *            an instance of FailedRequestException
     *
     * @throws HrException
     *             constructed with the parameters of the method.
     */

    private void throwHighRoadException(String title, String uri, FailedRequestException e) throws HrException {

        FailedRequest f = e.getFailedRequest();
        int status = -1;
        if (f != null) {
            status = f.getStatusCode();
        }
        HrException hre = ExceptionHelper.mapStatus(status, e.getMessage(), title, uri);
        throw hre;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Checks in an already checked out document in MarkLogic so other users can
     * modify it.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @Override
    public void checkin(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception {
        checkin(userToken, objectType, objectId, null);
    }

    /**
     *
     * Checks in an already checked out document.
     *
     * If you checkout a document, make many updates to it and decide to cancel
     * the checkout, you call checkin with
     * retain = "false" ;
     * Called with (retain = "false"), the checkin method will revert the
     * document to its previous version.
     *
     * @param retain
     *            if "true" A new version of the document is created with the
     *            content of the latest version.
     *            This is the normal use case when a user checks out a document,
     *            makes many updates to it
     *            and then checks the document in. At checkin the last version
     *            of the document is used to create
     *            a new version.
     *
     *            if "false" The document is reverted to its previous version.
     *            This is the use case when a user checks out a document and
     *            then checks it in without any changes.
     *            Or user checks out the document, makes many changes, and then
     *            decides to disregard the changes
     *            and checks the document in.
     */
    @Override
    public void checkin(final UserToken userToken, final P2ACollection objectType, final String objectId, final String retain) throws Exception {

        validator.validate(userToken, objectType, objectId);

        boolean retentionFlag = true;
        if (retain != null && retain.equalsIgnoreCase("false")) {
            retentionFlag = false;
        }

        try {
            DocumentVersionManager document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);

            if (retentionFlag == true) {
                document.checkin();
            } else {
                Integer versionId = documentFactoryBase.getVersionIdFromVersionedObjectId(objectId);
                if (versionId == null) {
                    versionId = getLatestVersionId(userToken, objectType, objectId, false);
                }
                String annotation = getAnnotation(document, versionId);
                revert(userToken, objectType, objectId, versionId, annotation);
            }

        } catch (FailedRequestException e) {
            throwHighRoadException("checkin", String.format("/%s/%s", objectType, objectId), e);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    private Integer getLatestVersionId(final UserToken userToken, final P2ACollection objectType, final String objectId, boolean includeNames)
            throws Exception {

        List<Map<String, Object>> allVersions = getAllVersionsAscendingOrder(userToken, objectType, objectId, includeNames);
        Collections.reverse(allVersions);
        Integer previousVersionId = Integer.valueOf((String) allVersions.get(1).get("version-id"));
        return previousVersionId;

    }

    // ----------------------------------------------------------------------------------------------------

    private String getAnnotation(DocumentVersionManager document, Integer versionId) {

        List<Map<String, Object>> allVersions = document.getHistory();

        String previousAnotation = null;
        Integer previousVersionId = null;
        for (Map<String, Object> version : allVersions) {
            Integer mapVersionId = Integer.valueOf((String) version.get("version-id"));
            if (mapVersionId != null && mapVersionId.equals(versionId)) {
                previousAnotation = (String) version.get("annotation");
                previousVersionId = Integer.valueOf((String) version.get("version-id"));
            }
        }

        return String.format("Checkout was canecled by user. Revert to version %s: %s", previousVersionId, previousAnotation);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Checks out a document in MarkLogic so other users cannot modify it.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @Override
    public void checkout(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception {

        validator.validate(userToken, objectType, objectId);

        try {
            DocumentVersionManager document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);
            document.checkout();
        } catch (FailedRequestException e) {
            throwHighRoadException("checkout", String.format("/%s/%s", objectType, objectId), e);
        }
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Gets version history on a document represented by objectType and
     * objectId.
     * some examples:
     * imagine that a document has 10 versions.
     * following shows how the return list will vary based on the values of
     * offset and limit parameters:
     *
     * offset limit result
     * null null all versions
     * null 4 last 4 versions
     * 0 5 5 first versions
     * 3 5 5 versions from 3rd (included) to 7th (included)
     *
     * @param offset
     *            optional. example: 4 means return the version history from
     *            version 4 and above
     * @param limit
     *            optional. example: 5 means return only 5 versions in the list
     *            starting from offset version
     *
     *            Please refer to the header comments for more info about the
     *            parameters and exceptions thrown
     *            by this method.
     */

    @Override
    public List<Map<String, Object>> getHistory(final UserToken userToken, final P2ACollection objectType, final String objectId,
            final Integer offset, final Integer limit, boolean includeNames) throws Exception {

        validator.validate(userToken, objectType, objectId);

        if (validator.noUriVariables(offset, limit)) {
            return getAllVersionsDescendingOrder(userToken, objectType, objectId, includeNames);
        }

        if (validator.bothUriVariablesProvided(offset, limit)) {
            return limitedVersions(userToken, objectType, objectId, offset, limit, includeNames);
        }

        // only limit is provided. return last versions
        return limitedLastVersions(userToken, objectType, objectId, limit, includeNames);
    }

    @Override
    public List<Map<String, Object>> getHistory(final UserToken userToken, final P2ACollection objectType, final String objectId,
            final Integer offset, final Integer limit, String format,boolean includeNames) throws Exception {

        validator.validate(userToken, objectType, objectId);

        if (validator.bothUriVariablesProvided(offset, limit)) {
            return limitedBranches(userToken, objectType, objectId, offset, limit, format,includeNames);
        }

        // only limit is provided. return last versions
        return limitedLastBranches(userToken, objectType, objectId, limit, format,includeNames);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * called internally by {@link #getHistory} method. This method gets the
     * history list from MarkLogic and sorts the list in a descending order
     * meaning the last version being the first in the returned list.
     *
     * @see #getHistory for more detail
     */

    private List<Map<String, Object>> getAllVersionsDescendingOrder(final UserToken userToken, final P2ACollection objectType, final String objectId,
            boolean includeNames)
            throws Exception {

        List<Map<String, Object>> allVersions = getAllVersionsAscendingOrder(userToken, objectType, objectId, includeNames);
        Collections.reverse(allVersions);
        return allVersions;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * called internally by {@link #getAllVersionsDescendingOrder} and
     * {@link #limitedVersions} methods.
     * This method gets the history list from MarkLogic in an ascending order
     * meaning the last version being the last in the returned list.
     *
     * @see #getHistory for more detail.
     */

    private List<Map<String, Object>> getAllVersionsAscendingOrder(final UserToken userToken, final P2ACollection objectType, final String objectId,
            boolean includeNames)
            throws Exception {

        VersionHistory allVersions = null;
        try {
            DocumentVersionManager document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);

            // document.getHistory() returns all the versions for a document. Including the versions that have been deleted as the result of applying
            // retention policy
            allVersions = new VersionHistory(document.getHistory());
            allVersions.removeDeletedVersions();
            allVersions.insertPrettyVersionId();
            allVersions.forceBooleanType();
            if (includeNames) {
                insertUserNameAndObjectName(userToken, objectType, contentService, allVersions.getHistory());
            }
        } catch (FailedRequestException e) {
            throwHighRoadException("getHistory", String.format("/%s/%s", objectType, objectId), e);
        }
        return allVersions.getHistory();
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     */

    protected void insertUserNameAndObjectName(final UserToken userToken, final P2ACollection objectType, final ContentService contentService,
            List<Map<String, Object>> history) {
        for (Map<String, Object> map : history) {

            String objectId = (String) map.get("document-uri");
            String version = (String) map.get("version-id");
            Map<String, Object> objectContent = contentService.getObjectById(userToken, objectId, objectType, null,
                    new String[] { "lastModifiedBy", "name" }, Integer.valueOf(version), null, true);
            map.put("author", objectContent.get("lastModifiedBy"));
            map.put("objectName", objectContent.get("name"));
        }
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     *
     * Called internally by {@link #getHistory} to retrieve a sublist of the
     * versions for a document.
     * This method is called if both offset and limit parameters are provided.
     *
     * @param offset
     *            example: 4 means return the version history from index 4 and
     *            above
     * @param limit
     *            example: 5 means return only 5 versions in the list starting
     *            from offset version
     *
     * @return a sublist of version history defined by offset and limit
     *         parameters
     *
     * @see #getHistory for more detail.
     */

    private List<Map<String, Object>> limitedVersions(final UserToken userToken, final P2ACollection objectType, final String objectId,
            final Integer offset, final Integer limit, boolean includeNames) throws Exception {

        List<Map<String, Object>> result = new LinkedList<>();

        List<Map<String, Object>> allVersions = getAllVersionsAscendingOrder(userToken, objectType, objectId, includeNames);

        validator.checkOffsetLimitCombination(offset, limit, allVersions.size());

        for (int index = 0; index < allVersions.size(); index++) {
            if (index + 1 >= offset && index + 1 < offset + limit) {
                result.add(allVersions.get(index));
            }
        }

        Collections.reverse(result);
        return result;
    }

    private List<Map<String, Object>> limitedBranches(final UserToken userToken, final P2ACollection objectType, final String objectId,
            final Integer offset, final Integer limit, String format, boolean includeNames) throws Exception {

        List<Map<String, Object>> result = new LinkedList<>();

        List<Map<String, Object>> allVersions = getAllVersionsAscendingOrder(userToken, objectType, objectId, includeNames);

        validator.checkOffsetLimitCombination(offset, limit, allVersions.size());

        for (int index = 0; index < allVersions.size(); index++) {
            if (index + 1 >= offset && index + 1 < offset + limit) {
                result.add(allVersions.get(index));
            }
        }

        Collections.reverse(result);
        return result;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     *
     * Called internally by {@link #getHistory} to retrieve a sublist of last
     * versions for a document.
     *
     * @param limit
     *            example: 5 means return only 5 last versions
     *
     * @return a sublist of last versions of the document defined by limit
     *         parameter
     *
     * @see #getHistory for more detail.
     */

    private List<Map<String, Object>> limitedLastVersions(final UserToken userToken, final P2ACollection objectType, final String objectId,
            final Integer limit, boolean includeNames) throws Exception {
        new LinkedList<Map<String, Object>>();
        List<Map<String, Object>> allVersions = getAllVersionsDescendingOrder(userToken, objectType, objectId, includeNames);

        validator.checkLimit(limit, allVersions.size());

        return allVersions.subList(0, limit);
    }

    private List<Map<String, Object>> limitedLastBranches(final UserToken userToken, final P2ACollection objectType, final String objectId,
            final Integer limit, String format, boolean includeNames) throws Exception {
        new LinkedList<Map<String, Object>>();
        List<Map<String, Object>> allVersions = getAllVersionsDescendingOrder(userToken, objectType, objectId, includeNames);

        validator.checkLimit(limit, allVersions.size());

        return allVersions.subList(0, limit);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Reverts a versioned document to a previous version.
     *
     * @param versionId
     *            a versionId inferior to the last versionId of the document
     *
     *
     * @throws HrBadRequestException
     *             if versionId is null or empty
     *             is a negative number
     *
     *             Please refer to the header comments for more info about the
     *             parameters and exceptions thrown
     *             by this method.
     */

    @Override
    public String revert(final UserToken userToken, final P2ACollection objectType, final String objectId, Integer versionId, String annotation)
            throws Exception {

        validator.validate(userToken, objectType, objectId);
        validator.validateVersionId(versionId);

        DocumentVersionManager document = null;
        try {

            if (versionId == -1) {
                versionId = getPreviousVersionId(userToken, objectType, objectId);
            }
            document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId, versionId, annotation);
        } catch (FailedRequestException e) {
            throwHighRoadException("revert", String.format("%s/%s?versionId=%s", objectType, objectId, versionId), e);
        }
        return document.revert();
    }

    // ----------------------------------------------------------------------------------------------------

    private Integer getPreviousVersionId(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception {

        List<Map<String, Object>> versions = getHistory(userToken, objectType, objectId, null, null, false);

        // versions list is sorted. list[0] contains the latest version.
        if (versions.size() >= 2) {
            return Integer.valueOf((String) versions.get(1).get("version-id"));
        } else if (versions.size() == 1) {
            return Integer.valueOf((String) versions.get(0).get("version-id"));
        }
        throw new FailedRequestException(
                String.format("Failed to get previous verisonId. Only %d versions available for %s/%s" + versions.size(), objectType, objectId));

    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Puts an existing JSON document represented by objectType and objectId
     * under management (versioning).
     *
     * @throws HrInternalServerErrorException
     *             if document does not exist (document represented by
     *             objectType and objectId)
     *             if document cannot be put under versioning
     *
     *             Please refer to the header comments for more info about the
     *             parameters and exceptions thrown
     *             by this method.
     */

    @Override
    public String manageJSON(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception {

        validator.validate(userToken, objectType, objectId);

        DocumentVersionManager document = null;
        try {
            document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);
        } catch (FailedRequestException e) {
            throwHighRoadException("manage", String.format("/%s", objectType), e);
        }
        return document.manageJSON();
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Puts an existing binary document represented by objectType and objectId
     * under management (versioning).
     *
     * @throws HrInternalServerErrorException
     *             if document does not exist (document represented by
     *             objectType and objectId)
     *             if document cannot be put under versioning
     *
     *             Please refer to the header comments for more info about the
     *             parameters and exceptions thrown
     *             by this method.
     */

    @Override
    public String manageBinary(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception {

        validator.validate(userToken, objectType, objectId);

        DocumentVersionManager document = null;
        try {
            document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);
        } catch (FailedRequestException e) {
            throwHighRoadException("manageBinary", String.format("/%s", objectType), e);
        }
        return document.manageBinary();
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Gets the lock status of a versioned document.
     * The returned status looks like this:
     * {
     * "lockedBy": "admin@local-shahin.com",
     * "isLocked": true,
     * "lockedAt": "2016-09-13T18:13:30.000-04:00"
     * }
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown by this method.
     */

    @Override
    public Map<String, Object> getStatus(final UserToken userToken, final P2ACollection objectType, final String objectId) throws Exception {

        validator.validate(userToken, objectType, objectId);

        DocumentVersionManager document = null;
        try {
            document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType, objectId);
        } catch (FailedRequestException e) {
            throwHighRoadException("manageBinary", String.format("/%s", objectType), e);
        }
        Map<String, Object> status = document.getStatus();

        Map<String, Object> result = new HashMap<>();
        String sIsLocked = (String) status.get("status");
        if (sIsLocked.equalsIgnoreCase("locked")) {
            result.put("isLocked", Boolean.TRUE);
        } else {
            result.put("isLocked", Boolean.FALSE);
        }
        result.put("lockedBy", status.get("username"));

        String lockedAt = getPrettyLockTime(status);
        // "lockedAt": "2016-09-13T18:13:30.000-04:00"
        if (!lockedAt.isEmpty()) {
            result.put("lockedAt", lockedAt);
        }

        return result;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * converts a long milliseconds to a human readable format
     *
     * @param documentStatus
     *            map containing the milliseconds in long
     * @return human readable format of the time:
     *         "JUNE 13 2016 at 14:58:26"
     */

    private String getPrettyLockTime(Map<String, Object> documentStatus) {

        if (documentStatus.get("lockTime") == null) {
            return "";
        }
        return versionUtil.getPrettyTime((Long) documentStatus.get("lockTime"));
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * converts all the documents of type objectType from non-versioned to
     * versioned.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> convertToVersioned(final UserToken userToken, final P2ACollection objectType) throws Exception {

        validator.validate(userToken, objectType);

        DocumentVersionManager document = null;
        try {
            document = documentVersionManagerFactory.create(userToken.getOrganization(), userToken.getUsername(), objectType);
        } catch (FailedRequestException e) {
            throwHighRoadException("manageBinary", String.format("/%s", objectType), e);
        }

        List<ObjectNode> results = null;
        int startIndex = 0;
        String objectId = "";
        int totalConversions = 0;
        int successfulConversions = 0;
        int failedConversions = 0;
        int alreadyManaged = 0;
        do {

            // I have seen search failing on bad binary data (with shadowId 0) on my local machine.
            // this will bypass that failure and continue with the rest of documents (instead of dying).
            try {
                String searchQuery = String.format("TYPE:\"%s\"", objectType);

                results = (List<ObjectNode>) getObjectCollection(userToken, objectType, 0, "objectId", startIndex, searchQuery);
            } catch (Exception e) {
                logger.error("search failed with this error: " + e.getMessage());
                startIndex++;
                continue;
            }

            for (ObjectNode objectNode : results) {
                try {
                    if (objectNode.get("objectId") != null) {
                        objectId = objectNode.get("objectId").asText();
                    } else {
                        continue;
                    }
                    validator.validateString("objectId", objectId);
                    if (document.manage()) {
                        successfulConversions++;
                    } else {
                        alreadyManaged++;
                    }
                } catch (Exception e) {
                    logger.error("failed to convert objectId:" + objectId + ": " + e.getMessage());
                    failedConversions++;
                }
                totalConversions++;
            }
            startIndex += results.size();
        } while (results == null || results.size() > 0);

        logger.info(String.format("total number of " + objectType + "s found                  : %10d", totalConversions));
        logger.info(String.format("      number of " + objectType + "s successfully converted : %10d", successfulConversions));
        logger.info(String.format("      number of " + objectType + "s already versioned      : %10d", alreadyManaged));
        logger.info(String.format("      number of " + objectType + "s failed to convert      : %10d", failedConversions));

        Map<String, Object> result = new LinkedHashMap<>();
        result.put("number of " + objectType + "s successfully converted", successfulConversions);
        result.put("number of " + objectType + "s failed to convert", failedConversions);

        return result;
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<?> getObjectCollection(UserToken userToken, P2ACollection singularObjectType, Integer associationExpansionLevel, String properties,
            Integer startIndex, String searchQuery) {

        final long startTime = System.nanoTime();
        final QueryManager queryManager = marklogicDAO.getDatabaseClient(userToken.getUsername(), userToken.getOrganization()).newQueryManager();
        ArrayList<JsonNode> docsArray = new ArrayList<>();
        JsonNode jsonResults;
        QueryDefinition query = null;
        if (startIndex == null) {
            startIndex = 0;
        }
        try {
            query = marklogicSearchManager.getSearchQueryDefinition(userToken.getOrganization(), userToken.getUsername(), queryManager, searchQuery, null, null,
                    null, null, null, null, false);
            addTransform(associationExpansionLevel, properties, query, userToken);
        } catch (final ParseException e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    String.format("Error parsing search query / Invalid search query parameter - q: \"%s\"", searchQuery));
        }

        do {
            final JacksonHandle results = queryManager.search(query, new JacksonHandle(), Long.valueOf(startIndex));
            jsonResults = results.get();
            final ArrayNode resultsArray = (ArrayNode) jsonResults.findPath("results");
            logger.debug(String.format("Search total found: %d", jsonResults.get("total").asLong()));
            logger.debug(String.format("Search total time: %s", jsonResults.get("metrics").get("total-time").asText()));
            for (final JsonNode resultDoc : resultsArray) {
                docsArray.add(resultDoc.get("content"));
            }
            if (docsArray.size() == 100) {
                logger.warn("Exceeded the Max size of Search result");
                break;
            }
            startIndex += jsonResults.get("page-length").asInt();
        } while (startIndex < jsonResults.get("total").asInt());

        return docsArray;
    }

    // ----------------------------------------------------------------------------------------------------

    private void addTransform(Integer associationExpansionLevel, String properties, QueryDefinition query, UserToken userToken) {

        final String expansionLevel = associationExpansionLevel == null ? String.valueOf(0) : String.valueOf(associationExpansionLevel);
        final ServerTransform serverTransform = new ServerTransform(mlProperties.getAssociationTransform());
        serverTransform.add("trans:associationExpansionLevel", expansionLevel);
        serverTransform.add("trans:organization", userToken.getOrganization());
        serverTransform.add("trans:origin", "search");
        if (null != properties) {
            serverTransform.add("trans:properties", StringUtils.split(properties, ","));
        }
        query.setResponseTransform(serverTransform);

    }
}
