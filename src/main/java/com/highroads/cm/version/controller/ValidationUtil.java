package com.highroads.cm.version.controller;

import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;

import lombok.NonNull;

/**
 *
 * @author Shahin
 *
 *         Utility class used for validating parameters passed to Versioning
 *         APIs.
 *
 */

public class ValidationUtil {

    // ----------------------------------------------------------------------------------------------------

    public void validate(final UserToken userToken, final P2ACollection objectType) {

        validate(userToken);
    }

    // ----------------------------------------------------------------------------------------------------

    public void validate(final UserToken userToken, final P2ACollection objectType, final String objectId) {

        validate(userToken);
        validateString("objectId", objectId);
    }

    // ----------------------------------------------------------------------------------------------------

    public void validateVersionId(@NonNull final Integer versionId) {

        validatePositiveOrMinusOne(versionId);
    }

    // ----------------------------------------------------------------------------------------------------

    public void validatePositiveOrMinusOne(final Integer versionId) {

        try {

            Validate.isTrue(versionId > 0 || versionId == -1,
                    "versionId should be positive number or -1 (to revert to previous version)", versionId);
        } catch (Exception e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public void validate(final UserToken userToken, final P2ACollection objectType, final Map<String, Object> objectProperties) {

        validate(userToken);
        validate(objectProperties);
    }

    // ----------------------------------------------------------------------------------------------------

    public void validate(final UserToken userToken, final P2ACollection objectType, byte[] binaryArray) {

        validate(userToken);
        validate(binaryArray);
    }

    // ----------------------------------------------------------------------------------------------------

    public void validate(final UserToken userToken) {

        try {
            Validate.notNull(userToken, "userToken cannot be null");
        } catch (Exception e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public void validate(final Map<String, Object> objectProperties) {

        try {
            Validate.notNull(objectProperties, "objectProperties cannot be null");
        } catch (Exception e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public void validate(final byte[] binaryArray) {

        try {
            Validate.notNull(binaryArray, "binary array cannot be null");
        } catch (Exception e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public void validateString(final String name, final String value) {

        try {
            Validate.notNull(value, name + " cannot be null");
            Validate.notEmpty(value, name + " cannot be empty");

            if (name.equalsIgnoreCase("objectId")) {
                int dashOccurrences = StringUtils.countOccurrencesOf(value, "-");
                if (dashOccurrences < 4) {
                    throw new Exception("invalid objectId: " + value);
                }

            }
        } catch (Exception e) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST, e);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public void checkLimit(final Integer limit, final Integer size) throws Exception {

        String message = null;

        if (limit == null || limit < 1) {
            message = "invliad limit " + limit;
        } else if (limit > size) {
            message = "limit should be less than or equal to " + size;
        }

        if (!(null == message)) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public void checkOffsetLimitCombination(final Integer offset, final Integer limit, final Integer size) throws Exception {

        String message = null;

        if (offset == null || offset < 1) {
            message = "invliad offset " + offset;
        } else if (limit == null || limit < 1) {
            message = "invliad limit " + limit;
        } else if (limit + offset > size + 1) {
            message = "(limit + offset) should be less than or equal to " + (size + 1);
        }

        if (!(null == message)) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public boolean bothUriVariablesProvided(final Integer offset, final Integer limit) {
        return offset != null && limit != null;
    }

    // ----------------------------------------------------------------------------------------------------

    public boolean noUriVariables(final Integer offset, final Integer limit) {
        return offset == null && limit == null;
    }
}
