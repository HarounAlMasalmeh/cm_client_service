package com.highroads.cm.version.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.highroads.cm.web.controller.GenericController;
import com.highroads.commons.HrSystem;
import com.highroads.commons.api.exceptions.HrBadRequestException;
import com.highroads.commons.authorization.AuthorizationManager;
import com.highroads.commons.dao.MarklogicDAO;
import com.highroads.commons.model.ModelManager;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.constants.P2ALibConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.Authorization;

/**
 * @author Shahin
 *
 *         Generic controller for Versioning in P2A.
 *         Rest APIs for managing versioned documents/objects in P2A.
 *
 *         All CRUD operations on versioned objects are performed in CM's
 *         {@link GenericController}.
 *
 *         This controller is used for management operations on versioned
 *         objects. These operations are:
 *         isVersioned
 *         getHistory
 *         revert
 *         checkout
 *         checkin
 *         breakCheckout
 *         manage
 *         manageJSON
 *         manageBinary
 *         getStatus
 *
 *         the following comments apply to all the methods in this controller:
 *
 * @param objectType
 *            example: plans
 * @param objectId
 *            example: e6e912a3-1cf9-4e7c-9a9b-afb781e59f8f
 * @param authorization
 *            user authorization token for the caller of this API
 *
 * @throws HrBadRequestException
 *             if authorization is null or empty
 *             if objectType is null or empty
 *             if objectId is null
 *             HrInternalServerErrorException if document does not exist
 *             is not managed
 *             HrUnauthorizedException if user is not authorized to perform this
 *             action for given objectType
 *
 */

@RestController
@Api(tags = { "Versioning Content Manager" }, authorizations = { @Authorization(value = "Token Authentication", scopes = {}) })
@ApiModel(value = "Versioning Content Manager API Documentation", description = "Interact with Content Manager")

public class GenericVersionController implements HrSystem {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    private AuthorizationManager authorizationManager;
    private VersionContentService versionContentService;
    @Autowired
    private ModelManager modelManager;
    // ----------------------------------------------------------------------------------------------------

    @Autowired
    public GenericVersionController(AuthorizationManager authorizationManager, VersionContentService versionContentService, MarklogicDAO marklogicDAO) {

        this.authorizationManager = authorizationManager;
        this.versionContentService = versionContentService;
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Verifies if a document is versioned or not.
     *
     * @return {"isVersioned": "true"} if this document is a managed document in
     *         ML
     *         {"isVersioned": "false"} otherwise
     *
     *         Please refer to the header comments for more info about the
     *         parameters and exceptions thrown
     *         by this method.
     */

    @RequestMapping(value = "/isVersioned", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> isVersioned(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/isVersioned" + "?objectType=" + objectType);

        authorizationManager.authorize(authorization, RequestMethod.GET, objectType.getObjectType());

        return new ResponseEntity<>(
                versionContentService.isVersioned(UserToken.deserialize(authorization), objectType, objectId), HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    /**
     * Retrieves the list of versions for a document.
     *
     * some examples:
     * imagine that a document has 10 versions.
     * following shows how the return list will vary based on the values of
     * offset and limit parameters:
     *
     * offset limit result
     * null null all versions
     * null 4 last 4 versions
     * 0 5 5 first versions
     * 3 5 5 versions from 3rd (included) to 7th (included)
     *
     * @param offset
     *            optional argument for the starting index of the versions to
     *            retrieve. For example
     *            to retrieve the first three versions of a document use:
     *            offset=0 and limit=3.
     * @param limit
     *            optional argument for the number of versions to retrieve. For
     *            example
     *            to retrieve the first three versions of a document use:
     *            offset=0 and limit=3.
     *            to retrieve the last 3 versions of a document use: limit=3 (No
     *            offset parameter should be provided)
     *
     * @return list of versions for the document represented by
     *         /{objectType}/{objectId}
     *
     * @throws Exception
     *             HrBadRequestException if offset is negative
     *             is bigger than the number of versions for the document
     *             if limit is negative
     *             is bigger than the number of versions for the document
     *
     *             Please refer to the header comments for more info about the
     *             parameters and exceptions thrown
     *             by this method.
     */

    @RequestMapping(value = "/{objectType}/{objectId}/versions", method = RequestMethod.GET)
    public ResponseEntity<List<Map<String, Object>>> getHistory(
            @PathVariable P2ACollection objectType,
            @PathVariable String objectId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "format", required = false) String format,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/" + objectType + "/" + objectId + "/versions?offset=" + offset + "&limit=" + limit + "&format=" + format);

        authorizationManager.authorize(authorization, RequestMethod.GET, objectType.getObjectType());

        if (format == null) {
            format = "flat";
        }

        if (modelManager.isBranched()) {
            return new ResponseEntity<>(
                    versionContentService.getHistory(UserToken.deserialize(authorization), objectType, objectId, offset, limit, format, true), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    versionContentService.getHistory(UserToken.deserialize(authorization), objectType, objectId, offset, limit, true), HttpStatus.OK);
        }
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Reverts a document to a previous version.
     *
     * @param versionId
     *            version of the document to revert to.
     *            A value of -1 means revert to the previous version.
     *
     * @return a confirmation message in case of a successful revert
     *
     * @throws Exception
     *             HrBadRequestException if versionId is zero or less than -1
     *             is bigger than number of versions for the document
     *
     *             Please refer to the header comments for more info about the
     *             parameters and exceptions thrown
     *             by this method.
     */

    @RequestMapping(value = "/revert", method = RequestMethod.POST)
    public ResponseEntity<Void> revert(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestParam(value = "versionId", required = true) Integer versionId,
            @RequestBody Object annotationPayload,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/revert/" + objectType + "/" + objectId + "/versions/" + versionId);
        logger.debug("annotationPayload: " + annotationPayload);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        @SuppressWarnings("unchecked")
        String annotation = (String) ((Map<String, Object>) annotationPayload).get(P2ALibConstants.ANNOTATION_PROPERTY);
        versionContentService.revert(UserToken.deserialize(authorization), objectType, objectId, versionId, annotation);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Checks out (locks) the document to prevent other users from modifying the
     * document.
     *
     * @return a confirmation message in case of a successful checkout
     *
     * @throws Exception
     *             HrInternalServerErrorException if document is already checked
     *             out
     *
     *
     *             Please refer to the header comments for more info about the
     *             parameters and exceptions thrown
     *             by this method.
     */

    @Deprecated
    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public ResponseEntity<Void> checkout_(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/checkout/" + objectType + "/" + objectId);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        versionContentService.checkout(UserToken.deserialize(authorization), objectType, objectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{collection}/{objectId}/checkout", method = RequestMethod.GET)
    public ResponseEntity<Void> checkout(
            @PathVariable(value = "collection") P2ACollection objectType,
            @PathVariable(value = "objectId") String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/checkout/{}/{}", objectType, objectId);

        authorizationManager.authorize(authorization, RequestMethod.GET, objectType.getObjectType());

        versionContentService.checkout(UserToken.deserialize(authorization), objectType, objectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Checks in (unlocks) the document at the specified URI to allow other
     * users to modify the document.
     * This method does not create a new version of the document.
     * You must explicitly use update() method to create new versions of a
     * document.
     *
     * @param retain
     *            A case insensitive String representation of a boolean
     *            (true/false) to indicate
     *            if the checkin should retain the last created of the document
     *            or should the document
     *            be reverted to its previous version.
     *            Absence of the parameter (null value) or a value of yes means
     *            that the last created
     *            version of the document should be retained.
     *            A value of false means no-retain means revert to previous
     *            version.
     *
     * @return a confirmation message in case of a successful checkin
     *
     *         Please refer to the header comments for more info about the
     *         parameters and exceptions thrown
     *         by this method.
     */

    @Deprecated
    @RequestMapping(value = "/checkin", method = RequestMethod.POST)
    public ResponseEntity<Void> checkin_(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestParam(value = "retain", required = false) String retain,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/checkin/" + objectType + "/" + objectId);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        versionContentService.checkin(UserToken.deserialize(authorization), objectType, objectId, retain);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{collection}/{objectId}/checkin", method = RequestMethod.GET)
    public ResponseEntity<Void> checkin(
            @PathVariable(value = "collection") P2ACollection collection,
            @PathVariable(value = "objectId") String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/checkin/{}/{}", collection, objectId);

        authorizationManager.authorize(authorization, RequestMethod.GET, collection.getObjectType());

        versionContentService.checkin(UserToken.deserialize(authorization), collection, objectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Breaks (unlocks) a checked-out document.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @RequestMapping(value = "/breakCheckout", method = RequestMethod.POST)
    public ResponseEntity<Void> breakCheckout(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/breakCheckout/" + objectType + "/" + objectId);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        versionContentService.breakCheckout(UserToken.deserialize(authorization), objectType, objectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Puts an existing JSON document under versioning.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @RequestMapping(value = "/manage", method = RequestMethod.POST)
    public ResponseEntity<Void> manageJson(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/manage/{}/{}", objectType, objectId);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        versionContentService.manageJSON(UserToken.deserialize(authorization), objectType, objectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Puts an existing binary document under versioning.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @RequestMapping(value = "/manageBinary", method = RequestMethod.POST)
    public ResponseEntity<Void> manageBinary(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestParam(value = "objectId", required = true) String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/manageBinary/" + objectType + "/" + objectId);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        versionContentService.manageBinary(UserToken.deserialize(authorization), objectType, objectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Returns the checkout status of a versioned document.
     *
     * @return if the document is checked out this API returns something like:
     *         {
     *         "status": "locked",
     *         "username": "admin@local-shahin.com",
     *         "lockTime": 1465844306000,
     *         "prettyLockTime": "JUNE 13 2016 at 14:58:26"
     *         }
     *
     *         if the document is not checked out the API returns:
     *         {
     *         "status": "open"
     *         }
     *
     *
     *         Please refer to the header comments for more info about the
     *         parameters and exceptions thrown
     *         by this method.
     */

    @RequestMapping(value = "/{objectType}/{objectId}/status", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getStatus(
            @PathVariable P2ACollection objectType,
            @PathVariable String objectId,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/" + objectType + "/" + objectId + "/status");

        authorizationManager.authorize(authorization, RequestMethod.GET, objectType.getObjectType());

        return new ResponseEntity<>(
                versionContentService.getStatus(UserToken.deserialize(authorization), objectType, objectId), HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------
    /**
     * Converts an objectType from non-versioned to versioned.
     *
     * Please refer to the header comments for more info about the parameters
     * and exceptions thrown
     * by this method.
     */

    @RequestMapping(value = "/convert", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> convertToVersioned(
            @RequestParam(value = "objectType", required = true) P2ACollection objectType,
            @RequestHeader(value = AUTHORIZATION_HEADER_NAME) String authorization) throws Exception {

        logger.debug("/convert/" + objectType);

        authorizationManager.authorize(authorization, RequestMethod.POST, objectType.getObjectType());

        return new ResponseEntity<>(
                versionContentService.convertToVersioned(UserToken.deserialize(authorization), objectType), HttpStatus.OK);
    }
}
