package com.highroads.cm;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.highroads.commons.sso.UserToken;

public interface MLRestExtensionService {

    public JsonNode executeExtension(UserToken userToken, String extension, String method, Map<String, String> queryMap, Map<String, Object> payload);

}
