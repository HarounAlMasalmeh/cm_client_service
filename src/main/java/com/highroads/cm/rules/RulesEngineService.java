package com.highroads.cm.rules;

import java.util.List;

import org.kie.api.runtime.StatelessKieSession;

import com.highroads.commons.sso.UserToken;

public interface RulesEngineService {

    public StatelessKieSession getRulesSession(UserToken userToken, String ruleGroupName);

    public StatelessKieSession getRulesSession(String tenant, String ruleGroupName);

    public void reloadRules(UserToken userToken, List<String> ruleGroups);

}
