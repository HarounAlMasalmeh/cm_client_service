package com.highroads.cm.rules;

import static com.highroads.cm.rules.RulesConstants.RULE_DECLARATIONS;
import static com.highroads.cm.rules.RulesConstants.RULE_GROUP;
import static com.highroads.cm.rules.RulesConstants.RULE_OBJECT_TYPE;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message.Level;
import org.kie.api.builder.Results;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.builder.model.KieSessionModel.KieSessionType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.highroads.cm.impl.TenantAwareBase;
import com.highroads.commons.HrSystem;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.api.exceptions.HrException;
import com.highroads.commons.sso.UserToken;

/**
 * This service loads "evalRule" object types into a Drools rules engine
 * per tenant and ruleType (field in evalRule)
 *
 * @author chong
 *
 */
@Component
public class RulesEngineServiceImpl extends TenantAwareBase implements RulesEngineService {

    private static final Logger log = LoggerFactory.getLogger(RulesEngineServiceImpl.class);

    // tenant - ruleGroup - ruleContainer
    private Map<String, Map<String, KieContainer>> rulesPerTenant = new HashMap<>();
    private KieServices kieServices;

    private void init() {
        kieServices = KieServices.Factory.get();
        List<String> tenantNames = getTenantNames();
        standupRules(tenantNames);
    }

    private void standupRules(List<String> tenantNames) {
        tenantNames.remove(HrSystem.SYSTEM_ORGANIZATION);
        for (String tenant : tenantNames) {
            List<JsonNode> rules = getObjectsForTenant("admin@" + tenant, tenant, RULE_OBJECT_TYPE);
            Map<String, KieContainer> containers = buildAllRuleGroupsForTenant(tenant, rules);
            rulesPerTenant.put(tenant, containers);
        }
    }

    private KieContainer buildRuleGroupForTenant(String tenantName, String ruleGroup, List<JsonNode> rules) {
        Set<String> declarations = new HashSet<>();
        StringBuilder rulesStrBuilder = new StringBuilder();
        for (JsonNode rule : rules) {
            String group = rule.get(RULE_GROUP).asText();
            if (!group.equals(ruleGroup)) {
                continue;
            }
            for (JsonNode decl : (ArrayNode) rule.get(RULE_DECLARATIONS)) {
                declarations.add(decl.asText());
            }
            rulesStrBuilder.append(ruleObjectToDefinitionString(rule));
        }
        for (String declaration : declarations) {
            rulesStrBuilder.insert(0, declaration);
        }
        return createContainerForTenantRuleGroup(tenantName, ruleGroup, rulesStrBuilder.toString());
    }

    private Map<String, KieContainer> buildAllRuleGroupsForTenant(String tenantName, List<JsonNode> rules) {
        Map<String, Set<String>> declarationsPerRuleGroup = new HashMap<>();
        Map<String, StringBuilder> drlBuilderPerRuleGroup = new HashMap<>();
        for (JsonNode rule : rules) {
            String ruleGroup = rule.get(RULE_GROUP).asText();
            collectDeclarations(declarationsPerRuleGroup, ruleGroup, rule);
            String ruleStr = ruleObjectToDefinitionString(rule);
            collectRules(drlBuilderPerRuleGroup, ruleGroup, ruleStr);
        }
        Map<String, KieContainer> containersPerRuleGroup = new HashMap<>();
        for (String ruleGroup : drlBuilderPerRuleGroup.keySet()) {
            StringBuilder sb = new StringBuilder();
            for (String declaration : declarationsPerRuleGroup.get(ruleGroup)) {
                sb.append(declaration);
            }
            sb.append(drlBuilderPerRuleGroup.get(ruleGroup).toString());
            KieContainer container = createContainerForTenantRuleGroup(tenantName, ruleGroup, sb.toString());
            containersPerRuleGroup.put(ruleGroup, container);
        }
        return containersPerRuleGroup;
    }

    private KieContainer createContainerForTenantRuleGroup(String tenantName, String ruleGroup, String ruleStr) {
        if (StringUtils.isEmpty(ruleStr)) {
            ExceptionHelper.throwException(HttpStatus.BAD_REQUEST, HrErrorCode.BAD_REQUEST,
                    "No such rule group: " + ruleGroup);
        }
        KieContainer container = null;
        try {
            KieFileSystem kFileSystem = kieServices.newKieFileSystem();
            log.info(String.format("Rules being loaded for tenant: %s, rule group: %s, rule definition: \n%s",
                    tenantName, ruleGroup, ruleStr));
            KieModuleModel moduleModel = kieServices.newKieModuleModel();
            KieBaseModel kBaseModel = moduleModel.newKieBaseModel(tenantName);
            log.info("Building Knowledge base: " + tenantName + ":" + ruleGroup);
            KieSessionModel sessionModel = kBaseModel.newKieSessionModel(tenantName + ":" + ruleGroup);
            sessionModel.setType(KieSessionType.STATELESS).setDefault(true);
            kFileSystem.writeKModuleXML(moduleModel.toXML());
            kFileSystem.write("src/main/resources/" + tenantName + "_" + ruleGroup + ".drl",
                    kieServices.getResources().newReaderResource(new StringReader(ruleStr)));
            KieBuilder kBuilder = kieServices.newKieBuilder(kFileSystem).buildAll();
            Results results = kBuilder.getResults();
            if (results.hasMessages(Level.ERROR)) {
                log.error(String.format("Loading of rules failed for: %s_%s\n%s", tenantName, ruleGroup,
                        results.getMessages(Level.ERROR).toString()));
                ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR,
                        "Loading of rules failed for rule group: " + ruleGroup);
            }
            container = kieServices.newKieContainer(kBuilder.getKieModule().getReleaseId());
        } catch (HrException hrex) {
            throw hrex;
        } catch (Exception ex) {
            log.error("Error while creating rule container", ex);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR,
                    "Error creating rules for rule group: " + ruleGroup);
        }
        return container;
    }

    private void collectDeclarations(Map<String, Set<String>> declarationsPerRuleGroup, String ruleGroup, JsonNode rule) {
        Set<String> declarations = declarationsPerRuleGroup.get(ruleGroup) == null ? new HashSet<>()
                : declarationsPerRuleGroup.get(ruleGroup);
        for (JsonNode decl : (ArrayNode) rule.get(RULE_DECLARATIONS)) {
            declarations.add(decl.asText());
        }
        declarationsPerRuleGroup.put(ruleGroup, declarations);
    }

    private void collectRules(Map<String, StringBuilder> drlBuilderPerRuleGroup, String ruleGroup, String ruleStr) {
        StringBuilder sb = drlBuilderPerRuleGroup.get(ruleGroup) == null ? new StringBuilder() : drlBuilderPerRuleGroup.get(ruleGroup);
        sb.append(ruleStr);
        drlBuilderPerRuleGroup.put(ruleGroup, sb);
    }

    private String ruleObjectToDefinitionString(JsonNode rule) {
        StringBuilder sb = new StringBuilder("rule \"");
        String ruleName = rule.get("name").asText();
        String salience = "Default Document Name".equals(ruleName) ? "salience 10000\n" : "salience 1\n";
        sb.append(ruleName).append("\"\n").append(salience);
        sb.append("when\n");
        sb.append(rule.get("when").asText() + "\n");
        sb.append("then\n");
        sb.append(rule.get("then").asText() + "\n");
        sb.append("end\n");
        return sb.toString();
    }

    private List<String> getTenantNames() {
        List<String> tenantNames = new ArrayList<>();
        List<JsonNode> tenants = getTenants();
        for (JsonNode tenant : tenants) {
            tenantNames.add(tenant.get("orgId").asText());
        }
        return tenantNames;
    }

    private void loadRulesIfNotLoaded() {
        if (rulesPerTenant.isEmpty()) {
            init();
        }
    }

    @Override
    public StatelessKieSession getRulesSession(UserToken userToken, String ruleGroupName) {
        return getRulesSession(userToken.getOrganization(), ruleGroupName);
    }

    @Override
    public StatelessKieSession getRulesSession(String tenantName, String ruleGroupName) {
        loadRulesIfNotLoaded();
        StatelessKieSession kieSession = null;
        try {
            KieContainer container = rulesPerTenant.get(tenantName).get(ruleGroupName);
            kieSession = container.newStatelessKieSession(tenantName + ":" + ruleGroupName);
        } catch (HrException hrex) {
            throw hrex;
        } catch (Exception ex) {
            log.error(String.format("Error while getting rules session for %s - %s", tenantName, ruleGroupName), ex);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR,
                    "Rules engine failed for :" + ruleGroupName);
        }
        return kieSession;
    }


    @Override
    public void reloadRules(UserToken userToken, List<String> ruleGroups) {
        loadRulesIfNotLoaded();
        String tenant = userToken.getOrganization();
        String user = userToken.getUsername();
        List<JsonNode> rules = getObjectsForTenant(user, tenant, RULE_OBJECT_TYPE);
        if(ruleGroups == null || ruleGroups.isEmpty()) {
            if(tenant.equals(HrSystem.SYSTEM_ORGANIZATION)) {
                init();
            }else {
                rulesPerTenant.put(tenant, buildAllRuleGroupsForTenant(tenant, rules));
            }
        }else {
            Map<String, KieContainer> ruleGroupContainers = new HashMap<>();
            for(String ruleGroup: ruleGroups) {
                ruleGroupContainers.put(ruleGroup, buildRuleGroupForTenant(tenant, ruleGroup, rules));
            }
            Map<String, KieContainer> rulesThisTenant = rulesPerTenant.get(tenant) == null ? new HashMap<>() : rulesPerTenant.get(tenant);
            for (String group : ruleGroupContainers.keySet()) {
                rulesThisTenant.put(group, ruleGroupContainers.get(group));
            }
            rulesPerTenant.put(tenant, rulesThisTenant);
        }
    }
}
