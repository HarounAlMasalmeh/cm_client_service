package com.highroads.cm.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.StatelessKieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.highroads.commons.HasLogger;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.contentservice.ContentService;

@Service
public class RuleServiceImpl implements RuleService, HasLogger {

    @Autowired
    ContentService contentService;

    @Autowired
    RulesEngineService rulesEngineService;

    @Override
    public void reloadRules(UserToken userToken, List<String> ruleGroups) {
        rulesEngineService.reloadRules(userToken, ruleGroups);
    }

    @SuppressWarnings("unchecked")
    @Override
    public String generateDocName(UserToken userToken, String docType, String language, List<String> planIdList, String offeringId, String templateId,
            String effectiveDate, String endDate, String groupId, String documentOutputType) throws Exception {
        Map<String, Object> docNameParams = new HashMap<>();
        Map<String, Object> template = contentService.getObjectById(userToken, templateId, P2ACollection.TEMPLATES, null, null);
        docNameParams.put("template", template);
        docNameParams.put("docType", docType);
        docNameParams.put("language", language);
        docNameParams.put("planCount", planIdList.size());
        Map<String, Object> plan = null;
        for (String planId : planIdList) {
            plan = contentService.getObjectById(userToken, planId, P2ACollection.PLANS, 0, null);
            List<String> productClasses = (List<String>) plan.getOrDefault("productClasses", new ArrayList<>());
            if (productClasses.size() > 0) {
                switch (productClasses.get(0)) {
                    case "Medical":
                        docNameParams.put("medPlan", plan);
                        break;
                    case "Pharmacy":
                        docNameParams.put("rxPlan", plan);
                        break;
                    case "Dental":
                    case "Vision":
                        docNameParams.put("dentalVisionPlan", plan);
                        break;
                }
            }
        }
        if(documentOutputType != null){
            docNameParams.put("documentOutputType", documentOutputType);
        }
        if (planIdList.size() == 1) {
            docNameParams.put("plan", plan);
        }
        if (offeringId != null) {
            Map<String, Object> offering = contentService.getObjectById(userToken, offeringId, P2ACollection.OFFERINGS, 0, null);
            docNameParams.put("offering", offering);
        }
        if (effectiveDate != null) {
            docNameParams.put("customEffectiveDate", effectiveDate);
        }
        if (endDate != null) {
            docNameParams.put("customEndDate", endDate);
        }
        if (groupId != null) {
            Map<String, Object> group = contentService.getObjectById(userToken, groupId, P2ACollection.GROUPS, 2, null);
            if (group.containsKey("groupPlans")) {
                List<Map<String, Object>> groupPlanInfo = (List<Map<String, Object>>) group.get("groupPlans");
                if (groupPlanInfo != null && !groupPlanInfo.isEmpty()) {
                    for (Map<String, Object> groupPlanDetail : groupPlanInfo) {
                        if (groupPlanDetail.get("planObjectId").equals(plan.get("objectId"))
                                && groupPlanDetail.get("groupObjectId").equals(group.get("objectId"))) {
                            group.put("groupPlanEffectiveDate", groupPlanDetail.get("groupPlanEffectiveDate"));
                            break;
                        }
                    }
                }
            }
            docNameParams.put("group", group);
        }
        StatelessKieSession rulesEngineSession = rulesEngineService.getRulesSession(userToken, "documentName");
        rulesEngineSession.execute(docNameParams);
        String name = (String) docNameParams.get("docName");
        String docName = name.replaceAll("[?&*<>/\\:|\"]", "");
        return docName;
    }
}
