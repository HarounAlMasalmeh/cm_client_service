package com.highroads.cm.rules;

import java.util.List;

import com.highroads.commons.sso.UserToken;

public interface RuleService {

    /**
     * Generates document names as using the global settings on tenant
     *
     * @param userToken
     * @param planId
     * @param offeringId
     * @param templateId
     * @param groupId
     * @param templateId2
     * @return
     */
    String generateDocName(UserToken userToken, String docType, String language, List<String> planIdList,
            String offeringId, String templateId, String effectiveDate, String endDate, String groupId, String documentOutputType) throws Exception;

    /**
     * Reloads rules for user's tenant and rule group
     *
     * @param userToken
     * @param ruleGroups
     */
    void reloadRules(UserToken userToken, List<String> ruleGroups);

}
