package com.highroads.cm.rules;


public class RulesConstants {

    public static final String RULE_GROUP = "ruleGroup";
    public static final String RULE_OBJECT_TYPE = "evalRule";
    public static final String RULE_DECLARATIONS = "declarations";

}
