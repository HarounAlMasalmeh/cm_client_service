package com.highroads.cm.batch.impl;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.highroads.cm.batch.BatchAction;
import com.highroads.cm.batch.BatchService;
import com.highroads.commons.HasLogger;
import com.highroads.commons.HrSystem;
import com.highroads.commons.api.exceptions.ExceptionHelper;
import com.highroads.commons.api.exceptions.HrErrorCode;
import com.highroads.commons.api.exceptions.HrException;
import com.highroads.commons.api.exceptions.HrObjectNotFoundException;
import com.highroads.commons.batch.BatchConstants;
import com.highroads.commons.batch.BatchProperties;
import com.highroads.commons.model.P2ACollection;
import com.highroads.commons.queue.PriorityQueueService;
import com.highroads.commons.sso.UserToken;
import com.highroads.p2alibs.constants.P2ALibConstants;
import com.highroads.p2alibs.contentservice.ContentService;

@Component
@ConditionalOnProperty(prefix = "batch.queue.", name = "redis")
public class BatchServiceImpl implements BatchService, HasLogger {

    @Value("${batch.priority.queue.size.limit}")
    private int priorityBatchSizeLimit;

    @Autowired
    ContentService contentService;

    @Autowired
    BatchProperties batchProperties;

    @Autowired
    @Qualifier("redisPriorityQueueServiceImpl")
    private PriorityQueueService queueService;

    private static final String BATCHARCHIVED = "batchArchived";

    @Value("#{'${batch.priority.queue.jobtypes}'.split(',')}")
    private List<String> priorityQueueJobTypes;

    @Override
    public Map<String, String> getInvalidBatches(UserToken userToken, List<Map<String, Object>> invalidBatches, List<String> batchIds,
            BatchAction flag) {
        Map<String, String> returnList = new HashMap<>();

        //for resubmitAll flag, the batchStatusList is an exclusion list (given batchId should not exist in this list)
        // for resubmitFailedCancelled flag, the batchStatusList is an inclusion list (the batch should exist in this list for a valid  batch)

        try {
            Iterator<String> iter = batchIds.iterator();
            while (iter.hasNext()) {
                String batchId = iter.next();
                Map<String, Object> batch = getBatchFromList(invalidBatches, batchId);

                // in case of resubmitAll, the batch should be null for being a valid batch and
                //in case of resubmitFailed, the batch should be  non-null for being a valid batch

                if (flag == BatchAction.CANCEL_FAILED && batch == null) {

                    //get the batch name for this batchId and put this in the error
                    Map<String, Object> batchObj = contentService.getObjectById(userToken, batchId, P2ACollection.BATCH, 0,
                            new String[] { BatchConstants.BATCH_STATUS_PROPERTY });
                    String invalidBatchSubmitError = String.format("Batch cannot be resubmitted since it is in %s state",
                            batchObj.get(BatchConstants.BATCH_STATUS_PROPERTY));
                    returnList.put((String) batchObj.get(P2ALibConstants.OBJECT_NAME), invalidBatchSubmitError);
                    iter.remove();
                }
                if (flag == BatchAction.RESUBMIT_ALL && batch != null) {

                    String invalidBatchSubmitError = String.format("Batch cannot be resubmitted since it is in %s state",
                            batch.get(BatchConstants.BATCH_STATUS_PROPERTY));
                    returnList.put((String) batch.get(P2ALibConstants.OBJECT_NAME), invalidBatchSubmitError);
                    iter.remove();

                }
                if (flag == BatchAction.ARCHIVE && batch != null) {

                    //get the batch name for this batchId and put this in the error
                    Map<String, Object> batchObj = contentService.getObjectById(userToken, batchId, P2ACollection.BATCH, 0,
                            new String[] { BatchConstants.BATCH_STATUS_PROPERTY });
                    String invalidBatchArchiveError = String.format("Batch cannot be Archived since it is in %s state",
                            batchObj.get(BatchConstants.BATCH_STATUS_PROPERTY));
                    returnList.put((String) batchObj.get(P2ALibConstants.OBJECT_NAME), invalidBatchArchiveError);
                    iter.remove();
                }
            }
        } catch (HrException ex) {
            throw ex;
        } catch (Exception ex) {
            getLogger().error("Batch Not Found :", ex);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR, ex);
        }
        return returnList;
    }

    //checks the given batchId in the  batchlist and returns the batch if it finds one or null

    private Map<String, Object> getBatchFromList(List<Map<String, Object>> batches, String batchId) {

        for (Map<String, Object> batch : batches) {
            if (batchId.equals(batch.get(P2ALibConstants.OBJECT_ID))) {
                return batch;
            }
        }
        return null;
    }

    private void updateBatchStatus(String batchId, UserToken userToken, BatchAction flag) {
        Map<String, Object> updateProperties = new HashMap<>();

        if (flag == BatchAction.RESUBMIT_ALL) {
            updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_DRAFT);
            updateProperties.put(BatchConstants.BATCH_SUBMITTED_BY_PROPERTY, userToken.getUsername());
            contentService.updateContent(userToken, P2ACollection.BATCH, batchId, updateProperties);
        } else if (flag == BatchAction.ARCHIVE) {
            updateProperties.put(BATCHARCHIVED, true);
            contentService.updateContent(userToken, P2ACollection.BATCH, batchId, updateProperties);
        }
    }

    @Override
    public Map<String, String> bulkArchiveBatch(UserToken userToken, List<String> batchIds, BatchAction flag) {
        Map<String, String> returnList = null;

        ////Draft, Successful, Partially Successful, Failed, or Cancelled.

        List<Map<String, Object>> invalidBatches = getListOfInvalidBatches(userToken, flag, batchIds);

        returnList = getInvalidBatches(userToken, invalidBatches, batchIds, flag);

        //since we are changing the status to draft prior, let the underlying call need not change it again
        updateBatchTrigger(userToken, batchIds, true, flag);

        return returnList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, String> submitBatches(UserToken userToken, String requestingUserAuthToken, List<String> batchIds) {
        Map<String, String> returnList = new HashMap<>();
        String tenantName = userToken.getOrganization();
        // each batch status should be set to Submitted after it is queued
        Map<String, Object> updateProperties = new HashMap<>();
        updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_SUBMITTED);
        updateProperties.put(BatchConstants.BATCH_SUBMITTED_BY_PROPERTY, userToken.getUsername());

        Map<String, Object> batch;
        for (String batchId : batchIds) {
            batch = contentService.getObjectById(userToken, batchId, P2ACollection.BATCH, null, null);
            final Map<String, Object> associations = (Map<String, Object>) batch.get(P2ALibConstants.ASSOCIATIONS);
            if (associations == null || associations.isEmpty()) {
                continue;
            }
            final List<Map<String, Object>> batchJobs = (List<Map<String, Object>>) associations.get(BatchConstants.BATCH_BATCHJOBS_PROPERTY);

            if (batchJobs == null || batchJobs.isEmpty()) {
                returnList.put(batchId, "Skipped. Only batches with Batch Jobs can be submitted.");
                continue;
            }

            if (!batch.get(BatchConstants.BATCH_STATUS_PROPERTY).equals(BatchConstants.BATCH_STATUS_VALUE_DRAFT)) {
                returnList.put(batchId, "Skipped. Only batches in \"Draft\" status can be submitted.");
                continue;
            }

            try {
                String queueName = String.format("%s%s.%s", batchProperties.getQueue().getName().getTenant(), tenantName,
                        batch.get(BatchConstants.BATCH_CLASS_PROPERTY));
                if (isPriorityBatch(userToken, batch)) {
                    queueService.enqueue(queueName, batchId, true);
                    getLogger().debug("Batch with ID: {} was pushed to priority queue", batchId);
                } else {
                    queueService.enqueue(queueName, batchId);
                }
                queueService.setValue(batchId, requestingUserAuthToken);
                getLogger().debug("Batch: {}, from tenant: {} added to queue: {}", batchId, tenantName, queueName);
                updateProperties.put(BatchConstants.BATCH_SUBMITTED_DATE_PROPERTY, ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
                batch.putAll(updateProperties);

                // Remove the batchJobs association property
                batch.remove(BatchConstants.BATCH_BATCHJOBS_PROPERTY);

                contentService.writeContent(userToken, batch, P2ACollection.BATCH, batchId);
                returnList.put(batchId, BatchConstants.BATCH_STATUS_VALUE_SUBMITTED);
            } catch (HrException e) {
                updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_FAILED);
                updateProperties.put(BatchConstants.BATCH_SUBMITTED_BY_PROPERTY, userToken.getUsername());
                updateProperties.put(BatchConstants.BATCH_PROCESSING_DETAILS_PROPERTY, e.getMessage());
                batch.putAll(updateProperties);
                contentService.writeContent(userToken, batch, P2ACollection.BATCH, batchId);
                returnList.put(batchId, "Skipped." + e.getMessage());
            } catch (Exception e) {
                getLogger().error("Unexpected exception:", e);
                updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_FAILED);
                updateProperties.put(BatchConstants.BATCH_SUBMITTED_BY_PROPERTY, userToken.getUsername());
                updateProperties.put(BatchConstants.BATCH_PROCESSING_DETAILS_PROPERTY, e.getMessage());
                batch.putAll(updateProperties);
                contentService.writeContent(userToken, batch, P2ACollection.BATCH, batchId);
                returnList.put(batchId, "Skipped." + e.getMessage());
            }

        }
        return returnList;
    }

    private boolean isPriorityBatch(UserToken userToken, Map<String, Object> batch) {
        boolean isPriority = true;
        String jobId = (String) batch.get("jobId");
        if (jobId != null) {
            Map<String, Object> parentJob = contentService.getObjectById(userToken, jobId, P2ACollection.JOBS, 0, null);
            String jobType = parentJob == null ? null : (String) parentJob.get("jobType");
            if (!priorityQueueJobTypes.contains(jobType)) {
                isPriority = false;
            }
        }
        List<String> batchJobIds = (List) batch.get("batchJobs");
        if (batchJobIds != null && batchJobIds.size() > priorityBatchSizeLimit) {
            isPriority = false;
        }
        return isPriority;
    }

    @SuppressWarnings("unchecked")
    private List<Map<String, Object>> getListOfInvalidBatches(UserToken userToken, BatchAction flag, List<String> batchIds) {
        ObjectMapper objectMapper = new ObjectMapper();
        P2ACollection objectType = P2ACollection.BATCH;
        String status = null;
        String searchQuery;
        if (flag == BatchAction.CANCEL_FAILED) {
            status = "batchStatus:\"Failed\" OR batchStatus:\"Partially Successful\" OR batchStatus:\"Cancelled\" ";
        } else if (flag == BatchAction.RESUBMIT_ALL) {
            status = "batchStatus:\"In Progress\" OR batchStatus:\"Draft\" OR batchStatus:\"Submitted\" OR batchStatus:\"Queued\" OR batchStatus:\"Pending\" ";
        } else if (flag == BatchAction.ARCHIVE) {
            status = "batchStatus:\"Submitted\" OR batchStatus:\"Queued\" OR batchStatus:\"In progress\" OR batchStatus:\"Scheduled\" OR batchStatus:\"Cancelling\" ";
        }
        searchQuery = String.format("TYPE:\"%s\" AND (%s) ", objectType, status);

        searchQuery = searchQuery + "AND (" + batchIds.stream().collect(Collectors.joining("\" OR objectId:\"", "objectId:\"", "\"")) + ")";

        //only get the properties that are required

        ArrayList<Map<String, Object>> batchList = null;
        List<Map<String, Object>> dataAsMap = null;
        try {
            batchList = (ArrayList<Map<String, Object>>) contentService.searchObjects(userToken, searchQuery, 0, null);
            String listString = batchList.toString();
            dataAsMap = objectMapper.readValue(listString, List.class);

        } catch (HrException ex) {
            throw ex;
        } catch (Exception ex) {
            getLogger().error("Search Failed :", ex);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR, ex);
        }
        return dataAsMap;
    }

    @Override
    public Map<String, String> bulkResubmitBatch(UserToken userToken, List<String> batchIds, BatchAction flag) {
        Map<String, String> returnList = null;

        List<Map<String, Object>> invalidBatches = getListOfInvalidBatches(userToken, flag, batchIds);

        //Make sure we have the right set of batches to resubmit. Sometimes, UI/API might submit
        //batches which might be  already running or other invalid states.
        //We would identify the right batches to submit and return the wrong ones back to caller

        returnList = getInvalidBatches(userToken, invalidBatches, batchIds, flag);
        //update the status of all batchIds to draft
        for (String batchId : batchIds) {
            updateBatchStatus(batchId, userToken, BatchAction.RESUBMIT_ALL);
        }
        //since we are changing the status to draft prior, let the underlying call need not change it again
        updateBatchTrigger(userToken, batchIds, false, flag);

        return returnList;
    }

    @Async
    public void updateBatchTrigger(UserToken userToken, List<String> batchIds, boolean statusCheck, BatchAction submitOption) {

        try {
            if (submitOption == BatchAction.RESUBMIT_ALL) {
                for (String batchId : batchIds) {
                    resubmitAllBatches(userToken, batchId, statusCheck);
                }
            } else if (submitOption == BatchAction.CANCEL_FAILED) {
                for (String batchId : batchIds) {
                    resubmitCancelledBatches(userToken, batchId, statusCheck);
                }
            } else if (submitOption == BatchAction.ARCHIVE) {
                for (String batchId : batchIds) {
                    updateBatchStatus(batchId, userToken, BatchAction.ARCHIVE);
                }
            } else {
                getLogger().error("Invalid submitOption  ", submitOption);
            }
            submitBatches(userToken, userToken.toBase64EncodedString(), batchIds);
        } catch (HrException ex) {
            throw ex;
        } catch (Exception ex) {
            getLogger().error("BulkResubmit Failed :", ex);
            ExceptionHelper.throwException(HttpStatus.INTERNAL_SERVER_ERROR, HrErrorCode.INTERNAL_SERVER_ERROR, ex);
        }
    }

    public Map<String, String> resubmitBatch(UserToken userToken, String requestingUserAuthToken, List<String> batchIds, BatchAction flag,
            boolean isBulkResubmit) {
        boolean doCheckAndUpdateStatusFlg = isBulkResubmit ? false : true;
        Map<String, String> returnList = new HashMap<>();
        for (String batchId : batchIds) {
            //Change the status of all the jobs to pending and status of the batch to draft
            if (flag == BatchAction.RESUBMIT_ALL) {
                resubmitAllBatches(userToken, batchId, doCheckAndUpdateStatusFlg);
            }
            // Change the status of only failed and cancelled jobs to pending and batch status to draft
            if (flag == BatchAction.CANCEL_FAILED) {
                resubmitCancelledBatches(userToken, batchId, doCheckAndUpdateStatusFlg);
            }
            returnList = submitBatches(userToken, requestingUserAuthToken, batchIds);
        }
        return returnList;
    }

    @Override
    public Map<String, String> resubmitBatch(UserToken userToken, String requestingUserAuthToken, List<String> batchIds, BatchAction flag) {
        return resubmitBatch(userToken, requestingUserAuthToken, batchIds, flag, false);
    }

    @SuppressWarnings("unchecked")
    private void resubmitCancelledBatches(UserToken userToken, String batchId, boolean doCheckBatchStatus) {
        Map<String, Object> batch = contentService.getObjectById(userToken, batchId, P2ACollection.BATCH, null, null);
        //Check for batch status only if its not bulkresubmit.
        String batchStatus = batch.get(BatchConstants.BATCH_STATUS_PROPERTY).toString();

        if (!doCheckBatchStatus || BatchConstants.BATCH_STATUS_VALUE_PARTSUCCESS.equalsIgnoreCase(batchStatus) ||
                BatchConstants.BATCH_STATUS_VALUE_CANCELLED.equalsIgnoreCase(batchStatus) ||
                BatchConstants.BATCH_STATUS_VALUE_FAILED.equalsIgnoreCase(batchStatus)) {
            final Map<String, Object> associations = (Map<String, Object>) batch.get(P2ALibConstants.ASSOCIATIONS);
            if (MapUtils.isNotEmpty(associations)) {
                List<Map<String, Object>> batchJobObjects = (List<Map<String, Object>>) associations.get(BatchConstants.BATCH_BATCHJOBS_PROPERTY);
                if (CollectionUtils.isNotEmpty(batchJobObjects)) {
                    updateBatchJobStatus(batchJobObjects, userToken);

                    //since bulkResubmit might change the batchstatus to draft already,
                    // we dont need to change it again. bulkResubmit will call this function with doCheckBatchStatus as false
                    if (doCheckBatchStatus) {
                        Map<String, Object> updateProperties = new HashMap<>();
                        updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_DRAFT);
                        batch.putAll(updateProperties);
                        batch.remove(BatchConstants.BATCH_BATCHJOBS_PROPERTY);
                        contentService.writeContent(userToken, batch, P2ACollection.BATCH, batchId);
                    }
                }
            }
        }
    }

    private void updateBatchJobStatus(List<Map<String, Object>> batchJobObjects, UserToken userToken) {
        for (Map<String, Object> batchJobObject : batchJobObjects) {

            String batchJobId = (String) batchJobObject.get(HrSystem.OBJECT_ID_PROPERTY);
            Map<String, Object> batchJob = contentService.getObjectById(userToken, batchJobId, P2ACollection.BATCHJOBS, null,
                    new String[] { BatchConstants.BATCHJOB_STATUS_PROPERTY });
            String batchJobStatus = batchJob.get(BatchConstants.BATCHJOB_STATUS_PROPERTY).toString();
            if (BatchConstants.BATCHJOB_STATUS_VALUE_FAILED.equalsIgnoreCase(batchJobStatus) ||
                    BatchConstants.BATCHJOB_STATUS_VALUE_CANCELLED.equalsIgnoreCase(batchJobStatus)) {
                Map<String, Object> statusUpdate = new HashMap<>();
                statusUpdate.put(BatchConstants.BATCHJOB_STATUS_PROPERTY, BatchConstants.BATCHJOB_STATUS_VALUE_PENDING);
                contentService.updateContent(userToken, P2ACollection.BATCHJOBS, batchJobId, statusUpdate);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void resubmitAllBatches(UserToken userToken, String batchId, boolean doUpdateBatchStatusFlag) {
        Map<String, Object> batch = contentService.getObjectById(userToken, batchId, P2ACollection.BATCH, null, null);
        final Map<String, Object> associations = (Map<String, Object>) batch.get(P2ALibConstants.ASSOCIATIONS);
        if (MapUtils.isNotEmpty(associations)) {
            final List<Map<String, Object>> batchJobObjects = (List<Map<String, Object>>) associations.get(BatchConstants.BATCH_BATCHJOBS_PROPERTY);
            if (CollectionUtils.isNotEmpty(batchJobObjects)) {
                for (Map<String, Object> batchJobObject : batchJobObjects) {
                    String batchJobId = (String) batchJobObject.get(HrSystem.OBJECT_ID_PROPERTY);
                    Map<String, Object> statusUpdate = new HashMap<>();
                    statusUpdate.put(BatchConstants.BATCHJOB_STATUS_PROPERTY, BatchConstants.BATCHJOB_STATUS_VALUE_PENDING);
                    contentService.updateContent(userToken, P2ACollection.BATCHJOBS, batchJobId, statusUpdate);
                }

                //when doing bulkBatch Resubmit, the status of the batch would be changed
                // to draft in the beginning. So we dont need to do here in that case

                if (doUpdateBatchStatusFlag) {
                    //Update the batch status to draft
                    String batchStatus = batch.get(BatchConstants.BATCH_STATUS_PROPERTY).toString();
                    if (BatchConstants.BATCH_STATUS_VALUE_SUCCESSFUL.equalsIgnoreCase(batchStatus) ||
                            BatchConstants.BATCH_STATUS_VALUE_FAILED.equalsIgnoreCase(batchStatus) ||
                            BatchConstants.BATCH_STATUS_VALUE_CANCELLED.equalsIgnoreCase(batchStatus) ||
                            BatchConstants.BATCH_STATUS_VALUE_PARTSUCCESS.equalsIgnoreCase(batchStatus)) {

                        Map<String, Object> updateProperties = new HashMap<>();
                        updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_DRAFT);
                        updateProperties.put(BatchConstants.BATCH_SUBMITTED_BY_PROPERTY, userToken.getUsername());
                        contentService.updateContent(userToken, P2ACollection.BATCH, batchId, updateProperties);
                    }
                }
            }
        }
    }

    @Override
    public Map<String, String> cancelBatch(UserToken userToken, List<String> batchIds) {

        Map<String, String> returnList = new HashMap<>();
        Map<String, Object> updateProperties = new HashMap<>();
        updateProperties.put(BatchConstants.BATCH_CANCELLED_BY_PROPERTY, userToken.getUsername());
        updateProperties.put(BatchConstants.BATCH_CANCELLED_DATE_PROPERTY, ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));

        // process each submitted batch individually
        for (String batchId : batchIds) {
            Map<String, Object> batch = null;
            try {
                batch = contentService.getObjectById(userToken, batchId, P2ACollection.BATCH, null, null);

                //only cancel batches in status submitted/in progress/queued
                String batchStatus = (String) batch.get(BatchConstants.BATCH_STATUS_PROPERTY);
                if (batchStatus.equals(BatchConstants.BATCH_STATUS_VALUE_SUBMITTED)
                        || batchStatus.equals(BatchConstants.BATCH_STATUS_VALUE_INPROGRESS)
                        || batchStatus.equals(BatchConstants.BATCH_STATUS_VALUE_QUEUED)) {
                    updateProperties.put(BatchConstants.BATCH_STATUS_PROPERTY, BatchConstants.BATCH_STATUS_VALUE_CANCELLED);
                    batch.putAll(updateProperties);
                    batch.remove(BatchConstants.BATCH_BATCHJOBS_PROPERTY);
                    contentService.writeContent(userToken, batch, P2ACollection.BATCH, batchId);
                    returnList.put(batchId, BatchConstants.BATCH_STATUS_VALUE_CANCELLED);
                } else {
                    StringBuilder sb = new StringBuilder("Skipped. Batch in \"" + batchStatus
                            + "\" status cannot be cancelled.");
                    returnList.put(batchId, sb.toString());
                }
            } catch (HrObjectNotFoundException e) {
                returnList.put(batchId, "Not found");
            }
        }
        return returnList;
    }

}
