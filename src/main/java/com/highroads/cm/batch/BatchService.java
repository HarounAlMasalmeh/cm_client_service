package com.highroads.cm.batch;

import java.util.List;
import java.util.Map;

import com.highroads.commons.sso.UserToken;

public interface BatchService {

    public Map<String, String> getInvalidBatches(UserToken userToken, List<Map<String, Object>> batchesStatusList, List<String> batchIds,
            BatchAction flag);

    /**
     * This is to cancel a batch in status Submitted , In Progress etc.
     *
     * @param userToken
     * @param batchIds
     * @return
     */
    Map<String, String> cancelBatch(UserToken userToken, List<String> batchIds);

    /*  *//**
           * This is to resubmit a batch in status Cancelled , Failed etc.
           *
           * @param userToken
           * @param batchIds
           * @return
           */
    Map<String, String> resubmitBatch(UserToken userToken, String requestingUserAuthToken, List<String> batchIds, BatchAction flag);

    /**
     * Submit draft batch to run jobs async
     *
     * @param userToken
     *            The userToken of the caller
     * @param requestingUserAuthToken
     *            The base64 encoded version of the userToken of the caller (as
     *            passed in to the controller)
     * @param batchIds
     *            List of batch Ids to put into the Batch running framework
     */
    Map<String, String> submitBatches(UserToken userToken, String requestingUserAuthToken, List<String> batchIds);

    Map<String, String> bulkResubmitBatch(UserToken userToken, List<String> batchIds, BatchAction flag);

    Map<String, String> bulkArchiveBatch(UserToken userToken, List<String> batchIds, BatchAction flag);

}
