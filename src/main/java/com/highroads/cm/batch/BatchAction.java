package com.highroads.cm.batch;

public enum BatchAction {

    CANCEL_FAILED, RESUBMIT_ALL, ARCHIVE, UNKNOWN;

}
