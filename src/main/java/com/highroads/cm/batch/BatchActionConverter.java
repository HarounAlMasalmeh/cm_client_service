package com.highroads.cm.batch;

import org.springframework.core.convert.converter.Converter;

public class BatchActionConverter implements Converter<String, BatchAction> {

    @Override
    public BatchAction convert(String source) {
        switch (source) {
            case "cancelfailed":
                return BatchAction.CANCEL_FAILED;
            case "resubmit_all":
                return BatchAction.RESUBMIT_ALL;
            case "archive":
                return BatchAction.ARCHIVE;
            default:
                return BatchAction.UNKNOWN;
        }
    }

}
